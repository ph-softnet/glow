#!/bin/sh
# \
exec tclsh $0 $*

source srcs.tcl
source srcs_mathematica.tcl
source srcs_sgs.tcl

namespace eval ::srcs::aimms {

	proc read {f} {
		set active 0
		set x [dict create]
		set n 0
		
		while { [gets $f line] >= 0 } {
			set line [string trim "$line"]
			
			if { [string match "x :=*" $line] } {
				set active 1
				continue
			}
			
			if {$active} {
				set line [string trim $line "\{ \} ;\t"]
				foreach term [split $line ","] {
					lassign [split $term ":"] i xi
					if {$i == ""} {continue}
					if {$xi == ""} {continue}
					set i [expr $i-1]
					set xi [expr $xi]					
					dict set x $i $xi
					
		 			set n [expr max($n, $i+1)]
				}
			}
			
			if { [string match "*;" $line] } {
				set active 0
				break
			}
		}
  		
 		set pairs [list]
		for {set i 0} {$i < $n} {incr i} {
 			if {![dict exists $x $i]} {
				dict set x $i 0
			}
			set xi [dict get $x $i]
 			lappend pairs [list $i $xi]
		}
 		set pairs [lsort -increasing -real -index 1 $pairs]
		
		return $pairs
	}
	
	proc sgsify {x0 plan conf} {
		# convert x0 to sgs rule
		set rule [list]
		foreach pair [lsort -index 0 -increasing -int $x0] {
			lappend rule [lindex $pair 0]
		}
		
		# convert rule to elig. sequence
		set eseq [srcs::sgs::eligseq $conf $rule .plan]
		
		# obtain sgs schedule
		set x1 [srcs::sgs::sgs $conf .plan $eseq]
		
		return $x1
	}
	
	
	# try to start each task `i' in order of 
	# increasing prescribed start (in x0).
	# if `i' cant start, start it at the earliest
	# of the feasible end times of the already started tasks.
	
	proc repair {x0 plan conf {preshift 1}} {
		set n [dict get $conf n]
		set k [expr [dict get $conf k]-1]
		set done [dict create]
		
		# for each task in order of increasing prescribed time (in x0),
		# determine a feasible start time (in x1)
		
		set x1 [list]
		foreach pair0 $x0 {
			lassign $pair0 j x0j
			set dj [dict get $conf $j:D:m1]
			
			# check predecessors and fix violations of prec. constraints
			# by re-adjusting x0j
			
			for {set i 0} {$i < $n} {incr i} {
				if {[srcs::c_plan_get_edge $plan $i $j]} {
					if { [catch {set fi [dict get $done $i]}] } {
						error "x0 is precedence-infeasible"
					}
					if { $fi > $x0j } {
						# puts stderr "repair: ($i,$j): $fi > $x0j"
						if {$preshift == 1} {
							set x0j $fi
						}						
					}
				}
			}
			
			# gather possible start time options
			
			set opts [list $x0j]
			foreach pair1 $x1 {
				lassign $pair1 i x1i
				set di [dict get $conf $i:D:m1]
				set f1i [expr $x1i + $di]
				
				if {$f1i > $x0j} {
					lappend opts $f1i
				}
			}
			
			# find earliest feasible option
			
			set opts [lsort -increasing -real $opts]
			set numopts [llength $opts]
			
			for {set p 0} {$p < $numopts} {incr p} {	
				set t [lindex $opts $p]
				set feas 1
								
				for {set r 0} {$r < $k} {incr r} {
					set b [dict get $conf $r:cap]
					set q [dict get $conf $j:req:$r]
					
					for {set p2 $p} {$p2 < $numopts} {incr p2} {
						set t2 [lindex $opts $p2]
						set u2 [srcs::use $r $t2 $x1 $conf]
						
						if {[expr $b-$u2] < $q} {
							set feas 0
							break
						}
					}
					if {$feas == 0} {
						break
					}
				}
				if {$feas == 1} {
					break
				}
			}
 			
			# t is the chosen option
			if {!$feas} {error "no feasible option for $j"}
			
			lappend x1 [list $j [set x1j $t]]
			dict set done $j [set fj [expr $x1j + $dj]]
		}
		
		return $x1
	}
	
	# assumes its input schedule is feasible.
	# it converts it into chain-form schedule 
	# and returns the earliest start times.
	
	proc cfsify {x cfs conf} {
		set k [expr [dict get $conf k]-1]
		set n [dict get $conf n]
		
		# initialize chains
		set chn [dict create]
		for {set r 0} {$r < $k} {incr r} {
			set b [dict get $conf $r:cap]
			for {set m 0} {$m < $b} {incr m} {
				dict set chn $r:$m 0
				dict set chn $r:$m:fi 0
			}
		}
		
		# chain each task (in order of incr. start)
		set x [lsort -increasing -real -index 1 $x]
		set done [dict create]
		
		foreach pair $x {
			lassign $pair j xj
			set dj [dict get $conf $j:D:m1]
			set fj [expr $xj + $dj]
			dict set done $j $fj
				
			if {$j == 0} {continue}

			for {set r 0} {$r < $k} {incr r} {
				set b [dict get $conf $r:cap]
				set q [dict get $conf $j:req:$r]
				if {$q == 0} {continue}
				
				for {set m 0} {$m < $b} {incr m} {
					set i [dict get $chn $r:$m]
					set fi [dict get $chn $r:$m:fi]
					if {$fi > $xj} {continue}
					
					if {![srcs::c_plan_get_edge $cfs $i $j]} {
						srcs::c_plan_set_edge $cfs $i $j 2					
					}
					dict set chn $r:$m $j
					dict set chn $r:$m:fi $fj
					
					if {[incr q -1] == 0} {
						break
					}
				}
				if {$q > 0} {
					error "couldn't chain $j: q=$q"
				}
			}
		}
		
		srcs::c_plan_order $cfs.top $cfs
		srcs::c_mcarlo_est $cfs.est $cfs $cfs.top $cfs.d 1			
		set xest [list]
		for {set i 0} {$i < $n} {incr i} {
			set esti [srcs::c_matr_get $cfs.est 0 $i real]
			lappend xest [list $i $esti]
		}
		return $xest
	}
}

if { [llength $argv] < 2 } {
	error "use: <tms path> <aimms output path> \[binlen\]"
}
lassign $argv path apath binlen
if {"$binlen" == ""} {
	set binlen 0
}

# read instance configuration
set conf [srcs::load_tms $path]
set n [dict get $conf n]
set k [expr [dict get $conf k]-1]
srcs::plan_new .plan $conf
srcs::plan_new .cfs  $conf
srcs::c_matr_new .cfs.top 1 $n
srcs::c_matr_new .cfs.est 1 $n
srcs::c_matr_new .cfs.d 1 $n
srcs::uni_d_new .cfs.d 1 $conf

# read the schedule output by aimms in variable x0
set afile [open $apath]
set x0 [srcs::aimms::read $afile]
close $afile

set x1 [srcs::aimms::sgsify $x0 .plan $conf]
set x2 [srcs::aimms::repair $x0 .plan $conf 1]
set x3 [srcs::aimms::cfsify $x2 .cfs  $conf]

set x0 [lsort -increasing -real -index 0 $x0]
set x1 [lsort -increasing -real -index 0 $x1]
set x2 [lsort -increasing -real -index 0 $x2]
set x3 [lsort -increasing -real -index 0 $x3]

puts $x0
puts "--"
puts $x1
puts "--"
puts $x2
puts "--"
puts $x3

set dir "/home/lum/ksmountakis_dropbox/Dropbox"
srcs::mathematica::profiles $x0 "$dir/plot0.pdf" $conf 1 $binlen
srcs::mathematica::profiles $x1 "$dir/plot1.pdf" $conf 1 $binlen
srcs::mathematica::profiles $x2 "$dir/plot2.pdf" $conf 1 $binlen
srcs::mathematica::profiles $x3 "$dir/plot3.pdf" $conf 1 $binlen
