#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include "srcs.h"
#define ABS(x) (((x)<0)?-(x):(x))

typedef struct pwq {
	real_t a, b, c;
	int t;
}pwq_t;

static struct {
	int pwsumlen;
	int pwsumnum;
	
	int pwmaxlen;
	int pwmaxnum;
	
} stats;

#define SF_LEN(SF, i) SF->cell[0][i]._int
#define SF_FLD_INDX(z, f, nf) (1+(z*nf)+f)
#define SF_FLD(SF, i, z, f,nf) SF->cell[SF_FLD_INDX(z,f,nf)][i]
#define PWL_LEN(SF, i) SF_LEN(SF,i)
#define PWL_FLD(SF, i, z, f) SF_FLD(SF,i, z, f, PWL_NF)
#define PWL_NF 2

#define PWC_LEN PWL_LEN
#define PWC_FLD PWL_FLD
#define PWC_NF PWL_NF

#define PWL_s(V,i, x) PWL_FLD(V,i, x, 0)._real
#define PWL_t(V,i, x) PWL_FLD(V,i, x, 1)._int



#define SF_ROWS(len, nf) 1+nf*len
#define PWL_ROWS(len) SF_ROWS(len, PWL_NF)
#define PWC_ROWS PWL_ROWS

static matr_t *S, *D, *C;
static real_t deltamax;
static real_t
(*pwqsample)(matr_t *sf, int i, pwq_t *pwq, int pwqlen, real_t conflev);


static void
sfcopy(matr_t *dst, int i, matr_t *src, int j, int nf)
{
	int z, f;
	
	assert(dst && src);
	assert(dst->cols >= src->cols);
	
 	srcs_matr_expand(dst, src->rows);
 	
	SF_LEN(dst,i) = SF_LEN(src,j);
 	for (z=0; z < SF_LEN(src,j); z++)
		for (f=0; f < nf; f++) 
			SF_FLD(dst,i, z,f,nf) = SF_FLD(src,j, z,f,nf);
}


int
pwqcmp(const void *q1ptr, const void*q2ptr)
{
	pwq_t *q1 = (pwq_t*)q1ptr;
	pwq_t *q2 = (pwq_t*)q2ptr;
	
	return q1->t - q2->t;
}

static int
pwqmerge(pwq_t *pwq, pwq_t *pwq0, int pwqlen0)
{
	int i, ti, j, tj;
	real_t a, b, c;
	int pwqlen;
	
	pwqlen = 0;
	i = 0;
	while (i < pwqlen0) {
		a = pwq0[i].a;
		b = pwq0[i].b;
		c = pwq0[i].c;
		ti = pwq0[i].t;
		
		for (j=i+1; j < pwqlen0; j++) {
			tj = pwq0[j].t;
			if (tj != ti)
				break;
			
			a += pwq0[j].a;
			b += pwq0[j].b;
			c += pwq0[j].c;
		}
		
		pwq[pwqlen].a = a;
		pwq[pwqlen].b = b;
		pwq[pwqlen].c = c;
		pwq[pwqlen++].t = ti;
		i = j;
	}
	
	return pwqlen;
}

static void
lf2pwlsf(matr_t *pwl, int i, pair_t *lf, int lflen)
{
	int x;
	int tx;
	real_t vx;
	
	srcs_matr_expand(pwl, PWL_ROWS(lflen));
	PWL_LEN(pwl,i) = lflen;
	
	PWL_s(pwl,i, 0) = lf[0].x._real;
	PWL_t(pwl,i, 0) = lf[0].y._int;
	for (x=1; x < lflen; x++) {
		vx = (lf[x].x._real - lf[x-1].x._real);
		tx = lf[x].y._int;
		
		PWL_s(pwl,i, x) = vx;
		PWL_t(pwl,i, x) = tx;
	}
}

static real_t
pwqsample_smart(matr_t *sf, int i, pwq_t *pwq, int pwqlen, real_t conflev)
{
	int x;
	real_t ax, bx, cx;
	int tx;
	real_t a, b, c;
	int t;
	real_t p0, p1, s;
	real_t p0pr, spr;
	real_t p1alt;
	int tpr;
	pair_t lf[pwqlen+1];
	int lflen;
	//real_t m1;
	real_t thres;
	real_t perc = 0;
 	int done;
 	
	int skip;
	
	
	a = b = c = 0;
	t = pwq[0].t;
	
	lflen = 0;
	//m1 = 0;
	perc = 0;
 	done = 0;
 	
	for (x=0; x < pwqlen; x++) {
		ax = pwq[x].a;
		bx = pwq[x].b;
		cx = pwq[x].c;
		tx = pwq[x].t;
		
		if (tx != t || x == pwqlen-1) {
			/* ax^2 + bx + c: current quadratic */
			p0 = a*(t*t) + b*t + c;
			p1 = a*(tx*tx) + b*tx + c;
			s = (p1-p0) / (tx-t);
			
			if (conflev != 0 && p0 <= conflev && conflev <= p1)
				perc = (conflev - p0)/s + t;
			
			
			//m1 += -(1.0/2)*b*(t*t)
			//	  -(2.0/3)*a*(t*t*t)
			//	  +(1.0/2)*b*(tx*tx)
			//	  +(2.0/3)*a*(tx*tx*tx);
			
			thres = deltamax;
			if (p1 >= 0.8)
				thres /= 2;
 			
			skip = 0;
			p1alt = p0pr + spr*(tx-tpr);
			
			if (lflen > 0 && ABS(p1-p1alt) <= thres) {
				skip = 1;

				spr = (p1-p0pr)/(real_t)(tx-tpr);
				lf[lflen-1].x._real = spr;
			}
			
			if (!skip) {
				lf[lflen].x._real = s;
				lf[lflen].y._int = t;
				lflen++;
				
				p0pr = p0;
				spr = s;
				tpr = t;
			}		
		}
 		
 		if (done) {
 			t = tx;
 			break;
 		}
		
		a += ax;
		b += bx;
		c += cx;
		t = tx;
	}
	
	lf[lflen].x._real = 0;
	lf[lflen].y._int = t;
	lflen++;
	
	lf2pwlsf(sf,i, lf, lflen);
	
	return perc;
}

 
static inline real_t
slope(real_t tpr, real_t ppr, real_t tp, real_t p)
{
	return (p - ppr)/(real_t)(tp - tpr);
}

static real_t
pwqsample_simple(matr_t *sf, int i, pwq_t *pwq, int pwqlen, real_t conflev)
{
	int x;
	real_t ax, bx, cx;
	int tx;
	real_t a, b, c;
	int t;
	real_t p0, p1, s;
    pair_t lf[pwqlen+1];
	int lflen;
	//real_t m1;
	real_t perc;
	real_t pnt[] = {0.1, 0.5, 0.8};
	int pntnum = sizeof(pnt) / sizeof(real_t);
	int tp, tpr;
	real_t p, ppr;
	int k;
  	
   	a = b = c = 0;
	t = pwq[0].t;
	
	lflen = 0;
	//m1 = 0;

  	k = 0;
  	ppr = 0.0;
  	tpr = pwq[0].t;
  	
 	for (x=0; x < pwqlen; x++) {
		ax = pwq[x].a;
		bx = pwq[x].b;
		cx = pwq[x].c;
		tx = pwq[x].t;
		
		if ((tx != t) || (x == pwqlen-1)) {
			/* ax^2 + bx + c: current quadratic */
			
 			p0 = a*(t*t) + b*t + c;
			p1 = a*(tx*tx) + b*tx + c;
			s = (p1-p0) / (tx-t);
	
			for (p=-1.0; (k < pntnum) && (p0 <= pnt[k] && pnt[k] <= p1); p=pnt[k++]);
			if (p != -1.0) {
				tp = (int)((p - p0)/s + (real_t)t - 0.5);
				assert(tp != 0);
				lf[lflen].x._real = slope(tpr, ppr, tp, p);
				lf[lflen].y._int = tpr;
				lflen++;
				ppr = p;
				tpr = tp;
			}
			
			//lf[lflen].x._real = s;
			//lf[lflen++].y._int = t;
			
 			//m1 += -(1.0/2)*b*(t*t)
			//	  -(2.0/3)*a*(t*t*t)
			//	  +(1.0/2)*b*(tx*tx)
			//	  +(2.0/3)*a*(tx*tx*tx);

		}
			
		a += ax;
		b += bx;
		c += cx;
		t = tx;
		
		if (k == pntnum)
			break;
	}
	
	perc = tpr;
	
	/* add last line */
	p = 1.0;
	tp = pwq[pwqlen-1].t;
	lf[lflen].x._real = slope(tpr, ppr, tp, p);
	lf[lflen].y._int = tpr;
	lflen++;
	
	/* add flat line */
	lf[lflen].x._real = 0;
	lf[lflen].y._int = tp;
	lflen++;
	
	lf2pwlsf(sf,i, lf, lflen);
	
	return perc;
}

 
static void pwsum_debug_1(int j);
static void pwsum_debug_2(int j, real_t m1);

static real_t
pwsum(int j, real_t conflev)
{
	real_t v, s;
	int t1, t2;
	int x, y;
	int pwqmaxnum = PWC_LEN(D,j)*PWL_LEN(S,j)+1;
	pwq_t pwq0[pwqmaxnum];
	pwq_t pwq[pwqmaxnum];
	int pwqlen0, pwqlen;
	real_t u,w;
	real_t perc;
 	
	stats.pwsumnum++;
	stats.pwsumlen += MAX(PWC_LEN(D,j), PWL_LEN(S,j));
	
	//printf("C%d = S%d(len=%d;%d) + D%d(len=%d;%d)\n", 
	//j, j, PWL_LEN(S,j), S->rows, j, PWC_LEN(D,j), D->rows);
 	
	//pwsum_debug_1(j);
	
	pwqlen0 = 0;
	for (x=0; x < PWC_LEN(D,j); x++) {
		v = PWC_FLD(D,j, x,0)._real;
		t1 = PWC_FLD(D,j, x,1)._int;
		
		for (y=0; y < PWL_LEN(S,j); y++) {
			s = PWL_FLD(S,j, y,0)._real;
			t2 = PWL_FLD(S,j, y,1)._int;
			
			pwq0[pwqlen0].a = (u=0.5*s*v);
			pwq0[pwqlen0].b = -2*(w=u*(t1+t2));
			pwq0[pwqlen0].c = w*(t1+t2);
			pwq0[pwqlen0++].t = (t1+t2);
 		}
	}
	
	qsort(pwq0, pwqlen0, sizeof(pwq_t), pwqcmp);
	pwqlen = pwqmerge(pwq, pwq0, pwqlen0);
	
  	perc = pwqsample(C,j, pwq, pwqlen, conflev);
	
	//pwsum_debug_2(j, 0);
	
	return perc;
}

static void pwmax_debug_1(int j, int i);
static void pwmax_debug_2(int j);

static void
pwmax(int j, int i)
{
	real_t s1, s2;
	int t1, t2;
	int x, y;
	int pwqmaxnum = PWL_LEN(S,j)*PWL_LEN(C,i)+1;
	pwq_t pwq0[pwqmaxnum];
	pwq_t pwq[pwqmaxnum];
	int pwqlen0, pwqlen;
	real_t u;
	
	stats.pwmaxnum++;
	stats.pwmaxlen += MAX(PWL_LEN(S,j), PWL_LEN(C,i));
		
	//printf("S%d = max(S%d(len=%d;%d), C%d(len=%d;%d))\n", 
	//j, j, PWL_LEN(S,j), S->rows, i, PWL_LEN(C,i), C->rows);	
 	
	//pwmax_debug_1(j, i);
	
	pwqlen0 = 0;
	for (x=0; x < PWL_LEN(S,j); x++) {
		s1 = PWL_s(S,j, x);
		t1 = PWL_t(S,j, x);
		
		for (y=0; y < PWL_LEN(C,i); y++) {
			s2 = PWL_s(C,i, y);
			t2 = PWL_t(C,i, y);
			
			pwq0[pwqlen0].a = (u=s1*s2);
			pwq0[pwqlen0].b = -(u*(t1+t2));
			pwq0[pwqlen0].c = u*(t1*t2);
			pwq0[pwqlen0++].t = MAX(t1,t2);
 		}
	}
	
	qsort(pwq0, pwqlen0, sizeof(pwq_t), pwqcmp);
	pwqlen = pwqmerge(pwq, pwq0, pwqlen0);
	
 	(void)pwqsample(S,j, pwq, pwqlen, 0);
	
	//pwmax_debug_2(j);
}


int
srcs_pwl_est(matr_t *_S, real_t *p95, plan_t *plan, matr_t *order,  matr_t *_D)
{
	int ordi, i, ordj, j, first;
   	
	S = _S;
	D = _D;
	deltamax = 0.1;
 	
	if (C && C->cols < _S->cols) {
		srcs_matr_free(C);
		C = NULL;
	}
	if (C == NULL) {
		assert((C = calloc(sizeof(matr_t), 1)));
		srcs_matr_init(1, _S->cols, C);
	}
	
  	//printf("C1 = D1\n");
  	sfcopy(C,1, D,1, PWC_NF);
  	
 	pwqsample = pwqsample_smart;
   
 	for (ordj=2; ordj < plan->n; ordj++) {
		j = order->cell[0][ordj]._int;
		first = 1;
		
 		for (ordi=1; ordi < ordj; ordi++) {
			i = order->cell[0][ordi]._int;
			
 			if (!plan->edge[i][j])
				continue;
			
			if (first) {
				//printf("S%d = C%d\n", j, i);
 				sfcopy(S,j, C,i, PWL_NF);
				first = 0;
				continue;
			}
			
 			pwmax(j, i);
  		}
		
		if (j == plan->n-1)
			*p95 = pwsum(j, 0.95);
		else
			(void)pwsum(j, 0);
   	}
   	
    //printf("p95 = %f\n", *p95);
  	
  	//printf("avg sum len: %f\n", 
  	//(real_t)stats.pwsumlen/stats.pwsumnum);
  	//printf("avg max len: %f\n", 
  	//(real_t)stats.pwmaxlen/stats.pwmaxnum);
  	
	return 0;
}



static void
pwsum_debug_1(int j)
{
	int x, y;
	real_t p, s;
	int t1, t2;
		
	printf(" D%d: ", j);
	for (x=0; x < PWC_LEN(D,j); x++) {
		
		p = PWC_FLD(D,j, x,0)._real;
		t1 = PWC_FLD(D,j, x,1)._int;
		
		printf("{%.3f, %d} ", p, t1);
		if (x != PWL_LEN(D,j)-1)
			printf(", ");		
	}
	puts("");
	
	printf(" S%d: ", j);
	for (y=0; y < PWL_LEN(S,j); y++) {
		
		s = PWL_FLD(S,j, y,0)._real;
		t2 = PWC_FLD(S,j, y,1)._int;
		
		printf("{%.3f, %d} ", s, t2);
		if (y != PWL_LEN(S,j)-1)
			printf(", ");
	}
	puts("");	
}

static void
pwmax_debug_1(int j, int i)
{
	int x, y;
	real_t s1, s2;
	int t1, t2;
		
	printf(" S%d: ", j);
	for (x=0; x < PWL_LEN(S,j); x++) {
		s1 = PWL_s(S,j, x);
		t1 = PWL_t(S,j, x);
		
		printf("{%f, %d} ", s1, t1);
		if (x != PWL_LEN(S,j)-1)
			printf(", ");		
	}
	puts("");
	
	printf(" C%d: ", i);
	for (y=0; y < PWL_LEN(C,i); y++) {
		s2 = PWL_s(C,i, y);
		t2 = PWL_t(C,i, y);
		
		printf("{%f, %d} ", s2, t2);
		if (y != PWL_LEN(C,i)-1)
			printf(", ");
	}
	puts("");	
}

static void
pwsum_debug_2(int j, real_t m1)
{
	int x;
	for (x=0; x < PWL_LEN(C,j); x++) {
		printf("{%f,%d}", PWL_s(C,j, x), PWL_t(C,j, x));
		if (x<PWL_LEN(C,j)-1)
			printf(", ");
	}
	printf("\tm1=%f", m1);
	
	puts("");
}

static void
pwmax_debug_2(int j)
{
	int x;
	for (x=0; x < PWL_LEN(S,j); x++) {
		printf("{%f,%d}", PWL_s(S,j, x), PWL_t(S,j, x));
		if (x<PWL_LEN(S,j)-1)
			printf(", ");
	}
	puts("");
}