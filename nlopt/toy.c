#include <stdio.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>

typedef struct {
	double a;
	double b;
} gparam_t;

double
g(unsigned n, const double *x, double *grad, void *param_ptr)
{
	gparam_t *param = (gparam_t*)param_ptr;
	double a = param->a;
	double b = param->b;
	double sum = a*x[0] + b;
	double val = pow(sum, 3) - x[1];
		
	if (grad) {
		grad[0] = 3.0*a*pow(sum, 2);
		grad[1] = -1.0;
	}
	
	return val;
}

double
f(unsigned n, const double *x, double *grad, void *data)
{
	static long numeval;
	printf("numeval %ld\n", ++numeval);
	
	if (grad) {
		grad[0] = 0.0;
		grad[1] = 0.5/sqrt(x[1]);
	}
	return sqrt(x[1]);
}

int
main(void)
{

	int n = 2;
	double *x;
	int i;
	nlopt_opt opt;
	nlopt_algorithm alg;
	double minf;
	gparam_t param[2] = {
		{2, 0},
		{-1, 1}
	};
	double lb[2] = {-HUGE_VAL, 0};
	
	/* allocations */
	assert((x = calloc(sizeof(double), n)));
	
	/* initialize opt (TODO) */
	alg = NLOPT_LD_MMA;
	opt = nlopt_create(alg, n);
	
	nlopt_set_min_objective(opt, f, NULL);
	nlopt_set_lower_bounds(opt, lb);
	nlopt_add_inequality_constraint(opt, g, &param[0], 1e-8);
	nlopt_add_inequality_constraint(opt, g, &param[1], 1e-8);
	nlopt_set_xtol_rel(opt, 1e-4);
	
	x[0] = 1.234;
	x[1] = 5.678;
	
	/* output solution */
	printf("%s\n", nlopt_algorithm_name(alg));
	if (nlopt_optimize(opt, x, &minf) < 0) 
		printf("failure");
	else
		for (i=0; i < n; i++) 
			printf("%-2.1f ", x[i]);
	printf("\n");
	
	free(x);
	return 0;
}