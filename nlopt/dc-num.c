#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>
 

/* parameters */

int n, m, N;
int **suc;
double *b;
double **q;
double *d;
double *x0;

double w = 1;

#define x(i) V[i]

static void read_params(void);

double 
f(unsigned _N, const double *V, double *grad, void *data)
{
	
	if (grad) {
		
	}
	return  0;
}


int
main(void)
{
	int i;
	int ret;
 	double *V;
 	double minf;
 	
 	nlopt_opt opt, lopt;
 	
	/* load parameters */
	read_params();
	N = n;

	/* initialize variables to early-start schedule */
	assert((V = calloc(sizeof(double), N)));
 	for (i=0; i < n; i++)
		x(i) = x0[i];
		
	/* create local_opt object */
	lopt = nlopt_create(NLOPT_LD_LBFGS, N);
 	nlopt_set_ftol_rel(lopt, 1e-4);
	
	/* create opt object */
	opt = nlopt_create(NLOPT_AUGLAG, N);
	nlopt_set_lower_bounds1(opt, 0.0);
	nlopt_set_min_objective(opt, f, NULL);
	nlopt_set_local_optimizer(opt, lopt);
 	nlopt_set_ftol_rel(opt, 1e-8);
 	
	/* call optimizer */
	fprintf(stderr, "optimize:");
	ret = nlopt_optimize(opt, V, &minf);
 	
	if (ret < 0) {
		fprintf(stderr, "failure\n");
		goto main_end;
	} else
		fprintf(stderr, "ok\n");
	
	/* print x */
	printf("x:");
	for(i=0; i < n; i++)
		printf("%.1f ", x(i));
	puts("");
	
main_end:
 	
	return  0;
}
 
static void
read_params(void)
{
	/* variables stuff */
 	int i, j, r;
 	 
	/* read n */
	scanf("%d %d", &n, &m);
	
	assert((suc = calloc(sizeof(int*), n)));
	for (i=0; i < n; i++)
		assert((suc[i] = calloc(sizeof(int), n)));
	assert((b = calloc(sizeof(double), m)));
	assert((q = calloc(sizeof(double*), m)));
	for (r=0; r < m; r++)
		assert((q[r] = calloc(sizeof(double), n)));		
 	assert((d = calloc(sizeof(double), n)));
 	assert((x0 = calloc(sizeof(double), n)));

	/* read capacities */
	//printf("b[] = ");
	for (r=0; r < m; r++) {
		scanf("%lf", &b[r]);
		//printf("%.0f ", b[r]);
	}
	//puts("");
	
	/* read durations, arcs, requests, start-times */
	for (i=0; i < n; i++) {
		scanf("%lf: ", &d[i]);
		//printf("d[%d]=%.0f\n", i, d[i]);
		//printf("suc[%d]=", i);
		for (j=0; j < n; j++) {
			scanf("%d", &suc[i][j]);
			//printf("%d ", suc[i][j]);
 		}
 		//puts("");
 		
 		//printf("q[%d]=", i);
 		for (r=0; r < m; r++) {
 			scanf("%lf", &q[r][i]);
 			//printf("%.0lf ", q[r][i]);
 		}
 		//puts("");
	}
	
	/* read initial point x0 */
	//printf("x0: ");
	for (i=0; i < n; i++) {
		scanf("%lf", &x0[i]); 
		//printf("%.0lf ", x0[i]);
	}
//	puts("");
  
}
