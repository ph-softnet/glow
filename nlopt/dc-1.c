#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>
 
struct fdata {
	double a;
};

/* parameters */

int n, m;
int **suc;
double *b;
double **q;
double *r;
double *xinit;
double w = 2;
double epsilon = 0.01;


double *V;
double *V0;

#define x(i) V[i]
#define y(i) V[n+i]

#define x0(i) V0[i]
#define y0(i) V0[n+i]

#define L	 V[N-1]
#define MAX(t1,t2) ((t1) > (t2))? (t1): (t2)

static void read_params(void);

static double
divide(double num, double den)
{
	if (num == 0)
		return 0;
	else if (den == 0)
		return HUGE_VAL;
	
	return num / den;
}

double
f_value(unsigned N, const double *V, double a)
{
	int i, j;
	double dij, dij0, mij, lgij, sum;
	double grad_dij0[4], dijT;
		
 	sum = (1-a)*L;
	for (i=0; i < n-1; i++) {
		for (j=i+1; j < n; j++) {	
			dij = sqrt(pow(x(i)-x(j),2)+pow(y(i)-y(j),2));
			dij0 = sqrt(pow(x0(i)-x0(j),2)+pow(y0(i)-y0(j),2));	
			
			grad_dij0[0] = divide(x0(i)-x0(j), dij0);
			grad_dij0[1] = -grad_dij0[0];
			grad_dij0[2] = divide(y0(i)-y0(j), dij0);
			grad_dij0[3] = -grad_dij0[2];
			
			dijT = dij;
			dijT += grad_dij0[0]*(x(i)-x0(i));
			dijT += grad_dij0[1]*(x(j)-x0(j));
			dijT += grad_dij0[2]*(y(i)-y0(i));
			dijT += grad_dij0[3]*(y(j)-y0(j));
 			
			mij = MAX(dij, r[i]+r[j]);
			lgij = log(exp(w*(dij-mij))+exp(w*(r[i]+r[j]-mij)));
			
			sum += a*(divide(lgij,w) + mij - dijT);
		}
	}
	
	printf("f=%f\n", sum);
 	return sum;
}

int
main(void)
{
	int N, i;
 	double *V;
  	
	/* load parameters */
	read_params();
	N = 2*n + 1;
	
	assert((V = calloc(sizeof(double), N)));
	assert((V0 = calloc(sizeof(double), N)));
	
	for (i=0; i < n; i++) {
		x(i) = x0(i) = xinit[i];
		y(i) = y0(i) = r[i];
	}
	L = 0;
	
	f_value(N, V, 0.5);
	x0(3) += 1;
	f_value(N, V, 0.5); 
	
	return  0;
}
 
static void
read_params(void)
{
	/* variables stuff */
 	int i, j, k;
 	 
	/* read n */
	scanf("%d %d", &n, &m);
	
	assert((suc = calloc(sizeof(int*), n)));
	for (i=0; i < n; i++)
		assert((suc[i] = calloc(sizeof(int), n)));
	assert((b = calloc(sizeof(double), m)));
	assert((q = calloc(sizeof(double*), m)));
	for (k=0; k < m; k++)
		assert((q[k] = calloc(sizeof(double), n)));		
 	assert((r = calloc(sizeof(double), n)));
 	assert((xinit = calloc(sizeof(double), n)));

	/* read capacities */
	//printf("b[] = ");
	for (k=0; k < m; k++) {
		scanf("%lf", &b[k]);
		//printf("%.0f ", b[k]);
	}
	//puts("");
	
	/* read durations, arcs, requests, start-times */
	for (i=0; i < n; i++) {
		scanf("%lf: ", &r[i]);
		//printf("d[%d]=%.0f\n", i, r[i]);
		//printf("suc[%d]=", i);
		for (j=0; j < n; j++) {
			scanf("%d", &suc[i][j]);
			//printf("%d ", suc[i][j]);
 		}
 		//puts("");
 		
 		//printf("q[%d]=", i);
 		for (k=0; k < m; k++) {
 			scanf("%lf", &q[k][i]);
 			//printf("%.0lf ", q[k][i]);
 		}
 		//puts("");
	}
	
	/* read initial point xinit */
	//printf("xinit: ");
	for (i=0; i < n; i++) {
		scanf("%lf", &xinit[i]); 
		//printf("%.0lf ", xinit[i]);
	}
//	puts("");
  
}
