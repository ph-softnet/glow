#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>
 

/* parameters */

int n, m, N;
int **suc;
double *b;
double **q;
double *d;
double *x0;

double w = 1;

#define x(i) v[i]

static void read_params(void);

double 
f(unsigned _N, const double *v, double *grad, void *data)
{
	fprintf(stderr,".\n");
	
	if (grad) {
		memset(grad, 0, sizeof(double)*N);
		grad[n-1] = 1.0;
	}
	return x(n-1);
}

struct pair {
	int p1, p2;
 };

double 
pre(unsigned _N, const double *v, double *grad, void *data)
{
	struct pair *pair = (struct pair*)data;
	int i = pair->p1;
	int j = pair->p2;
	
 	if (grad) {
		memset(grad, 0, sizeof(double)*N);
		grad[i] = 1.0;
		grad[j] = -1.0;
	}
	
	return x(i) + d[i] - x(j);
}

#define sech(t) (1.0/cosh(t))
#define sech2(t) pow(sech(t), 2)

double 
res(unsigned _N, const double *v, double *grad, void *data)
{
	struct pair *pair = (struct pair*)data;
	int r = pair->p1;
	double t = (double)pair->p2;
	int i;
	double sum;
	
 	if (grad) {
		for (i=0; i < n; i++)
			grad[i] = 0.5*q[r][i]*w*(sech2(w*(t-x(i)-d[i]))-sech2(w*(t-x(i))));
 	}
	
	sum = 0.0;
	for (i=0; i < n; i++)
		sum += 0.5*q[r][i]*(tanh(w*(t-x(i))) - tanh(w*(t-x(i)-d[i])));
		
	return sum - b[r];
}

int
main(void)
{
	nlopt_opt opt, lopt;
	struct pair **pair_pre;
	struct pair **pair_res;
	int i, j, r;
	double *v;
	double minf;
	nlopt_result ret;
	double T;
	int t;
 	
	/* load parameters */
	read_params();
	
	N = n;
 
	T = x0[n-1];
	
 	printf("n = %d, N = %d, m = %d, T = %.0f\n", n, N, m, T);
	
	/* create local_opt object */
	lopt = nlopt_create(NLOPT_LD_LBFGS, N);
 	nlopt_set_ftol_rel(lopt, 1e-2);
	
	/* create opt object */
	opt = nlopt_create(NLOPT_AUGLAG, N);
	nlopt_set_lower_bounds1(opt, 0.0);
	nlopt_set_min_objective(opt, f, NULL);
	nlopt_set_local_optimizer(opt, lopt);
	
	/* initialize pair[][] */
	assert((pair_pre = calloc(sizeof(struct pair*), n)));
	for (i=0; i < n; i++)
		assert((pair_pre[i] = calloc(sizeof(struct pair), n)));
		
	assert((pair_res = calloc(sizeof(struct pair*), m)));
	for (r=0; r < m; r++)
		assert((pair_res[r] = calloc(sizeof(struct pair), T)));	
		
	/* add precedence constraints */	
	for (i=0; i < n; i++) {
		for (j=0; j < n; j++) {
			if (suc[i][j]) {
				pair_pre[i][j].p1 = i;
				pair_pre[i][j].p2 = j;
				nlopt_add_inequality_constraint(opt, pre, &(pair_pre[i][j]), 1e-1);
			}
		}
	}

		
	/* add resource constraints */
	for (r=0; r < m; r++) {
		for (t=0; t <= (int)T; t += 2.5) {
			pair_res[r][(int)t].p1 = r;
			pair_res[r][(int)t].p2 = t;
			nlopt_add_inequality_constraint(opt, res, &(pair_res[r][t]), 1e-1);
		}
	}
	
	/* add stopping criteria */
 	nlopt_set_ftol_rel(opt, 1e-4);
	
	/* initialize variables to early-start schedule */
	assert((v = calloc(sizeof(double), N)));
 	for (i=0; i < n; i++)
		x(i) = x0[i];
 	
	/* call optimizer */
	fprintf(stderr, "optimize:");
	ret = nlopt_optimize(opt, v, &minf);
 	
	if (ret < 0) {
		fprintf(stderr, "failure\n");
		goto endpoint;
	} else
		fprintf(stderr, "ok\n");
	
	/* print x */
	printf("x:");
	for(i=0; i < n; i++)
		printf("%.1f ", x(i));
	puts("");
	
	/* print precedence violations */
	for (i=0; i < n; i++) {
		for (j=0; j < n; j++) {
			if (!suc[i][j])
				continue;
			
			if ((int)(x(i) + d[i]) > (int)x(j)) {
				printf("(x[%d]+d[%d] = %.1f) >= (x[%d] = %.1f)\n",
						i, i, x(i)+d[i], j, x(j));
			}
		}
	}
endpoint:
 	
	return  0;
}
 
static void
read_params(void)
{
	/* variables stuff */
 	int i, j, r;
 	 
	/* read n */
	scanf("%d %d", &n, &m);
	
	assert((suc = calloc(sizeof(int*), n)));
	for (i=0; i < n; i++)
		assert((suc[i] = calloc(sizeof(int), n)));
	assert((b = calloc(sizeof(double), m)));
	assert((q = calloc(sizeof(double*), m)));
	for (r=0; r < m; r++)
		assert((q[r] = calloc(sizeof(double), n)));		
 	assert((d = calloc(sizeof(double), n)));
 	assert((x0 = calloc(sizeof(double), n)));

	/* read capacities */
	//printf("b[] = ");
	for (r=0; r < m; r++) {
		scanf("%lf", &b[r]);
		//printf("%.0f ", b[r]);
	}
	//puts("");
	
	/* read durations, arcs, requests, start-times */
	for (i=0; i < n; i++) {
		scanf("%lf: ", &d[i]);
		//printf("d[%d]=%.0f\n", i, d[i]);
		//printf("suc[%d]=", i);
		for (j=0; j < n; j++) {
			scanf("%d", &suc[i][j]);
			//printf("%d ", suc[i][j]);
 		}
 		//puts("");
 		
 		//printf("q[%d]=", i);
 		for (r=0; r < m; r++) {
 			scanf("%lf", &q[r][i]);
 			//printf("%.0lf ", q[r][i]);
 		}
 		//puts("");
	}
	
	/* read initial point x0 */
	//printf("x0: ");
	for (i=0; i < n; i++) {
		scanf("%lf", &x0[i]); 
		//printf("%.0lf ", x0[i]);
	}
//	puts("");
  
}
