#include <stdio.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>

typedef struct {
	double a;
	double b;
} gp_t;
 
void
g(unsigned m, double *res, unsigned n, 
  const double *x, double *grad, void *gp_ptr)
{
	gp_t *gp = (gp_t*)gp_ptr;
	double a, b, sum, sum3, sum2;
	int i;
	
 	for (i=0; i < m; i++) {
 		a = gp[i].a;
 		b = gp[i].b;
 		
 		sum = a*x[0] + b;
 		sum2 = pow(sum, 2);
 		sum3 = pow(sum, 3);
		res[i] = sum3 - x[1];
		
		if (grad) {
			grad[i*n + 0] = 3.0*a*sum2;
			grad[i*n + 1] = -1.0;
		}
	}
 
}

double
f(unsigned n, const double *x, double *grad, void *data)
{
	static long numeval;
	printf("numeval %ld\n", ++numeval);
	
	if (grad) {
		grad[0] = 0.0;
		grad[1] = 0.5/sqrt(x[1]);
	}
	return sqrt(x[1]);
}

int
main(void)
{
	/* variables stuff */
	static int n = 2;
	double *x;
	double lb[2] = {
		-HUGE_VAL, 
		0.0
	};
	
	/* constraints stuff */
	int m = 2;
	double *gtol;
	gp_t gp[2] = {
		{2, 0},
		{-1, 1}
	};
	
	/* other stuff */
	int i;
	nlopt_opt opt;
	nlopt_algorithm alg;
	double minf;

	/* allocate vectors, matrices */
	assert((x = calloc(sizeof(double), n)));
	assert((gtol = calloc(sizeof(double), m)));
	
	/* create opt object */
	alg = NLOPT_LD_MMA;
	opt = nlopt_create(alg, n);
	
	/* set objective */
	nlopt_set_min_objective(opt, f, NULL);
	nlopt_set_xtol_rel(opt, 1e-4);
	
	/* set bounds */
	nlopt_set_lower_bounds(opt, lb);
	
	/* set nonlinear constraints */
	for (i=0; i < m; i++)
		gtol[i] = 1e-8;
	nlopt_add_inequality_mconstraint(opt, m, g, gp, gtol);
	
	/* initial solution */
	x[0] = 1.234;
	x[1] = 5.678;
	
	/* output solution */
	printf("%s\n", nlopt_algorithm_name(alg));
	if (nlopt_optimize(opt, x, &minf) < 0) 
		printf("failure");
	else
		for (i=0; i < n; i++) 
			printf("%-2.1f ", x[i]);
	printf("\n");
	
	free(x);
	return 0;
}