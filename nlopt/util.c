#include <math.h>



double
smax(double *x, int n, double sf, double *sum)
{
	double xmax;
	int i;

	xmax = x[0];
	for (i=0; i < n; i++)
 		if (xmax < x[i])
			xmax = x[i];
	
	*sum = 0.0;
	for (i=0; i < n; i++)
		*sum += exp(sf*(x[i] - xmax));
		
	return (log(*sum) + sf*xmax) / sf;
}