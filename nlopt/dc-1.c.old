#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>
 
struct fdata {
	double a;
};

/* parameters */

int n, m;
int **suc;
double *b;
double **q;
double *dr;
double *xinit;
double w = 1;

/* structures for computing f(x,y) */

double *V0;
double **r;
double **d;
double **d0;
double **mx;
double **zet;
double **ksi;
double **psi_x;
double **psi_y;
double **phi_x;
double **phi_y;

#define x(i) V[i]
#define y(i) V[n+i]
#define x0(i) V0[i]
#define y0(i) V0[n+i]
#define L	 V[N-1]
#define MAX(t1,t2) ((t1) > (t2))? (t1): (t2)

static void read_params(void);

static double
divide(double num, double den)
{
	if (num == 0)
		return 0;
	else if (den == 0)
		return HUGE_VAL;
	
	return num / den;
}

double
f_value(unsigned N, const double *V, double a)
{
	int i, j;
	double sum;
	double dijg[4];
	double dijT;
	double logij;
	
	sum = (1.0 - a)*L;
	for (i=0; i < n-1; i++) {
		for (j=i+1; j < n; j++) {
			dijg[0] = divide(x(i)-x(j), d[i][j]);
			dijg[1] = -dijg[1];
			dijg[2] = divide(y(i)-y(j), d[i][j]);
			dijg[3] = -dijg[2];
			
			dijT = 0;
			dijT += dijg[0]*(x(i)-x0(i));
			dijT += dijg[1]*(x(j)-x0(j));
			dijT += dijg[2]*(y(i)-y0(i));
			dijT += dijg[3]*(y(j)-y0(j));
			
			logij = log(exp(w*(d[i][j]-mx[i][j])) + exp(w*(r[i][j]-mx[i][j])));
			
			sum += a*((1.0/w)*logij + mx[i][j] - dijT);
		}
	}
	
	return sum;
}

double 
f(unsigned N, const double *V, double *grad, void *data)
{
	struct fdata *fdata = (struct fdata*)data;
	double a = fdata->a;
	double den;
	double t1, t2;
  	int i, j;
	
	/* step 1: rij, dij, dwvij */
	for (i=0; i < n-1; i++) {
		for (j=i+1; j < n; j++) {
			r[i][j] = dr[i] + dr[j];
			d[i][j] = sqrt(pow(x(i)-x(j),2) + pow(y(i)-y(j),2));
			d0[i][j] = sqrt(pow(x0(i)-x0(j),2) + pow(y0(i)-y0(j),2));
			
 		}
	}
	
	/* step 2: mij */
	for (i=0; i < n-1; i++) 
		for (j=i+1; j < n; j++)
			mx[i][j] = MAX(r[i][j], d[i][j]);

	
	/* step 3: zeta, ksi, psi_x, psi_y */
	for (i=0; i < n-1; i++) {
		for (j=i+1; j < n; j++) {
			zet[i][j] = exp(w*(d[i][j] - mx[i][j]));
			ksi[i][j] = exp(w*(r[i][j] - mx[i][j]));
			
			psi_x[i][j] = divide(x0(i)-x0(j), d0[i][j]);
			psi_y[i][j] = divide(y0(i)-y0(j), d0[i][j]);
		}
	}
	
	/* step 4: phi */
	for (i=0; i < n-1; i++) {
		for (j=i+1; j < n; j++) {
			den = d[i][j]*(zet[i][j] + ksi[i][j]);
			phi_x[i][j] = divide(a*(x(i)-x(j))*zet[i][j], den);
			phi_y[i][j] = divide(a*(y(i)-y(j))*zet[i][j], den);
		}
	}
	
	if (grad) {
		/* x[1 .. n] */
		for (j=0; j < n; j++) {
			t1 = 0;
			for (i=j+1; i < n; i++)
				t1 += phi_x[i][j] - psi_x[i][j];
			
			t2 = 0;
			for (i=0; i < j; i++)
				t2 += phi_x[i][j] - psi_x[i][j];
			
			grad[j] = t1 - t2;			
		}
		
		/* y[1 .. n] */
		for (j=0; j < n; j++) {
			t1 = 0;
			for (i=j+1; i < n; i++)
				t1 += phi_y[i][j] - psi_y[i][j];
			
			t2 = 0;
			for (i=0; i < j; i++)
				t2 += phi_y[i][j] - psi_y[i][j];
			
			grad[n+j] = t1 - t2;	
		}
		
		/* L */
		grad[N-1] = 1.0 - a;
	}

 	
 	return f_value(N, V, a);
}
 

int
main(void)
{
	int N, i;
	nlopt_opt opt;
	double *V, minf;
	struct fdata fdata;
 	
	/* load parameters */
	read_params();
	
	N = 2*n + 1;
	assert((V = calloc(sizeof(double), N)));
	assert((V0 = calloc(sizeof(double), N)));
	for (i=0; i < n; i++)
		x0(i) = xinit[i];
	
	assert((r = calloc(sizeof(double*), n)));
	assert((d = calloc(sizeof(double*), n)));
	assert((d0 = calloc(sizeof(double*), n)));
	assert((mx = calloc(sizeof(double*), n)));
	assert((zet = calloc(sizeof(double*), n)));
	assert((ksi = calloc(sizeof(double*), n)));
	assert((psi_x = calloc(sizeof(double*), n)));
	assert((psi_y = calloc(sizeof(double*), n)));
	assert((phi_x = calloc(sizeof(double*), n)));
	assert((phi_y = calloc(sizeof(double*), n))); 
		
	for (i=0; i < n; i++) {
		assert((r[i] = calloc(sizeof(double), n)));
		assert((d[i] = calloc(sizeof(double), n)));
		assert((d0[i] = calloc(sizeof(double), n)));
		assert((mx[i] = calloc(sizeof(double), n)));
		
		assert((zet[i] = calloc(sizeof(double), n)));
		assert((ksi[i] = calloc(sizeof(double), n)));
		assert((psi_x[i] = calloc(sizeof(double), n)));
		assert((psi_y[i] = calloc(sizeof(double), n)));	
		
		assert((phi_x[i] = calloc(sizeof(double), n)));
		assert((phi_y[i] = calloc(sizeof(double), n)));				
 	}
	
	/* create opt object */
	opt = nlopt_create(NLOPT_LD_LBFGS, N);
 	nlopt_set_ftol_rel(opt, 1e-2);
	nlopt_set_lower_bounds1(opt, 0.0);
	nlopt_set_min_objective(opt, f, &fdata);
	fdata.a = 0.5;
	
	/* define starting point */
 
	/* find local minimum */
	printf("optimize: ");
	if (nlopt_optimize(opt, V, &minf) < 0)
		puts("failure");
 	else
 		printf("f(x)=%.1f\n", minf);
 	
 	/* print solution  */
 	for (i=0; i < n; i++)
 		printf("%.1f %.1f\n", x(i), y(i));
 	
 	printf("%f\n", V[N-1]);	
 	
	return  0;
}
 
static void
read_params(void)
{
	/* variables stuff */
 	int i, j, r;
 	 
	/* read n */
	scanf("%d %d", &n, &m);
	
	assert((suc = calloc(sizeof(int*), n)));
	for (i=0; i < n; i++)
		assert((suc[i] = calloc(sizeof(int), n)));
	assert((b = calloc(sizeof(double), m)));
	assert((q = calloc(sizeof(double*), m)));
	for (r=0; r < m; r++)
		assert((q[r] = calloc(sizeof(double), n)));		
 	assert((dr = calloc(sizeof(double), n)));
 	assert((xinit = calloc(sizeof(double), n)));

	/* read capacities */
	//printf("b[] = ");
	for (r=0; r < m; r++) {
		scanf("%lf", &b[r]);
		//printf("%.0f ", b[r]);
	}
	//puts("");
	
	/* read durations, arcs, requests, start-times */
	for (i=0; i < n; i++) {
		scanf("%lf: ", &dr[i]);
		//printf("d[%d]=%.0f\n", i, dr[i]);
		//printf("suc[%d]=", i);
		for (j=0; j < n; j++) {
			scanf("%d", &suc[i][j]);
			//printf("%d ", suc[i][j]);
 		}
 		//puts("");
 		
 		//printf("q[%d]=", i);
 		for (r=0; r < m; r++) {
 			scanf("%lf", &q[r][i]);
 			//printf("%.0lf ", q[r][i]);
 		}
 		//puts("");
	}
	
	/* read initial point xinit */
	//printf("xinit: ");
	for (i=0; i < n; i++) {
		scanf("%lf", &xinit[i]); 
		//printf("%.0lf ", xinit[i]);
	}
//	puts("");
  
}
