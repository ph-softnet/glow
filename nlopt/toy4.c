#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nlopt.h>
#include <math.h>
#include <assert.h>

extern double smax(double*, int, double, double*);

typedef struct {
	double **q;
	double *b;
} up_t;

typedef struct {
	int i;
	int j;
	int on;
} pp_t;

double z = 10;
double w = 8;
double *d;
double **phi;
double **psi;
double **ksi;
double **h;
double *g;
double **ggrad;
double *theta;
double *u;
double **ugrad;

void
make_u(unsigned m, double *res, unsigned n, 
  const double *x, double *grad, void *up_ptr)
{
	up_t *up = (up_t*)up_ptr;
 	int r;
 	int i, j;
 	double ksi_1, ksi_2;
 	double h_1, h_2;
 	double *q, b;
 	double gmax;
 	double thetanorm;
 
 	
 	/* (1): phi, psi */
 	for (i=0; i < n; i++) {
 		for (j=0; j < n; j++) {
 			phi[i][j] = exp(2*w*(x[i]+0.5*d[i]-x[j]));
 			psi[i][j] = exp(2*w*(x[i]+0.5*d[i]-x[j]-d[j]));
 		}
 	}
 	
 	for (r=0; r < m; r++) {
 		q = up->q[r];
 		b = up->b[r];
 		
 		//printf("\nr = %d\n", r);
 		
	 	/* (2): ksi, h */
	 	for (i=0; i < n; i++) {
	 		for (j=0; j < n; j++) {
	 			ksi_1 = phi[i][j]/pow((1.0+phi[i][j]),2);
	 			ksi_2 = psi[i][j]/pow((1.0+psi[i][j]),2);
	 			ksi[i][j] = 2*w*q[j]*(ksi_1 - ksi_2);
	 			
	 			h_1 = 1.0/(1.0 + 1.0/phi[i][j]);
	  			h_2 = 1.0/(1.0 + 1.0/psi[i][j]);
	  			h[i][j] = h_1 - h_2;
	  		}
	 	}
	 	
	 	/* (3): g */
	 	for (i=0; i < n; i++) {
	 		g[i] = q[i];
	 		for (j=0; j < n; j++) {
	 			if (i == j)
	 				continue;
	 			g[i] += q[j] * h[i][j];
	 		}
	   		//printf("g[%d]=%.3f\n", i, g[i]);
	 	}
	 	
	 	/* (3): grad(g) */
	 	if (grad) {
	 		/* compute ggrad[i][i] */
	 		for (i=0; i < n; i++) {
	 			/* compute grad[i][i] */
	 			ggrad[i][i] = 0;
	 			for (j=0; j < n; j++) {
	 				if (j == i)
	 					continue;
	 				ggrad[i][i] += ksi[i][j];
	 			}
	 		}
	 		/* compute grad[i][j], i!=j */
	 		for (i=0; i < n; i++) {
	 			for (j=0; j < n; j++) {
	 				if (j == i)
	 					continue;
	 				ggrad[i][j] = -ksi[i][j];
	 			}
	 		}
	 		
	 		//for (i=0; i < n; i++) {
	 		//	printf("ggrad[%d]: ", i);
	 		//	for (j=0; j < n; j++)
	 		//		printf("%-2.3f ", ggrad[i][j]);
	 		//	puts(""); 
	 		//}
 	 	}
 	 	
 	 	/* (4): theta(x), thetanorm */
 	 	gmax = g[0];
 	 	for (i=0; i < n; i++)
 	 		if (g[i] > gmax)
 	 			gmax = g[i];
 	 	
 	 	//printf("g[r=%d][] = ", r);
 	 	//for (i=0; i < n; i++)
 	 	//	printf("%-2.3f ", g[i]);
 	 	//puts(""); 
 	 			
 	 	thetanorm = 0.0;
 	 	for (i=0; i < n; i++) {
 	 		theta[i] = exp(z*(g[i] - gmax));
 	 		thetanorm += theta[i];
 	 	}
 	 	
 	 	/* (5): u[r] */
  	 	u[r] = (log(thetanorm) + z*gmax)/z - b;
  	 	res[r] = u[r];
  	 	//printf("u = %.3f (%.3f)\n", u[r] + b, gmax);
  	 	
  	 	/* (5): ugrad[r] */
  	 	if (grad) {
	  	 	//printf("ugrad = ");
	  	 	for (i=0; i < n; i++) {
	  	 		ugrad[r][i] = 0;
	  	 		for (j=0; j < n; j++)
	  	 			ugrad[r][i] += ggrad[j][i]*theta[j];
	  	 		
	  	 		ugrad[r][i] /= thetanorm;
	  	 		//printf("%-2.3f ", ugrad[r][i]);
	  	 		grad[r*n+i]=ugrad[r][i];
	  	 	}
	  	 	//puts("");
	  	 }
	 }

}

double
f(unsigned n, const double *x, double *grad, void *data)
{
	if (grad) {
		memset(grad, 0, n*sizeof(double));
		grad[n-1] = 1;
	}
	return x[n-1]+d[n-1];
}

double
p(unsigned n, const double *x, double *grad, void *data)
{
	pp_t *pp = (pp_t*)data;
	 
	if (grad) {
		memset(grad, 0, n*sizeof(double));
		grad[pp->i] = 1.0;
		grad[pp->j] = -1.0;
	}
	
	return x[pp->i]+d[pp->i] - x[pp->j];
}

int
main(void)
{
	/* variables stuff */
	static int n;
	double *x;
	double *lb;
	
	/* constraints stuff */
	int m;
	double *utol;
	up_t up;
	pp_t **pp;
	
	/* other stuff */
	int i, j, r;
	nlopt_opt opt, lopt;
	nlopt_algorithm alg;
	double minf;
 	
	/* read n */
	scanf("%d %d", &n, &m);
 
	/* allocate variables, parameters */
	assert((x = calloc(sizeof(double), n)));
	assert((lb = calloc(sizeof(double), n)));
	assert((pp = calloc(sizeof(pp_t*), n)));
	for (i=0; i < n; i++)
		assert((pp[i] = calloc(sizeof(pp_t), n)));
	
	assert((utol = calloc(sizeof(double), m)));
	assert((d = calloc(sizeof(double), n)));
	assert((up.b = calloc(sizeof(double), m)));
	assert((up.q = calloc(sizeof(double*), m)));
	for (r=0; r < m; r++)
		assert((up.q[r] = calloc(sizeof(double), n)));	

	/* read capacities */
	//printf("b[] = ");
	for (r=0; r < m; r++) {
		scanf("%lf", &up.b[r]);
		//printf("%.0f ", up.b[r]);
	}
	//puts("");
	
	/* read durations, arcs, requests, start-times */
	for (i=0; i < n; i++) {
		scanf("%lf: ", &d[i]);
		//printf("d[%d]=%.0f\n", i, d[i]);
		//printf("suc[%d]=", i);
		for (j=0; j < n; j++) {
			pp[i][j].i = i;
			pp[i][j].j = j;
			scanf("%d", &pp[i][j].on);
			//printf("%d ", pp[i][j].on);
 		}
 		//puts("");
 		
 		//printf("q[%d]=", i);
 		for (r=0; r < m; r++) {
 			scanf("%lf", &up.q[r][i]);
 			//printf("%.0lf ", up.q[r][i]);
 		}
 		//puts("");
	}

	/* allocate computations stuff */
	assert((phi = calloc(sizeof(double*), n)));
	assert((psi = calloc(sizeof(double*), n)));
	assert((ksi = calloc(sizeof(double*), n)));
	assert((h = calloc(sizeof(double*), n)));
	assert((g = calloc(sizeof(double), n)));
	assert((ggrad = calloc(sizeof(double*), n)));
	assert((theta = calloc(sizeof(double), n)));
	assert((u = calloc(sizeof(double), m)));
	assert((ugrad = calloc(sizeof(double*), m)));
	for (r=0; r < m; r++)
		assert((ugrad[r] = calloc(sizeof(double), n)));

	for (i=0; i < n; i++) {
		assert((phi[i] = calloc(sizeof(double), n)));
		assert((psi[i] = calloc(sizeof(double), n)));
		assert((ksi[i] = calloc(sizeof(double), n)));
		assert((h[i] = calloc(sizeof(double), n)));
		assert((ggrad[i] = calloc(sizeof(double), n)));
	}
	
	/* create lopt object */
	lopt = nlopt_create(NLOPT_LD_LBFGS, n);
	nlopt_set_ftol_rel(lopt, 1e-2);
	
	/* create opt object */
	alg = NLOPT_LD_SLSQP;
	alg = NLOPT_LD_CCSAQ;
	alg = NLOPT_LD_MMA;
	alg = NLOPT_AUGLAG;
	
	opt = nlopt_create(alg, n);
	//printf("%s\n", nlopt_algorithm_name(alg));
	
	/* set objective */
	nlopt_set_min_objective(opt, f, NULL);
	nlopt_set_ftol_rel(opt, 1e-2);
	nlopt_set_local_optimizer(opt, lopt);
	
	/* set bounds */
	nlopt_set_lower_bounds(opt, lb);
	
	/* set nonlinear constraints */
	for (r=0; r < m; r++)
		utol[r] = 1e-2;
	nlopt_add_inequality_mconstraint(opt, m, make_u, &up, utol);
	
	/* set linear constraints */
	for (i=0; i < n; i++) {
		for (j=0; j < n; j++) {
			if (pp[i][j].on) {
				//printf("constraint x[%d]+d[%d] <= x[%d]\n", i, i, j);
 				nlopt_add_inequality_constraint(opt, p, &pp[i][j], 1e-2);
 			}
		}
	}

 	do {
  		for (i=0; i < n; i++)
 			if(scanf("%lf", &x[i]) == EOF)
 				break;
  		
  		printf("x0=");
  		for (i=0; i < n; i++)
  			printf("%-2.1lf ", x[i]);
  		puts("");
  		
		/* find solution */
		printf("x =");
		if (nlopt_optimize(opt, x, &minf) < 0) 
			printf("failure");
		else
			for (i=0; i < n; i++) 
				printf("%-2.1lf ", x[i]);
		puts("\n");	
		
 	} while(!feof(stdin));

	for (r=0; r < m; r++)
		printf("u[%d]=%.1lf\n", r, u[r]);

	free(x);
	return 0;
}