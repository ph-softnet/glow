#ifndef LOGSUMEXP_H
#define LOGSUMEXP_H

double flogsumexp(const double* __restrict__ buf, int N);

#endif // LOGSUMEXP_H
