#reset;

#
# model & data
#

param n integer >= 1;
param k integer >= 0;
param alpha >= 0;

# precedence constraints
set TE within {1..n, 1..n};
set E within TE;
set Ec := {(i,j) in {1..n, 1..n}: (i,j) not in E and i < j};

# resource constraints
param b {1..k};
param q {1..n, 1..k};

# durations
param samsize >= 1;
param d {1..n, 1..samsize} >= 0;

# earliest and latest starts
param es {1..n};
param ls {1..n};

# variables
var est {1..n} >= 0;
var lst {1..n} >= 0;
var tf;

maximize totfloat:
	tf;

subject to tf_definition:
	tf = sum {i in 1..n} (lst[i] - est[i]);

subject to est_definition {(i,j) in E}:
	est[j] >= est[i] + d[i,1];

subject to lst_definition {(i,j) in E}:
	lst[j] >= lst[i] + d[i,1];

subject to cmax:
	lst[n] <= es[n];
