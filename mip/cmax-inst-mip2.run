#reset;

#
# model & data
#

param n integer >= 1;
param k integer >= 0;

# precedence constraints
set TE within {1..n, 1..n};
set E within TE;
set TEc := {(i,j) in {1..n, 1..n}: (i,j) not in TE and (j,i) not in TE};

# resource constraints
param b {1..k};
param q {1..n, 1..k};

# durations
param samsize >= 1;
param d {1..n, 1..samsize} >= 0;

# earliest and latest starts
param es {1..n};
param ls {1..n};
param alpha >= 0;

#
# variables
#

#var x {i in 1..n, j in 1..n: (i,j) in TEc} binary;
var x {i in 1..n, j in 1..n: (i,j) in TEc} >= 0;
var f {i in 1..n, j in 1..n, r in 1..k} >= 0;
var s {i in 1..n, 1..samsize} >= 0;
var t {i in 1..n} >= 0;
var cmax {1..samsize};
var expcmax;
var expinst_task {1..n};
var expinst;

#
# constraints
#

minimize makespan: 
	alpha*expcmax + (1-alpha)*expinst;

subject to expinst_definition:
	expinst = sum {i in 1..n} expinst_task[i];

subject to expinst_task_definition {i in 1..n}:
	expinst_task[i] = (sum {g in 1..samsize} (s[i,g] - t[i]))/samsize;

subject to expcmax_definition:
	expcmax = (sum {g in 1..samsize} cmax[g])/samsize;

subject to cmax_definition {i in 1..n, g in 1..samsize}:
	cmax[g] >= s[i,g] + d[i,g];

subject to fij_TE {(i,j) in TE, r in 1..k}:
	f[i,j,r] <= min(q[i,r], q[j,r]);

subject to fij_TEinv {(i,j) in TE, r in 1..k}:
	f[j,i,r] = 0;

subject to fij_TEc {(i,j) in TEc, r in 1..k}:
	f[i,j,r] <= min(q[i,r], q[j,r]) * x[i,j];

subject to precedence_1 {(i,j) in E, g in 1..samsize}:
	s[j,g] >= s[i,g] + d[i,g];

subject to precedence_2 {(i,j) in TEc, g in 1..samsize}:
	s[j,g] >= s[i,g] + d[i,g] + (x[i,j]-1)*ls[n];

subject to precedence_3 {i in 1..n, g in 1..samsize}:
	s[i,g] >= t[i];

subject to flow_into_1 {r in 1..k}:
	sum {i in 1..n} f[i,1,r] = 0;

subject to flow_outof_n {r in 1..k}:
	sum {i in 1..n} f[n,i,r] = 0;

subject to flow_into_i {i in 2..n, r in 1..k}:
	sum {j in 1..n-1} f[j,i,r] = q[i,r];

subject to flow_outof_i {i in 1..n-1, r in 1..k}:
	sum {j in 2..n} f[i,j,r] = q[i,r];

subject to relax {(i,j) in TEc}:
	x[i,j] <= 1;

