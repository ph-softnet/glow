package in5000.sgs;

import in5000.sgs.alg.AdaptiveGen;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

public class Solver
{
	public static void main(String[] args)
	{
		if (args.length > 0)
		{
			Instance lInstance = null;

			// Read the instance.
			try
			{
				File lFile = new File(args[0]);
				//lInstance = KolischParser.parseKolischFile(lFile);
				lInstance = MichelTrainParser.parseFile(lFile);
 			}
			catch (FileNotFoundException lException)
			{
				System.err.println(String.format("File '%s' not found.", args[0]));
			}
			catch (Exception lException)
			{
				System.err.println("Parse error. File not in ProGen format.");
			}

			// Solve the instance.
			if (lInstance != null)
			{
				int lRuns = 1000;

				// Read the number of scheduling passes.
				if (args.length > 1)
				{
					try
					{
						lRuns = Integer.valueOf(args[1]);
					}
					catch (NumberFormatException lException)
					{
						System.err.println("Second argument not a number of schedules to solve.");
					}
				}

				// Solve the instance
				Map<Activity, Integer> lSchedule = AdaptiveGen.solve(lInstance, lRuns);

				// Parse the schedule.
				boolean lForward = true;
				int lMakespan    = 0;
				if (lSchedule.get(lInstance.getActivity(1)) != 0)
				{
					lForward  = false;
					lMakespan = lSchedule.get(lInstance.getActivity(1)) 
								+ lInstance.getActivity(1).getDuration();
				}

				for (int i = 1; i <= lInstance.getNumActivities() + 2; i++)
				{
					int lActivity  = lInstance.getActivity(i).getID();
					int lStartTime = lSchedule.get(lInstance.getActivity(i));

					if (lForward == false)
						lStartTime = lMakespan - 
									 (lStartTime + lInstance.getActivity(i).getDuration());

					System.out.println(String.format("%d\t%d", lActivity, lStartTime));
				}
			}
		}
		else
		{
			System.err.println("No input file specified.");
		}
	}
}
