package in5000.sgs.test;

import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.File;

public class OrderStrengthCounter
{
	public static void main(String[] args) throws Exception 
	{
		int[] lOScount = new int[100];

		for (int j = 1; j <= 21; j++)
		{
			int k = j * 3;
			if (j > 10)
				k = 30 + (j-10) * 6;

			for (int i = 1; i <= 1000; i++)
			{
				Instance lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+k+"-"+i+".rcp"));
	
				lOScount[(int)(lInst.getOrderStrength()*(lOScount.length-1))]++;
	
				if (i % 100 == 0)
					System.out.println(k+"-"+i);
			}
		}

		for (int i = 0; i < lOScount.length; i++)
		{
			System.out.println(i + " - " + lOScount[i]);
		}
	}
}
