package in5000.sgs.test;

import in5000.sgs.alg.SerialGen;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class VerifySchedule
{
	public static boolean verify(Instance pInstance, Map<Activity, Integer> pSchedule)
	{
		int lNumActivities		= pInstance.getNumActivities() + 2;
		int lNumResources		= pInstance.getNumResources();
		Activity lSuperSource	= pInstance.getActivity(1);
		Activity lSuperSink		= pInstance.getActivity(lNumActivities);

		if (lSuperSource.getInDegree() != 0)
			System.err.println("Dummy Source not found!");

		if (lSuperSink.getOutDegree() != 0)
			System.err.println("Dummy Sink not found!");

		int lTimeZero = pSchedule.get(lSuperSource);
		int lMakespan = pSchedule.get(lSuperSink);

		if (lTimeZero < 0)
			System.err.println("Schedule starts before zero!");
		
		int lCurrentTime	= lTimeZero;
		int[] lResourceUse	= new int[pInstance.getNumResources()];
		boolean[] lStarted  = new boolean[lNumActivities];
		boolean[] lFinished = new boolean[lNumActivities];
		lFinished[lSuperSource.getID()] = true;

		while (lCurrentTime <= lMakespan)
		{
			for (int i = 2; i < lNumActivities; i++)
			{
				Activity lActivity = pInstance.getActivity(i);
				int lEnd		   = pSchedule.get(lActivity) + lActivity.getDuration();

				if (lEnd > lMakespan)
					System.err.println("Activity " + i + " ends after the schedule does.");

				if (lCurrentTime == lEnd)
				{
					lFinished[i] = true;

					for (int j = 0; j < lNumResources; j++)
					{
						if (lActivity.usesResource(pInstance.getResource(j)))
							lResourceUse[j] -= lActivity.getResourceUse(pInstance.getResource(j));
					}
				}
			}

			for (int i = 2; i < lNumActivities; i++)
			{
				Activity lActivity = pInstance.getActivity(i);
				int lStart		   = pSchedule.get(lActivity);

				if (lStart < lTimeZero)
					System.err.println("Activity " + i + " starts before the schedule does.");

				if (lCurrentTime == lStart)
				{
					lStarted[i] = true;

					for (Activity lPredecessor : lActivity.getPredecessors())
					{
						if (!lFinished[lPredecessor.getID()])
							System.err.println("Starting Activity " + i + " before " + lPredecessor.getID() + " finishes!");
					}
					for (int j = 0; j < lNumResources; j++)
					{
						if (lActivity.usesResource(pInstance.getResource(j)))
							lResourceUse[j] += lActivity.getResourceUse(pInstance.getResource(j));
					}
				}
			}

			for (int j = 0; j < lNumResources; j++)
			{
				if (lResourceUse[j] > pInstance.getResource(j).getCapacity())
				{
					System.err.println("Overusage of Resource " + j + " at time " + lCurrentTime);
				}
			}

			lCurrentTime++;
		}

		return true;
	}

	public static void main(String[] args) throws IOException
	{
		SerialGen lGen						= new SerialGen();
		Instance lInst						= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+20+"_"+4+".sm"));
		Map<Activity, Integer> lSchedule	= lGen.solve(lInst, new RandomRule());

		System.out.println(VerifySchedule.verify(lInst, lSchedule));
	}
}
