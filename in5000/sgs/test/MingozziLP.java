package in5000.sgs.test;

import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBModel;

public class MingozziLP
{
	public static void main(String[] args)
	{
		/*
		Instance lInst = null;
		
		try
		{
			lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(10*3)+"-"+326+".rcp"));
		}
		catch (IOException e) { }
		/**/
		
		try
		{
			GRBEnv    env   = new GRBEnv("GRB_temp.log");
			GRBModel  model = new GRBModel(env);

			model.update();

			model.optimize();

			System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

			model.dispose();
			env.dispose();
		}
		catch (GRBException e) { }
	}
}
