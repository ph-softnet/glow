package in5000.sgs.test;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

public class TestScheduleDirection
{
	public static void main(String[] args)
	{
		Instance lInstance	 = new Instance();
		Activity[] lActivity = new Activity[6];

		for (int i = 0; i < lActivity.length; i++)
		{
			lActivity[i] = lInstance.addActivity(i+1);
			lActivity[i].setDuration(1);
		}

		lInstance.addPrecedenceConstraint(lActivity[0], lActivity[1]);
		lInstance.addPrecedenceConstraint(lActivity[0], lActivity[4]);
		lInstance.addPrecedenceConstraint(lActivity[1], lActivity[2]);
		lInstance.addPrecedenceConstraint(lActivity[2], lActivity[3]);
		lInstance.addPrecedenceConstraint(lActivity[4], lActivity[3]);
		lInstance.addPrecedenceConstraint(lActivity[3], lActivity[5]);

		lInstance.initialize();

		System.out.println(lInstance.getDirection());
		System.out.println(lInstance.getDummySource().getID() + "\t" +  lInstance.getDummySource().getOutDegree());
		System.out.println(lInstance.getDummySink().getID()   + "\t" +  lInstance.getDummySink().getInDegree());
		for (int i = 1; i <= lActivity.length; i++)
			System.out.println("\t" + lInstance.getActivity(i).getID() + "\t" + lInstance.getActivity(i).getEST()
					+ "\t" + lInstance.getActivity(i).getProgressiveLevel() + "\t" + lInstance.getActivity(i).getRegressiveLevel()
					+ "\t" + lInstance.getActivity(i).computeTransitiveSuccessors().size());

		lInstance.flipDirection();
		lInstance.initialize();

		System.out.println(lInstance.getDirection());
		System.out.println(lInstance.getDummySource().getID() + "\t" +  lInstance.getDummySource().getOutDegree());
		System.out.println(lInstance.getDummySink().getID()   + "\t" +  lInstance.getDummySink().getInDegree());
		for (int i = 1; i <= lActivity.length; i++)
			System.out.println("\t" + lInstance.getActivity(i).getID() + "\t" + lInstance.getActivity(i).getEST()
					+ "\t" + lInstance.getActivity(i).getProgressiveLevel() + "\t" + lInstance.getActivity(i).getRegressiveLevel()
					+ "\t" + lInstance.getActivity(i).computeTransitiveSuccessors().size());
	}
}
