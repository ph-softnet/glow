package in5000.sgs.alg;

import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.GivenPriorityRule;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bootstrapper
{
	public Map<Activity, Integer> solve(Instance pInstance, PriorityRule pRule)
	{
		SerialGen lSerGen = new SerialGen();

		Activity lSink = pInstance.getActivity(pInstance.getNumActivities()+2);
		List<Map<Activity, Integer>> lBestList = new ArrayList<Map<Activity, Integer>>();
		Map<Activity, Integer> lWorstSchedule  = null;
		int lWorstMakespan = -1;
		for (int i = 0; i < 100; i++)
		{
			Map<Activity, Integer> lBootstrapSchedule = ForwardBackwardIteration.performOne(pInstance, lSerGen.solve(pInstance, pRule));
			int lMakespan = lBootstrapSchedule.get(lSink);
			if (lBestList.size() < 10)
			{
				lBestList.add(lBootstrapSchedule);
			}
			else if (lMakespan < lWorstMakespan)
			{
				lBestList.remove(lWorstSchedule);
				lBestList.add(lBootstrapSchedule);
			}

			lWorstSchedule = null;
			lWorstMakespan = -1;
			for (int j = 0; j < lBestList.size(); j++)
			{
				if (lBestList.get(j).get(lSink) > lWorstMakespan)
				{
					lWorstSchedule = lBestList.get(j);
					lWorstMakespan = lWorstSchedule.get(lSink);
				}
			}
		}

		Map<Activity, Integer> lAvgStartTime = new HashMap<Activity, Integer>();
		for (int i = 1; i <= pInstance.getNumActivities()+2; i++)
		{
			Activity lAct = pInstance.getActivity(i);
			int lStartSum = 0;
			for (int j = 0; j < lBestList.size(); j++)
			{
				lStartSum += lBestList.get(j).get(lAct);
			}
			lAvgStartTime.put(lAct, lStartSum);
		}

		Map<Activity, Integer> lBestSchedule  = null;
		int lBestMakespan = Integer.MAX_VALUE;
		for (int i = 0; i < 400; i++)
		{
			PriorityRule lRule = new BiasedRandomRule(new GivenPriorityRule(lAvgStartTime), 10);
			Map<Activity, Integer> lSolutionSchedule = ForwardBackwardIteration.performOne(pInstance, lSerGen.solve(pInstance, lRule));
			int lMakespan = lSolutionSchedule.get(lSink);

			if (lMakespan < lBestMakespan)
			{
				lBestSchedule = lSolutionSchedule;
				lBestMakespan = lMakespan;
			}
		}

		return lBestSchedule;
	}

	public static void main(String[] args) throws IOException
	{
		SerialGen	 lSerGen	= new SerialGen();
		//ParallelGen	 lParGen	= new ParallelGen();

		int tries	   = 500;
		int lBootBetter = 0;
		int lRandBetter = 0;
		int lBootBetterFB = 0;
		int lRandBetterFB = 0;
		double lAvgDistBoot = 0;
		double lAvgDistRand = 0;
		double lAvgDistBootFB = 0;
		double lAvgDistRandFB = 0;
		for (int i = 1; i <= 60; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				Activity lSink = lInst.getActivity(lInst.getNumActivities()+2);
				PriorityRule lRule		= new BiasedRandomRule(new LatestFinishTime(lInst, false), 10);
//				lRule		= new MaxBiasedRandomRule(new ESTanalysis(lInst));

				int lCritBound    = lInst.computeLowerBound0();
				int lBestBoot     = Integer.MAX_VALUE;	
				int lBestSerial	  = Integer.MAX_VALUE;
				int lBestBootFB   = Integer.MAX_VALUE;	
				int lBestSerialFB = Integer.MAX_VALUE;

				List<Map<Activity, Integer>> lBestList   = new ArrayList<Map<Activity, Integer>>();
				Map<Activity, Integer> lWorstSchedule   = null;
				int lWorstMakespan   = -1;

				PriorityRule lRule2	= null;
				for (int k = 0; k < tries; k++)
				{
					Map<Activity, Integer> lLFT		= lSerGen.solve(lInst, lRule);
					Map<Activity, Integer> lLFTFBI	= ForwardBackwardIteration.performOne(lInst, lLFT);

					lBestSerial   = Math.min(lBestSerial,   lLFT.get(lSink));
					lBestSerialFB = Math.min(lBestSerialFB, lLFTFBI.get(lSink));

					if (lRule2 != null)
					{
						Map<Activity, Integer> lRPR		= lSerGen.solve(lInst, lRule2);
						Map<Activity, Integer> lRPRFBI	= ForwardBackwardIteration.performOne(lInst, lRPR);
						lBestBoot	  = Math.min(lBestBoot,     lRPR.get(lSink));
						lBestBootFB	  = Math.min(lBestBootFB,   lRPRFBI.get(lSink));
					}
					else
					{
						lBestBoot	  = lBestSerial;
						lBestBootFB	  = lBestSerialFB;
					}

					int lMakespan = lLFTFBI.get(lSink);
					if (lBestList.size() < 10)
					{
						lBestList.add(lLFTFBI);
					}
					else if (lMakespan < lWorstMakespan)
					{
						lBestList.remove(lWorstSchedule);
						lBestList.add(lLFTFBI);
					}

					lWorstSchedule = null;
					lWorstMakespan = -1;
					for (int l = 0; l < lBestList.size(); l++)
					{
						if (lBestList.get(l).get(lSink) > lWorstMakespan)
						{
							lWorstSchedule = lBestList.get(l);
							lWorstMakespan = lWorstSchedule.get(lSink);
						}
					}

					if (k == 150)
					{
						Map<Activity, Integer> lAvgStartTime = new HashMap<Activity, Integer>();
						for (int l = 1; l <= lInst.getNumActivities()+2; l++)
						{
							Activity lAct = lInst.getActivity(l);
							int lStartSum = 0;
							for (int m = 0; m < lBestList.size(); m++)
							{
								lStartSum += lBestList.get(m).get(lAct);
							}
							lAvgStartTime.put(lAct, lStartSum);
						}

						lRule2 = new BiasedRandomRule(new GivenPriorityRule(lAvgStartTime), 10);
					}
				}

				lAvgDistBoot += (double)(lBestBoot - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistRand += (double)(lBestSerial  - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistBootFB += (double)(lBestBootFB - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistRandFB += (double)(lBestSerialFB  - lCritBound) / (double)lCritBound * 100.0;

				if (lBestBoot < lBestSerial)
					lBootBetter++;
				else if (lBestBoot > lBestSerial)
					lRandBetter++;

				if (lBestBootFB < lBestSerialFB)
					lBootBetterFB++;
				else if (lBestBootFB > lBestSerialFB)
					lRandBetterFB++;
			}
			System.out.println(i + "\t" + lBootBetter + "\t" + lRandBetter + "\t" + (lAvgDistBoot / (i*10)) + "\t" + (lAvgDistRand / (i*10))
								 + "\t" + lBootBetterFB + "\t" + lRandBetterFB + "\t" + (lAvgDistBootFB / (i*10)) + "\t" + (lAvgDistRandFB / (i*10)));
		}

		/**
		PriorityRule lRule		= new BiasedRandomRule(new LatestFinishTime());
		Bootstrapper lBootGen	= new Bootstrapper();
		SerialGen	 lSerGen	= new SerialGen();

		int tries	   = 500;
		int lBootBetter = 0;
		int lRandBetter = 0;
		double lAvgDistBoot = 0;
		double lAvgDistRand = 0;
		for (int i = 1; i <= 60; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst		= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));

				int lCritBound = lInst.computeLowerBound0();
				int lBestBoot = lBootGen.solve(lInst, lRule).get(lInst.getActivity(122));	

				int lBestSerial		= Integer.MAX_VALUE;
				for (int k = 0; k < tries; k++)
				{
					Map<Activity, Integer> lSerSchedule = ForwardBackwardIteration.performOne(lInst, lSerGen.solve(lInst, lRule));
					lBestSerial		= Math.min(lBestSerial,   lSerSchedule.get(lInst.getActivity(122)));
				}

				lAvgDistBoot += (double)(lBestBoot   - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistRand += (double)(lBestSerial - lCritBound) / (double)lCritBound * 100.0;

				if (lBestBoot < lBestSerial)
					lBootBetter++;
				else if (lBestBoot > lBestSerial)
					lRandBetter++;
			}
			System.out.println(i + "\t" + lBootBetter + "\t" + lRandBetter + "\t" + (lAvgDistBoot / (i*10)) + "\t" + (lAvgDistRand / (i*10)));
		}
		*/

		/*
		SerialGen lSerGen = new SerialGen();
		Instance lInst		= PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(10*3)+"-"+25+".rcp"));
		Activity lSink = lInst.getActivity(lInst.getNumActivities()+2);
		Map<Activity, Integer> lBestSchedule  = null;
		int lBestMakespan = Integer.MAX_VALUE;
		for (int i = 0; i < 2500; i++)
		{
			PriorityRule lRule = new BiasedRandomRule(new LatestFinishTime());
			Map<Activity, Integer> lSolutionSchedule = ForwardBackwardIteration.performAll(lInst, lSerGen.solve(lInst, lRule), 10);
			int lMakespan = lSolutionSchedule.get(lSink);

			if (lMakespan < lBestMakespan)
			{
				lBestSchedule = lSolutionSchedule;
				lBestMakespan = lMakespan;
			}
		}
		
		System.out.println(lBestSchedule.get(lSink));

		Bootstrapper lBoot	= new Bootstrapper();

		System.out.println(lBoot.solve(lInst, new RandomRule()).get(lSink));
		*/
		
	}
}
