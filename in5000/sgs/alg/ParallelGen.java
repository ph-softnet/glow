package in5000.sgs.alg;

import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ParallelGen implements GenerationScheme
{
	public Map<Activity, Integer> solve(Instance pInstance, PriorityRule pRule)
	{
		//int lNumScheduled								= 0;
		List<Activity>			 lEligible				= new LinkedList<Activity>();
		Map<Activity, Integer>	 lPredStarted			= new HashMap<Activity, Integer>();
		TimePoint				 lTnow					= new TimePoint(pInstance, 0);
		Map<Activity, TimePoint> lEarliestTime			= new HashMap<Activity, TimePoint>();
		Map<Activity, Integer>	 lSchedule				= new HashMap<Activity, Integer>();
		Map<TimePoint, List<Activity>> lBecomeEligible	= new HashMap<TimePoint, List<Activity>>();

		// Reset the priority rule state.
		pRule.resetPriority();

		/*
		 * Maintain a set of eligible tasks.
		 */
		Activity lRoot = pInstance.getDummySource();
		lBecomeEligible.put(lTnow, new ArrayList<Activity>());
		lBecomeEligible.get(lTnow).add(lRoot);

		do
		{
			for (Activity lNewEligible : lBecomeEligible.get(lTnow))
			{
				lEligible.add(lNewEligible);
			}

			List<Activity> lStartable = new ArrayList<Activity>();
			lStartable.addAll(lEligible);

			while (!lStartable.isEmpty())
			{
				Activity lHighestPriority = pRule.selectNext(lStartable);
				lStartable.remove(lHighestPriority);

				boolean lResourceFeasible = true;
				TimePoint lPreEndPoint = lTnow;
				TimePoint lEndPoint    = lTnow;
				while (lResourceFeasible && lEndPoint != null && lEndPoint.getTime() - lTnow.getTime() < lHighestPriority.getDuration())
				{
					lPreEndPoint		= lEndPoint;
					lEndPoint			= lEndPoint.getNextTimePoint();
					lResourceFeasible	= lPreEndPoint.resourceFeasible(lHighestPriority);
				}

				if (lResourceFeasible)
				{
					// Compute the end time for this task.
					int lEndTime = lTnow.getTime() + lHighestPriority.getDuration();
					if (lEndPoint == null || lEndPoint.getTime() != lEndTime)
					{
						lEndPoint = new TimePoint(lPreEndPoint, lEndTime);
						lBecomeEligible.put(lEndPoint, new ArrayList<Activity>());
					}

					// Schedule the startable task by claiming the resources for it.
					lEligible.remove(lHighestPriority);
					lSchedule.put(lHighestPriority, lTnow.getTime());
					pRule.activityScheduled(lHighestPriority, lSchedule.get(lHighestPriority));
					TimePoint lPointer = lTnow;
					while (lPointer != lEndPoint)
					{
						lPointer.claimResources(lHighestPriority);
						lPointer = lPointer.getNextTimePoint();
					}

					// Update successor startability.
					for (Activity lSuccessor : lHighestPriority.getSuccessors())
					{
						if (lPredStarted.containsKey(lSuccessor))
						{
							int lStarted = lPredStarted.get(lSuccessor) + 1;
							lPredStarted.put(lSuccessor, lStarted);

							if (lEarliestTime.get(lSuccessor).getTime() < lEndPoint.getTime())
								lEarliestTime.put(lSuccessor, lEndPoint);
						}
						else
						{
							lPredStarted.put(lSuccessor, 1);
							lEarliestTime.put(lSuccessor, lEndPoint);
						}

						if (lPredStarted.get(lSuccessor).equals(lSuccessor.getInDegree()))
						{
							lBecomeEligible.get(lEarliestTime.get(lSuccessor)).add(lSuccessor);
						}
					}
				}
			}

			if (lTnow.getTime() != 0 || lTnow.getNextTimePoint() != null)
				lTnow = lTnow.getNextTimePoint();
		}
		while (lTnow != null);

		/*
		 * Order the set of eligible tasks by their priority.
		 */

		/*
		 * Traverse the set of eligible tasks in the order of their priority,
		 * starting any that fit in the current time point.
		 */

		/*
		 * Move up to the next time point.
		 * At every next time point some new tasks may become eligible.
		 */
		return lSchedule;
	}

	public static void main(String[] args) throws IOException
	{
		PriorityRule lRule  = new RandomRule();
		ParallelGen lParGen = new ParallelGen();
		SerialGen   lSerGen = new SerialGen();

		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./j120_compu.dat")));

		int tries	   = 500;
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst		= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				long nanotime		= 0;
				long nanotime2		= 0;
				int lBestParallel	= Integer.MAX_VALUE;
				int lBestSerial		= Integer.MAX_VALUE;

				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness() + "\t");

				for (int k = 0; k < tries; k++)
				{
					nanotime -= System.nanoTime();
					Map<Activity, Integer> lParSchedule = lParGen.solve(lInst, lRule);
					nanotime += System.nanoTime();
	
					nanotime2 -= System.nanoTime();
					Map<Activity, Integer> lSerSchedule = lSerGen.solve(lInst, lRule);
					nanotime2 += System.nanoTime();
					
					lBestParallel	= Math.min(lBestParallel, lParSchedule.get(lInst.getActivity(122)));
					lBestSerial		= Math.min(lBestSerial,   lSerSchedule.get(lInst.getActivity(122)));
				}

				lOut.append(lBestParallel				+ "\t" + 
							lBestSerial					+ "\t" +
							nanotime  / (double)tries	+ "\t" +
							nanotime2 / (double)tries	+ "\n");

				System.out.println(String.format("%2d - %2d:\t%4d\t%4d\t%f\t%f", i, j, 
																				 lBestParallel,
																				 lBestSerial,
																				 nanotime  / 1000000.0 / tries,
																				 nanotime2 / 1000000.0 / tries));
			}
			//System.out.println((double)i / 48);
		}
		lOut.close();

		//System.out.println(nanotime / 1000000.0 / 1000 / 480);
	}

}
