package in5000.sgs.alg;

import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.alg.rules.RightJustify;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ForwardBackwardIteration
{
	public static Map<Activity, Integer> performAll(Instance pInstance, Map<Activity, Integer> pSchedule, int pMax)
	{
		Map<Activity, Integer> lResult = pSchedule;

		int lRun = 0;
		int lActivities = pInstance.getNumActivities() + 2;
		int lPrevMakespan = pSchedule.get(pInstance.getActivity(lActivities));
		int lNewMakespan  = pSchedule.get(pInstance.getActivity(lActivities));

		do
		{
			lResult = ForwardBackwardIteration.performOne(pInstance, lResult);

			lPrevMakespan = lNewMakespan;
			lNewMakespan  = lResult.get(pInstance.getActivity(lActivities));

			lRun++;
		}
		while (lNewMakespan < lPrevMakespan && lRun < pMax);

		return lResult;
	}

	/*
	public static Map<Activity, Integer> performOne(Instance pInstance, Map<Activity, Integer> pSchedule)
	{
		int lActivities = pInstance.getNumActivities() + 2;
		int lMakespan   = pSchedule.get(pInstance.getActivity(lActivities));

		//VerifySchedule.verify(pInstance, pSchedule);

		SerialGen lGen = new SerialGen();
		Map<Activity, Integer> lReversed = lGen.solveInverse(pInstance, new RightJustify(pSchedule));
		Map<Activity, Integer> lRightJustify = new HashMap<Activity, Integer>();
		for (Activity lActivity : lReversed.keySet())
		{
			int lReverse = lMakespan - (lReversed.get(lActivity) + lActivity.getDuration());
			lRightJustify.put(lActivity, lReverse);
		}

		//VerifySchedule.verify(pInstance, lRightJustify);

		Map<Activity, Integer> lLeftJustify  = lGen.solve(pInstance, new LeftJustify(lRightJustify));

		//VerifySchedule.verify(pInstance, lLeftJustify);

//		if (lLeftJustify.get(pInstance.getActivity(122)) > lRightJustify.get(pInstance.getActivity(122)))
//		{
//			for (int i = 1; i <= 122; i++)
//			{
//				Activity lActivity = pInstance.getActivity(i);
//				
//				System.out.println(i + "\t" + pSchedule.get(lActivity)		+ "\t"
//								+  lRightJustify.get(lActivity) + "\t" 
//								+  lLeftJustify.get(lActivity));
//			}
//			System.exit(0);
//		}

		return lLeftJustify;
	}
		/**/

	public static Map<Activity, Integer> performOne(Instance pInstance, Map<Activity, Integer> pSchedule)
	{
		SerialGen lGen = new SerialGen();

		PriorityRule lJustify = new RightJustify(pSchedule);
		pInstance.flipDirection();
		Map<Activity, Integer> lSchedule = lGen.solve(pInstance, lJustify);

		lJustify = new RightJustify(lSchedule);
		pInstance.flipDirection();
		lSchedule = lGen.solve(pInstance, lJustify);

		return lSchedule;
	}

	public static void main(String[] args) throws IOException
	{
		SerialGen lGen = new SerialGen();
		/*
		Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j30"+1+"_"+1+".sm"));
		Map<Activity, Integer> lSchedule = lGen.solve(lInst, new RandomRule());
		Map<Activity, Integer> lSchedule2 = lGen.solveInverse(lInst, new RightJustify(lSchedule));

		
		for (int i = 1; i <= lInst.getNumActivities()+2; i++)
		{
			Activity lActivity = lInst.getActivity(i);

			System.out.println(i + ": " + lSchedule.get(lActivity) + " " + lSchedule2.get(lActivity));
		}
		/**/

		
		long nanotime = 0;
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				nanotime -= System.nanoTime();
				Map<Activity, Integer> lSchedule = lGen.solve(lInst, new RandomRule());
				nanotime += System.nanoTime();

				//VerifySchedule.verify(lInst, lSchedule);

				nanotime -= System.nanoTime();
				Map<Activity, Integer> lSchedule2 = ForwardBackwardIteration.performOne(lInst, lSchedule);
				nanotime += System.nanoTime();

				System.out.print(i + " - " + j + ": " + lSchedule.get(lInst.getActivity(122)) + " " +  lSchedule2.get(lInst.getActivity(122)));

				nanotime -= System.nanoTime();
				Map<Activity, Integer> lSchedule3 = ForwardBackwardIteration.performAll(lInst, lSchedule, 100);
				nanotime += System.nanoTime();

				System.out.println(" " + lSchedule3.get(lInst.getActivity(122)) + " - " + (lSchedule2.get(lInst.getActivity(122)) - lSchedule3.get(lInst.getActivity(122))));
			}
			//System.out.println((double)i / 48);
		}

		System.out.println(nanotime / 1000000.0 / 480);
		/**/
	}
}
