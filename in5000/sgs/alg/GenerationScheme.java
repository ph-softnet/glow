package in5000.sgs.alg;

import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

import java.util.Map;

public interface GenerationScheme
{
	public Map<Activity, Integer> solve(Instance pInstance, PriorityRule pRule);
}
