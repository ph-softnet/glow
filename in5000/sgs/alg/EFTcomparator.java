package in5000.sgs.alg;

import in5000.sgs.data.InsertionActivity;

import java.util.Comparator;

public class EFTcomparator implements Comparator<InsertionActivity>
{
	@Override
	public int compare(InsertionActivity arg0, InsertionActivity arg1)
	{
		int lEFTdiff = arg0.getEFT() - arg1.getEFT();
		int lIDdiff  = arg0.getActivity().getID() - arg1.getActivity().getID();

		if (lEFTdiff == 0)
			return lIDdiff;

		return lEFTdiff;
	}
}
