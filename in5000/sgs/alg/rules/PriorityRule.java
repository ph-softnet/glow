package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.List;

public interface PriorityRule
{
	public Activity selectNext(List<Activity> pEligible);

	public int getPriority(Activity lEligible);
	
	public void resetPriority();

	public void activityScheduled(Activity pActivity, int pStartTime);
}
