package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.List;
import java.util.Map;

public class GivenPriorityRule implements PriorityRule
{
	private final Map<Activity, Integer> fGivenPriorities;

	public GivenPriorityRule(Map<Activity, Integer> pGivenPriorities)
	{
		this.fGivenPriorities = pGivenPriorities;
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		int lBestValue = Integer.MAX_VALUE;
		Activity lBest = null;

		for (Activity lEligible : pEligible)
		{
			if (this.fGivenPriorities.get(lEligible) < lBestValue)
			{
				lBestValue = this.fGivenPriorities.get(lEligible);
				lBest	   = lEligible;
			}
		}

		return lBest;
	}

	@Override
	public int getPriority(Activity pEligible)
	{
		return fGivenPriorities.get(pEligible);
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }
}
