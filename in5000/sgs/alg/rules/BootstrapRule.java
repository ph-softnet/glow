package in5000.sgs.alg.rules;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.ParallelGen;
import in5000.sgs.alg.SerialGen;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BootstrapRule implements PriorityRule
{
	private final Map<Activity, Double> fChains;

	public BootstrapRule(Instance pInstance, List<Map<Activity, Integer>> pSchedules, int pNumSelect)
	{
		this.fChains = new HashMap<Activity, Double>();

		/*
		 * 		Compute the list of selected (best pNumSelect) schedules.
		 */
		List<Map<Activity, Integer>> lSelectedSchedules = new ArrayList<Map<Activity,Integer>>();
		Map<Activity, Integer>		 lWorstSchedule		= null;
		int							 lWorstMakespan		= -1;

		Activity lSource = pInstance.getDummySource();
		Activity lSink   = pInstance.getDummySink();

		// Test every schedule.
		for (Map<Activity, Integer> lSchedule : pSchedules)
		{
			// Determine schedule makespan.
			int lMakespan = lSchedule.get(lSource);
			lMakespan     = Math.max(lMakespan, lSchedule.get(lSink));

			// If the list is full but this schedule is better than the worst selected schedule...
			if (lMakespan < lWorstMakespan && lSelectedSchedules.size() >= pNumSelect)
			{
				// Delete the worst.
				lSelectedSchedules.remove(lWorstSchedule);

				// Determine then new worst schedule.
				lWorstSchedule = null;
				lWorstMakespan = -1;
				for (Map<Activity, Integer> lGoodSchedule : lSelectedSchedules)
				{
					int lGoodMakespan = lGoodSchedule.get(lSource);
					lGoodMakespan     = Math.max(lGoodMakespan, lGoodSchedule.get(lSink));

					if (lGoodMakespan > lWorstMakespan)
					{
						lWorstMakespan = lGoodMakespan;
						lWorstSchedule = lGoodSchedule;
					}
				}
			}

			// If the list is (no longer) full, add the current schedule.
			if (lSelectedSchedules.size() < pNumSelect)
			{
				lSelectedSchedules.add(lSchedule);

				// And update the worst schedule should this one be it.
				if (lMakespan > lWorstMakespan)
				{
					lWorstMakespan = lMakespan;
					lWorstSchedule = lSchedule;
				}
			}
		}

		// Compute per activity its average start time.
		for (int i = 1; i <= pInstance.getNumActivities() + 2; i++)
		{
			Activity lActivity = pInstance.getActivity(i);
			int lTotalStart = 0;

			for (Map<Activity, Integer> lGoodSchedule : lSelectedSchedules)
			{
				lTotalStart += lGoodSchedule.get(lActivity);
			}

			this.fChains.put(lActivity, lTotalStart / (double)pNumSelect);
		}

		/*
		// Compute per activity its average delay compared to EST.
		Map<Activity, Double> lAverageDelay = new HashMap<Activity, Double>();
		for (int i = 1; i <= pInstance.getNumActivities() + 2; i++)
		{
			Activity lActivity = pInstance.getActivity(i);
			int lActEST     = lActivity.getEST();
			int lTotalDelay = 0;

			for (Map<Activity, Integer> lGoodSchedule : lSelectedSchedules)
			{
				int lRealDelay = lGoodSchedule.get(lActivity) - lActEST;
				lTotalDelay += lRealDelay;
			}

			lAverageDelay.put(lActivity, lTotalDelay / (double)pNumSelect);
		}

		// Propagate the estimated delays along the chains.
		pInstance.getDummySource().breadthFirstAccumulate(lAverageDelay, this.fChains);
		*/
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		int lMinStart	   = Integer.MAX_VALUE;
		Activity lSelected = null;

		for (Activity lEligible : pEligible)
		{
			if (this.getPriority(lEligible) < lMinStart)
			{
				lMinStart = this.getPriority(lEligible);
				lSelected = lEligible;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;

		/*
		double lMaxVal = -1;
		Activity lSelected = null;

		for (Activity lEligible : pEligible)
		{
			if (this.fChains.get(lEligible) > lMaxVal)
			{
				lMaxVal	  = this.fChains.get(lEligible);
				lSelected = lEligible;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;
		/**/
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return (int)Math.round(this.fChains.get(lEligible) * 1000);
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }

	public static void main(String[] args) throws IOException
	{
		SerialGen	 lSerGen	= new SerialGen();
		ParallelGen	 lParGen	= new ParallelGen();

		int tries	   = 1000;
		int lBootBetter = 0;
		int lRandBetter = 0;
		int lBootBetterFB = 0;
		int lRandBetterFB = 0;
		double lAvgDistBoot = 0;
		double lAvgDistRand = 0;
		double lAvgDistBootFB = 0;
		double lAvgDistRandFB = 0;
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j60"+i+"_"+j+".sm"));
				Activity lSink = lInst.getActivity(lInst.getNumActivities()+2);
				PriorityRule lRule		= new BiasedRandomRule(new LatestFinishTime(lInst, false), 10);
//				lRule		= new MaxBiasedRandomRule(new ESTanalysis(lInst));

				int lCritBound    = lInst.computeLowerBound0();
				int lBestBoot     = Integer.MAX_VALUE;	
				int lBestSerial	  = Integer.MAX_VALUE;
				int lBestBootFB   = Integer.MAX_VALUE;	
				int lBestSerialFB = Integer.MAX_VALUE;

				List<Map<Activity, Integer>> lSchedules = new ArrayList<Map<Activity, Integer>>();

				PriorityRule lRule2	= null;
				for (int k = 0; k < tries; k++)
				{
					Map<Activity, Integer> lLFT		= lSerGen.solve(lInst, lRule);
					Map<Activity, Integer> lLFTFBI	= ForwardBackwardIteration.performOne(lInst, lLFT);
					lSchedules.add(lLFTFBI);

					lBestSerial   = Math.min(lBestSerial,   lLFT.get(lSink));
					lBestSerialFB = Math.min(lBestSerialFB, lLFTFBI.get(lSink));

					if (lRule2 != null)
					{
						Map<Activity, Integer> lRPR		= lParGen.solve(lInst, lRule2);
						Map<Activity, Integer> lRPRFBI	= ForwardBackwardIteration.performOne(lInst, lRPR);
						lBestBoot	  = Math.min(lBestBoot,     lRPR.get(lSink));
						lBestBootFB	  = Math.min(lBestBootFB,   lRPRFBI.get(lSink));
					}
					else
					{
						lBestBoot	  = lBestSerial;
						lBestBootFB	  = lBestSerialFB;
					}

					if (k == 500)
					{
						lRule2 = new BiasedRandomRule(new BootstrapRule(lInst, lSchedules, 25), 10);
					}
				}

				lAvgDistBoot += (double)(lBestBoot - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistRand += (double)(lBestSerial  - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistBootFB += (double)(lBestBootFB - lCritBound) / (double)lCritBound * 100.0;
				lAvgDistRandFB += (double)(lBestSerialFB  - lCritBound) / (double)lCritBound * 100.0;

				if (lBestBoot < lBestSerial)
					lBootBetter++;
				else if (lBestBoot > lBestSerial)
					lRandBetter++;

				if (lBestBootFB < lBestSerialFB)
					lBootBetterFB++;
				else if (lBestBootFB > lBestSerialFB)
					lRandBetterFB++;
			}
			System.out.println(i + "\t" + lBootBetter + "\t" + lRandBetter + "\t" + (lAvgDistBoot / (i*10)) + "\t" + (lAvgDistRand / (i*10))
								 + "\t" + lBootBetterFB + "\t" + lRandBetterFB + "\t" + (lAvgDistBootFB / (i*10)) + "\t" + (lAvgDistRandFB / (i*10)));
		}
	}
}
