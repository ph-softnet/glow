package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.List;
import java.util.Random;

public class RandomRule implements PriorityRule
{
	private final Random lRanGen;

	public RandomRule()
	{
		lRanGen = new Random();
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		return pEligible.remove(lRanGen.nextInt(pEligible.size()));
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return 0;
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }
}
