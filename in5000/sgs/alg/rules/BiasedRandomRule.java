package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BiasedRandomRule implements PriorityRule
{
	private final Random fRanGen;

	private final double fAlpha;

	private PriorityRule fBasicRule;

	public BiasedRandomRule(PriorityRule pBasicRule, double pAlpha)
	{
		this.fRanGen	= new Random();
		this.fBasicRule = pBasicRule;
		this.fAlpha		= pAlpha;
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		int lMaxPriority = 0;
		for (Activity lActivity : pEligible)
		{
			int lMyPriority = this.fBasicRule.getPriority(lActivity);

			lMaxPriority = (lMaxPriority < lMyPriority) ? lMyPriority : lMaxPriority;
		}

		double[] lRegrets = new double[pEligible.size()];
		double lRegretSum = 0;
		for (int i = 0; i < pEligible.size(); i++)
		{
			int lRegret = lMaxPriority - this.fBasicRule.getPriority(pEligible.get(i)) + 1;
			lRegretSum += Math.pow(lRegret, fAlpha);
			lRegrets[i] = lRegretSum;
		}

		double[] lSelectProb  = new double[pEligible.size()];
		for (int i = 0; i < pEligible.size(); i++)
		{
			double lProbability = (double)lRegrets[i] / (double) lRegretSum;
			lSelectProb[i] = lProbability;
			//System.out.println(lSelectProb[i]);
		}

		Activity lSelected = null;
		double lRandom     = fRanGen.nextDouble();
		for (int i = 0; i < pEligible.size(); i++)
		{
			if (lRandom < lSelectProb[i])
			{
				lSelected = pEligible.get(i);
				break;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return 0;
	}

	@Override
	public void resetPriority()
	{
		this.fBasicRule.resetPriority();
	}

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime)
	{
		this.fBasicRule.activityScheduled(pActivity, pStartTime);
	}

	public static void main(String[] args) throws Exception
	{
		Instance lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+6+"-"+456+".rcp"));
		List<Activity> lFakeEligible = new ArrayList<Activity>();

		int[] lHist = new int[15];
		for (int j = 0; j < 100000; j++)
		{
			lFakeEligible = new ArrayList<Activity>();

			for (int i = 1; i <= lHist.length; i++)
			{
				Activity lSelected = lInst.getActivity(i);
				lFakeEligible.add(lSelected);
			}
	
			PriorityRule lLFT = new BiasedRandomRule(new LatestFinishTime(lInst, false), 10);
	
			Activity lSelected = lLFT.selectNext(lFakeEligible);
			lHist[lSelected.getID()-1]++;
		}

		lFakeEligible = new ArrayList<Activity>();
		for (int i = 1; i <= lHist.length; i++)
		{
			Activity lSelected = lInst.getActivity(i);
			lFakeEligible.add(lSelected);
		}

		PriorityRule lLFT = new LatestFinishTime(lInst, false);
		while (!lFakeEligible.isEmpty())
		{
			Activity lSelected = lLFT.selectNext(lFakeEligible);
			System.out.println(lSelected.getID() + "\t" + lSelected.getLFT() + "\t" + lHist[lSelected.getID()-1]);
		}
	}
}
