package in5000.sgs.alg.rules;

import in5000.sgs.alg.ParallelGen;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LatestFinishTime implements PriorityRule
{
	private boolean fDynamic;

	private final Instance fInstance;

	private final Map<Activity, Integer> fScheduledMap;

	private final Map<Activity, Integer> fPriorityMap;

	public LatestFinishTime(Instance pInstance, boolean pDynamic)
	{
		this.fInstance		= pInstance;
		this.fScheduledMap	= new HashMap<Activity, Integer>();
		this.fPriorityMap	= new HashMap<Activity, Integer>();
		this.fDynamic		= pDynamic;

		this.computePriority();
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		int lMinLFT		   = Integer.MAX_VALUE;
		Activity lSelected = null;

		for (Activity lEligible : pEligible)
		{
			if (this.getPriority(lEligible) < lMinLFT)
			{
				lMinLFT	  = this.getPriority(lEligible);
				lSelected = lEligible;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return this.fPriorityMap.get(lEligible);
	}

	public void resetPriority()
	{
		this.fScheduledMap.clear();

		this.computePriority();
	}

	public void activityScheduled(Activity pActivity, int pStartTime)
	{
		this.fScheduledMap.put(pActivity, pStartTime);

		if (this.fDynamic)
		{
			this.computePriority();
		}
	}

	private void computePriority()
	{
		Map<Activity, Integer> lESTschedule = new HashMap<Activity, Integer>();
		this.fPriorityMap.clear();

		for (Activity lActivity : this.fScheduledMap.keySet())
			lESTschedule.put(lActivity, this.fScheduledMap.get(lActivity));

		Activity lSource = this.fInstance.getDummySource();
		Activity lSink   = this.fInstance.getDummySink();

		this.computeESTschedule(lSource, 0, lESTschedule);
		int lSinkLFT = lESTschedule.get(lSink) + lSink.getDuration();
		this.computeLFTschedule(lSink, lSinkLFT, this.fPriorityMap);
	}

	private void computeESTschedule(Activity pSource, int pEST, Map<Activity, Integer> pESTmap)
	{
		int lMapEST = -1;

		if (pESTmap.containsKey(pSource))
			lMapEST = pESTmap.get(pSource);

		if (pEST > lMapEST)
		{
			pESTmap.put(pSource, pEST);
		}

		for (Activity lSuccessor : pSource.getSuccessors())
		{
			this.computeESTschedule(lSuccessor, pESTmap.get(pSource) + pSource.getDuration(), pESTmap);
		}
	}

	private void computeLFTschedule(Activity pSource, int pLFT, Map<Activity, Integer> pLFTmap)
	{
		int lMapLFT = Integer.MAX_VALUE;

		if (pLFTmap.containsKey(pSource))
			lMapLFT = pLFTmap.get(pSource);

		if (pLFT < lMapLFT)
		{
			pLFTmap.put(pSource, pLFT);
		}

		for (Activity lPredecessor : pSource.getPredecessors())
		{
			this.computeLFTschedule(lPredecessor, pLFTmap.get(pSource) - pSource.getDuration(), pLFTmap);
		}
	}

	public static void main(String[] args) throws IOException
	{
		ParallelGen lGen = new ParallelGen();
		for (int i = 1; i <= 60; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst		= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				Activity lSink		= lInst.getDummySink();
				PriorityRule lLFT	= new BiasedRandomRule(new LatestFinishTime(lInst, false), 10);
				PriorityRule lLFTd	= new BiasedRandomRule(new LatestFinishTime(lInst, true), 10);

				int lBestMakeLFT  = Integer.MAX_VALUE;
				int lBestMakeLFTd = Integer.MAX_VALUE;
				for (int k = 0; k < 500; k++)
				{
					int lMakeLFT	= lGen.solve(lInst, lLFT).get(lSink);
					int lMakeLFTd	= lGen.solve(lInst, lLFTd).get(lSink);

					lBestMakeLFT  = Math.min(lBestMakeLFT,  lMakeLFT);
					lBestMakeLFTd = Math.min(lBestMakeLFTd, lMakeLFTd);
				}

				System.out.println(i + "\t" + j + "\t" + lBestMakeLFT + "\t" + lBestMakeLFTd);
			}
		}
	}
}
