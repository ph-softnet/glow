package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.List;

public class LatestStartTime implements PriorityRule
{
	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		int lMinLST		   = Integer.MAX_VALUE;
		Activity lSelected = null;

		for (Activity lEligible : pEligible)
		{
			if (this.getPriority(lEligible) < lMinLST)
			{
				lMinLST	  = this.getPriority(lEligible);
				lSelected = lEligible;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity pEligible)
	{
		return pEligible.getLST();
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }
}
