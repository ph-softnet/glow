package in5000.sgs.alg.rules;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.SerialGen;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ESTanalysis implements PriorityRule
{
	private boolean fDynamic;

	private final Instance fInstance;

	private final int[][] fUsage;

	private final double fAvgTaskDuration;

	private final Map<Activity, Integer> fEST;

	private final Map<Activity, Double> fChains;

	public ESTanalysis(Instance pInstance, boolean pDynamic)
	{
		this.fDynamic   = pDynamic;
		this.fInstance	= pInstance;
		this.fEST		= new HashMap<Activity, Integer>();
		this.fChains	= new HashMap<Activity, Double>();

		/*
		 *		Compute the average task duration.
		 */
		int lTaskDurationSum = 0;		
		for (int i = 0; i < pInstance.getNumActivities(); i++)
			lTaskDurationSum += pInstance.getActivity(i+2).getDuration();

		this.fUsage		= new int[pInstance.getNumResources()][lTaskDurationSum];
		fAvgTaskDuration = (double)lTaskDurationSum / (double)pInstance.getNumActivities();

		this.resetPriority();
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		double lMaxVal = -1;
		Activity lSelected = null;

		for (Activity lEligible : pEligible)
		{
			if (this.fChains.get(lEligible) > lMaxVal)
			{
				lMaxVal	  = this.fChains.get(lEligible);
				lSelected = lEligible;
			}
		}

		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return (int)Math.round(this.fChains.get(lEligible) * 1000);
	}

	@Override
	public void resetPriority()
	{
		/*
		 *		Compute the per Resource usage profile based on an EST schedule. Kind of inefficient.
		 */
		this.fEST.clear();

		this.computeESTschedule(this.fInstance.getDummySource(), 0, this.fEST);

		this.computePriority();
	}

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime)
	{
		if (this.fDynamic)
		{
			this.computeESTschedule(pActivity, pStartTime, this.fEST);
	
			this.computePriority();
		}
	}

	private void computeESTschedule(Activity pSource, int pEST, Map<Activity, Integer> pESTmap)
	{
		int lMapEST = -1;

		if (pESTmap.containsKey(pSource))
			lMapEST = pESTmap.get(pSource);

		if (pEST > lMapEST)
		{
			pESTmap.put(pSource, pEST);
		}

		for (Activity lSuccessor : pSource.getSuccessors())
		{
			this.computeESTschedule(lSuccessor, pESTmap.get(pSource) + pSource.getDuration(), pESTmap);
		}
	}

	private void computePriority()
	{
		this.fChains.clear();
		int lSink	= this.fInstance.getNumActivities()+2;

		for (int i = 0; i < this.fUsage.length; i++)
			for (int j = 0; j < this.fUsage[0].length; j++)
				this.fUsage[i][j] = 0;

		for (int i = 1; i <= lSink; i++)
		{
			Activity lAct = this.fInstance.getActivity(i);

			for (int j = this.fEST.get(lAct); j < this.fEST.get(lAct) + lAct.getDuration(); j++)
			{
				for (int k = 0; k < this.fInstance.getNumResources(); k++)
				{
					this.fUsage[k][j] += lAct.getResourceUse(this.fInstance.getResource(k));
				}
			}
		}

		/*
		 *		Compute per activity how large its delay might be.
		 */
		Map<Activity, Double> lVirtualDelays = new HashMap<Activity, Double>();
		for (int i = 1; i <= lSink; i++)
		{
			double lMaxVirtualDelay = 0;
			Activity lAct = this.fInstance.getActivity(i);

			for (int k = 0; k < this.fInstance.getNumResources(); k++)
			{
				int lPeak = this.fUsage[k][this.fEST.get(lAct)];
				int lCap  = this.fInstance.getResource(k).getCapacity();
				int lUse  = lAct.getResourceUse(this.fInstance.getResource(k));
				
				double lResVirtualDelay = ((double)Math.max(0, lPeak - lCap) / (double)lCap) *
										  ((double)lUse                      / (double)lCap) *
										  fAvgTaskDuration;

				lMaxVirtualDelay = Math.max(lMaxVirtualDelay, lResVirtualDelay);
			}

			lVirtualDelays.put(lAct, lMaxVirtualDelay);
		}

		this.fInstance.getDummySource().breadthFirstAccumulate(lVirtualDelays, this.fChains);
	}

	public static void main(String[] args) throws IOException
	{
		SerialGen lSerGen = new SerialGen();

		int tries = 0;

		int lAlpha = 2;

		for (int set = 30; set <= 120; set = set + 30)
		{
			int lMaxSet = (set < 120) ? 48 : 60;

			int lSame		= 0;
			int lBootBetter = 0;
			int lRandBetter = 0;
			int lSameFB		= 0;
			int lBootBetterFB = 0;
			int lRandBetterFB = 0;
			double lAvgDistBoot = 0;
			double lAvgDistRand = 0;
			double lAvgDistBootFB = 0;
			double lAvgDistRandFB = 0;

			lAlpha = lAlpha + 2;
			tries += (set < 120) ? 100 : 200;

			for (int i = 1; i <= lMaxSet; i++)
			{
				for (int j = 1; j <= 10; j++)
				{
					Instance lInst		= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j"+set+""+i+"_"+j+".sm"));
					PriorityRule lRule	= new BiasedRandomRule(new LatestFinishTime(lInst, false), lAlpha);
//					PriorityRule lRule	= new MaxBiasedRandomRule(new ESTanalysis(lInst, true));
					PriorityRule lRule2	= new MaxBiasedRandomRule(new ESTanalysis(lInst, false), lAlpha);
//					PriorityRule lRule2	= new MaxBiasedRandomRule(new TotalSuccessors(lInst));
	
					int lCritBound     = lInst.computeLowerBound0();
					int lBestAnalyze   = Integer.MAX_VALUE;	
					int lBestSerial	   = Integer.MAX_VALUE;
					int lBestAnalyzeFB = Integer.MAX_VALUE;	
					int lBestSerialFB  = Integer.MAX_VALUE;
	
					for (int k = 0; k < tries; k++)
					{
						Map<Activity, Integer> lLFT		= lSerGen.solve(lInst, lRule);
						Map<Activity, Integer> lRPR		= lSerGen.solve(lInst, lRule2);
						Map<Activity, Integer> lLFTFBI	= ForwardBackwardIteration.performOne(lInst, lLFT);
						Map<Activity, Integer> lRPRFBI	= ForwardBackwardIteration.performOne(lInst, lRPR);
	
						lBestAnalyze	= Math.min(lBestAnalyze,  lRPR.get(lInst.getDummySink()));
						lBestSerial		= Math.min(lBestSerial,   lLFT.get(lInst.getDummySink()));
						lBestAnalyzeFB	= Math.min(lBestAnalyzeFB,lRPRFBI.get(lInst.getDummySink()));
						lBestSerialFB	= Math.min(lBestSerialFB, lLFTFBI.get(lInst.getDummySink()));
					}
	
					lAvgDistBoot += (double)(lBestAnalyze - lCritBound) / (double)lCritBound * 100.0;
					lAvgDistRand += (double)(lBestSerial  - lCritBound) / (double)lCritBound * 100.0;
					lAvgDistBootFB += (double)(lBestAnalyzeFB - lCritBound) / (double)lCritBound * 100.0;
					lAvgDistRandFB += (double)(lBestSerialFB  - lCritBound) / (double)lCritBound * 100.0;
	
					if (lBestAnalyze < lBestSerial)
						lBootBetter++;
					else if (lBestAnalyze > lBestSerial)
						lRandBetter++;
					else
						lSame++;
	
					if (lBestAnalyzeFB < lBestSerialFB)
						lBootBetterFB++;
					else if (lBestAnalyzeFB > lBestSerialFB)
						lRandBetterFB++;
					else
						lSameFB++;
	
//					System.out.println("\t" + i + "\t" + lBootBetter + "\t" + lRandBetter + "\t" + (lAvgDistBoot / (i*10)) + "\t" + (lAvgDistRand / (i*10))
//							 + "\t" + lBootBetterFB + "\t" + lRandBetterFB + "\t" + (lAvgDistBootFB / (i*10)) + "\t" + (lAvgDistRandFB / (i*10)));
				}
				System.out.print(i + "(" + lBootBetter + "/" + lRandBetter + "), ");
			}
			System.out.println(set + "\t" + lBootBetter + "\t" + lSame + "\t" + lRandBetter + "\t" + (lAvgDistBoot / (lMaxSet*10)) + "\t" + (lAvgDistRand / (lMaxSet*10))
					 + "\t" + lBootBetterFB + "\t" + lSameFB + "\t" + lRandBetterFB + "\t" + (lAvgDistBootFB / (lMaxSet*10)) + "\t" + (lAvgDistRandFB / (lMaxSet*10)));
		}
	}
}
