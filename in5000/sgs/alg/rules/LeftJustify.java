package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LeftJustify implements PriorityRule
{
	private final Random fRanGen = new Random();

	private final Map<Activity, Integer> fSchedule;

	public LeftJustify(Map<Activity, Integer> pSchedule)
	{
		this.fSchedule = pSchedule;
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		List<Activity> lSelectable = new ArrayList<Activity>();
		int lLowestStart = Integer.MAX_VALUE;

		for (Activity lActivity : pEligible)
		{
			int lActivityStart = fSchedule.get(lActivity);

			if (lActivityStart <= lLowestStart)
			{
				if (lActivityStart < lLowestStart)
				{
					lLowestStart = lActivityStart;
					lSelectable = new ArrayList<Activity>();
				}
				lSelectable.add(lActivity);
			}
		}

		Activity lSelected = lSelectable.get(fRanGen.nextInt(lSelectable.size()));
		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return 0;
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }
}
