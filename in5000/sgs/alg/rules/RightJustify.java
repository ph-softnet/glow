package in5000.sgs.alg.rules;

import in5000.sgs.data.Activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RightJustify implements PriorityRule
{
	private final Random fRanGen = new Random();

	private final Map<Activity, Integer> fSchedule;

	public RightJustify(Map<Activity, Integer> pSchedule)
	{
		this.fSchedule = pSchedule;
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		List<Activity> lSelectable = new ArrayList<Activity>();
		int lHighestFinish = 0;

		for (Activity lActivity : pEligible)
		{
			int lActivityFinish = fSchedule.get(lActivity) + lActivity.getDuration();

			if (lActivityFinish >= lHighestFinish)
			{
				if (lActivityFinish > lHighestFinish)
				{
					lHighestFinish = lActivityFinish;
					lSelectable = new ArrayList<Activity>();
				}
				lSelectable.add(lActivity);
			}
		}

		Activity lSelected = lSelectable.get(fRanGen.nextInt(lSelectable.size()));
		pEligible.remove(lSelected);

		return lSelected;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return 0;
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }
}
