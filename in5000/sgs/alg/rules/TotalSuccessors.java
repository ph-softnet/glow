package in5000.sgs.alg.rules;

import in5000.sgs.alg.ParallelGen;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TotalSuccessors implements PriorityRule
{
	private final Map<Activity, Integer> fPriorityMap;

	public TotalSuccessors(Instance pInstance)
	{
		this.fPriorityMap = new HashMap<Activity, Integer>();

		for (int i = 1; i <= pInstance.getNumActivities()+2; i++)
		{
			Activity lActivity = pInstance.getActivity(i);

			this.fPriorityMap.put(lActivity, lActivity.computeTransitiveSuccessors().size());
		}
	}

	@Override
	public Activity selectNext(List<Activity> pEligible)
	{
		Activity lBest		= null;
		int lBestPriority	= -1;

		for (Activity lEligible : pEligible)
		{
			if (this.getPriority(lEligible) > lBestPriority)
			{
				lBest = lEligible;
				lBestPriority = this.getPriority(lEligible);
			}
		}

		return lBest;
	}

	@Override
	public int getPriority(Activity lEligible)
	{
		return this.fPriorityMap.get(lEligible);
	}

	@Override
	public void resetPriority() { }

	@Override
	public void activityScheduled(Activity pActivity, int pStartTime) { }

	public static void main(String[] args) throws IOException
	{
		ParallelGen lParGen = new ParallelGen();

		int lNumLFTbest = 0;
		int lNumTOTbest = 0;
		int lNumESTbest = 0;

		for (int i = 1; i <= 60; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst		 = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				PriorityRule lLFT	 = new LatestFinishTime(lInst, false);
				PriorityRule lTOTSUC = new TotalSuccessors(lInst);
				PriorityRule lESTANA = new ESTanalysis(lInst, false);

				int lBestLFT	= Integer.MAX_VALUE;
				int lBestTOTSUC = Integer.MAX_VALUE;
				int lBestESTANA = Integer.MAX_VALUE;

				for (int k = 0; k < 1; k++)
				{
					lBestLFT	= Math.min(lBestLFT,    lParGen.solve(lInst, lLFT).get(lInst.getActivity(122)));
					lBestTOTSUC	= Math.min(lBestTOTSUC, lParGen.solve(lInst, lTOTSUC).get(lInst.getActivity(122)));
					lBestESTANA	= Math.min(lBestESTANA, lParGen.solve(lInst, lESTANA).get(lInst.getActivity(122)));
				}

				if (lBestLFT < lBestTOTSUC && lBestLFT < lBestESTANA)
					lNumLFTbest++;
				if (lBestTOTSUC < lBestLFT && lBestTOTSUC < lBestESTANA)
					lNumTOTbest++;
				if (lBestESTANA < lBestLFT && lBestESTANA < lBestTOTSUC)
					lNumESTbest++;

				System.out.println(i + "\t" + j + "\t" + 
						lNumLFTbest + "\t" + lNumTOTbest + "\t" + lNumESTbest + "\t" + 
						lBestLFT + "\t" + lBestTOTSUC + "\t" + lBestESTANA + "\t");
			}
		}
	}
}
