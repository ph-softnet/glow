package in5000.sgs.alg;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.Resource;

import java.util.HashMap;
import java.util.Map;

public class TimePoint
{
	private final int fTime;

	private TimePoint fSuccessor;

	private Map<Resource, Integer> fResourcesAvailable;

	public TimePoint(Instance pInstance, int pTime)
	{
		this.fTime					= pTime;
		this.fSuccessor				= null;
		this.fResourcesAvailable	= new HashMap<Resource, Integer>();

		for (int i = 0; i < pInstance.getNumResources(); i++)
		{
			Resource lResource = pInstance.getResource(i);
			this.fResourcesAvailable.put(lResource, lResource.getCapacity());
		}
	}

	public TimePoint(TimePoint pPredecessor, int pTime)
	{
		this.fTime					= pTime;
		this.fSuccessor				= pPredecessor.getNextTimePoint();
		this.fResourcesAvailable	= new HashMap<Resource, Integer>();

		pPredecessor.setSuccessor(this);

		Map<Resource, Integer> pUsageBeforeMe = pPredecessor.getResourcesAvailable();

		for (Resource lResource : pUsageBeforeMe.keySet())
		{
			this.fResourcesAvailable.put(lResource, pUsageBeforeMe.get(lResource));
		}
	}

	private Map<Resource, Integer> getResourcesAvailable()
	{
		return this.fResourcesAvailable;
	}

	private void setSuccessor(TimePoint pSuccessor)
	{
		this.fSuccessor = pSuccessor;
	}

	public int getTime()
	{
		return this.fTime;
	}

	public TimePoint getNextTimePoint()
	{
		return this.fSuccessor;
	}

	public boolean resourceFeasible(Activity pActivity)
	{
		boolean lFeasible = true;

		Map<Resource, Integer> lUsage = pActivity.getResourceRequirements();

		for (Resource lResource : lUsage.keySet())
		{
			if (lUsage.get(lResource) > this.fResourcesAvailable.get(lResource))
			{
				lFeasible = false;
				break;
			}
		}

		return lFeasible;
	}

	public void claimResources(Activity pActivity)
	{
		Map<Resource, Integer> lUsage = pActivity.getResourceRequirements();

		for (Resource lResource : lUsage.keySet())
		{
			int lAvailable	= this.fResourcesAvailable.get(lResource);
			int lUsed		= lUsage.get(lResource);

			this.fResourcesAvailable.put(lResource, lAvailable - lUsed);
		}
	}

	@Override
	public int hashCode()
	{
		return this.fTime;
	}
}
