package in5000.sgs.alg;

import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.ESTanalysis;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.MaxBiasedRandomRule;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.alg.rules.TotalSuccessors;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.Solution;
import in5000.sgs.parser.KolischParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class AdaptiveGen
{
	public static int[] lSelectedRule = new int[6];

	public static Map<Activity, Integer> solve(Instance pInstance, int pMaxSchedules)
	{
		ParallelGen lSolver = new ParallelGen();
		Solution lSolution  = new Solution(pInstance);
		Activity lSink		= pInstance.getDummySink();

		Map<Activity, Integer> lSchedule = null;
		int lNumSchedules				 = 0;

		/*
		 *	Decide on the best priority rule for this instance.
		 */
		int[] lMakespan = new int[6];
		for (int i = 0; i < lMakespan.length; i++)
			lMakespan[i] = Integer.MAX_VALUE;

		PriorityRule[] lRules = new PriorityRule[6];
		lRules[0] = new BiasedRandomRule(new LatestFinishTime(pInstance, false), 10);
		lRules[1] = new MaxBiasedRandomRule(new TotalSuccessors(pInstance), 10);
		lRules[2] = new MaxBiasedRandomRule(new ESTanalysis(pInstance, false), 10);

		for (int i = 0; i < 2; i++)
		{
			//lMakespan[i] = 0;
			lMakespan[i] = Integer.MAX_VALUE;
			for (int j = 0; j < 15; j++)
			{
				//lSchedule = lSolver.solve(pInstance, lRules[i]);
				//lNumSchedules = lNumSchedules + 1;
				lSchedule = ForwardBackwardIteration.performOne(pInstance, lSolver.solve(pInstance, lRules[i]));
				lNumSchedules = lNumSchedules + 3;
				lSolution.offerSolution(lSchedule);
				//lMakespan[i] += lSchedule.get(lSink);
				lMakespan[i] = Math.min(lMakespan[i], lSchedule.get(lSink));
			}
		}

		pInstance.flipDirection();
		pInstance.resetTiming();

		lSink	 = pInstance.getDummySink();
		lRules[3] = new BiasedRandomRule(new LatestFinishTime(pInstance, false), 10);
		lRules[4] = new MaxBiasedRandomRule(new TotalSuccessors(pInstance), 10);
		lRules[5] = new MaxBiasedRandomRule(new ESTanalysis(pInstance, false), 10);

		for (int i = 3; i < 5; i++)
		{
			//lMakespan[i] = 0;
			lMakespan[i] = Integer.MAX_VALUE;
			for (int j = 0; j < 15; j++)
			{
				//lSchedule = lSolver.solve(pInstance, lRules[i]);
				//lNumSchedules = lNumSchedules + 1;
				lSchedule = ForwardBackwardIteration.performOne(pInstance, lSolver.solve(pInstance, lRules[i]));
				lNumSchedules = lNumSchedules + 3;
				lSolution.offerSolution(lSchedule);
				//lMakespan[i] += lSchedule.get(lSink);
				lMakespan[i] = Math.min(lMakespan[i], lSchedule.get(lSink));
			}
		}

		int lBestRule = 0;
		for (int i = 0; i < lRules.length; i++)
			if (lMakespan[i] < lMakespan[lBestRule])
				lBestRule = i;

		lSelectedRule[lBestRule]++;

		if (lBestRule < 3)
			pInstance.flipDirection();

		int lTotalMakespan 	= lMakespan[lBestRule];
		int lTotalSchedules = 1;
		while (lNumSchedules < pMaxSchedules)
		{
			lSink			= pInstance.getDummySink();
			lSchedule		= lSolver.solve(pInstance, lRules[lBestRule]);
			lNumSchedules++;
			int lNewMakespan= lSchedule.get(lSink);
			lSolution.offerSolution(lSchedule);

			double lMeanMakespan = ((double)lTotalMakespan / (double)lTotalSchedules);
			lTotalMakespan	= lTotalMakespan + lNewMakespan;
			lTotalSchedules = lTotalSchedules + 1;
			if (lNewMakespan < lMeanMakespan)//(lMeanMakespan - (lMeanMakespan - lSolution.getMakespan())/2.0))
			{
				lSchedule = ForwardBackwardIteration.performOne(pInstance, lSchedule);
				lNumSchedules = lNumSchedules + 2;

				int lFBIpass        = 1;
				int lFBIMakespan    = lNewMakespan;
				int lNewFBIMakespan = lSchedule.get(lSink);
				while (lNewFBIMakespan < lFBIMakespan && lFBIpass < 3)
				{
					lSchedule		= ForwardBackwardIteration.performOne(pInstance, lSchedule);
					lNumSchedules   = lNumSchedules + 2;

					lFBIMakespan    = lNewFBIMakespan;
					lNewFBIMakespan = lSchedule.get(lSink);

					lFBIpass++;
				}

				lSolution.offerSolution(lSchedule);
			}
		}

		return lSolution.getSolution();
	}

	/*
	public static Map<Activity, Integer> solveOld(Instance pInstance, int pMaxSchedules)
	{
		ParallelGen lSolver = new ParallelGen();
		Solution lSolution  = new Solution(pInstance);
		Activity lSink		= pInstance.getDummySink();

		Map<Activity, Integer> lSchedule = null;
		int lNumSchedules				 = 0;

		 //	Decide on the best priority rule for this instance.
		int[] lMakespan = new int[3];

		PriorityRule[] lForward = new PriorityRule[3];
		lForward[0] = new LatestFinishTime(pInstance, false);
		lForward[1] = new TotalSuccessors(pInstance);
		lForward[2] = new ESTanalysis(pInstance, false);

		for (int i = 0; i < lForward.length; i++)
		{
			lSchedule = ForwardBackwardIteration.performOne(pInstance, lSolver.solve(pInstance, lForward[i]));
			lNumSchedules = lNumSchedules + 3;
			lSolution.offerSolution(lSchedule);
			lMakespan[i] = lSchedule.get(lSink);
		}

		lForward[0] = new BiasedRandomRule(lForward[0]);
		lForward[1] = new MaxBiasedRandomRule(lForward[1]);
		lForward[2] = new MaxBiasedRandomRule(lForward[2]);

		pInstance.flipDirection();
		pInstance.resetTiming();

		lSink	 = pInstance.getDummySink();
		PriorityRule[] lBackward = new PriorityRule[3];
		lBackward[0] = new LatestFinishTime(pInstance, false);
		lBackward[1] = new TotalSuccessors(pInstance);
		lBackward[2] = new ESTanalysis(pInstance, false);

		for (int i = 0; i < lBackward.length; i++)
		{
			lSchedule = ForwardBackwardIteration.performOne(pInstance, lSolver.solve(pInstance, lBackward[i]));
			lNumSchedules = lNumSchedules + 3;
			lSolution.offerSolution(lSchedule);
			lMakespan[i] = Math.min(lMakespan[i], lSchedule.get(lSink));
		}

		lBackward[0] = new BiasedRandomRule(lBackward[0]);
		lBackward[1] = new MaxBiasedRandomRule(lBackward[1]);
		lBackward[2] = new MaxBiasedRandomRule(lBackward[2]);

		int lBestRule = 0;
		if (lMakespan[1] < lMakespan[lBestRule])
			lBestRule = 1;
		if (lMakespan[2] < lMakespan[lBestRule])
			lBestRule = 2;

		lSelectedRule[lBestRule]++;

		pInstance.flipDirection();

		int lTotalMakespan 	= lMakespan[lBestRule];
		int lTotalSchedules = 1;
		boolean lSDForward = true;
		while (lNumSchedules < pMaxSchedules)
		{
			lSink			= pInstance.getDummySink();
			lSchedule		= lSolver.solve(pInstance, lSDForward ? lForward[lBestRule] : lBackward[lBestRule]);
			lNumSchedules++;
			int lNewMakespan= lSchedule.get(lSink);
			lTotalMakespan	= lTotalMakespan + lNewMakespan;
			lTotalSchedules = lTotalSchedules + 1;
			lSolution.offerSolution(lSchedule);

			if (lNewMakespan < ((double)lTotalMakespan / (double)lTotalSchedules))
			{
				lSchedule = ForwardBackwardIteration.performOne(pInstance, lSchedule);
				int lFBMakespan = lSchedule.get(lSink);
				lNumSchedules = lNumSchedules + 2;

				if (lFBMakespan < lNewMakespan)
				{
					lSchedule = ForwardBackwardIteration.performOne(pInstance, lSchedule);
					lNumSchedules = lNumSchedules + 2;
				}

				lSolution.offerSolution(lSchedule);
			}

			pInstance.flipDirection();
			lSDForward = !lSDForward;
		}

		return lSolution.getSolution();
	}
	 */

	public static void main(String[] args) throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/adaptive1000-s.dat")));
		double lAvgDistLB = 0;
		for (int i = 1; i <= 60; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				int lCritBound    = lInst.computeLowerBound0();

				Map<Activity, Integer> lSchedule = AdaptiveGen.solve(lInst, 1000);
				int lMakespan = lSchedule.get(lInst.getDummySink());
				lMakespan     = Math.max(lMakespan, lSchedule.get(lInst.getDummySource()));

				lAvgDistLB += (double)(lMakespan - lCritBound) / (double)lCritBound * 100.0;

				lOut.append(i + "\t" + j + "\t" + 
							lInst.getOrderStrength() + "\t" + 
							lInst.getResourceStrength() + "\t" + 
							lInst.getResourceFactor() + "\t" + 
							lCritBound + "\t" + 
							lMakespan);
				lOut.newLine();
				lOut.flush();
			}
			System.out.println(i + "\t" + (lAvgDistLB / (i*10)) + "\t" + Arrays.toString(lSelectedRule));
		}
		lOut.close();
	}
}
