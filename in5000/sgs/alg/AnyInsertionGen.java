package in5000.sgs.alg;

import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.InsertionActivity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.MaximalCut;
import in5000.sgs.data.Resource;
import in5000.sgs.data.ResourceFlow;
import in5000.sgs.parser.KolischParser;
import in5000.sgs.test.VerifySchedule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class AnyInsertionGen implements GenerationScheme
{
	public Map<Activity, Integer> solve(Instance pInstance, PriorityRule pRule)
	{
		// Determine the maximum ID of real+dummy activities.
		int lNumActivities = pInstance.getNumActivities()+2;
		int lNumResources  = pInstance.getNumResources();

		// Create adjacency matrix of resource flows.
		ResourceFlow[][] lAdjacency = new ResourceFlow[lNumActivities+1][lNumActivities+1];

		// Find {s,t}
		InsertionActivity lSource = new InsertionActivity(pInstance.getActivity(1));
		InsertionActivity lSink   = new InsertionActivity(pInstance.getActivity(lNumActivities));

		// Create the maximum resource flow from source to sink.
		ResourceFlow lInitialFlow = new ResourceFlow(lSource, lSink);
		for (int i = 0; i < lNumResources; i++)
			lInitialFlow.setFlow(pInstance.getResource(i), pInstance.getResource(i).getCapacity());
		lSource.addSuccessor(lInitialFlow);
		lSink.addPredecessor(lInitialFlow);
		lAdjacency[lSource.getActivity().getID()][lSink.getActivity().getID()] = lInitialFlow;

		// Record scheduled tasks.
		Set<Activity> lScheduled = new HashSet<Activity>();
		Map<Activity, InsertionActivity> lScheduleMap = new HashMap<Activity, InsertionActivity>();
		lScheduled.add(lSource.getActivity());
		lScheduleMap.put(lSource.getActivity(), lSource);
		lScheduled.add(lSink.getActivity());
		lScheduleMap.put(lSink.getActivity(), lSink);

		// Initialize EST / LST.
		lSource.computeEST(0, lScheduleMap);
		lSink.computeLST(0, lScheduleMap);

		// Determine Decision set D.
		List<Activity> lDecisionSet = new ArrayList<Activity>();
		for (int i = 2; i < lNumActivities; i++)
			lDecisionSet.add(pInstance.getActivity(i));

		while (!lDecisionSet.isEmpty())
		{
			// Get the activity with the next highest priority.
			InsertionActivity lNext = new InsertionActivity(pRule.selectNext(lDecisionSet));

			// Determine which activities MUST be before / after the selected task.
			Set<Activity> lAllPredecessors	= lNext.getActivity().computeTransitivePredecessors();
			Set<Activity> lAllSuccessors	= lNext.getActivity().computeTransitiveSuccessors();

			// Determine which of the project predecessors / successors are already scheduled.
			lAllPredecessors.retainAll(lScheduled);
			lAllSuccessors.retainAll(lScheduled);
			/**/

			// Compute the earliest allowed starting time of the next task.
			int lNextEST = 0;
			for (Activity lPredecessor : lAllPredecessors)
			{
				InsertionActivity lScheduledPredecessor = lScheduleMap.get(lPredecessor);
				if (lScheduledPredecessor != null && lScheduledPredecessor.getEFT() > lNextEST)
					lNextEST = lScheduledPredecessor.getEFT();
			}

			// Compute the latest finishing time of the next task if no tasks are delayed.
			int lNextLFT = Integer.MAX_VALUE;
			for (Activity lSuccessor : lAllSuccessors)
			{
				InsertionActivity lScheduledSuccessor = lScheduleMap.get(lSuccessor);
				if (lScheduledSuccessor != null && lScheduledSuccessor.getLST() < lNextLFT)
					lNextLFT = lScheduledSuccessor.getLST();
			}

			// Create the Initial Cut.
			MaximalCut lInitialCut = new MaximalCut(lSource);
			lInitialCut.generateInitialCut(lNextEST);

			// Iterate over all maximal insertion cuts, while finding the optimal insertion sub-cut in each.
			Set<ResourceFlow> lOptimalCut = null;
			int lMakespanIncrease		  = Integer.MAX_VALUE;
			do
			{
				// Compute the best cut out of this maximal cut.
				Set<ResourceFlow> lCurrentCut = lInitialCut.findOptimalSubCut(lNext.getActivity().getResourceRequirements());

				// Compute the max EFT of the left side, min LST of the right side.
				int lMaxEFT = Integer.MIN_VALUE;
				int lMinLST = Integer.MAX_VALUE;
				for (ResourceFlow lCurFlow : lCurrentCut)
				{
					if (lCurFlow.getFrom().getEFT() > lMaxEFT)
						lMaxEFT = lCurFlow.getFrom().getEFT();
					if (lCurFlow.getTo().getLST() < lMinLST)
						lMinLST = lCurFlow.getTo().getLST();
				}

				int lMakespanDelta = Math.max(0, Math.max(lNextEST, lMaxEFT) + 
												 lNext.getActivity().getDuration() -
												 Math.min(lNextLFT, lMinLST));

				if (lMakespanDelta < lMakespanIncrease)
				{
					lMakespanIncrease = lMakespanDelta;
					lOptimalCut = lCurrentCut;

					//System.out.println("\t" + lMakespanDelta + "\t" + lCurrentCut.toString());
				}
			}
			while (lInitialCut.generateNextCut(lNextLFT));

			// Determine which activities will be the flow predecessors / successors.
			Set<InsertionActivity> lFlowPredecessors = new TreeSet<InsertionActivity>(new EFTcomparator());
			Set<InsertionActivity> lFlowSuccessors   = new TreeSet<InsertionActivity>(new EFTcomparator());
			for (ResourceFlow lOptFlow : lOptimalCut)
			{
				lFlowPredecessors.add(lOptFlow.getFrom());
				lFlowSuccessors.add(lOptFlow.getTo());
			}
			
			// Create empty resource flows that signify project constraints.
			for (Activity lActivity : lAllPredecessors)
			{
				InsertionActivity lMapped = lScheduleMap.get(lActivity);
				if (lMapped != null)
				{
					ResourceFlow lProjectConstraint = new ResourceFlow(lMapped, lNext);
					lMapped.addSuccessor(lProjectConstraint);
					lNext.addPredecessor(lProjectConstraint);
					lAdjacency[lMapped.getActivity().getID()][lNext.getActivity().getID()] = lProjectConstraint;
				}
			}

			// Create empty resource flows that signify project constraints.
			for (Activity lActivity : lAllSuccessors)
			{
				InsertionActivity lMapped = lScheduleMap.get(lActivity);
				if (lMapped != null)
				{
					ResourceFlow lProjectConstraint = new ResourceFlow(lNext,lMapped);
					lNext.addSuccessor(lProjectConstraint);
					lMapped.addPredecessor(lProjectConstraint);
					lAdjacency[lNext.getActivity().getID()][lMapped.getActivity().getID()] = lProjectConstraint;
				}
			}
			/**/

			// Determine how much resources need to be diverted from the flows.
			Map<Resource, Integer> lRequirements = new HashMap<Resource, Integer>();
			for (Resource lResource : lNext.getActivity().getResourceRequirements().keySet())
				lRequirements.put(lResource, lNext.getActivity().getResourceUse(lResource));

			for (InsertionActivity lFrom : lFlowPredecessors)
			{
				for (InsertionActivity lTo : lFlowSuccessors)
				{
					ResourceFlow lOptFlow = lAdjacency[lFrom.getActivity().getID()][lTo.getActivity().getID()];

					if (lOptFlow != null)
					{
						// Determine how much of this flow we can use.
						Map<Resource, Integer> lDivertedFlow = new HashMap<Resource, Integer>();
						for (Resource lResource : lRequirements.keySet())
						{
							int lDivertedAmount = Math.min(lRequirements.get(lResource),
														   lOptFlow.getFlow(lResource));

							if (lDivertedAmount > 0)
								lDivertedFlow.put(lResource, lDivertedAmount);
						}

						// IF we divert any from this flow,
						if (lDivertedFlow.size() > 0)
						{
							// Construct the new intermediate flows.
							InsertionActivity lLeft  = lOptFlow.getFrom();
							InsertionActivity lRight = lOptFlow.getTo();
			
							if (lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()] == null)
							{
								ResourceFlow lToNext	= new ResourceFlow(lLeft, lNext);
								lLeft.addSuccessor(lToNext);
								lNext.addPredecessor(lToNext);
								lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()] = lToNext;
							}

							if (lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()] == null)
							{
								ResourceFlow lFromNext	= new ResourceFlow(lNext, lRight);
								lNext.addSuccessor(lFromNext);
								lRight.addPredecessor(lFromNext);
								lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()] = lFromNext;
							}

							// And update all flows.
							for (Resource lResource : lDivertedFlow.keySet())
							{
								int lDivertedAmount = lDivertedFlow.get(lResource);

								lRequirements.put(lResource, lRequirements.get(lResource) - lDivertedAmount);
								lOptFlow.decreaseFlow(lResource, lDivertedAmount);
								lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()].increaseFlow(lResource, lDivertedAmount);
								lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()].increaseFlow(lResource, lDivertedAmount);
							}
						}
					}
				}
			}

			/**
			// Now, cut open all flows in the optimal cut to insert the `next' activity.
			for (ResourceFlow lOptFlow : lOptimalCut)
			{
				// Determine how much of this flow we can use.
				Map<Resource, Integer> lDivertedFlow = new HashMap<Resource, Integer>();
				for (Resource lResource : lRequirements.keySet())
				{
					int lDivertedAmount = Math.min(lRequirements.get(lResource),
												   lOptFlow.getFlow(lResource));

					if (lDivertedAmount > 0)
						lDivertedFlow.put(lResource, lDivertedAmount);
				}

				// IF we divert any from this flow,
				if (lDivertedFlow.size() > 0)
				{
					// Construct the new intermediate flows.
					InsertionActivity lLeft  = lOptFlow.getFrom();
					InsertionActivity lRight = lOptFlow.getTo();
	
					if (lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()] == null)
					{
						ResourceFlow lToNext	= new ResourceFlow(lLeft, lNext);
						lLeft.addSuccessor(lToNext);
						lNext.addPredecessor(lToNext);
						lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()] = lToNext;
					}

					if (lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()] == null)
					{
						ResourceFlow lFromNext	= new ResourceFlow(lNext, lRight);
						lNext.addSuccessor(lFromNext);
						lRight.addPredecessor(lFromNext);
						lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()] = lFromNext;
					}

					// And update all flows.
					for (Resource lResource : lDivertedFlow.keySet())
					{
						int lDivertedAmount = lDivertedFlow.get(lResource);

						lRequirements.put(lResource, lRequirements.get(lResource) - lDivertedAmount);
						lOptFlow.decreaseFlow(lResource, lDivertedAmount);
						lAdjacency[lLeft.getActivity().getID()][lNext.getActivity().getID()].increaseFlow(lResource, lDivertedAmount);
						lAdjacency[lNext.getActivity().getID()][lRight.getActivity().getID()].increaseFlow(lResource, lDivertedAmount);
					}
				}
			}
			/**/

			/*
			for (Resource lResource : lRequirements.keySet())
				System.out.print(lRequirements.get(lResource) + "\t");
			System.out.println();
			/**/

			/*
			System.out.println(lNext.getActivity().getID() + "\t" + lAllPredecessors.toString());
			System.out.println(lNext.getActivity().getID() + "\t" + lAllSuccessors.toString());
			System.out.println(lNextEST);
			System.out.println(lInitialCut.toString());
			System.out.println();
			/**/

			// Record the scheduling.
			lScheduled.add(lNext.getActivity());
			lScheduleMap.put(lNext.getActivity(), lNext);

			for (InsertionActivity lActivity : lScheduleMap.values())
				lActivity.resetTiming();
				
			lSource.computeEST(0, lScheduleMap);
			lSink.computeLST(lSink.getEST(), lScheduleMap);
		}

		/*
		System.out.println(lSource.getActivity().getID() + "\t" + lSource.getOutDegree());
		System.out.println(lSink.getActivity().getID() + "\t" + lSink.getInDegree());
		System.out.println(lDecisionSet.size());
		/**/

		// Finally, compute the schedule
		Map<Activity, Integer> lSchedule = new HashMap<Activity, Integer>();
		for (InsertionActivity lActivity : lScheduleMap.values())
			lSchedule.put(lActivity.getActivity(), lActivity.getEST());
		
		return lSchedule;
	}

	public static void main(String[] args)
	{
		SerialGen lParGen			= new SerialGen();
		AnyInsertionGen lSinGen	= new AnyInsertionGen();

		int lMkSumPar = 0;
		int lMkSumSin = 0;
		for (int i = 0; i < 1000; i++)
		{
			Instance lInst = null;
			try
			{
				lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+33+"_"+4+".sm"));
			}
			catch (IOException e) { }
	
			Map<Activity, Integer> lSchedule = lParGen.solve(lInst, new BiasedRandomRule(new LatestFinishTime(lInst, false), 1));
			lMkSumPar += lSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));

			lSchedule = lSinGen.solve(lInst, new BiasedRandomRule(new LatestFinishTime(lInst, false), 1));
			lMkSumSin += lSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
			/**/

			VerifySchedule.verify(lInst, lSchedule);
			System.out.println(String.format("%3d - %f - %f", i, (double)lMkSumPar / (i+1), (double)lMkSumSin / (i+1)));
		}
	}
}
