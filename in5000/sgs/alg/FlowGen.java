package in5000.sgs.alg;

import in5000.sgs.alg.rules.LeftJustify;
import in5000.sgs.data.Activity;
import in5000.sgs.data.InsertionActivity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.Resource;
import in5000.sgs.data.ResourceFlow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FlowGen
{
	public List<ResourceFlow> solve(Instance pInstance, LeftJustify pRule, Map<Activity, Integer> pOriginal)
	{
		List<ResourceFlow>		 lEligibleFlow			= new LinkedList<ResourceFlow>();
		Map<Activity, Integer>	 lPredStarted			= new HashMap<Activity, Integer>();
		TimePoint				 lTnow					= new TimePoint(pInstance, 0);
		Map<Activity, TimePoint> lEarliestTime			= new HashMap<Activity, TimePoint>();
		Map<Activity, Integer>	 lSchedule				= new HashMap<Activity, Integer>();
		Map<TimePoint, List<Activity>> lBecomeEligible	= new HashMap<TimePoint, List<Activity>>();
		Map<TimePoint, List<ResourceFlow>> lBecomeEligFlow= new HashMap<TimePoint, List<ResourceFlow>>();
		List<ResourceFlow>		 lNonRedundentFlow		= new ArrayList<ResourceFlow>();

		/*
		 * Maintain a set of eligible tasks.
		 */
		Activity lRoot = pInstance.getActivity(1);
		InsertionActivity lSink = new InsertionActivity(pInstance.getActivity(pInstance.getNumActivities()+2));
		ResourceFlow lAllFlow = new ResourceFlow(new InsertionActivity(lRoot), lSink);
		for (int i = 0; i < pInstance.getNumResources(); i++)
		{
			Resource lResource = pInstance.getResource(i);
			lAllFlow.setFlow(lResource, lResource.getCapacity());
		}
		lBecomeEligible.put(lTnow, new ArrayList<Activity>());
		lBecomeEligFlow.put(lTnow, new ArrayList<ResourceFlow>());
		lBecomeEligible.get(lTnow).add(lRoot);
		lBecomeEligFlow.get(lTnow).add(lAllFlow);

		do
		{
			List<Activity> lEligible = new LinkedList<Activity>();
			for (Activity lNewEligible : pOriginal.keySet())
			{
				if (!lSchedule.containsKey(lNewEligible) && pOriginal.get(lNewEligible) <= lTnow.getTime())
					lEligible.add(lNewEligible);
			}
			lEligibleFlow.addAll(lBecomeEligFlow.get(lTnow));

			List<Activity> lStartable = new ArrayList<Activity>();
			lStartable.addAll(lEligible);

			while (!lStartable.isEmpty())
			{
				Activity lHighestPriority = pRule.selectNext(lStartable);
				lStartable.remove(lHighestPriority);
				InsertionActivity lHighInsertAct = new InsertionActivity(lHighestPriority);

				boolean lResourceFeasible = true;
				TimePoint lPreEndPoint = lTnow;
				TimePoint lEndPoint    = lTnow;
				while (lResourceFeasible && lEndPoint != null && lEndPoint.getTime() - lTnow.getTime() < lHighestPriority.getDuration())
				{
					lPreEndPoint		= lEndPoint;
					lEndPoint			= lEndPoint.getNextTimePoint();
					lResourceFeasible	= lPreEndPoint.resourceFeasible(lHighestPriority);
				}

				if (lResourceFeasible)
				{
					// Determine how many resources must be claimed.
					Map<Resource, Integer> lResourceToClaim = new HashMap<Resource, Integer>();
					int lResourceClaimSum = 0;
					for (int i = 0; i < pInstance.getNumResources(); i++)
					{
						Resource lResource = pInstance.getResource(i);
						int lResourceUsage = lHighestPriority.getResourceUse(lResource);
						lResourceClaimSum += lResourceUsage;
						if (lResourceUsage > 0)
							lResourceToClaim.put(lResource, lResourceUsage);
					}

					// Try to claim as much resource flow from our predecessors as possible.
					Set<Activity> lPredecessors = lHighestPriority.computeTransitivePredecessors();
					boolean lClaimedResources = false;
					do
					{
						// Find the predecessor with the latest finish time that can give us flow.
						lClaimedResources = false;
						int lLatestFinishTime		= -1;
						ResourceFlow lSelectedFlow	= null;
						for (ResourceFlow lFlow : lEligibleFlow)
						{
							Activity lSource = lFlow.getFrom().getActivity();
							if (lPredecessors.contains(lSource))
							{
								boolean lHasFlowToGive = false;
								for (Resource lResource : lResourceToClaim.keySet())
									if (lResourceToClaim.get(lResource) > 0 && lFlow.getFlow(lResource) > 0)
										lHasFlowToGive = true;

								if (lHasFlowToGive)
								{
									int lFinishTime = lSchedule.get(lSource) + lSource.getDuration();
									if (lFinishTime > lLatestFinishTime)
									{
										lLatestFinishTime = lFinishTime;
										lSelectedFlow = lFlow;
									}
								}
							}
						}

						if (lSelectedFlow != null)
						{
							for (Resource lResource : lResourceToClaim.keySet())
								if (lSelectedFlow.getFlow(lResource) > 0)
								{
									int lClaim = Math.min(lSelectedFlow.getFlow(lResource),
														  lResourceToClaim.get(lResource));

									int lStillRequired = lResourceToClaim.get(lResource) - lClaim;
									lResourceToClaim.put(lResource, lStillRequired);
									lResourceClaimSum = lResourceClaimSum - lClaim;
									lSelectedFlow.decreaseFlow(lResource, lClaim);
								}

							if (lSelectedFlow.isEmpty())
								lEligibleFlow.remove(lSelectedFlow);
							
							lClaimedResources = true;
						}
					}
					while (lClaimedResources);

					// If we need more flow, claim it from the tasks with the lowest start times.
					while (lResourceClaimSum > 0)
					{
						int lMostFlowAvailable		= 0;
						ResourceFlow lSelectedFlow	= null;
						for (ResourceFlow lFlow : lEligibleFlow)
						{
							//Activity lSource = lFlow.getFrom().getActivity();

							boolean lHasFlowToGive = false;
							int lFlowAvailable = 0;
							for (Resource lResource : lResourceToClaim.keySet())
								if (lResourceToClaim.get(lResource) > 0 && lFlow.getFlow(lResource) > 0)
								{
									lHasFlowToGive  = true;
									lFlowAvailable += Math.min(lResourceToClaim.get(lResource),
															   lFlow.getFlow(lResource));
								}

							if (lHasFlowToGive)
							{
								if (lFlowAvailable > lMostFlowAvailable)
								{
									lMostFlowAvailable = lFlowAvailable;
									lSelectedFlow = lFlow;
								}
							}
						}

						if (lSelectedFlow != null)
						{
							ResourceFlow lDivertedFlow = new ResourceFlow(lSelectedFlow.getFrom(), lHighInsertAct);
							for (Resource lResource : lResourceToClaim.keySet())
								if (lSelectedFlow.getFlow(lResource) > 0)
								{
									int lClaim = Math.min(lSelectedFlow.getFlow(lResource),
														  lResourceToClaim.get(lResource));

									int lStillRequired = lResourceToClaim.get(lResource) - lClaim;
									lResourceToClaim.put(lResource, lStillRequired);
									lResourceClaimSum = lResourceClaimSum - lClaim;
									lSelectedFlow.decreaseFlow(lResource, lClaim);
									lDivertedFlow.setFlow(lResource, lClaim);
								}
							lNonRedundentFlow.add(lDivertedFlow);

							if (lSelectedFlow.isEmpty())
								lEligibleFlow.remove(lSelectedFlow);
						}
						else
						{
							System.err.println("No more flow to give!");
						}
					}

					//System.out.println(lResourceClaimSum + " " + lNonRedundentFlow.size());
					// Compute the outgoing flow, that drips into the sink.
					ResourceFlow lOutFlow = new ResourceFlow(lHighInsertAct, lSink);
					for (int i = 0; i < pInstance.getNumResources(); i++)
					{
						Resource lResource = pInstance.getResource(i);
						if (lHighestPriority.usesResource(lResource))
							lOutFlow.setFlow(lResource, lHighestPriority.getResourceUse(lResource));
					}

					// Compute the end time for this task.
					List<ResourceFlow> lFlowList = null;
					int lEndTime = lTnow.getTime() + lHighestPriority.getDuration();
					if (lEndPoint == null || lEndPoint.getTime() != lEndTime)
					{
						lEndPoint = new TimePoint(lPreEndPoint, lEndTime);
						lFlowList = new ArrayList<ResourceFlow>();
						lBecomeEligible.put(lEndPoint, new ArrayList<Activity>());
						lBecomeEligFlow.put(lEndPoint, lFlowList);
					}
					else
					{
						lFlowList = lBecomeEligFlow.get(lEndPoint);
					}
					lFlowList.add(lOutFlow);

					// Schedule the startable task by claiming the resources for it.
					lEligible.remove(lHighestPriority);
					lSchedule.put(lHighestPriority, lTnow.getTime());
					TimePoint lPointer = lTnow;
					while (lPointer != lEndPoint)
					{
						lPointer.claimResources(lHighestPriority);
						lPointer = lPointer.getNextTimePoint();
					}

					// Update successor startability.
					for (Activity lSuccessor : lHighestPriority.getSuccessors())
					{
						if (lPredStarted.containsKey(lSuccessor))
						{
							int lStarted = lPredStarted.get(lSuccessor) + 1;
							lPredStarted.put(lSuccessor, lStarted);

							if (lEarliestTime.get(lSuccessor).getTime() < lEndPoint.getTime())
								lEarliestTime.put(lSuccessor, lEndPoint);
						}
						else
						{
							lPredStarted.put(lSuccessor, 1);
							lEarliestTime.put(lSuccessor, lEndPoint);
						}

						if (lPredStarted.get(lSuccessor).equals(lSuccessor.getInDegree()))
						{
							lBecomeEligible.get(lEarliestTime.get(lSuccessor)).add(lSuccessor);
						}
					}
				}
			}

			if (lTnow.getTime() != 0 || lTnow.getNextTimePoint() != null)
				lTnow = lTnow.getNextTimePoint();
		}
		while (lTnow != null);

		return lNonRedundentFlow;
	}

}
