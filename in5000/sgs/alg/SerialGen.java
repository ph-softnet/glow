package in5000.sgs.alg;

import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SerialGen implements GenerationScheme
{
	public Map<Activity, Integer> solve(Instance pInstance, PriorityRule pRule)
	{
		final int				 lNumToSchedule	= pInstance.getNumActivities() + 2;
		List<Activity>			 lEligible		= new LinkedList<Activity>();
		Map<Activity, Integer>	 lPredStarted	= new HashMap<Activity, Integer>();
		TimePoint				 lTzero			= new TimePoint(pInstance, 0);
		Map<Activity, TimePoint> lEarliestTime	= new HashMap<Activity, TimePoint>();
		Map<Activity, Integer>	 lSchedule		= new HashMap<Activity, Integer>();

		// Reset the priority rule state.
		pRule.resetPriority();

		/*
		 * Maintain list of eligible tasks, initialize the eligible set with the super-root node.
		 */
		Activity lRoot = pInstance.getDummySource();
		lEligible.add(lRoot);
		lEarliestTime.put(lRoot, lTzero);

		/*
		 * Schedule all activities in the instance.
		 */
		for (int lScheduled = 0; lScheduled < lNumToSchedule; lScheduled++)
		{
			/*
			 * Sequentially consider startable tasks in the order of the priority.
			 */
			Activity lNextActivity = null;
			switch (lEligible.size())
			{
			case 0:
				throw new IllegalArgumentException("Instance not connected.");
			case 1:
				lNextActivity = lEligible.remove(0);
				break;
			default:
				lNextActivity = pRule.selectNext(lEligible);
			}

			/*
			 * Select the earliest opportunity to start the task.
			 */
			TimePoint lStartPoint  = lEarliestTime.get(lNextActivity);
			TimePoint lPreEndPoint = lEarliestTime.get(lNextActivity);
			TimePoint lEndPoint    = lEarliestTime.get(lNextActivity);
			while (lEndPoint != null && lEndPoint.getTime() - lStartPoint.getTime() < lNextActivity.getDuration())
			{
				// Advance the start time to a feasible point.
				while (!lStartPoint.resourceFeasible(lNextActivity))
				{
					lStartPoint = lStartPoint.getNextTimePoint();
				}

				// Try to obtain a time slot that is both resource feasible and large enough.
				lPreEndPoint = lStartPoint;
				lEndPoint	 = lStartPoint.getNextTimePoint();
				while (lEndPoint != null && lEndPoint.getTime() - lStartPoint.getTime() < lNextActivity.getDuration())
				{
					if (!lEndPoint.resourceFeasible(lNextActivity))
					{
						lStartPoint = lEndPoint.getNextTimePoint();
						break;
					}

					lPreEndPoint = lEndPoint;
					lEndPoint	 = lEndPoint.getNextTimePoint();
				}
			}

			// If the End Point lies beyond the known schedule, construct a new time point.
			if (lEndPoint == null || lEndPoint.getTime() != lStartPoint.getTime() + lNextActivity.getDuration())
			{
				lEndPoint = new TimePoint(lPreEndPoint, lStartPoint.getTime() + lNextActivity.getDuration());
			}

			/*
			 * Claim the resources used by this task for the discovered duration.
			 */
			TimePoint lPointer = lStartPoint;
			while (lPointer != lEndPoint)
			{
				lPointer.claimResources(lNextActivity);
				lPointer = lPointer.getNextTimePoint();
			}

			/*
			 * Store the start time of this activity.
			 */
			lSchedule.put(lNextActivity, lStartPoint.getTime());
			pRule.activityScheduled(lNextActivity, lSchedule.get(lNextActivity));

			for (Activity lSuccessor : lNextActivity.getSuccessors())
			{
				if (lPredStarted.containsKey(lSuccessor))
				{
					int lStarted = lPredStarted.get(lSuccessor) + 1;
					lPredStarted.put(lSuccessor, lStarted);

					if (lEarliestTime.get(lSuccessor).getTime() < lEndPoint.getTime())
						lEarliestTime.put(lSuccessor, lEndPoint);
				}
				else
				{
					lPredStarted.put(lSuccessor, 1);
					lEarliestTime.put(lSuccessor, lEndPoint);
				}

				if (lPredStarted.get(lSuccessor).equals(lSuccessor.getInDegree()))
				{
					lEligible.add(lSuccessor);
				}
			}

			//System.out.println(lScheduled + ": (" + lNextActivity.getID() + " at " + lStartPoint.getTime() + ")");
			// - " + lEligible.size() + " - " + lStartPoint + " - " + lEndPoint);
		}

		return lSchedule;

		// 

		/*
		 * Re-compute the priority of each eligible task, keep track of the best.
		 * 
		 * TODO It may be more efficient to recompute it `on change':
		 * 		We may know from the change which tasks are (un)affected.
		 * 		This suggest a (Fibonnaci) Heap implementation of the eligible tasks.
		 */

		/*
		 *  Maintain list of insertion points, a (time, resources, completed) tuple:
		 *  	- Time is the fixed time of the point.
		 *		- Resources is the current (decreasing) resource availability of the point.
		 *		- Completed is the list of tasks that have finished at or before this time point.
		 *
		 * TODO A lot of fathoming is possible here:
		 *		- Time points which have too few resources to start any remaining task can be discarded.
		 *		- Time points which have too few started tasks to start any remaining task can be discarded.
		 *		- Time points which have identical `resources' and `completed' can be collapsed into the earliest.
		 *		- Time points which are earlier than that of the earliest starting time of all to be scheduled.
		 *		- Tasks can start looking at the first time point which contains all predecessors.
		 *		- Tasks can start looking at the first time point which contains enough resources.
		 *
		 *	Implementation may be a Linked List, with pointers to the earliest (interesting) time point for each task.
		 *	Then traversal can start here, and when a predecessor is placed, we can update its successor time points
		 *	to its finish time point (if it is later than the previous time point).
		 *
		 *	Inserting a task then means:
		 *		- Adjusting the selected time points resource availability.
		 *		- Moving along the pointers to the earliest time point before the activity finish time,
		 *			while updating resource availability of each visited time point.
		 *		- Creating a new time point for the activities finish time.
		 *			(rarely, this time point will exist already, which is more convenient).
		 *		- The new time point inherits the resource availability of its predecessor,
		 *			plus the released resources.
		 *		- This new time point becomes the starting time point of my successors, if later than the stored.
		 *		- All subsequent points must be updated on `completed'.
		 *			(maybe this is fathomed by the fact that we start looking from the correct point anyway...)
		 */

		// Traverse insertion points in time order, and insert the current best at the earliest time.

			/*
			 * Start the considered task at the earliest opportunity,
			 * update the opportunities (the start and end points of the considered task)
			 */
	}
//
//	public Map<Activity, Integer> solveInverse(Instance pInstance, PriorityRule pRule)
//	{
//		final int				 lNumToSchedule	= pInstance.getNumActivities() + 2;
//		List<Activity>			 lEligible		= new LinkedList<Activity>();
//		Map<Activity, Integer>	 lPredStarted	= new HashMap<Activity, Integer>();
//		TimePoint				 lTzero			= new TimePoint(pInstance, 0);
//		Map<Activity, TimePoint> lEarliestTime	= new HashMap<Activity, TimePoint>();
//		Map<Activity, Integer>	 lSchedule		= new HashMap<Activity, Integer>();
//
//		/*
//		 * Maintain list of eligible tasks, initialize the eligible set with the super-root node.
//		 */
//		Activity lRoot = pInstance.getActivity(lNumToSchedule);
//		lEligible.add(lRoot);
//		lEarliestTime.put(lRoot, lTzero);
//
//		/*
//		 * Schedule all activities in the instance.
//		 */
//		for (int lScheduled = 0; lScheduled < lNumToSchedule; lScheduled++)
//		{
//			/*
//			 * Sequentially consider startable tasks in the order of the priority.
//			 */
//			Activity lNextActivity = null;
//			switch (lEligible.size())
//			{
//			case 0:
//				throw new IllegalArgumentException("Instance not connected.");
//			case 1:
//				lNextActivity = lEligible.remove(0);
//				break;
//			default:
//				lNextActivity = pRule.selectNext(lEligible);
//			}
//
//			/*
//			 * Select the earliest opportunity to start the task.
//			 */
//			TimePoint lStartPoint  = lEarliestTime.get(lNextActivity);
//			TimePoint lPreEndPoint = lEarliestTime.get(lNextActivity);
//			TimePoint lEndPoint    = lEarliestTime.get(lNextActivity);
//			while (lEndPoint != null && lEndPoint.getTime() - lStartPoint.getTime() < lNextActivity.getDuration())
//			{
//				// Advance the start time to a feasible point.
//				while (!lStartPoint.resourceFeasible(lNextActivity))
//				{
//					lStartPoint = lStartPoint.getNextTimePoint();
//				}
//
//				// Try to obtain a time slot that is both resource feasible and large enough.
//				lPreEndPoint = lStartPoint;
//				lEndPoint	 = lStartPoint.getNextTimePoint();
//				while (lEndPoint != null && lEndPoint.getTime() - lStartPoint.getTime() < lNextActivity.getDuration())
//				{
//					if (!lEndPoint.resourceFeasible(lNextActivity))
//					{
//						lStartPoint = lEndPoint.getNextTimePoint();
//						break;
//					}
//
//					lPreEndPoint = lEndPoint;
//					lEndPoint	 = lEndPoint.getNextTimePoint();
//				}
//			}
//
//			// If the End Point lies beyond the known schedule, construct a new time point.
//			if (lEndPoint == null || lEndPoint.getTime() != lStartPoint.getTime() + lNextActivity.getDuration())
//			{
//				lEndPoint = new TimePoint(lPreEndPoint, lStartPoint.getTime() + lNextActivity.getDuration());
//			}
//
//			/*
//			 * Claim the resources used by this task for the discovered duration.
//			 */
//			TimePoint lPointer = lStartPoint;
//			while (lPointer != lEndPoint)
//			{
//				lPointer.claimResources(lNextActivity);
//				lPointer = lPointer.getNextTimePoint();
//			}
//
//			/*
//			 * Store the start time of this activity.
//			 */
//			lSchedule.put(lNextActivity, lStartPoint.getTime());
//
//			for (Activity lPredecessor : lNextActivity.getPredecessors())
//			{
//				if (lPredStarted.containsKey(lPredecessor))
//				{
//					int lStarted = lPredStarted.get(lPredecessor) + 1;
//					lPredStarted.put(lPredecessor, lStarted);
//
//					if (lEarliestTime.get(lPredecessor).getTime() < lEndPoint.getTime())
//						lEarliestTime.put(lPredecessor, lEndPoint);
//				}
//				else
//				{
//					lPredStarted.put(lPredecessor, 1);
//					lEarliestTime.put(lPredecessor, lEndPoint);
//				}
//
//				if (lPredStarted.get(lPredecessor).equals(lPredecessor.getOutDegree()))
//				{
//					lEligible.add(lPredecessor);
//				}
//			}
//
//			//System.out.println(lScheduled + ": (" + lNextActivity.getID() + " at " + lStartPoint.getTime() + ")");
//			// - " + lEligible.size() + " - " + lStartPoint + " - " + lEndPoint);
//		}
//
//		return lSchedule;
//	}

	public static void main(String[] args) throws IOException
	{
		/*
		SerialGen lGen = new SerialGen();
		Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j30"+20+"_"+4+".sm"));
		Map<Activity, Integer> lSchedule	 = lGen.solve(lInst, new RandomRule());
		Map<Activity, Integer> lRightJustify = lGen.solveInverse(lInst, new RightJustify(lSchedule));

		// Actually perform the justify.
		int lMakespan = lSchedule.get(lInst.getActivity(32));
		for (Activity lActivity : lRightJustify.keySet())
		{
			int lReverse = lMakespan - (lRightJustify.get(lActivity) + lActivity.getDuration());
			lRightJustify.put(lActivity, lReverse);
		}

		for (int i = 1; i <= 32; i++)
		{
			Activity lAct = lInst.getActivity(i);

			System.out.println(i + "\t" + lSchedule.get(lAct)
								 + "\t" + lRightJustify.get(lAct));
		}
		SerialGen lGen = new SerialGen();

		long nanotime = 0;
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				nanotime -= System.nanoTime();
				Map<Activity, Integer> lSchedule = lGen.solve(lInst, new RandomRule());
				nanotime += System.nanoTime();
				System.out.println(i + " - " + j + ": " + lSchedule.get(lInst.getActivity(122)));
			}
			//System.out.println((double)i / 48);
		}

		System.out.println(nanotime / 1000000.0 / 480);
		/**/
	}
}
