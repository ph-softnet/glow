package in5000.sgs.experiments;

import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class TestSetProperties
{
	public static void exploreSet() throws IOException
	{
		PrintWriter lOut = new PrintWriter(new BufferedWriter(new FileWriter(new File("./new20.dat"))));

		for (int lSet = 2; lSet < 14; lSet++)
		{
			PattersonParser lParser = new PattersonParser(new File(String.format("C:/RanGen/Set-%d-%d.rcpm", lSet, 20)));

			while (lParser.hasNext())
			{
				Instance lInst = lParser.getNext();
				lOut.println((int)(lInst.getSerialParallelIndicator()*19) + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness());
			}

			System.out.println(lSet + " done.");
		}

		lOut.close();
	}

	public static void main(String[] args) throws IOException
	{
		TestSetProperties.exploreSet();
	}
}
