package in5000.sgs.experiments;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.SerialGen;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class SGSdistribution 
{
	public static void testOne()
	{
		Instance lInst = null;
		BufferedWriter lOut = null;

		try
		{
			lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+3+"-"+235+".rcp"));
			lOut  = new BufferedWriter(new FileWriter(new File("./data/rand_distr.dat")));
			lOut.append("distr\tfbidistr\tndistr\tnfbidistr\n");
		} catch (IOException e) { }
		

		System.out.println(lInst.getOrderStrength());
		if (lInst != null && lOut != null)
		{
			int[] lMakespans		= new int[25000];
			int[] lFBIMakespans		= new int[lMakespans.length];
			int   lMakespanSum		= 0;
			int   lFBIMakespanSum	= 0;

			SerialGen lGen	 = new SerialGen();
			RandomRule lRule = new RandomRule();

			for (int i = 0; i < lMakespans.length; i++)
			{
				Map<Activity, Integer> lSchedule = lGen.solve(lInst, lRule);
				Map<Activity, Integer> lFBISchedule = ForwardBackwardIteration.performOne(lInst, lSchedule);
				int lMakespan 	 = lSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
				int lFBIMakespan = lFBISchedule.get(lInst.getActivity(lInst.getNumActivities()+2));

				lMakespanSum	+= lMakespan;
				lFBIMakespanSum	+= lFBIMakespan;
				lMakespans[i]	 = lMakespan;
				lFBIMakespans[i] = lFBIMakespan;
			}

			double lMeanMakespan	= (double)lMakespanSum	  / (double)lMakespans.length;
			double lMeanFBIMakespan = (double)lFBIMakespanSum / (double)lMakespans.length;
			for (int i = 0; i < lMakespans.length; i++)
			{
				try
				{
					lOut.append((double)lMakespans[i]	 / lMeanMakespan	+ "\t"
							  + (double)lFBIMakespans[i] / lMeanFBIMakespan + "\t"
							  + lMakespans[i]								+ "\t"
							  + lFBIMakespans[i]							+ "\n");
				} catch (IOException e) { }
			}
		}

		try
		{
			lOut.close();
		} catch (Exception e) { }
	}

	public static void testThree()
	{
		Instance[] lInst = new Instance[3];
		BufferedWriter lOut = null;

		try
		{
			lInst[0] = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+3+"-"+235+".rcp"));
			lInst[1] = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+36+"-"+542+".rcp"));
			lInst[2] = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+90+"-"+462+".rcp"));

			lOut  = new BufferedWriter(new FileWriter(new File("./data/rand_distr_three.dat")));
			lOut.append("one\ttwo\tthree\n");
		} catch (IOException e) { }
		

		System.out.println(lInst[0].getOrderStrength());
		System.out.println(lInst[1].getOrderStrength());
		System.out.println(lInst[2].getOrderStrength());

//		int[][] lMakespans		= new int[3][25000];
//		int[][] lFBIMakespans	= new int[3][25000];
//		int[]   lMakespanSum	= new int[3];
//		int[]   lFBIMakespanSum	= new int[3];

		SerialGen lGen	 = new SerialGen();
		RandomRule lRule = new RandomRule();

		try
		{
			for (int i = 0; i < 25000; i++)
			{
				for (int k = 0; k < lInst.length; k++)
				{
					Map<Activity, Integer> lSchedule = lGen.solve(lInst[k], lRule);
	//				Map<Activity, Integer> lFBISchedule = ForwardBackwardIteration.performOne(lInst, lSchedule);
					int lMakespan 	 = lSchedule.get(lInst[k].getActivity(lInst[k].getNumActivities()+2));
	//				int lFBIMakespan = lFBISchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
	
					lOut.append(lMakespan + "\t");
	
	//				lMakespanSum[k]	+= lMakespan;
	//				lFBIMakespanSum[k]	+= lFBIMakespan;
	//				lMakespans[k][i] = lMakespan;
	//				lFBIMakespans[k][i] = lFBIMakespan;
				}
				lOut.append("\n");
			}
	
			lOut.close();
		} catch (Exception e) { }
	}

	public static void testAll()
	{
		BufferedWriter lOut = null;

		try
		{
			lOut  = new BufferedWriter(new FileWriter(new File("./data/rand_distr_all.dat")));
			lOut.append("normdist\n");
			lOut.append("normdistfbi\n");
			lOut.append("dist\n");
			lOut.append("distfbi\n");
		} catch (IOException e) { }
		
		SerialGen lGen	 = new SerialGen();
		RandomRule lRule = new RandomRule();

		if (lOut != null)
		{
			int tries	   = 2500;
			int blocks	   = 21;
			int files	   = 25;
			for (int j = 1; j <= blocks; j++)
			{
				for (int i = 1; i <= files; i++)
				{
					int inst = j * 3;
					if (j > 10) inst = 30 + (j-10) * 6;

					Instance lInst		= null;
					try
					{
						lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+inst+"-"+i+".rcp"));
					} catch (IOException e) { }
		
					int[] lMakespans		= new int[tries];
					int[] lFBIMakespans		= new int[lMakespans.length];
					int   lMakespanSum		= 0;
					int   lFBIMakespanSum	= 0;
					for (int k = 0; k < tries; k++)
					{
						Map<Activity, Integer> lSchedule = lGen.solve(lInst, lRule);
						Map<Activity, Integer> lFBISchedule = ForwardBackwardIteration.performOne(lInst, lSchedule);

						int lMakespan 	 = lSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
						int lFBIMakespan = lFBISchedule.get(lInst.getActivity(lInst.getNumActivities()+2));

						lMakespanSum	+= lMakespan;
						lFBIMakespanSum	+= lFBIMakespan;
						lMakespans[k]	 = lMakespan;
						lFBIMakespans[k] = lFBIMakespan;
					}
		
					double lMeanMakespan	= (double)lMakespanSum	  / (double)lMakespans.length;
					double lMeanFBIMakespan = (double)lFBIMakespanSum / (double)lMakespans.length;
					for (int k = 0; k < lMakespans.length; k++)
					{
						try
						{
							lOut.append((double)lMakespans[k]	 / lMeanMakespan	+ "\t"
									  + (double)lFBIMakespans[k] / lMeanFBIMakespan + "\t"
									  + lMakespans[k]								+ "\t"
									  + lFBIMakespans[k]							+ "\n");
						} catch (IOException e) { }
					}
				}
			}
		}

		try
		{
			lOut.close();
		} catch (Exception e) { }
	}

	public static void main(String[] args)
	{
		//SGSdistribution.testOne();
		SGSdistribution.testThree();
		//SGSdistribution.testAll();
	}
}
