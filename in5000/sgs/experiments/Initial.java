package in5000.sgs.experiments;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.ParallelGen;
import in5000.sgs.alg.SerialGen;
import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.KolischParser;
import in5000.sgs.parser.PattersonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class Initial
{
	public static void exploreJ120() throws IOException
	{
		PriorityRule lRule  = new RandomRule();
		ParallelGen lParGen = new ParallelGen();
		SerialGen   lSerGen = new SerialGen();

		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./j120_compu.dat")));

		int tries	   = 500;
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst		= KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j120"+i+"_"+j+".sm"));
				long nanotime		= 0;
				long nanotime2		= 0;
				int lBestParallel	= Integer.MAX_VALUE;
				int lBestSerial		= Integer.MAX_VALUE;

				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness() + "\t");

				for (int k = 0; k < tries; k++)
				{
					nanotime -= System.nanoTime();
					Map<Activity, Integer> lParSchedule = lParGen.solve(lInst, lRule);
					nanotime += System.nanoTime();
	
					nanotime2 -= System.nanoTime();
					Map<Activity, Integer> lSerSchedule = lSerGen.solve(lInst, lRule);
					nanotime2 += System.nanoTime();
					
					lBestParallel	= Math.min(lBestParallel, lParSchedule.get(lInst.getActivity(122)));
					lBestSerial		= Math.min(lBestSerial,   lSerSchedule.get(lInst.getActivity(122)));
				}

				lOut.append(lBestParallel				+ "\t" + 
							lBestSerial					+ "\t" +
							nanotime  / (double)tries	+ "\t" +
							nanotime2 / (double)tries	+ "\n");

				System.out.println(String.format("%2d - %2d:\t%4d\t%4d\t%f\t%f", i, j, 
																				 lBestParallel,
																				 lBestSerial,
																				 nanotime  / 1000000.0 / tries,
																				 nanotime2 / 1000000.0 / tries));
			}
			//System.out.println((double)i / 48);
		}
		lOut.close();

		//System.out.println(nanotime / 1000000.0 / 1000 / 480);
	}

	public static void explorePatSet1() throws IOException
	{
		PriorityRule lRule  = new RandomRule();
		ParallelGen lParGen = new ParallelGen();
		SerialGen   lSerGen = new SerialGen();

		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./pat_compu.dat")));

		int tries	   = 100;
		int files	   = 1000;
		for (int j = 3; j < 31; j = j + 3)
		{
			for (int i = 1; i <= files; i++)
			{
				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+j+"-"+i+".rcp"));
				long nanotime		 = 0;
				long nanotime2		 = 0;

				long lParallelSum	 = 0;
				long lSerialSum		 = 0;
				long lParallelFBIsum = 0;
				long lSerialFBIsum   = 0;

				int lBestParallel	 = Integer.MAX_VALUE;
				int lBestSerial		 = Integer.MAX_VALUE;
				int lBestParallelFBI = Integer.MAX_VALUE;
				int lBestSerialFBI	 = Integer.MAX_VALUE;
	
				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness() + "\t");
	
				for (int k = 0; k < tries; k++)
				{
					nanotime -= System.nanoTime();
					Map<Activity, Integer> lParSchedule = lParGen.solve(lInst, lRule);
					nanotime += System.nanoTime();
	
					nanotime2 -= System.nanoTime();
					Map<Activity, Integer> lSerSchedule = lSerGen.solve(lInst, lRule);
					nanotime2 += System.nanoTime();

					Map<Activity, Integer> lParScheduleFBI = ForwardBackwardIteration.performOne(lInst, lParSchedule);
					Map<Activity, Integer> lSerScheduleFBI = ForwardBackwardIteration.performOne(lInst, lSerSchedule);
					
					lParallelSum	+= lParSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
					lSerialSum		+= lSerSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
					lParallelFBIsum += lParScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2));
					lSerialFBIsum   += lSerScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2));

					lBestParallel	 = Math.min(lBestParallel,		lParSchedule.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestSerial		 = Math.min(lBestSerial,		lSerSchedule.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestParallelFBI = Math.min(lBestParallelFBI,	lParScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestSerialFBI	 = Math.min(lBestSerialFBI,		lSerScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2)));
				}
	
				double lParallelAvg		= (double)lParallelSum		/ (double)tries;
				double lSerialAvg		= (double)lSerialSum		/ (double)tries;
				double lParallelFBIavg	= (double)lParallelFBIsum	/ (double)tries;
				double lSerialFBIavg	= (double)lSerialFBIsum		/ (double)tries;
				double lParTimeAvgMs	= (double)nanotime			/ 1000000.0 / (double)tries;
				double lSerTimeAvgMs	= (double)nanotime2			/ 1000000.0 / (double)tries;

				lOut.append(lParallelAvg	+ "\t"
						+	lSerialAvg		+ "\t"
						+	lParallelFBIavg	+ "\t"
						+	lSerialFBIavg	+ "\t"
						+	lBestParallel	+ "\t"
						+	lBestSerial		+ "\t"
						+	lBestParallelFBI+ "\t"
						+	lBestSerialFBI	+ "\t"
						+	lParTimeAvgMs	+ "\t"
						+	lSerTimeAvgMs	+ "\n");
	
				if (i % 50 == 0)
					System.out.println(String.format("%2d - %4d:\t%4d\t%4d\t%f\t%f",  j, i, 
																				lBestParallel,
																				lBestSerial,
																				nanotime  / 1000000.0 / tries,
																				nanotime2 / 1000000.0 / tries));
			}
		}
		lOut.close();

		//System.out.println(nanotime / 1000000.0 / 1000 / 480);
	}

	public static void comparePriorityRule() throws IOException
	{
		PriorityRule lRandomRule  = new RandomRule();
		SerialGen   lSerGen = new SerialGen();

		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./rule_test.dat")));

		int tries	   = 100;
		int files	   = 1000;
		for (int j = 3; j < 31; j = j + 3)
		{
			for (int i = 1; i <= files; i++)
			{
				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+j+"-"+i+".rcp"));
				PriorityRule lRanLFTRule  = new BiasedRandomRule(new LatestFinishTime(lInst, false), 1);
				long nanotime		 = 0;
				long nanotime2		 = 0;

				long lRandomSum		= 0;
				long lRanLFTSum		= 0;
				long lRandomSumFBI	= 0;
				long lRanLFTSumFBI	= 0;

				int lBestRandom		= Integer.MAX_VALUE;
				int lBestRanLFT		= Integer.MAX_VALUE;
				int lBestRandomFBI	= Integer.MAX_VALUE;
				int lBestRanLFTFBI	= Integer.MAX_VALUE;
	
				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness() + "\t");
	
				for (int k = 0; k < tries; k++)
				{
					nanotime -= System.nanoTime();
					Map<Activity, Integer> lRanSchedule = lSerGen.solve(lInst, lRandomRule);
					nanotime += System.nanoTime();
	
					nanotime2 -= System.nanoTime();
					Map<Activity, Integer> lLFTSchedule = lSerGen.solve(lInst, lRanLFTRule);
					nanotime2 += System.nanoTime();

					Map<Activity, Integer> lRanScheduleFBI = ForwardBackwardIteration.performOne(lInst, lRanSchedule);
					Map<Activity, Integer> lLFTScheduleFBI = ForwardBackwardIteration.performOne(lInst, lLFTSchedule);
					
					lRandomSum		+= lRanSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
					lRanLFTSum		+= lLFTSchedule.get(lInst.getActivity(lInst.getNumActivities()+2));
					lRandomSumFBI	+= lRanScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2));
					lRanLFTSumFBI	+= lLFTScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2));

					lBestRandom		= Math.min(lBestRandom,		lRanSchedule.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestRanLFT		= Math.min(lBestRanLFT,		lLFTSchedule.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestRandomFBI	= Math.min(lBestRandomFBI,	lRanScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2)));
					lBestRanLFTFBI	= Math.min(lBestRanLFTFBI,	lLFTScheduleFBI.get(lInst.getActivity(lInst.getNumActivities()+2)));
				}
	
				double lRandomAvg		= (double)lRandomSum		/ (double)tries;
				double lRanLFTAvg		= (double)lRanLFTSum		/ (double)tries;
				double lRandomFBIavg	= (double)lRandomSumFBI		/ (double)tries;
				double lRanLFTFBIavg	= (double)lRanLFTSumFBI		/ (double)tries;
				double lParTimeAvgMs	= (double)nanotime			/ 1000000.0 / (double)tries;
				double lSerTimeAvgMs	= (double)nanotime2			/ 1000000.0 / (double)tries;

				lOut.append(lRandomAvg	+ "\t"
						+	lRanLFTAvg		+ "\t"
						+	lRandomFBIavg	+ "\t"
						+	lRanLFTFBIavg	+ "\t"
						+	lBestRandom		+ "\t"
						+	lBestRanLFT		+ "\t"
						+	lBestRandomFBI	+ "\t"
						+	lBestRanLFTFBI	+ "\t"
						+	lParTimeAvgMs	+ "\t"
						+	lSerTimeAvgMs	+ "\n");
	
				if (i % 50 == 0)
					System.out.println(String.format("%2d - %4d:\t%4d\t%4d\t%f\t%f",  j, i, 
																				lBestRandom,
																				lBestRanLFT,
																				nanotime  / 1000000.0 / tries,
																				nanotime2 / 1000000.0 / tries));
			}
		}
		lOut.close();

		//System.out.println(nanotime / 1000000.0 / 1000 / 480);
	}

	public static void main(String[] args)
	{
		try
		{
			Initial.comparePriorityRule();
		}
		catch (IOException e)
		{
		}
	}
}
