package in5000.sgs.experiments;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.GenerationScheme;
import in5000.sgs.alg.ParallelGen;
import in5000.sgs.alg.SerialGen;
import in5000.sgs.alg.SerialInsertionGen;
import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.alg.rules.RandomRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class TestInsertionGen
{
	public static void bnaicNewExperiment() throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/bnaic.dat")));

		int tries	   = 500;
		int blocks	   = 21;
		int files	   = 1000;
		long lStartTime = System.nanoTime();
		for (int j = 1; j <= blocks; j++)
		{
			for (int i = 1; i <= files; i++)
			{
				int inst = j * 3;

				if (j > 10) inst = 30 + (j-10) * 6;

				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+inst+"-"+i+".rcp"));

				PriorityRule[] lRules = new PriorityRule[] { new RandomRule(),
						 new BiasedRandomRule(new LatestFinishTime(lInst, false), 1) };

				ScheduleGenerationTester[] lTests = new ScheduleGenerationTester[] {
						 new ScheduleGenerationTester(new SerialGen(), lRules)
						,new ScheduleGenerationTester(new ParallelGen(), lRules)
				};

				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
						lInst.getDistributionIndicator()   + "\t" +
						lInst.getShortArcIndicator()	   + "\t" +
						lInst.getLongArcIndicator()		   + "\t" +
						lInst.getTopologicalIndicator()	   + "\t" +
						lInst.getOrderStrength()		   + "\t" +
						lInst.getResourceFactor()		   + "\t" +
						lInst.getResourceStrength()		   + "\t" +
						lInst.getResourceConstrainedness());

				for (int k = 0; k < lTests.length; k++)
				{
					lTests[k].testGS_All(lInst, tries, lOut);
				}

				lOut.append("\n");
				lOut.flush();

				long lCurrentTime = System.nanoTime();
				int lFilesDone = (j-1)*files+i;
				int lFilesToDo =  (blocks*files) - lFilesDone;
				double lTimePerFile = (double)(lCurrentTime - lStartTime) / (double)(lFilesDone);
				double lTimeToGo    = lFilesToDo * lTimePerFile;
				System.out.println(String.format("%4d (%2d-%4d) / %3.2f%%:\t%12fs to go ",
												lFilesDone,
												inst, i,
												((double)lFilesDone / (double)(blocks*files))*100.0,
												lTimeToGo / 1000000000.0));
			}
		}

		lOut.close();
	}

	public static void comparePriorityRule3() throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/priority2.dat")));

		int tries	   = 250;
		int blocks	   = 11;
		int files	   = 1000;
		long lStartTime = System.nanoTime();
		for (int j = 1; j <= blocks; j++)
		{
			for (int i = 1; i <= files; i++)
			{
				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(30+j*6)+"-"+i+".rcp"));

				PriorityRule[] lRules = new PriorityRule[] { new RandomRule(),
						 new BiasedRandomRule(new LatestFinishTime(lInst, false), 1) };

				ScheduleGenerationTester[] lTests = new ScheduleGenerationTester[] {
						 new ScheduleGenerationTester(new SerialGen(), lRules)
						,new ScheduleGenerationTester(new ParallelGen(), lRules)
				};

				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
						lInst.getDistributionIndicator()   + "\t" +
						lInst.getShortArcIndicator()	   + "\t" +
						lInst.getLongArcIndicator()		   + "\t" +
						lInst.getTopologicalIndicator()	   + "\t" +
						lInst.getOrderStrength()		   + "\t" +
						lInst.getResourceFactor()		   + "\t" +
						lInst.getResourceStrength()		   + "\t" +
						lInst.getResourceConstrainedness());

				for (int k = 0; k < lTests.length; k++)
				{
					lTests[k].testGS(lInst, tries, lOut);
				}

				lOut.append("\n");
				lOut.flush();

				long lCurrentTime = System.nanoTime();
				int lFilesDone = (j-1)*files+i;
				int lFilesToDo =  (blocks*files) - lFilesDone;
				double lTimePerFile = (double)(lCurrentTime - lStartTime) / (double)(lFilesDone);
				double lTimeToGo    = lFilesToDo * lTimePerFile;
				System.out.println(String.format("%4d (%2d-%4d) / %3.2f%%:\t%12fs to go ",
												lFilesDone,
												30+j*6, i,
												((double)lFilesDone / (double)(10 *files))*100.0,
												lTimeToGo / 1000000000.0));
			}
		}

		lOut.close();
	}

	public static void comparePriorityRule2() throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/priority.dat")));

		int tries	   = 250;
		int files	   = 1000;
		long lStartTime = System.nanoTime();
		for (int j = 1; j <= 10; j++)
		{
			for (int i = 1; i <= files; i++)
			{
				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(j*3)+"-"+i+".rcp"));

				PriorityRule[] lRules = new PriorityRule[] { new RandomRule(),
						 new BiasedRandomRule(new LatestFinishTime(lInst, false), 1) };

				ScheduleGenerationTester[] lTests = new ScheduleGenerationTester[] {
						 new ScheduleGenerationTester(new SerialGen(), lRules)
						,new ScheduleGenerationTester(new ParallelGen(), lRules)
				};


				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
						lInst.getDistributionIndicator()   + "\t" +
						lInst.getShortArcIndicator()	   + "\t" +
						lInst.getLongArcIndicator()		   + "\t" +
						lInst.getTopologicalIndicator()	   + "\t" +
						lInst.getOrderStrength()		   + "\t" +
						lInst.getResourceFactor()		   + "\t" +
						lInst.getResourceStrength()		   + "\t" +
						lInst.getResourceConstrainedness());

				for (int k = 0; k < lTests.length; k++)
				{
					lTests[k].testGS(lInst, tries, lOut);
				}

				lOut.append("\n");
				lOut.flush();

				long lCurrentTime = System.nanoTime();
				int lFilesDone = (j-1)*files+i;
				int lFilesToDo =  (10 *files) - lFilesDone;
				double lTimePerFile = (double)(lCurrentTime - lStartTime) / (double)(lFilesDone);
				double lTimeToGo    = lFilesToDo * lTimePerFile;
				System.out.println(String.format("%4d (%2d-%3d) / %3.2f%%:\t%12fs to go ",
												lFilesDone,
												j, i,
												((double)lFilesDone / (double)(10 *files))*100.0,
												lTimeToGo / 1000000000.0));
			}
		}

		lOut.close();
	}

	public static void comparePriorityRule() throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/combined.dat")));

		int tries	   = 50;
		int files	   = 1000;
		long lStartTime = System.nanoTime();
		for (int j = 1; j <= 10; j++)
		{
			for (int i = 1; i <= files; i++)
			{
				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(j*3)+"-"+i+".rcp"));

				PriorityRule[] lRules = new PriorityRule[] { new BiasedRandomRule(new LatestFinishTime(lInst, false), 1) };

				ScheduleGenerationTester[] lTests = new ScheduleGenerationTester[] {
											 new ScheduleGenerationTester(new SerialGen(), lRules)
											,new ScheduleGenerationTester(new ParallelGen(), lRules)
											,new ScheduleGenerationTester(new SerialInsertionGen(), lRules)
									};

				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
						lInst.getDistributionIndicator()   + "\t" +
						lInst.getShortArcIndicator()	   + "\t" +
						lInst.getLongArcIndicator()		   + "\t" +
						lInst.getTopologicalIndicator()	   + "\t" +
						lInst.getOrderStrength()		   + "\t" +
						lInst.getResourceFactor()		   + "\t" +
						lInst.getResourceStrength()		   + "\t" +
						lInst.getResourceConstrainedness());

				for (int k = 0; k < lTests.length; k++)
				{
					lTests[k].testGS(lInst, tries, lOut);
				}

				lOut.append("\n");
				lOut.flush();

				long lCurrentTime = System.nanoTime();
				int lFilesDone = (j-1)*files+i;
				int lFilesToDo =  (10 *files) - lFilesDone;
				double lTimePerFile = (double)(lCurrentTime - lStartTime) / (double)(lFilesDone);
				double lTimeToGo    = lFilesToDo * lTimePerFile;
				System.out.println(String.format("%4d (%2d-%3d) / %3.2f%%:\t%12fs to go ",
												lFilesDone,
												j, i,
												((double)lFilesDone / (double)(10 *files))*100.0,
												lTimeToGo / 1000000000.0));
			}
		}

		lOut.close();
	}

	public static void main(String[] args)
	{
		try
		{
			TestInsertionGen.bnaicNewExperiment();
		}
		catch (IOException e) {}
	}
}

class ScheduleGenerationTester
{
	private final GenerationScheme fScheme;
	private final PriorityRule[] fRules;

	private long[] fNanoTime;
	private int[]  fWorstMakespan;
	private int[]  fAverageMakespan;
	private int[]  fBestMakespan;
	private int[]  fFBIWorstMakespan;
	private int[]  fFBIAverageMakespan;
	private int[]  fFBIBestMakespan;

	public ScheduleGenerationTester(GenerationScheme pScheme, PriorityRule[] pRules)
	{
		this.fScheme = pScheme;
		this.fRules  = pRules;

		this.fNanoTime			 = new long[fRules.length];
		this.fWorstMakespan		 = new int[fRules.length];
		this.fAverageMakespan	 = new int[fRules.length];
		this.fBestMakespan		 = new int[fRules.length];
		this.fFBIWorstMakespan	 = new int[fRules.length];
		this.fFBIAverageMakespan = new int[fRules.length];
		this.fFBIBestMakespan	 = new int[fRules.length];
	}

	public void testGS(Instance pInstance, int pRuns, BufferedWriter pOut) throws IOException
	{
		Activity lSink = pInstance.getActivity(pInstance.getNumActivities()+2);
		for (int i = 0; i < fRules.length; i++)
		{
			this.fNanoTime[i]			= 0;
			this.fWorstMakespan[i]		= 0;
			this.fAverageMakespan[i]	= 0;
			this.fBestMakespan[i]		= Integer.MAX_VALUE;
			this.fFBIWorstMakespan[i]	= 0;
			this.fFBIAverageMakespan[i] = 0;
			this.fFBIBestMakespan[i]	= Integer.MAX_VALUE;

			for (int j = 0; j < pRuns; j++)
			{
				this.fNanoTime[i] -= System.nanoTime();
				Map<Activity, Integer> lSchedule = fScheme.solve(pInstance, this.fRules[i]);
				this.fNanoTime[i] += System.nanoTime();
				Map<Activity, Integer> lFBISchedule = ForwardBackwardIteration.performOne(pInstance, lSchedule);

				//VerifySchedule.verify(pInstance, lSchedule);
				//VerifySchedule.verify(pInstance, lFBISchedule);

				int lMakeSpan	 = lSchedule.get(lSink);
				this.fWorstMakespan[i]		 = Math.max(this.fWorstMakespan[i], lMakeSpan);
				this.fAverageMakespan[i]	+= lMakeSpan;
				this.fBestMakespan[i]		 = Math.min(this.fBestMakespan[i], lMakeSpan);

				int lFBIMakeSpan = lFBISchedule.get(lSink);
				this.fFBIWorstMakespan[i]	 = Math.max(this.fFBIWorstMakespan[i], lMakeSpan);
				this.fFBIAverageMakespan[i]	+= lFBIMakeSpan;
				this.fFBIBestMakespan[i]	 = Math.min(this.fFBIBestMakespan[i], lFBIMakeSpan);
			}

			double lAvgTime		 = (double)this.fNanoTime[i]	/ 1000000.0 / (double)pRuns;
			double lAvgMkSpan	 = (double)this.fAverageMakespan[i] / (double)pRuns;
			double lAvgFBIMkSpan = (double)this.fFBIAverageMakespan[i] / (double)pRuns;

			pOut.append(							  "\t"
					+	lAvgTime					+ "\t"
					+	this.fWorstMakespan[i]		+ "\t"
					+	lAvgMkSpan					+ "\t"
					+	this.fBestMakespan[i]		+ "\t"
					+	this.fFBIWorstMakespan[i]	+"\t"
					+	lAvgFBIMkSpan				+ "\t"
					+	this.fFBIBestMakespan[i]);
		}
	}

	public void testGS_All(Instance pInstance, int pRuns, BufferedWriter pOut) throws IOException
	{
		Activity lSink = pInstance.getActivity(pInstance.getNumActivities()+2);
		for (int i = 0; i < fRules.length; i++)
		{
			this.fNanoTime[i]			= 0;
			this.fWorstMakespan[i]		= 0;
			this.fAverageMakespan[i]	= 0;
			this.fBestMakespan[i]		= Integer.MAX_VALUE;
			this.fFBIWorstMakespan[i]	= 0;
			this.fFBIAverageMakespan[i] = 0;
			this.fFBIBestMakespan[i]	= Integer.MAX_VALUE;

			for (int j = 0; j < pRuns; j++)
			{
				this.fNanoTime[i] -= System.nanoTime();
				Map<Activity, Integer> lSchedule = fScheme.solve(pInstance, this.fRules[i]);
				this.fNanoTime[i] += System.nanoTime();
				Map<Activity, Integer> lFBISchedule = ForwardBackwardIteration.performAll(pInstance, lSchedule, 100);

				//VerifySchedule.verify(pInstance, lSchedule);
				//VerifySchedule.verify(pInstance, lFBISchedule);

				int lMakeSpan	 = lSchedule.get(lSink);
				this.fWorstMakespan[i]		 = Math.max(this.fWorstMakespan[i], lMakeSpan);
				this.fAverageMakespan[i]	+= lMakeSpan;
				this.fBestMakespan[i]		 = Math.min(this.fBestMakespan[i], lMakeSpan);

				int lFBIMakeSpan = lFBISchedule.get(lSink);
				this.fFBIWorstMakespan[i]	 = Math.max(this.fFBIWorstMakespan[i], lMakeSpan);
				this.fFBIAverageMakespan[i]	+= lFBIMakeSpan;
				this.fFBIBestMakespan[i]	 = Math.min(this.fFBIBestMakespan[i], lFBIMakeSpan);
			}

			double lAvgTime		 = (double)this.fNanoTime[i]	/ 1000000.0 / (double)pRuns;
			double lAvgMkSpan	 = (double)this.fAverageMakespan[i] / (double)pRuns;
			double lAvgFBIMkSpan = (double)this.fFBIAverageMakespan[i] / (double)pRuns;

			pOut.append(							  "\t"
					+	lAvgTime					+ "\t"
					+	this.fWorstMakespan[i]		+ "\t"
					+	lAvgMkSpan					+ "\t"
					+	this.fBestMakespan[i]		+ "\t"
					+	this.fFBIWorstMakespan[i]	+"\t"
					+	lAvgFBIMkSpan				+ "\t"
					+	this.fFBIBestMakespan[i]);
		}
	}
}