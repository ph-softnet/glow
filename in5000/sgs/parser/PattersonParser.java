package in5000.sgs.parser;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.Resource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class PattersonParser
{
	private final Scanner fMergedFile;
	private final int fSize;
	private int fNumRead;

	public PattersonParser(File pMergedFile) throws IOException
	{
		// Open the Merged Instance File.
		this.fMergedFile = new Scanner(pMergedFile);
		this.fSize		 = this.fMergedFile.nextInt();
		this.fNumRead	 = 0;
	}

	public int getSize()
	{
		return this.fSize;
	}

	public int getCurrent()
	{
		return this.fNumRead;
	}

	public boolean hasNext()
	{
		return this.fNumRead < this.fSize;
	}

	public Instance getNext()
	{
		this.fNumRead++;

		return PattersonParser.parseFile(this.fMergedFile);
	}

	public void skipInstance()
	{
		this.fNumRead++;
		int lNumActivities	= this.fMergedFile.nextInt();
		for (int i = 0; i < lNumActivities+2; i++)
			this.fMergedFile.nextLine();
	}

	public void copyInstance(PrintWriter pOut)
	{
		this.fNumRead++;

		String lFirstLine = this.fMergedFile.nextLine();
		if (!(lFirstLine.length() > 0))
			lFirstLine = this.fMergedFile.nextLine();

		pOut.println(lFirstLine);
		int lNumActivities = Integer.valueOf(lFirstLine.split("[ ]+")[1]);
		for (int i = 0; i < lNumActivities+1; i++)
			pOut.println(this.fMergedFile.nextLine());
	}

	public static Instance parseFile(File pInstanceFile) throws IOException
	{
		// Open the Instance File.
		Scanner lScanner = new Scanner(pInstanceFile);

		// Read the Instance.
		Instance lResult = parseFile(lScanner);

		// Finished reading instance file, close scanner.
		lScanner.close();

		return lResult;
	}

	public static Instance parseFile(Scanner lScanner)
	{
		/* Construct the Instance. */
		Instance lInstance = new Instance();

		/* Line 1: Instance Size */
		int lNumActivities	= lScanner.nextInt();
		int lNumResources	= lScanner.nextInt();

		/* Line 2: Resource Capacities */
		int[] lResourceCap		= new int[lNumResources];
		Resource[] lResources	= new Resource[lNumResources];
		for (int i = 0; i < lNumResources; i++)
		{
			lResourceCap[i] = lScanner.nextInt();
			lResources[i]	= lInstance.getResource(i);
			lResources[i].setCapacity(lResourceCap[i]);
		}

		/* Activities */
		for (int lActivityID = 1; lActivityID <= lNumActivities; lActivityID++)
		{
			Activity lActivity = lInstance.addActivity(lActivityID);

			/* Activity Duration */
			int lDuration = lScanner.nextInt();
			lActivity.setDuration(lDuration);

			/* Resource Usage */
			int[] lUsage = new int[lNumResources];
			for (int i = 0; i < lNumResources; i++)
			{
				lUsage[i] = lScanner.nextInt();
				lActivity.setResourceUse(lResources[i], lUsage[i]);
			}

			/* Successors */
			int lNumSuccessors = lScanner.nextInt();
			int[] lSuccessors = new int[lNumSuccessors];
			for (int i = 0; i < lNumSuccessors; i++)
			{
				lSuccessors[i] = lScanner.nextInt();
				Activity lSuccessor = lInstance.getActivity(lSuccessors[i]);
				lInstance.addPrecedenceConstraint(lActivity, lSuccessor);
			}
		}

		// Finalize the instance.
		lInstance.initialize();

		// Return the instance.
		return lInstance;
	}

	public static void pattersonToTrains(File pInstanceFile, File pOutputFile) throws IOException
	{
		Scanner lScanner = new Scanner(pInstanceFile);
		BufferedWriter lWriter = new BufferedWriter(new FileWriter(pOutputFile));

		/* Line 1: Instance Size */
		int lNumActivities	= lScanner.nextInt();
		int lNumResources	= lScanner.nextInt();

		/* Line 2: Resource Capacities */
		for (int i = 0; i < lNumResources; i++)
		{
			int lCapacity = lScanner.nextInt();
			lWriter.append(String.format("R %d %d \"%s\"\n", i, lCapacity, String.format("R %d", i+1)));
		}

		lWriter.append(String.format("T 0 0 800 \"%s\"\n", pOutputFile.getName()));

		/* Activities */
		int[][] lUsage = new int[lNumActivities][lNumResources];
		int[][] lPrecedences = new int[lNumActivities][lNumActivities];

		for (int lActivityID = 0; lActivityID < lNumActivities; lActivityID++)
		{
			/* Activity Duration */
			int lDuration = lScanner.nextInt();
			if (lDuration < 1) lDuration = 1;

			lWriter.append(String.format("A 0 %d %d \"%s\"\n", lActivityID, lDuration, String.format("A %d", lActivityID+1)));

			/* Resource Usage */
			for (int i = 0; i < lNumResources; i++)
			{
				lUsage[lActivityID][i] = lScanner.nextInt();
			}



			/* Successors */
			lPrecedences[lActivityID][0] = lScanner.nextInt();
			for (int i = 1; i <= lPrecedences[lActivityID][0]; i++)
			{
				lPrecedences[lActivityID][i] = lScanner.nextInt()-1;
			}
		}

		for (int i = 0; i < lNumResources; i++)
			for (int lActivityID = 0; lActivityID < lNumActivities; lActivityID++)
				if (lUsage[lActivityID][i] > 0)
					lWriter.append(String.format("Q 0 %d %d %d\n", lActivityID, i, lUsage[lActivityID][i]));

		for (int lActivityID = 0; lActivityID < lNumActivities; lActivityID++)
			for (int i = 1; i <= lPrecedences[lActivityID][0]; i++)
				lWriter.append(String.format("P 0 %d 0 %d\n", lActivityID, lPrecedences[lActivityID][i]));

		// Finished reading instance file, close scanner.
		lScanner.close();
		lWriter.close();
	}

	private static void testMultiParser() throws IOException
	{
		PattersonParser lParser = new PattersonParser(new File("C:/RanGen/Set-12-20.rcpm"));

		int lBins = 500;
		int[] lOSs = new int[lBins+1];
		int lRead = 0;
		while (lParser.hasNext())
		{
			Instance lInstance = lParser.getNext();
			if (lRead == 0)
				System.out.println(lInstance.getSerialParallelIndicator());
			lOSs[(int)Math.floor(lInstance.getOrderStrength()*lBins)]++;
			lRead++;
		}
		System.out.println(lRead);
		for (int i = 0; i < lOSs.length; i++)
			if (lOSs[i] > 0)
				System.out.println(i + "\t" + lOSs[i]);		
	}

	public static void main(String[] args) throws IOException
	{
		testMultiParser();

		/*
		File lInDirectory  = new File("C:/RanGen/Output/");
		File lOutDirectory = new File("C:/RanGen/Trans/");

		int i = 0;
		for (String lPath : lInDirectory.list())
		{
			File lInput  = new File(lInDirectory.getPath() + '/' + lPath);
			File lOutput = new File(lOutDirectory.getPath() + '/' + lInput.getName().split("[.]")[0] + ".instance");

			PattersonParser.pattersonToTrains(lInput, lOutput);

			i++;
			if (i >= 3) break;
		}
		/**/

		/*
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./pat_indi.dat")));
		for (int j = 3; j < 31; j = j + 3)
		{
			for (int i = 1; i <= 1000; i++)
			{
				Instance lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+j+"-"+i+".rcp"));
				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
						lInst.getDistributionIndicator()   + "\t" +
						lInst.getShortArcIndicator()	   + "\t" +
						lInst.getLongArcIndicator()		   + "\t" +
						lInst.getTopologicalIndicator()	   + "\t" +
						lInst.getOrderStrength()		   + "\t" +
						lInst.getResourceFactor()		   + "\t" +
						lInst.getResourceStrength()		   + "\t" +
						lInst.getResourceConstrainedness() + "\n");
	
				if (i % 100 == 0)
					System.out.println(i / 5000.0);
			}
		}

		lOut.close();
		/**/
	}
}
