package in5000.sgs.parser;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;
import in5000.sgs.data.ResourceFlow;

import java.util.List;
import java.util.Map;

public class MichelSolutionWriter
{
	public static void outputSchedule(Map<Activity, Integer> pSchedule, List<ResourceFlow> pPosts, Instance pInstance)
	{
		int lNumActivities = pInstance.getNumActivities()+2;

		// Precedence Constraints.
		for (ResourceFlow lFlow : pPosts)
		{
			// EST constraints.
			for (int i = 1; i <= lNumActivities; i++)
			{
				Activity lActivity = pInstance.getActivity(i);
				lActivity.resetTiming();
			}

			// LST propagation.
			Activity lHead = pInstance.getActivity(1);
			Activity lTail = pInstance.getActivity(lNumActivities);
			lHead.computeESTschedule(0);
			lTail.computeLSTschedule(Math.max(pInstance.getDeadline(), lTail.getEST() + lTail.getDuration()));

			// Output Solution.
			System.out.print("STATE: ");
			for (int i = 1; i <= lNumActivities; i++)
			{
				Activity lActivity = pInstance.getActivity(i);

				System.out.print("0 " + lActivity.getEST() + " " + lActivity.getLST() + " 1 0 " + (i-1) + " ");
			}
			System.out.println("-1");

			pInstance.addPrecedenceConstraint(lFlow.getFrom().getActivity(), lFlow.getTo().getActivity());

			System.out.println("PC: 0 " + (lFlow.getFrom().getActivity().getID()-1) + " 0 " + (lFlow.getTo().getActivity().getID()-1));
		}

		// EST constraints.
		for (int i = 1; i <= lNumActivities; i++)
		{
			Activity lActivity = pInstance.getActivity(i);
			lActivity.resetTiming();
		}

		// LST propagation.
		Activity lHead = pInstance.getActivity(1);
		Activity lTail = pInstance.getActivity(lNumActivities);
		lHead.computeESTschedule(0);
		lTail.computeLSTschedule(Math.max(pInstance.getDeadline(), lTail.getEST() + lTail.getDuration()));

		// Output Solution.
		System.out.print("STATE: ");
		for (int i = 1; i <= lNumActivities; i++)
		{
			Activity lActivity = pInstance.getActivity(i);
			System.out.print("0 " + lActivity.getEST() + " " + lActivity.getLST() + " 1 0 " + (i-1) + " ");
		}
		System.out.println("-1");

		if (pInstance.getDeadline() >= lTail.getEST() + lTail.getDuration())
			System.out.println("Instance solved.");
		else
			System.out.println("Instance not solved.");
	}
}
