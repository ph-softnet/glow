package in5000.sgs.parser;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class KolischParser
{
	public static Instance parseKolischFile(File pInstanceFile) throws IOException
	{
		// Construct reader for the input file.
		BufferedReader lInputReader = new BufferedReader(new FileReader(pInstanceFile));

		// Construct instance.
		Instance lInstance = new Instance();

		// Read header lines.
		for (int i = 0; i < 18; i++)
			lInputReader.readLine();

		// Read the first task.
		String lTaskline = lInputReader.readLine();
		while (!lTaskline.startsWith("*"))
		{
			String[] lSplit = lTaskline.trim().split("[ ]+");

			int lID   = Integer.valueOf(lSplit[0]);
			Activity lActivity = lInstance.addActivity(lID);

			int lNumSucc = Integer.valueOf(lSplit[2]);
			for (int i = 0; i < lNumSucc; i++)
			{
				int lSucc = Integer.valueOf(lSplit[3+i]);
				lInstance.addPrecedenceConstraint(lActivity, lInstance.getActivity(lSucc));
			}

			lTaskline = lInputReader.readLine();
		}

		// Read the subheader
		for (int i = 0; i < 3; i++)
			lInputReader.readLine();

		// Read the first task.
		lTaskline = lInputReader.readLine();
		while (!lTaskline.startsWith("*"))
		{
			String[] lSplit = lTaskline.trim().split("[ ]+");

			int lID   = Integer.valueOf(lSplit[0]);
			Activity lActivity = lInstance.getActivity(lID);

			int lDuration = Integer.valueOf(lSplit[2]);
			lActivity.setDuration(lDuration);
			
			for (int i = 0; i < lSplit.length-3; i++)
			{
				int lUse = Integer.valueOf(lSplit[3+i]);
				lActivity.setResourceUse(lInstance.getResource(i), lUse);
			}

			lTaskline = lInputReader.readLine();
		}

		// Read the subheader
		for (int i = 0; i < 2; i++)
			lInputReader.readLine();

		// Set resource capacities.
		lTaskline = lInputReader.readLine();
		String[] lSplit = lTaskline.trim().split("[ ]+");
		for (int i = 0; i < lSplit.length; i++)
		{
			int lCap = Integer.valueOf(lSplit[i]);
			lInstance.getResource(i).setCapacity(lCap);
		}

		// Close the input.
		lInputReader.close();

		// Compute instance statistics.
		lInstance.initialize();

		// Return the instance.
		return lInstance;
	}

	public static void main(String[] args) throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./j60_indi.dat")));
		for (int i = 1; i <= 48; i++)
		{
			for (int j = 1; j <= 10; j++)
			{
				Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j60"+i+"_"+j+".sm"));
				lOut.append(lInst.getSerialParallelIndicator() + "\t" + 
							lInst.getDistributionIndicator()   + "\t" +
							lInst.getShortArcIndicator()	   + "\t" +
							lInst.getLongArcIndicator()		   + "\t" +
							lInst.getTopologicalIndicator()	   + "\t" +
							lInst.getOrderStrength()		   + "\t" +
							lInst.getResourceFactor()		   + "\t" +
							lInst.getResourceStrength()		   + "\t" +
							lInst.getResourceConstrainedness() + "\n");
			}
			System.out.println((double)i / 48);
		}
		lOut.close();

		/*
		String lDotFilename = "./temp.dot";

		lInst.toDot(new File(lDotFilename));

		Process dot = new ProcessBuilder("dot", lDotFilename, "-Tpdf", "-O").start();

		try 
		{
			dot.waitFor();
		} 
		catch (InterruptedException e) { }

		System.out.println("I1 = " + lInst.getNumActivities());
		/**/
	}
}
