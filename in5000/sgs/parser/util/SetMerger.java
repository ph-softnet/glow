package in5000.sgs.parser.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The generator RanGen2 produces RCPSP instances one per file. This class contains methods to
 * merge such a generated set of files into one single file. The purpose is to reduce strain on the file system and OS,
 * and to limit the number of calls to open / close file(reader)s.
 * 
 * @author Frits de Nijs
 */
public class SetMerger
{
	/**
	 * Collects all non-empty lines from all files in directory pInDir into a single file pOutFile.
	 * The first line of pOutFile indicates how many files were concatenated in this way.
	 * 
	 * @param pInDir	Directory containing only RCPSP instance files.
	 * @param pOutFile	File that will after invocation contain all files concatenated.
	 * @param pDelete	If true, causes appended files to be deleted, cleaning the output folder.
	 * 
	 * @throws IOException
	 */
	public static void mergeSet(File pInDir, File pOutFile, boolean pDelete) throws IOException
	{
		// Open the output file.
		PrintWriter lOut = new PrintWriter(new BufferedWriter(new FileWriter(pOutFile)));

		// Discover the files to merge.
		String[] lFilesToMerge = pInDir.list();

		// Print the number of appended files.
		lOut.println(lFilesToMerge.length);

		// Append each file.
		int lAppended	 = 0;
		int lFivePercent = lFilesToMerge.length / 20;
		for (String lFileToMerge : lFilesToMerge)
		{
			// Open a handle to read the file.
			File		   lFile   = new File(pInDir.getPath() + '/' + lFileToMerge);
			BufferedReader lReader = new BufferedReader(new FileReader(lFile));
			String		   lRead   = null;

			// Append all non-zero lines into the new file.
			while ((lRead = lReader.readLine()) != null)
				if (lRead.length() > 1)
					lOut.println(lRead);

			// Close file reader handle.
			lReader.close();

			// Optionally, delete the file (permanently!).
			if (pDelete)
				lFile.deleteOnExit();

			// Indicate each 5%.
			if (++lAppended % lFivePercent == 0)
				System.out.println(lAppended * 100.0 / lFilesToMerge.length);
		}

		// Close file writer handle, also flushing it.
		lOut.close();
	}

	/*
	public static void compressSet(File pInFile, File pOutFile) throws IOException
	{
		// Open the Instance File.
		Scanner lReader = new Scanner(pInFile);

		// Open the Output File.
		RandomAccessFile lWriter = new RandomAccessFile(pOutFile, "rw");

		// Read how many files there are as an int.
		int lNumFiles = lReader.nextInt();
		lWriter.writeInt(lNumFiles);

		while (lReader.hasNext())
		{
			int lInt = lReader.nextInt();
			lWriter.writeShort((short)lInt);
		}

		lReader.close();
		lWriter.close();
	}

	public static void main(String[] args) throws IOException
	{
		//SetMerger.mergeSet(new File("C:/RanGen/Output/"), new File("C:/RanGen/Set-2-20.rcpm"), true);
		//SetMerger.compressSet(new File("C:/RanGen/Set-2-20.rcpm"), new File("C:/RanGen/Set-2-20.rcps"));
	}
	/**/
}
