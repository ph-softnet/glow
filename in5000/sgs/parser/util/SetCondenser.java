package in5000.sgs.parser.util;

import in5000.sgs.data.Instance;
import in5000.sgs.parser.PattersonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.PriorityQueue;

public class SetCondenser
{
	private static InstanceSet[][] collectByLevel(PattersonParser pParser, int pOSlvls, int pI5lvls)
	{
		InstanceSet[][] lOrderedSet = new InstanceSet[pOSlvls][pI5lvls];

		for (int i = 0; i < pOSlvls; i++)
			for (int j = 0; j < pI5lvls; j++)
				lOrderedSet[i][j] = new InstanceSet();

		int lRead = 0;
		int lSize = pParser.getSize();
		while (pParser.hasNext())
		{
			Instance lInstance = pParser.getNext();
			
			int x = (int)Math.floor(lInstance.getOrderStrength() * (pOSlvls-1));
			int y = (int)Math.floor(lInstance.getShortArcIndicator() * (pI5lvls-1));
			lOrderedSet[x][y].add(lRead);

			lRead++;
			if (lRead % 500 == 0)
				System.out.println(lRead*100.0/lSize);
		}

		return lOrderedSet;
	}

	private static InstanceSet selectUniformFromLevels(InstanceSet[][] pSet, int pOSlvls, int pI5lvls)
	{
		InstanceSet lElected = new InstanceSet();
		int lDesired = pOSlvls * pI5lvls;
		while (lElected.size() < lDesired)
		{
			for (int i = 0; i < pOSlvls && (lElected.size() < lDesired); i++)
			{
				for (int j = 0; j < pI5lvls && (lElected.size() < lDesired); j++)
				{
					if (!pSet[i][j].isEmpty())
					{
						lElected.add(pSet[i][j].poll());
					}
				}
			}
		}

		return lElected;
	}

	private static void copySelectedSets(InstanceSet pSelected, File pInSet, File pOutSet) throws IOException
	{
		PrintWriter lOut = new PrintWriter(new BufferedWriter(new FileWriter(pOutSet)));
		
		lOut.println(pSelected.size());

		PattersonParser lParser = new PattersonParser(pInSet);
		while (!pSelected.isEmpty())
		{
			int lNextInstance = pSelected.poll();

			while (lParser.getCurrent() != lNextInstance)
				lParser.skipInstance();

			lParser.copyInstance(lOut);
		}

		lOut.close();
	}

	public static void condenseSet(File pInSet, File pOutSet, int pOSlvls, int pI5lvls) throws IOException
	{
		PattersonParser lParser = new PattersonParser(pInSet);

		InstanceSet[][] lLeveledSets = SetCondenser.collectByLevel(lParser, pOSlvls, pI5lvls);

		InstanceSet lElected = SetCondenser.selectUniformFromLevels(lLeveledSets, pOSlvls, pI5lvls);

		SetCondenser.copySelectedSets(lElected, pInSet, pOutSet);
	}

	public static void main(String[] args) throws IOException
	{
		int lSetID		= 13;
		int lActivities	= 20;

		File lSourceDir		= new File("C:/RanGen/Output/");
		File lMergedFile	= new File(String.format("C:/RanGen/Temp-%d-%d.rcpm", lSetID, lActivities));
		File lCondensedFile	= new File(String.format("C:/RanGen/Set-%d-%d.rcpm", lSetID, lActivities));

		SetMerger.mergeSet(lSourceDir, lMergedFile, true);
		SetCondenser.condenseSet(lMergedFile, lCondensedFile, 100, 25);
	}

	/*
	public static void main(String[] args)
	{
		File lDirectory = new File("C:/RanGen/Output/");
		File lMergeFile = new File("C:/RanGen/Set-2.rcpm");
		int lOSlvl = 100;
		int lI5lvl = 25;
		PrintWriter out = null;

		try
		{
			out = new PrintWriter(new BufferedWriter(new FileWriter(lMergeFile)));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		InstanceSet[][] lOrderedSet = new InstanceSet[lOSlvl+1][lI5lvl+1];


		for (int i = 0; i <= lOSlvl; i++)
			for (int j = 0; j <= lI5lvl; j++)
				lOrderedSet[i][j] = new InstanceSet();

		System.out.println(lDirectory.isDirectory());
		try
		{
			int lSeen	  = 0;
			int lMax   	  = lDirectory.list().length;
			//double lChunk = lMax / 100.0;
			for (String lPath : lDirectory.list())
			{
				//System.out.println(lDirectory.getPath() + "\\" + lPath);
				Instance lInstance = PattersonParser.parseFile(new File(lDirectory.getPath() + '\\' + lPath));
				lSeen++;

				//RandomRule lRule = new RandomRule();
				//SerialGen lGen   = new SerialGen();
				//VerifySchedule.verify(lInstance, lGen.solve(lInstance, lRule));
				//System.out.println(lInstance.getOrderStrength());
				//if (lSeen % lChunk == 0)
				//System.out.println(lInstance.getResourceConstrainedness() + "\t" + lInstance.getResourceStrength());
				
				int x = (int)Math.floor(lInstance.getOrderStrength() * lOSlvl);
				int y = (int)Math.floor(lInstance.getShortArcIndicator() * lI5lvl);
				lOrderedSet[x][y].add(lDirectory.getPath() + '\\' + lPath);

				if (lSeen % 500 == 0)
					//break;
					System.out.println((double)lSeen/(double)lMax*100.0);
			}
		}
		catch (IOException lException) { System.err.println("Error!");}

		InstanceSet[][] lElected = new InstanceSet[lOSlvl+1][lI5lvl+1];

		for (int i = 0; i <= lOSlvl; i++)
			for (int j = 0; j <= lI5lvl; j++)
				lElected[i][j] = new InstanceSet();

		int lNumElected  = 0;
		int lDesired = 2500;
		while (lNumElected < lDesired)
		{
			for (int i = 0; i <= lOSlvl && lNumElected < lDesired; i++)
			{
				for (int j = 0; j <= lI5lvl && lNumElected < lDesired; j++)
				{
					if (!lOrderedSet[i][j].isEmpty())
					{
						lElected[i][j].add(lOrderedSet[i][j].removeFirst());
						lNumElected++;
					}
				}
			}
		}

		out.println(lNumElected);

		for (int i = 0; i <= lOSlvl; i++)
		{
			int lOrderSum = 0;
			for (int j = 0; j <= lI5lvl; j++)
			{
				for (int k = 0; k < lElected[i][j].size(); k++)
				{
					try
					{
						String lRead = null;
						BufferedReader lReader = new BufferedReader(new FileReader(lElected[i][j].get(k)));
						while ((lRead = lReader.readLine()) != null)
						{
							if (lRead.length() > 1)
								out.println(lRead);
						}
						lReader.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				lOrderSum += lElected[i][j].size();
			}

			if (lOrderSum > 0)
			{
				System.out.print(i + "\t");
				for (int j = 0; j <= lI5lvl; j++)
				{
					System.out.print(lElected[i][j].size() + "\t");
				}
				System.out.println();
			}
		}

		out.close();
	}
	/**/
}

class InstanceSet extends PriorityQueue<Integer>
{
	private static final long serialVersionUID = 5337569193107213148L;
}