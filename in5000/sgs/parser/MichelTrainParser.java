package in5000.sgs.parser;

import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MichelTrainParser
{
	public static Instance parseFile(File file) throws IOException
	{
		BufferedReader lRead= new BufferedReader(new FileReader(file));
		String line			= null;
		List<String> lLines = new ArrayList<String>();

		while ((line = lRead.readLine()) != null)
			lLines.add(line);

		lRead.close();

		return MichelTrainParser.parseFile(lLines);
	}

	public static Instance parseFile(List<String> lines) throws IOException
	{
		Instance lInstance = new Instance();
				
		for(String line : lines)
		{			
			Scanner scanner = new Scanner(line);
			if(scanner.hasNext())
			{
				String command = scanner.next();
				switch(command.charAt(0))
				{
					case 'J': // project?
					{
						scanner.nextInt();					// Project ID.
						scanner.nextInt();					// Project EST.
						int projectLFT = scanner.nextInt();
						nextQuotedString(scanner);			// Project name.
	
						lInstance.setDeadline(projectLFT);
						
						break;
					}
					case 'T': // project
					{
						scanner.nextInt();					// Project ID.
						scanner.nextInt();					// Project EST.
						int projectLFT = scanner.nextInt();
						nextQuotedString(scanner);			// Project name.
	
						lInstance.setDeadline(projectLFT);
						
						break;
					}
					case 'R': // resource
					{
						int resourceId = scanner.nextInt();
						int capacity = scanner.nextInt();
						nextQuotedString(scanner);

						lInstance.getResource(resourceId).setCapacity(capacity);
						
						break;
					}
					case 'A': //activity
					{
						scanner.nextInt();
						int activityId = scanner.nextInt()+1;
						int duration = scanner.nextInt();
						
						lInstance.addActivity(activityId).setDuration(duration);
		
						break;	
					}
					case 'Q': // activity resource requirement
					{
						scanner.nextInt();
						int activityId = scanner.nextInt()+1;
						int resourceId = scanner.nextInt();
						int amount = scanner.nextInt();
	
						lInstance.getActivity(activityId).setResourceUse(lInstance.getResource(resourceId), amount);
						
						break;
					}
					case 'P': // precedence constraint
					{
						scanner.nextInt();
						int activity1Id = scanner.nextInt()+1;
						scanner.nextInt();
						int activity2Id = scanner.nextInt()+1;

						lInstance.addPrecedenceConstraint(lInstance.getActivity(activity1Id),
														  lInstance.getActivity(activity2Id));

						break;
					}
					default:
						// ignore unknown commands.
				}
			}
			scanner.close();
		}

		return lInstance;
	}

	private static boolean hasNextQuotedString(Scanner scanner) 
	{
		return scanner.hasNext("\\\".*"); 
	}

	//Scans a quoted string value from the scanner input.
	private static String nextQuotedString(Scanner scanner)
	{
		if (!scanner.hasNext()) throw new java.util.NoSuchElementException();
		if (!hasNextQuotedString(scanner)) throw new java.util.InputMismatchException();
		   // This is necessary because findInLine would skip over other tokens.
		String s = scanner.findInLine("\\\".*?\\\"");
		if (s == null) throw new java.util.InputMismatchException();
		return s.substring(1,s.length()-1); 
	}

	public static void main(String[] args) throws Exception
	{
		Instance lInst = MichelTrainParser.parseFile(new File("./benchmarks/j60_deadmax/j6034_10.instance"));

		for (int i = 1; i <= lInst.getNumActivities()+2; i++)
		{
			Activity lActivity = lInst.getActivity(i);
			System.out.println(lActivity.getID() + " " + lActivity.getSuccessors().size());
		}
	}
}
