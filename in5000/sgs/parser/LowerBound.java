package in5000.sgs.parser;

import in5000.sgs.data.Instance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LowerBound
{
	public static void main(String[] args) throws IOException
	{
		BufferedWriter lOut = new BufferedWriter(new FileWriter(new File("./data/bounds.dat")));
		lOut.append("LB0\tLB3\n");

		int blocks	   = 21;
		int files	   = 1000;
		for (int j = 1; j <= blocks; j++)
		{
			for (int i = 1; i <= files; i++)
			{
				int inst = j * 3;

				if (j > 10) inst = 30 + (j-10) * 6;

				Instance lInst		 = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+inst+"-"+i+".rcp"));

				/** Resource Based Lower Bound **/
				/*
				int[] lResourceUse = new int[lInst.getNumResources()];
				for (int k = 1; k <= lInst.getNumActivities()+1; k++)
				{
					Activity lActivity = lInst.getActivity(k);

					for (int l = 0; l < lInst.getNumResources(); l++)
						lResourceUse[l] += lActivity.getResourceUse(lInst.getResource(l));
				}

				int lMaxDuration = 0;
				for (int l = 0; l < lInst.getNumResources(); l++)
					lMaxDuration = Math.max(lMaxDuration, 
											(int)Math.ceil((double)lResourceUse[l] / (double)lInst.getResource(l).getCapacity()));
				
				lOut.append(Math.max(lInst.getActivity(lInst.getNumActivities()+2).getEST(), lMaxDuration) + "\n");
				/**/

				lOut.append(lInst.computeLowerBound0() + "\t" + lInst.computeLowerBound3() + "\n");

				if (i % 50 == 0)
					System.out.println(String.format("%2d-%4d",
													inst, i));
			}
		}

		lOut.close();		
	}
}
