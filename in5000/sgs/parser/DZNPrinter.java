package in5000.sgs.parser;

import in5000.sgs.alg.ForwardBackwardIteration;
import in5000.sgs.alg.ParallelGen;
import in5000.sgs.alg.rules.BiasedRandomRule;
import in5000.sgs.alg.rules.LatestFinishTime;
import in5000.sgs.alg.rules.PriorityRule;
import in5000.sgs.data.Activity;
import in5000.sgs.data.Instance;

import java.io.File;
import java.io.PrintStream;

import util.StreamReaderThread;

public class DZNPrinter
{
	public static void writeInstanceAsDZN(Instance pInstance, int pUpperBound, PrintStream pOut)
	{
		int lResources		= pInstance.getNumResources();
		int lActivities		= pInstance.getNumActivities();

		// Print upper bound
		pOut.println(String.format("t_max = %d;", pUpperBound));

		// Print num Resources
		pOut.println(String.format("n_res = %d;", lResources));

		// Print Resource Capacities
		pOut.print(String.format("rc = [ %d", pInstance.getResource(0).getCapacity()));
		for (int i = 1; i < lResources; i++)
			pOut.print(String.format(", %d", pInstance.getResource(i).getCapacity()));
		pOut.println(" ];");

		// Print num Tasks
		pOut.println(String.format("n_tasks = %d;", lActivities));

		// Print Task Durations
		pOut.print(String.format("d = [ %d", pInstance.getActivity(2).getDuration()));
		for (int j = 3; j <= lActivities+1; j++)
			pOut.print(String.format(", %d", pInstance.getActivity(j).getDuration()));			
		pOut.println(" ];");

		// Print Resource Usage
		for (int i = 0; i < lResources; i++)
		{
			if (i == 0)	pOut.print(String.format("rr = ["));
			else		pOut.print(String.format("      "));

			for (int j = 2; j <= lActivities+1; j++)
			{
				int lResourceUse = pInstance.getActivity(j).getResourceUse(pInstance.getResource(i));
				if (j == 2) pOut.print(String.format("| %2d", lResourceUse));
				else		pOut.print(String.format(", %2d", lResourceUse));
			}

			if (i < lResources-1)
				pOut.println("");
			else
				pOut.println(" |];");
		}

		// Print Activity Successors.
		for (int j = 2; j <= lActivities+1; j++)
		{
			if (j == 2)	pOut.print("suc = [ { ");
			else		pOut.print("        { ");

			boolean first = true;
			//List<Integer> lSucc = new ArrayList<Integer>();
			for (Activity lActivity : pInstance.getActivity(j).getSuccessors())
			{
				if (lActivity.getID() <= lActivities+1)
				{
					if (!first)
						pOut.print(", ");
					pOut.print(lActivity.getID()-1);
					first = false;
				}
			}

			if (j <= lActivities)
				pOut.println(" },");
			else
				pOut.println(" } ];");
		}
	}

	public static void main(String[] args) throws Exception
	{
		//File in  = new File("./benchmarks/j30/PSP23.SCH");
		File out = new File("temp.dzn");

//		Instance lInst = new PattersonParser(new File("C:/RanGen/Set-12-20.rcpm")).getNext();
		Instance lInst = KolischParser.parseKolischFile(new File("./benchmarks/rcpsp/j1201_5.sm"));
//		Instance lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(10*3)+"-"+326+".rcp"));
//		Instance lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(30+8*6)+"-"+326+".rcp"));
		ParallelGen lGen   = new ParallelGen();
		PriorityRule lRule = new BiasedRandomRule(new LatestFinishTime(lInst, false), 1);
		int lMakespan = Integer.MAX_VALUE;
		for (int i = 0; i < 500; i++)
		{
			lMakespan = Math.min(lMakespan, ForwardBackwardIteration.performAll(lInst,
																				lGen.solve(lInst, lRule),
																				100).get(lInst.getActivity(lInst.getNumActivities()+2)));
			
		}
		System.out.println("FBI best : " + lMakespan);
		DZNPrinter.writeInstanceAsDZN(lInst, lMakespan, new PrintStream(out));
		
		ProcessBuilder pb = new ProcessBuilder();
		pb.environment().put("Path", "C:/Program Files/MiniZinc 1.6/bin/");
		pb.directory(new File("C:/Program Files/MiniZinc 1.6/benchmarks/rcpsp/"));
		long time = -System.nanoTime();
		Process process      = pb.command("cmd",
										  "/c",
										  "mzn-g12lazy.bat -d \""+out.getAbsolutePath()+"\" rcpsp.mzn -a").start();


		StreamReaderThread lerr = new StreamReaderThread(process.getErrorStream(), true);
		StreamReaderThread lin  = new StreamReaderThread(process.getInputStream(), false);

		lerr.start();
		lin.start();
//		BufferedInputStream reader = new BufferedInputStream(process.getInputStream());

		try 
		{
			process.waitFor();
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		time += System.nanoTime();
		System.out.println("Solved in " + time / 1000000000.0);
	}
}
