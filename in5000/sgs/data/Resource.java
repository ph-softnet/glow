package in5000.sgs.data;

public class Resource
{
	private final int fID;

	private int fCapacity;

	public Resource(int pRID)
	{
		this.fID = pRID;
	}

	public void setCapacity(int pCap)
	{
		this.fCapacity = pCap;
	}

	public int getID()
	{
		return this.fID;
	}

	public int getCapacity()
	{
		return this.fCapacity;
	}
}
