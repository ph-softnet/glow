package in5000.sgs.data;

import in5000.sgs.parser.PattersonParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UndominatedFeasibleSets
{
	private final Instance fInstance;

	private final Map<String, List<Integer>> fUFS;

	private final boolean[][] fMasks;
	private final int[] fMasked;

	private final int[] fResourceUse;
	private final int[] fResourceCap;

	private final Deque<Integer> fFeasibleSet;

	private int fNextID;

	public UndominatedFeasibleSets(Instance pInstance)
	{
		int lNumAct = pInstance.getNumActivities();
		int lNumRes = pInstance.getNumResources();

		this.fInstance		= pInstance;
		this.fUFS			= new HashMap<String, List<Integer>>();
		this.fMasks			= new boolean[lNumAct][lNumAct];
		this.fMasked		= new int[lNumAct];
		this.fResourceUse	= new int[lNumRes];
		this.fResourceCap	= new int[lNumRes];
		this.fFeasibleSet	= new LinkedList<Integer>();
		this.fNextID		= 0;

		// Fill Resource Capacity.
		for (int i = 0; i < lNumRes; i++)
		{
			Resource lResource = pInstance.getResource(i);
			this.fResourceUse[i] = 0;
			this.fResourceCap[i] = lResource.getCapacity();
		}

		// Fill Instance Precedence Masks.
		for (int i = 0; i < lNumAct; i++)
		{
			Activity lActivity = pInstance.getActivity(i+2);

			this.fMasks[lActivity.getID()-2][lActivity.getID()-2] = true;

			for (Activity lOther : lActivity.computeTransitivePredecessors())
				if (lOther.getID() > 1)
					this.fMasks[lActivity.getID()-2][lOther.getID()-2] = true;

			for (Activity lOther : lActivity.computeTransitiveSuccessors())
				if (lOther.getID() < lNumAct+2)
					this.fMasks[lActivity.getID()-2][lOther.getID()-2] = true;
		}

		// Compute the Undominated Feasible Sets.
		for (int i = 0; i < lNumAct; i++)
			this.computeUndominatedFeasibleSets(i);
	}

	private void computeUndominatedFeasibleSets(int pNextID)
	{
		// Obtain Instance Params.
		int lNumAct			= this.fInstance.getNumActivities();
		int lNumRes			= this.fInstance.getNumResources();
		Activity lActivity	= this.fInstance.getActivity(pNextID + 2);

		// Claim the resource use.
		for (int i = 0; i < lNumRes; i++)
		{
			Resource lResource = this.fInstance.getResource(i);
			this.fResourceUse[i] += lActivity.getResourceUse(lResource);
		}
		
		// Add to Deque.
		this.fFeasibleSet.push(pNextID);
		// Mask related tasks.
		for (int i = 0; i < lNumAct; i++)
			if (this.fMasks[pNextID][i])
				this.fMasked[i]++;

		// Determine if this set is maximal.
		boolean lIsMaximal = true;
		for (int i = 0; i < lNumAct; i++)
		{
			if (this.fMasked[i] == 0)
			{
				// Determine if activity is Resource Feasible.
				Activity lOther = this.fInstance.getActivity(i+2);
				boolean lResFeasible = true;
				for (int j = 0; j < lNumRes; j++)
				{
					Resource lResource = this.fInstance.getResource(j);
					if (this.fResourceUse[j] + lOther.getResourceUse(lResource) > this.fResourceCap[j])
						lResFeasible = false;
				}

				if (lResFeasible && i > pNextID)
					this.computeUndominatedFeasibleSets(i);

				lIsMaximal = lIsMaximal && !lResFeasible;
			}
		}

		if (lIsMaximal)
		{
			// This is an undominated feasible set consisting of the tasks in 'this.fFeasibleSet'
			List<Integer> fUndominatedFS = new ArrayList<Integer>();
			for (Integer lID : this.fFeasibleSet)
				fUndominatedFS.add(lID);
			this.fUFS.put(String.valueOf(fNextID), fUndominatedFS);
			fNextID++;
		}

		// Done, clear this element from the set.
		this.fFeasibleSet.pop();
		// Mask related tasks.
		for (int i = 0; i < lNumAct; i++)
			if (this.fMasks[pNextID][i])
				this.fMasked[i]--;
		// Free the resource use.
		for (int i = 0; i < lNumRes; i++)
		{
			Resource lResource = this.fInstance.getResource(i);
			this.fResourceUse[i] -= lActivity.getResourceUse(lResource);
		}
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.fUFS.size());
	}

	public static void main(String[] args)
	{
		Instance lInst = null;
		
		try
		{
			lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(10*3)+"-"+326+".rcp"));
		}
		catch (IOException e) { }

		long nanotime = -System.nanoTime();
		UndominatedFeasibleSets lSets = new UndominatedFeasibleSets(lInst);
		nanotime += System.nanoTime();
		System.out.println(lSets + " in " + nanotime / 1000000000.0);
	}
}
