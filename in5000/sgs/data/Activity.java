package in5000.sgs.data;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Activity implements Comparable<Activity>
{
	private final int fID;
	private final Instance fInstance;

	private Set<Activity>   fPredecessors;
	private Set<Activity>   fSuccessors;

	private int fDuration;

	private Map<Resource, Integer> fResourceUse;

	private int fPL;
	private int fRL;

	private int fEST;
	private int fLST;

	public Activity(Instance pInstance, int pID)
	{
		this.fID		= pID;
		this.fInstance	= pInstance;

		this.fPredecessors = new HashSet<Activity>();
		this.fSuccessors   = new HashSet<Activity>();

		this.fResourceUse = new HashMap<Resource, Integer>();

		this.fPL  = 1;
		this.fEST = -1;
		this.fLST = Integer.MAX_VALUE;
	}

	public int getID()
	{
		return this.fID;
	}

	/*
	public void setEST(int pEST)
	{
		this.fEST = pEST;
	}
	/**/

	public int getEST()
	{
		return this.fEST;
	}

	public int getLST()
	{
		return this.fLST;
	}

	public int getEFT()
	{
		return this.fEST + this.fDuration;
	}

	public int getLFT()
	{
		return this.fLST + this.fDuration;
	}

	public int getProgressiveLevel()
	{
		return this.fPL;
	}

	public int getRegressiveLevel()
	{
		return this.fRL;
	}

	public int getTopologicalFloat()
	{
		return this.getRegressiveLevel() - this.getProgressiveLevel();
	}

	public Set<Activity> getPredecessors()
	{
		if (this.fInstance.getDirection() == ScheduleDirection.FORWARD)
		{
			return this.fPredecessors;
		}
		else
		{
			return this.fSuccessors;
		}

		/*
		Set<Activity> lResult = new HashSet<Activity>();

		for (Precedence lPrec : this.fIncoming)
			lResult.add(lPrec.getFrom());

		return lResult;
		/**/
	}

	public Set<Activity> getSuccessors()
	{
		if (this.fInstance.getDirection() == ScheduleDirection.FORWARD)
		{
			return this.fSuccessors;
		}
		else
		{
			return this.fPredecessors;
		}

		/*
		Set<Activity> lResult = new HashSet<Activity>();

		for (Precedence lPrec : this.fOutgoing)
			lResult.add(lPrec.getTo());

		return lResult;
		/**/
	}

	public int getInDegree()
	{
		return this.getPredecessors().size();
	}

	public int getOutDegree()
	{
		return this.getSuccessors().size();
	}

	public int getDuration()
	{
		return this.fDuration;
	}

	public boolean usesResource(Resource lRes)
	{
		return this.fResourceUse.containsKey(lRes);
	}

	public int getResourceUse(Resource lRes)
	{
		if (!this.fResourceUse.containsKey(lRes))
			return 0;

		return this.fResourceUse.get(lRes);
	}

	public Map<Resource, Integer> getResourceRequirements()
	{
		return this.fResourceUse;
	}

	public int getNumResourcesUsed()
	{
		return fResourceUse.size();
	}

	public void addPredecessor(Precedence lConstraint)
	{
		this.fPredecessors.add(lConstraint.getFrom());
		this.fPL = Math.max(this.fPL,  lConstraint.getFrom().getProgressiveLevel() + 1);
	}

	public void addSuccessor(Precedence lConstraint)
	{
		this.fSuccessors.add(lConstraint.getTo());
	}

	public void setResourceUse(Resource pResource, int pUse)
	{
		if (pUse > 0)
			this.fResourceUse.put(pResource, pUse);
	}

	public void setDuration(int pDuration)
	{
		this.fDuration = pDuration;
	}

	public void resetTiming()
	{
		this.fEST = Integer.MIN_VALUE;
		this.fLST = Integer.MAX_VALUE;
	}

	public void resetLevels()
	{
		this.fPL = Integer.MIN_VALUE;
		this.fRL = Integer.MAX_VALUE;
	}

	public double breadthFirstAccumulate(Map<Activity, Double> pBasis, Map<Activity, Double> pAccumulate)
	{
		if (pAccumulate.containsKey(this))
			return pAccumulate.get(this);

		double lValue	 = pBasis.get(this) + this.getDuration();
		double lMaxChild = 0;

		for (Activity lSuccessor : this.getSuccessors())
			lMaxChild = Math.max(lMaxChild, lSuccessor.breadthFirstAccumulate(pBasis, pAccumulate));

		lValue = lValue + lMaxChild;

		pAccumulate.put(this, lValue);

		return lValue;
	}

	public void computeESTschedule(int pEST)
	{
		if (pEST > this.getEST())
		{
			this.fEST = pEST;

			for (Activity lSuccessor : this.getSuccessors())
			{
				lSuccessor.computeESTschedule(this.getEFT());
			}
		}
	}

	public void computeLSTschedule(int pLST)
	{
		if ((pLST - this.getDuration()) < this.getLST())
		{
			this.fLST = (pLST - this.getDuration());

			for (Activity lPredecessor : this.getPredecessors())
			{
				lPredecessor.computeLSTschedule(this.getLST());
			}
		}
	}

	public int computeProgressiveLevel(int m)
	{
		/*
		 * Minimizing approach:
		 */
		///*
		this.fPL = m;
		if (!this.getPredecessors().isEmpty())
		{
			for (Activity lPredecessor : this.getPredecessors())
			{
				this.fPL  = Math.max(this.fPL, lPredecessor.getProgressiveLevel()+1);
			}
		}
		 /**/

		for (Activity lSuccessor : this.getSuccessors())
			lSuccessor.computeProgressiveLevel(m);

		return 0;
	}

	public int computeRegressiveLevel(int m)
	{
		/*
		if (this.fOutgoing.isEmpty())
			this.fRL = m;
		else
		{
			for (Precedence lPrec : this.fOutgoing)
			{
				this.fRL = Math.max(this.fRL, lPrec.getTo().getRegressiveLevel()-1);
			}
		}
		/**/

		/*
		 * Minimizing approach:
		 */
		///*
		this.fRL = m;
		if (!this.getSuccessors().isEmpty())
		{
			for (Activity lSuccessor : this.getSuccessors())
			{
				this.fRL  = Math.min(this.fRL, lSuccessor.getRegressiveLevel()-1);
			}
		}
		 /**/

		for (Activity lPredecessor : this.getPredecessors())
			lPredecessor.computeRegressiveLevel(m);

		return 0;
	}

	public String toDot()
	{
		String lNode = "\tn"+this.fID+" [label=\"L "+this.fPL+" - "+this.fRL+"\"];\n";

		return lNode;
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.fID);
	}

	public Deque<Activity> computeLongestPath()
	{
		if (this.getSuccessors().isEmpty())
			return new LinkedList<Activity>();

		int lLongestPathLength = -1;
		Deque<Activity> lLongestPath = null;
		for (Activity lNext : this.getSuccessors())
		{
			int lPathLength = 0;
			Deque<Activity> lPath = lNext.computeLongestPath();
			for (Activity lPathAct : lPath)
				lPathLength += lPathAct.getDuration();
			if (lPathLength > lLongestPathLength)
			{
				lLongestPathLength	= lPathLength;
				lLongestPath		= lPath;
			}
		}

		if (this.fID != 1)
			lLongestPath.addFirst(this);

		return lLongestPath;
	}

	public void computeConnectivity(boolean[] lPredecessors, boolean[][] lConnected)
	{
		for (int i = 0; i < lPredecessors.length; i++)
			if (lPredecessors[i])
				lConnected[i][this.fID-1] = true;

		lPredecessors[this.fID-1] = true;
		for (Activity lSuccessor : this.getSuccessors())
			lSuccessor.computeConnectivity(lPredecessors, lConnected);
		lPredecessors[this.fID-1] = false;
	}

	@Override
	public int hashCode()
	{
		return this.fID;
	}

	@Override
	public int compareTo(Activity that)
	{
		int lFloatDiff = this.getTopologicalFloat() - that.getTopologicalFloat();

		if (lFloatDiff == 0)
			return that.getID() - this.getID();
		else
			return lFloatDiff;
	}

	public Set<Activity> computeTransitivePredecessors()
	{
		Set<Activity> lPredecessors = new HashSet<Activity>();

		for (Activity lPredecessor : this.getPredecessors())
			lPredecessor.computeTransitivePredecessors(lPredecessors);

		return lPredecessors;
	}

	private void computeTransitivePredecessors(Set<Activity> lPredecessors)
	{
		if (lPredecessors.add(this))
			for (Activity lPredecessor : this.getPredecessors())
				lPredecessor.computeTransitivePredecessors(lPredecessors);		
	}

	public Set<Activity> computeTransitiveSuccessors()
	{
		Set<Activity> lSuccessors = new HashSet<Activity>();

		for (Activity lSuccessor : this.getSuccessors())
			lSuccessor.computeTransitiveSuccessors(lSuccessors);

		return lSuccessors;
	}

	private void computeTransitiveSuccessors(Set<Activity> lSuccessors)
	{
		if (lSuccessors.add(this))
			for (Activity lSuccessor : this.getSuccessors())
				lSuccessor.computeTransitiveSuccessors(lSuccessors);		
	}
}
