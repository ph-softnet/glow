package in5000.sgs.data;

import java.util.HashMap;
import java.util.Map;

public class ResourceFlow
{
	private final InsertionActivity fFrom;
	private final InsertionActivity fTo;
	
	private final Map<Resource, Integer> fFlow;

	public ResourceFlow(InsertionActivity pFrom, InsertionActivity pTo)
	{
		this.fFrom	= pFrom;
		this.fTo	= pTo;
		this.fFlow	= new HashMap<Resource, Integer>();
	}

	public void setFlow(Resource pResource, int pFlow)
	{
		this.fFlow.put(pResource, pFlow);
	}

	public int getFlow(Resource pResource)
	{
		if (!this.fFlow.containsKey(pResource))
			return 0;

		return this.fFlow.get(pResource);
	}

	public boolean isEmpty()
	{
		boolean lNonZeroFlow = false;

		for (Integer lFlow : fFlow.values())
			if (lFlow > 0)
				lNonZeroFlow = true;

		return !lNonZeroFlow;
	}

	public InsertionActivity getFrom()
	{
		return this.fFrom;
	}

	public InsertionActivity getTo()
	{
		return this.fTo;
	}

	@Override
	public String toString()
	{
		String lHeader = this.fFrom.toString() + " -> " + this.fTo.toString() + ":";

		for (Resource lResource : this.fFlow.keySet())
			lHeader += " " + this.fFlow.get(lResource) + "/" + lResource.getCapacity();

		return lHeader;
	}

	public void decreaseFlow(Resource pResource, int pDivertedAmount)
	{
		this.fFlow.put(pResource, this.fFlow.get(pResource) - pDivertedAmount);
	}

	public void increaseFlow(Resource pResource, int pDivertedAmount)
	{
		if (!this.fFlow.containsKey(pResource))
			this.fFlow.put(pResource, 0);

		this.fFlow.put(pResource, this.fFlow.get(pResource) + pDivertedAmount);
	}
}
