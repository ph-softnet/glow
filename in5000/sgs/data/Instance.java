package in5000.sgs.data;

import in5000.sgs.parser.PattersonParser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class Instance
{
	private Map<Integer, Activity> fActivities;

	private List<Precedence> fPrecedences;

	private Map<Activity, List<Precedence>> fAdjacency;

	private List<List<Activity>> fProgressiveLevels;

	private Map<Integer, Resource> fResources;

	private int fMaxNumShortArcs;

	private int[] fNprime;

	private double fOrderStrength;

	private int fDeadline;

	private ScheduleDirection fDirection;

	public Instance()
	{
		this.fActivities		= new HashMap<Integer, Activity>();
		this.fPrecedences		= new ArrayList<Precedence>();
		this.fAdjacency  		= new HashMap<Activity, List<Precedence>>();
		this.fProgressiveLevels = new ArrayList<List<Activity>>();
		this.fResources			= new HashMap<Integer, Resource>();
		this.fDirection			= ScheduleDirection.FORWARD;
	}

	public ScheduleDirection getDirection()
	{
		return this.fDirection;
	}

	public void flipDirection()
	{
		if (this.fDirection == ScheduleDirection.FORWARD)
		{
			this.fDirection = ScheduleDirection.BACKWARD;
		}
		else
		{
			this.fDirection = ScheduleDirection.FORWARD;
		}
	}

	public Activity getDummySource()
	{
		if (this.fDirection == ScheduleDirection.FORWARD)
		{
			return this.getActivity(1);
		}
		else
		{
			return this.getActivity(this.fActivities.size());
		}
	}

	public Activity getDummySink()
	{
		if (this.fDirection == ScheduleDirection.FORWARD)
		{
			return this.getActivity(this.fActivities.size());
		}
		else
		{
			return this.getActivity(1);
		}
	}

	public void setDeadline(int pDeadline)
	{
		this.fDeadline = pDeadline;
	}

	public int getDeadline()
	{
		return this.fDeadline;
	}

	public int getNumActivities()
	{
		return this.fActivities.size()-2;
	}

	public int getNumResources()
	{
		return this.fResources.size();
	}

	public int getNumPrecedences()
	{
		int lIn  = this.fProgressiveLevels.get(0).get(0).getOutDegree();
		int lOut = this.fProgressiveLevels.get(this.fProgressiveLevels.size()-1).get(0).getInDegree();

		return this.fPrecedences.size() - lIn - lOut;
	}

	public int getNumLevels()
	{
		return this.fProgressiveLevels.size()-2;
	}

	public double getSerialParallelIndicator()
	{
		if (this.getNumActivities() == 1)
			return 1.0;
		else
			return (double)(this.getNumLevels() - 1) / (double)(this.getNumActivities() - 1);
	}

	public double getDistributionIndicator()
	{
		if (this.getNumLevels() == 1 || this.getNumLevels() == this.getNumActivities())
			return 0.0;

		double lAvgWidth = (double)this.getNumActivities() / (double)this.getNumLevels();

		double lLevelSum = 0.0;
		for (int i = 1; i < 1 + this.getNumLevels(); i++)
			lLevelSum += Math.abs((double)this.fProgressiveLevels.get(i).size() - lAvgWidth);

		return lLevelSum / (2.0 * ((double)this.getNumLevels()-1.0) * (lAvgWidth-1.0));
	}

	public double getShortArcIndicator()
	{
		if (this.fMaxNumShortArcs == (this.getNumActivities() - this.fProgressiveLevels.get(1).size()))
			return 1.0;
		else
		{
			int lFactor = this.getNumActivities() - this.fProgressiveLevels.get(1).size();
			int lNum	= this.fNprime[1] - lFactor;
			int lDenom	= this.fMaxNumShortArcs - lFactor;

			return ((double)lNum) /
				   ((double)lDenom);
		}
	}

	public double getLongArcIndicator()
	{
		if (this.getNumPrecedences() == (this.getNumActivities() - this.fProgressiveLevels.get(1).size()))
			return 1.0;
		else
		{
			int m = this.getNumLevels();
			double lWeightedArcLength = 0.0;
			for (int l = 2; l < m-1; l++)
			{
				lWeightedArcLength += this.fNprime[l] * ((double)(m-l-1))/((double)(m-2));
			}

			return (double)(lWeightedArcLength + this.fNprime[1] - this.getNumActivities() + this.fProgressiveLevels.get(1).size()) /
				   (double)(this.getNumPrecedences() - this.getNumActivities() + this.fProgressiveLevels.get(1).size());
		}
	}

	public double getTopologicalIndicator()
	{
		int n = this.getNumActivities();
		int m = this.getNumLevels();

		if (m == 1 || m == n)
		{
			return 0.0;
		}
		else
		{
			int lFloatSum = 0;
			for (Activity lActi : this.fActivities.values())
				lFloatSum += lActi.getTopologicalFloat();

			return ((double)lFloatSum) / ((double)((m-1)*(n-m)));
		}
	}

	public double getOrderStrength()
	{
		return this.fOrderStrength;
	}

	public double getResourceFactor()
	{
		int lResourcesUsed = 0;
		for (Activity lActi : fActivities.values())
			if (lActi.getInDegree() != 0 && lActi.getOutDegree() != 0)
			{
				lResourcesUsed += lActi.getNumResourcesUsed();
			}

		return ((double)lResourcesUsed) / ((double)(this.getNumActivities() * this.getNumResources()));
	}

	public double getResourceStrength()
	{
		Queue<Activity> lSchedule = new PriorityQueue<Activity>(fActivities.size(), new ESTfirst());
		Queue<Activity> lStarted  = new PriorityQueue<Activity>(fActivities.size(), new EFTfirst());

		for (Activity lAct : this.fActivities.values())
			lSchedule.add(lAct);

		int[] lCurrUse    = new int[this.fResources.size()];
		int[] lMaxUse     = new int[this.fResources.size()];
		int[] lHighestUse = new int[this.fResources.size()];

		while (!lSchedule.isEmpty())
		{
			Activity lStarting = lSchedule.poll();

			while (!lStarted.isEmpty() && lStarting.getEST() >= lStarted.peek().getEFT())
			{
				Activity lFinished = lStarted.poll();

				for (int i = 0; i < this.fResources.size(); i++)
					if (lFinished.usesResource(this.fResources.get(i)))
						lCurrUse[i] -= lFinished.getResourceUse(this.fResources.get(i));
			}

			lStarted.add(lStarting);

			for (int i = 0; i < this.fResources.size(); i++)
				if (lStarting.usesResource(this.fResources.get(i)))
				{
					lCurrUse[i] += lStarting.getResourceUse(this.fResources.get(i));

					if (lStarting.getResourceUse(this.fResources.get(i)) > lHighestUse[i])
						lHighestUse[i] = lStarting.getResourceUse(this.fResources.get(i));
				}

			for (int i = 0; i < this.fResources.size(); i++)
				if (lCurrUse[i] > lMaxUse[i])
					lMaxUse[i] = lCurrUse[i];
		}

		double[] lResourceStrength = new double[this.fResources.size()];
		double lResourceSum = 0.0;

		for (int i = 0; i < this.fResources.size(); i++)
		{
			if (this.fResources.get(i).getCapacity() == lMaxUse[i])
				lResourceStrength[i] = 1.0;
			else			
				lResourceStrength[i] =	(double)(this.fResources.get(i).getCapacity() - lHighestUse[i]) /
										(double)(lMaxUse[i] - lHighestUse[i]);

			lResourceSum += lResourceStrength[i];

			//System.out.print(this.fResources.get(i).getCapacity() + "\t" + lHighestUse[i] + "\t" + lMaxUse[i] + "\t" + lResourceStrength[i] + "\n");
		}
		//System.out.println();
		
		return lResourceSum / this.fResources.size();
	}

	public double getResourceConstrainedness()
	{
		double lRC = 0.0;

		for (Resource lRes : fResources.values())
		{
			int lTotalUse = 0;
			int lNumUsing = 0;
			for (Activity lAct : fActivities.values())
			{
				if (lAct.usesResource(lRes))
				{
					lTotalUse += lAct.getResourceUse(lRes);
					lNumUsing++;
				}
			}

			lRC += ((double)lTotalUse / (double)lNumUsing) / (double)lRes.getCapacity();
		}

		return lRC / (double)fResources.size();
	}

	public Activity addActivity(int pID)
	{
		if (!this.fActivities.containsKey(pID))
		{
			Activity lActivity = new Activity(this, pID);
			this.fActivities.put(pID, lActivity);
			this.fAdjacency.put(lActivity, new ArrayList<Precedence>());
		}

		return this.fActivities.get(pID);
	}

	public Activity getActivity(int pID)
	{
		return this.addActivity(pID);
	}

	public Resource getResource(int pRID)
	{
		if (!this.fResources.containsKey(pRID))
		{
			Resource lResource = new Resource(pRID);
			this.fResources.put(pRID, lResource);
		}

		return this.fResources.get(pRID);
	}

	public void addPrecedenceConstraint(Activity pFrom, Activity pTo)
	{
		Precedence lConstraint = new Precedence(this, pFrom, pTo);

		pFrom.addSuccessor(lConstraint);
		pTo.addPredecessor(lConstraint);

		this.fPrecedences.add(lConstraint);
		this.fAdjacency.get(pFrom).add(lConstraint);
	}

	public void resetTiming()
	{
		/*
		 *		Determine the maximum progressive level and the sink node. 
		 */
		Activity lSource = this.getDummySource();
		Activity lSink   = this.getDummySink();
		for (Activity lActivity : fActivities.values())
		{
			lActivity.resetTiming();
			lActivity.resetLevels();
		}

		/*
		 *		Compute the EST schedule.
		 */
		lSource.computeESTschedule(0);
		lSink.computeLSTschedule(lSink.getEFT());

		/*
		 * 		Compute the regressive level of all activities.
		 */
		lSource.computeProgressiveLevel(1);
		lSink.computeRegressiveLevel(lSink.getProgressiveLevel());
	}

	public void initialize()
	{
		Activity lSource = this.getDummySource();
		Activity lSink   = this.getDummySink();

		this.resetTiming();

		int m = lSink.getProgressiveLevel();

		/*
		 *		Compute the connectivity of the graph.
		 */
		boolean[][] lConn = new boolean[fActivities.size()][fActivities.size()];
		lSource.computeConnectivity(new boolean[fActivities.size()], lConn);

		int n = this.getNumActivities();
		int lNumPrecs = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (lConn[i][j])
					lNumPrecs++;

		fOrderStrength = ((double)lNumPrecs) / ((double)(n*(n-1))/2.0);

		/*
		 * 		Populate the list of progressive levels.
		 */
		for (int i = 1; i <= m; i++)
		{
			List<Activity> lLevel = new ArrayList<Activity>();

			for (Activity lActivity : fActivities.values())
			{
				if (lActivity.getProgressiveLevel() == i)
					lLevel.add(lActivity);
			}

			fProgressiveLevels.add(lLevel);
		}

		/*
		 *		Compute the value of D, the maximum number of short arcs.
		 */
		this.fMaxNumShortArcs = 0;
		for (int i = 2; i < (m-1); i++)
		{
			this.fMaxNumShortArcs += this.fProgressiveLevels.get(i-1).size() * this.fProgressiveLevels.get(i).size();
		}

		/*
		 *		Compute the values of n', where n'l is the number of arcs of length l.
		 */
		this.fNprime = new int[m];
		for (Precedence lPrec : this.fPrecedences)
		{
			if (lPrec.getFrom().getProgressiveLevel() > 1 &&
				lPrec.getTo().getProgressiveLevel()   < m)
				this.fNprime[lPrec.getLength()]++;
		}
	}

	private List<NodePackingActivity> computeHWNPlist()
	{
		List<NodePackingActivity> lResult = new ArrayList<NodePackingActivity>();
		Deque<Activity> lPI = this.getActivity(1).computeLongestPath();

		//System.out.println(lPI);
		for (Activity lActivity : lPI)
			lResult.add(new NodePackingActivity(lActivity, this));

		PriorityQueue<NodePackingActivity> lQueue = new PriorityQueue<NodePackingActivity>();
		for (int i = 2; i < this.getNumActivities()+2; i++)
		{
			Activity lActivity = this.getActivity(i);

			if (!lPI.contains(lActivity))
				lQueue.add(new NodePackingActivity(lActivity, this));
		}

		while (!lQueue.isEmpty())
			lResult.add(lQueue.poll());

		return lResult;
	}

	public int computeLowerBound0()
	{
		return this.getActivity(this.getNumActivities()+2).getEST();
	}

	public int computeLowerBound3()
	{
		List<NodePackingActivity> lList = this.computeHWNPlist();
		int lLB3 = 0;

		for (int i = 0; i < this.getNumActivities(); i++)
		{
			List<NodePackingActivity> lS = new ArrayList<NodePackingActivity>();

			
			int lR = i;
			boolean lAdded = false;
			do
			{
				lAdded = false;
				for (int j = 0; j < lList.size(); j++)
				{
					int lIndex = (j + lR) % lList.size();
					NodePackingActivity lProposed = lList.get(lIndex);
					//System.out.print(lIndex);
					if (!lS.contains(lProposed))
					{
						boolean lAddable = true;
						for (NodePackingActivity lNodeActi : lS)
						{
							if (lProposed.isNeighbor(lNodeActi.getActivity()))
								lAddable = false;
						}

						//System.out.print("T");

						if (lAddable)
						{
							lAdded = true;
							lR = (lIndex + 1) % lList.size();
							lS.add(lProposed);
							break;
						}
					}
					//System.out.print(" ");
				}
				//System.out.println(" = " + lS.size());
			}
			while (lAdded);
			/**/

			int lPathLength = 0;
			for (NodePackingActivity lNodeActi : lS)
				lPathLength += lNodeActi.getActivity().getDuration();

			//System.out.println(lPathLength + " " + lS);
			if (lPathLength > lLB3)
				lLB3 = lPathLength;
		}

		return lLB3;
	}

	public void toDot(File pOutFile) throws IOException
	{
		FileWriter lWriter = new FileWriter(pOutFile);

		lWriter.append("digraph G {\n");
		for (int i = 1; i <= this.fProgressiveLevels.size(); i++)
		{
			lWriter.append("\t{ rank=same;\n");

			Queue<Activity> lSorted = new PriorityQueue<Activity>();
			for (Activity lActivity : fActivities.values())
			{
				if (lActivity.getRegressiveLevel() == i)
					lSorted.add(lActivity);
			}

			while (!lSorted.isEmpty())
			{
				lWriter.append(lSorted.poll().toDot());
			}

			lWriter.append("\t}\n");
		}
		lWriter.append("\n");
		for (Activity lActivity : fActivities.values())
			for (Precedence lPrec : fAdjacency.get(lActivity))
				lWriter.append(lPrec.toDot());
		lWriter.append("}\n");

		lWriter.close();
	}

	public static void main(String[] args)
	{
		Instance lInst = null;
		
		try
		{
			lInst = PattersonParser.parseFile(new File("./lib/RanGen/Testset/Set-"+(5*3)+"-"+326+".rcp"));
			//lInst = PattersonParser.parseFile(new File("./benchmarks/mingozzi_example.rcp"));
		}
		catch (IOException e) { }

		System.out.println(lInst.computeLowerBound0());
		System.out.println(lInst.computeLowerBound3());
	}
}

class ESTfirst implements Comparator<Activity>
{
	@Override
	public int compare(Activity o1, Activity o2)
	{
		return o1.getEST() - o2.getEST();
	}
}

class EFTfirst implements Comparator<Activity>
{
	@Override
	public int compare(Activity o1, Activity o2)
	{
		return o1.getEFT() - o2.getEFT();
	}
}
