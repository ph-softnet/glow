package in5000.sgs.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MaximalCut
{
	// The maximal cut consists of a set of activities C- to the `left' of the cut.
	private final Set<ResourceFlow> fMaximalCut;

	// On the other hand, a cut also represents all `earlier' tasks.
	private final Set<InsertionActivity> fEarlierTasks;

	public MaximalCut(InsertionActivity pSource)
	{
		this.fMaximalCut	= new HashSet<ResourceFlow>();
		this.fEarlierTasks	= new HashSet<InsertionActivity>();

		this.fMaximalCut.addAll(pSource.getSuccessors());
		this.fEarlierTasks.add(pSource);
	}

	public void generateInitialCut(int lActivityEST)
	{
		int lEarliestFinishTime	= Integer.MAX_VALUE;

		do
		{
			lEarliestFinishTime			= Integer.MAX_VALUE;
			InsertionActivity lSelected = null;
	
			for (ResourceFlow lFlow : fMaximalCut)
			{
				InsertionActivity lConsidered = lFlow.getTo();
	
				if (lConsidered.getSuccessors().size() != 0 && lConsidered.getEFT() < lEarliestFinishTime)
				{
					lSelected = lConsidered;
					lEarliestFinishTime = lSelected.getEFT();
				}
			}
	
			if (lEarliestFinishTime <= lActivityEST)
			{
				this.fMaximalCut.removeAll(lSelected.getPredecessors());
				this.fMaximalCut.addAll(lSelected.getSuccessors());
				this.fEarlierTasks.add(lSelected);
			}
		}
		while (lEarliestFinishTime <= lActivityEST);
	}

	public boolean generateNextCut(int lActivityLFT)
	{
		int lLatestStartTime = Integer.MAX_VALUE;
		InsertionActivity lSelected = null;

		for (ResourceFlow lFlow : fMaximalCut)
		{
			InsertionActivity lConsidered = lFlow.getTo();

			if (lConsidered.getSuccessors().size() != 0 && lConsidered.getLST() < lLatestStartTime)
			{
				lSelected = lConsidered;
				lLatestStartTime = lSelected.getLST();
			}
		}

		if (lSelected != null && lLatestStartTime < lActivityLFT)
		{
			this.fMaximalCut.removeAll(lSelected.getPredecessors());
			this.fMaximalCut.addAll(lSelected.getSuccessors());
			this.fEarlierTasks.add(lSelected);

			return true;
		}

		return false;
	}

	public Set<ResourceFlow> findOptimalSubCut(Map<Resource, Integer> pResourceUse)
	{
		// The initial optimal cut is the entire cut.
		Set<ResourceFlow> lOptimalCut = new HashSet<ResourceFlow>();
		lOptimalCut.addAll(fMaximalCut);

		/*
		// Compute the capacity of the optimal cut.
		Map<Resource, Integer> lCutCapacity = new HashMap<Resource, Integer>();
		for (ResourceFlow lFlow : lOptimalCut)
		{
			for (Resource lResource : pResourceUse.keySet())
			{
				if (!lCutCapacity.containsKey(lResource))
					lCutCapacity.put(lResource, 0);
				
				int lCap = lCutCapacity.get(lResource);

				lCap += lFlow.getFlow(lResource);
				
				lCutCapacity.put(lResource, lCap);
			}
		}
		/**/

		// And the entire cut has the maximum capacity.
		Map<Resource, Integer> lResourceAvailability = new HashMap<Resource, Integer>();
		for (Resource lResource : pResourceUse.keySet())
			lResourceAvailability.put(lResource, lResource.getCapacity());
		boolean lEnoughCapacity = true;

		// Now we strip away the task with the maximum Earliest Finish Time until capacity is reached.
		while (lEnoughCapacity && !lOptimalCut.isEmpty())
		{
			int lEarliestFinishTime = Integer.MIN_VALUE;
			InsertionActivity lMaxEFT = null;

			// Find the task with maximum Earliest Finish Time in the current cut.
			for (ResourceFlow lFlow : lOptimalCut)
			{
				if (lFlow.getFrom().getEFT() > lEarliestFinishTime)
				{
					lMaxEFT = lFlow.getFrom();
					lEarliestFinishTime = lMaxEFT.getEFT();
				}
			}

			// Determine those flows that this activity contributes to the cut.
			Set<ResourceFlow> lRemovedFlows = new HashSet<ResourceFlow>();
			for (ResourceFlow lFlow : lOptimalCut)
				if (lFlow.getFrom() == lMaxEFT)
					lRemovedFlows.add(lFlow);

			// Determine the new capacity after removing the contributed flows.
			for (ResourceFlow lFlow : lRemovedFlows)
				for (Resource lResource : lResourceAvailability.keySet())
				{
					int lReq  = pResourceUse.get(lResource);
					int lCap  = lResourceAvailability.get(lResource);
					int lDrop = lFlow.getFlow(lResource);

					if (lCap - lDrop < lReq)
						lEnoughCapacity = false;
					
					lResourceAvailability.put(lResource, lCap - lDrop);
				}

			// If there was enough capacity, we can remove the flows from the cut.
			if (lEnoughCapacity)
				lOptimalCut.removeAll(lRemovedFlows);
		}

		return lOptimalCut;
	}

	@Override
	public String toString()
	{
		Set<InsertionActivity> lLeftSideCut = new HashSet<InsertionActivity>();

		for (ResourceFlow lFlow : fMaximalCut)
		{
			lLeftSideCut.add(lFlow.getFrom());
		}

		return lLeftSideCut.toString();
	}
}
