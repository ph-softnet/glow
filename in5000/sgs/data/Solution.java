package in5000.sgs.data;

import java.util.Map;

public class Solution
{
	private final Instance fInstance;
	
	private Map<Activity, Integer> fSolution;

	private int fBestMakespan;

	public Solution(Instance pInstance)
	{
		this.fInstance = pInstance;
		this.fBestMakespan = Integer.MAX_VALUE;
	}

	public void offerSolution(Map<Activity, Integer> pPotentialSolution)
	{
		int lMakespan = pPotentialSolution.get(this.fInstance.getDummySink());

		if (lMakespan < this.fBestMakespan)
		{
			this.fSolution	   = pPotentialSolution;
			this.fBestMakespan = lMakespan;
		}
	}

	public int getMakespan()
	{
		return this.fBestMakespan;
	}

	public Map<Activity, Integer> getSolution()
	{
		return this.fSolution;
	}
}
