package in5000.sgs.data;

public class Precedence
{
	private final Instance fInstance;
	private final Activity fFrom;
	private final Activity fTo;

	public Precedence(Instance pInstance, Activity pFrom, Activity pTo)
	{
		this.fInstance	= pInstance;
		this.fFrom		= pFrom;
		this.fTo		= pTo;
	}

	public Activity getFrom()
	{
		if (this.fInstance.getDirection() == ScheduleDirection.FORWARD)
		{
			return this.fFrom;
		}
		else
		{
			return this.fTo;
		}
	}

	public Activity getTo()
	{
		if (this.fInstance.getDirection() == ScheduleDirection.FORWARD)
		{
			return this.fTo;
		}
		else
		{
			return this.fFrom;
		}
	}

	public int getLength()
	{
		return this.getTo().getProgressiveLevel() - this.getFrom().getProgressiveLevel();
	}

	public String toDot()
	{
		String lPrec = "\tn"+this.getFrom().getID()+" -> n"+this.getTo().getID()+"\n";

		return lPrec;
	}
}
