package in5000.sgs.data;

import java.util.HashSet;
import java.util.Set;

/**
 * A Node Packing Activity is an activity from the graph ~G, page 720 in paper:
 *		"An Exact Algorithm for the Resource-Constrained Project Scheduling
 *		 Problem Based on a New Mathematical Formulation"
 *
 * @author Frits de Nijs
 *
 */
public class NodePackingActivity implements Comparable<NodePackingActivity>
{
	private final Activity fActivity;

	private final Set<Activity> fNeighbours;

	public NodePackingActivity(Activity pActivity, Instance pInstance)
	{
		this.fActivity	 = pActivity;
		this.fNeighbours = new HashSet<Activity>();

		for (int i = 2; i <= pInstance.getNumActivities()+1; i++)
		{
			Activity lOther = pInstance.getActivity(i);

			if (!(lOther == pActivity) && 
				!this.fActivity.computeTransitiveSuccessors().contains(lOther) &&
				!lOther.computeTransitiveSuccessors().contains(this.fActivity))
			{
				boolean lOverCapacity = false;
				for (int j = 0; j < pInstance.getNumResources(); j++)
				{
					Resource lResource = pInstance.getResource(j);
					if (this.fActivity.getResourceUse(lResource) + lOther.getResourceUse(lResource) >
							lResource.getCapacity())
						lOverCapacity = true;
				}
				if (!lOverCapacity)
					this.fNeighbours.add(lOther);
			}
		}
	}

	public int getID()
	{
		return this.fActivity.getID();
	}

	public int getDegree()
	{
		return this.fNeighbours.size();
	}

	public Activity getActivity()
	{
		return this.fActivity;
	}

	public boolean isNeighbor(Activity pActivity)
	{
		return this.fNeighbours.contains(pActivity);
	}

	@Override
	public int compareTo(NodePackingActivity pOther)
	{
		if (this.getDegree() == pOther.getDegree())
			return this.getID() - pOther.getID();

		return pOther.getDegree() - this.getDegree();
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.getID());
	}
}
