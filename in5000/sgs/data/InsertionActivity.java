package in5000.sgs.data;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class InsertionActivity
{
	private final Activity fActivity;

	private Set<ResourceFlow> fIncoming;
	private Set<ResourceFlow> fOutgoing;

	private int fEST;
	private int fLST;

	public InsertionActivity(Activity pActivity)
	{
		this.fActivity	= pActivity;
		this.fEST		= Integer.MIN_VALUE;
		this.fLST		= Integer.MAX_VALUE;
		this.fIncoming	= new HashSet<ResourceFlow>();
		this.fOutgoing	= new HashSet<ResourceFlow>();
	}

	public void resetTiming()
	{
		this.fEST		= Integer.MIN_VALUE;
		this.fLST		= Integer.MAX_VALUE;
	}

	public Activity getActivity()
	{
		return this.fActivity;
	}

	public int getInDegree()
	{
		return this.fIncoming.size();
	}

	public int getOutDegree()
	{
		return this.fOutgoing.size();
	}

	public void addPredecessor(ResourceFlow pIncoming)
	{
		this.fIncoming.add(pIncoming);
	}

	public void addSuccessor(ResourceFlow pOutgoing)
	{
		this.fOutgoing.add(pOutgoing);
	}

	public void computeEST(int pEST, Map<Activity, InsertionActivity> pScheduled)
	{
		if (pEST > this.fEST)
		{
			this.fEST = pEST;

			// Go over resource successors.
			for (ResourceFlow lFlow : this.fOutgoing)
			{
				lFlow.getTo().computeEST(this.fEST + this.fActivity.getDuration(), pScheduled);
			}

			// Go over scheduled precedence successors.
			for (Activity lSuccessor : this.fActivity.getSuccessors())
			{
				InsertionActivity lOther = pScheduled.get(lSuccessor);

				if (lOther != null)
					lOther.computeEST(this.fEST + this.fActivity.getDuration(), pScheduled);
			}
		}
	}

	public void computeLST(int pLFT, Map<Activity, InsertionActivity> pScheduled)
	{
		if (pLFT - this.fActivity.getDuration() < this.fLST)
		{
			this.fLST = pLFT - this.fActivity.getDuration();

			// Go over resource predecessors.
			for (ResourceFlow lFlow : this.fIncoming)
			{
				lFlow.getFrom().computeLST(this.fLST, pScheduled);
			}

			// Go over scheduled precedence predecessors.
			for (Activity lPredecessor : this.fActivity.getPredecessors())
			{
				InsertionActivity lOther = pScheduled.get(lPredecessor);

				if (lOther != null)
					lOther.computeLST(this.fLST, pScheduled);
			}
		}
	}

	public int getEST()
	{
		return this.fEST;
	}

	public int getEFT()
	{
		return this.fEST + fActivity.getDuration();
	}

	public int getLST()
	{
		return this.fLST;
	}

	public Set<ResourceFlow> getSuccessors()
	{
		return this.fOutgoing;
	}

	public Set<ResourceFlow> getPredecessors()
	{
		return this.fIncoming;
	}

	@Override
	public String toString()
	{
		return fActivity.toString();
	}
}
