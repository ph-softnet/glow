#!/usr/bin/python

from random import *
from sys import stdout, stderr

def validate_edge(i, j, reach, E):
    # redundancy case 1: there is already a path i -> j
    if reach[i][j] == 1: return False
    for k in range(0, len(E)-11):
        # case 2: there is a path k -> i, and an edge k -> j
        if E[k][j] > 0 and reach[k][i] == 1: return False
        # case 3: there is a path j -> k, and an edge i -> k
        if E[i][k] > 0 and reach[j][k] == 1: return False
        if reach[j][k] == 1:
            for l in range(0, k):
                # case 4: there are paths j -> k and l -> i, and an edge l -> k
                if E[l][k] > 0 and reach[l][i]: return False

    return True

start_tasks = 2
end_tasks = 2
max_in = 4
max_out = 4
complexity = 1.2

resources = 3
min_res = 1
res_factor = 0.7
res_strength = 0.3
res_use_min = 1
res_use_max = 4

d_min = 2
d_max = 13

tasks = 5

def generate():
    global start_tasks, end_tasks, max_in, max_out, complexity
    global resources, min_res, res_factor, res_strength, res_use_min, res_use_max
    global d_min, d_max
    global tasks

    total_edges = complexity * (tasks+2)

    stderr.write(".")
    E = []
    reach = []
    for i in range(0, tasks+2):
        E.append([0]*(tasks+2))
        reach.append([0]*(tasks+2))
        reach[i][i] = 1
    n_in = [0] * (tasks+2)
    n_out = [0] * (tasks+2)

    # connect start tasks to node 0
    for i in range(1, start_tasks+1):
        E[0][i] = 1
        reach[0][i] = 1
        n_out[0] += 1
        n_in[i] += 1
    # and end tasks to node tasks+1
    for i in range(tasks-end_tasks+1, tasks+1):
        E[i][tasks+1] = 1
        reach[i][tasks+1] = 1
        n_out[i] += 1
        n_in[tasks+1] += 1
        

    # give all tasks one incoming edge
    for i in range(start_tasks+1, tasks+1):
        tries = 0
        while True:
            tries += 1
            if i > tasks-end_tasks: # end tasks are never connected to start tasks
                prev = randint(start_tasks+1, i-1)
            else:
                prev = randint(1, i-1)
            if n_out[prev] < max_out: break
        E[prev][i] = 2
        reach[prev][i] = 1
        n_out[prev] += 1
        n_in[i] += 1
        # All nodes which can reach prev, can now reach i + reach[i]
        for j in range(0, tasks+2):
            if reach[j][prev] == 1:
                reach[j][i] = 1
                for k in range(0, tasks+2):
                    if reach[i][k] == 1:
                        reach[j][k] = 1

    # and one outgoing edge
    for i in range(1, tasks-end_tasks+1):
        if sum(E[i]) == 0:
            tries = 0
            while True:
                tries += 1
                if tries > 50: return False
                if i <= start_tasks:
                    nxt = randint(i+1, tasks-end_tasks)
                else:
                    nxt = randint(i+1, tasks)
                if n_in[nxt] < max_in: break
            E[i][nxt] = 3
            reach[i][nxt] = 1
            n_out[i] += 1
            n_in[nxt] += 1
            # All nodes which can reach i, can now reach nxt + reach[nxt]
            for j in range(0, tasks+2):
                if reach[j][i] == 1:
                    reach[j][nxt] = 1
                    for k in range(0, tasks+2):
                        if reach[nxt][k] == 1:
                            reach[j][k] = 1

    stderr.write("o")
    # add other edges until complexity is high enough
    while sum(n_in) < total_edges:
        tries = 0
        while True:
            tries += 1
            if tries > 50: return False
            i = randint(1, tasks-end_tasks)
            if n_out[i] >= max_out: continue
            if i <= start_tasks:
                j = randint(i+1, tasks-end_tasks)
            else:
                j = randint(i+1, tasks)
            if E[i][j] > 0: continue
            if n_in[j] < max_in and validate_edge(i, j, reach, E): break
        E[i][j] = 4
        reach[i][j] = 1
        n_out[i] += 1
        n_in[j] += 1
        for k in range(0, tasks+2):
            if reach[k][i] == 1:
                reach[k][j] = 1
                for l in range(0, tasks+2):
                    if reach[j][l] == 1:
                        reach[k][l] = 1

    stderr.write("#\n")
    R = []
    for i in range(0, tasks+2):
        R.append([0]*resources)

    for i in range(1, tasks+1):
        l = range(0, resources)
        shuffle(l)
        for j in l[:min_res]:
            R[i][j] = randint(res_use_min, res_use_max)

    n_requests = tasks * min_res
    while n_requests < res_factor * resources * tasks:
        i = randint(1, tasks)
        l = range(0, resources)
        shuffle(l)
        for j in l:
            if R[i][j] == 0:
                n_requests += 1
                R[i][j] = randint(res_use_min, res_use_max)
                break

    est = [0] * (tasks+2)
    duration = []
    for i in range(0, tasks+2):
        if i == 0 or i == tasks+1:
            duration.append(1)
        else:
            duration.append(randint(d_min, d_max))
        t = 0
        for j in range(0, i):
            if E[j][i] > 0:
                t = max(t, est[j]+duration[j])
        est[i] = t

    horizon = t+1 
    profile = []
    res_lowest = resources * [0]
    res_highest = resources * [0]
    for i in range(0, resources):
        profile.append(horizon * [0])
    for i in range(0, tasks+2):
        for j in range(0, resources):
            if R[i][j] == 0: continue
            if R[i][j] > res_lowest[j]: res_lowest[j] = R[i][j]
            for k in range(est[i], est[i]+duration[i]):
                profile[j][k] += R[i][j]
    for j in range(0, resources):
        res_highest[j] = max(profile[j])

    s = ""
    for j in range(0, resources):
        s += "R %d %d \"Res %d\"\n" % (j, res_lowest[j] + res_strength * (res_highest[j] - res_lowest[j]), j+1)
    s +=  "T 0 0 %d \"autogenerated\"\n" % (horizon*5) #FIXME look at this
    for i in range(0, tasks+2):
        s += "A 0 %d %d \"Act %d\"\n" % (i, duration[i], i+1)
    for i in range(0, tasks+2):
        for j in range(0, resources):
            if R[i][j] == 0: continue
            s += "Q 0 %d %d %d\n" % (i, j, R[i][j])
    for i in range(0, tasks+2):
        for j in range(0, tasks+2):
            if E[i][j] > 0:
                s += "P 0 %d 0 %d\n" % (i, j)

    return s

#complexity = 0

#for t in [500,1000,1500,2000]:
#    tasks = t
#    start_tasks = end_tasks = int(0.02*tasks)
#    for i in range(0,10):
#        name = "%dt-%03d.instance" % (tasks, i)
#        print "Generating %s" % name
#        f = open(name, "w")
#        f.write("# tasks %d\n" % tasks)
#        f.write("# start_tasks %d\n" % start_tasks)
#        f.write("# end_tasks %d\n" % end_tasks)
#        f.write("# max_in %d\n" % max_in)
#        f.write("# max_out %d\n" % max_out)
#        f.write("# complexity %1.2f\n" % complexity)
#        f.write("# resources %d\n" % resources)
#        f.write("# min_res %d\n" % min_res)
#        f.write("# res_factor %1.2f\n" % res_factor)
#        f.write("# res_strength %1.2f\n" % res_strength)
#        f.write("# res_use_min %d\n" % res_use_min)
#        f.write("# res_use_max %d\n" % res_use_max)
#        f.write("# d_min %d\n" % d_min)
#        f.write("# d_max %d\n" % d_max)
#
#        s = False
#        while s == False:
#            s = generate()
#        f.write(s)
#        f.close()

s = False
while s == False:
    s = generate()
print s
