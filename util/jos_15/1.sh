#!/bin/sh

i=$1
if [ $# -lt 1 ];then
	echo "use: <instance>" >&2
	exit 1
fi

samsize=800
alpha=10
datpath=`mktemp`
echo "datpath $datpath"

if ! test -d $i.s;then
	echo "$i.s not found"
#	exit 1
fi

#for schpath in $i.s/s*;do
for s in `seq 5`;do
	schpath=`mktemp`
	./util/frits $i 1000 > $schpath
	echo "schedule $schpath" >&2
	for m in `seq 5`;do
		# generate dat file
		./util/jos_15/tms2dat $i $samsize $alpha $schpath > $datpath

		# cmax inst
		cmaxinst=`./util/jos_15/cmax_inst $datpath 2>/dev/null \
		| awk '{cmax=$1; inst=$2;}END{printf("%.3f %.3f", cmax, inst);}'`

		# total float
		tf=`./util/jos_15/total_float $datpath 2>/dev/null`

		# free float
		ff=`./util/jos_15/free_float $datpath 2>/dev/null`

		# concurrent slack
		cs=`./util/jos_15/conc_slack $datpath 2>/dev/null`

		# even concurrent_slack
		cs_even=`./util/jos_15/conc_slack_even $datpath 2>/dev/null`

		echo "cmaxinst $cmaxinst tf $tf ff $ff cs $cs cs_even $cs_even"
	done
done

rm -f $datpath
