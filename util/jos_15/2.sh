#!/bin/sh

i=$1
alpha=$2

if [ $# -lt 1 ];then
	echo "use: <instance>" >&2
	exit 1
fi

if [ $# -ge 2 ];then
	alpha=$2
else
	alpha=5
fi

samsize=850
NS=3
NC=3

durpath=`mktemp`
datpath=`mktemp`
schpath=`mktemp`

# generate durations
./util/jos_15/tms2dur $i $samsize > $durpath
the_cmax=-1

for sid in `seq $NS`;do
	# generate schedule
 	found=0;
	while [ $found -eq 0 ];do
		./util/frits $i 1000 > $schpath
		sch_cmax=`awk '{print $NF}' $schpath`
		if [ $sch_cmax -eq $the_cmax -o $the_cmax -eq -1 ];then
			found=1
			the_cmax=$sch_cmax
 		fi
 		echo -n "* "
 	done
 	echo ""
	
	for cid in `seq $NC`;do
		# generate dat file
		./util/jos_15/tms2dat $i $alpha $schpath > $datpath
		cat $durpath >> $datpath
		
		# cmax inst
		cmax_inst=`./util/jos_15/cmax_inst $datpath 2>/dev/null \
			| awk '{cmax=$1; inst=$2;}END{printf("%.3f %.3f", cmax, inst);}'`
		
		# total float
		tf=`./util/jos_15/total_float $datpath 2>/dev/null`
		
		# total float
		ff=`./util/jos_15/free_float $datpath 2>/dev/null`
		
		# concurrent slack
		cs=`./util/jos_15/conc_slack $datpath 2>/dev/null`
			
		echo $sch_cmax $cmax_inst $tf $ff $cs
	done
done

rm -f $durpath $datpath $schpath
