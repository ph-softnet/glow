(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20665,        488]
NotebookOptionsPosition[     20270,        470]
NotebookOutlinePosition[     20604,        485]
CellTagsIndexPosition[     20561,        482]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], ";"}], "\n"}], "\n", 
 RowBox[{
  RowBox[{"tfdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9923383356656105", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9924241205706841", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9945472969712568", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.991325001474428", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9937484250427584", ",", "0.5849056603773585"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9881509599867033", ",", "0.6226415094339622"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9836686986966057", ",", "0.6981132075471698"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9901615436993667", ",", "0.6981132075471698"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9900596741245917", ",", "0.7169811320754716"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9772348308160824", ",", "0.7547169811320755"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9926064134939655", ",", "0.5849056603773585"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9994584827867227", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.997238798367942", ",", "0.6226415094339622"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.5471698113207547"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9931372075941087", ",", "0.6792452830188679"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.97150332684585", ",", "0.8490566037735849"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9656645917442752", ",", "0.9245283018867925"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9770847072322036", ",", "0.8490566037735849"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9673052280538085", ",", "0.8679245283018868"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9743503133829814", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.984735648453459", ",", "0.5094339622641509"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9875987196602917", ",", "0.49056603773584906"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9828537420984059", ",", "0.49056603773584906"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9889712781414699", ",", "0.4339622641509434"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9878292665926771", ",", "0.4528301886792453"}], "}"}]}], 
    "}"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"ffdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9923383356656105", ",", "0.47058823529411764"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9924241205706841", ",", "0.47058823529411764"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9945472969712568", ",", "0.47058823529411764"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.991325001474428", ",", "0.47058823529411764"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9937484250427584", ",", "0.5"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9881509599867033", ",", "0.7352941176470589"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9836686986966057", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9901615436993667", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9900596741245917", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9772348308160824", ",", "0.9411764705882353"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9926064134939655", ",", "0.6176470588235294"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9994584827867227", ",", "0.5882352941176471"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.997238798367942", ",", "0.6176470588235294"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.5882352941176471"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9931372075941087", ",", "0.7352941176470589"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.97150332684585", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9656645917442752", ",", "0.8235294117647058"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9770847072322036", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9673052280538085", ",", "0.7941176470588235"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9743503133829814", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.984735648453459", ",", "0.5294117647058824"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9875987196602917", ",", "0.5294117647058824"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9828537420984059", ",", "0.5294117647058824"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9889712781414699", ",", "0.5294117647058824"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9878292665926771", ",", "0.5294117647058824"}], "}"}]}], 
    "}"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"csdata", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"0.9923383356656105", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9924241205706841", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9945472969712568", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.991325001474428", ",", "0.5428571428571428"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9937484250427584", ",", "0.5714285714285714"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9881509599867033", ",", "0.8"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9836686986966057", ",", "0.8571428571428571"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9901615436993667", ",", "0.8571428571428571"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9900596741245917", ",", "0.8571428571428571"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9772348308160824", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9926064134939655", ",", "0.6857142857142857"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9994584827867227", ",", "0.6285714285714286"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.997238798367942", ",", "0.6857142857142857"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"1.0", ",", "0.6285714285714286"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9931372075941087", ",", "0.7714285714285715"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.97150332684585", ",", "0.8"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9656645917442752", ",", "0.8857142857142857"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9770847072322036", ",", "0.8"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9673052280538085", ",", "0.8"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9743503133829814", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.984735648453459", ",", "0.5428571428571428"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9875987196602917", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9828537420984059", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9889712781414699", ",", "0.5428571428571428"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9878292665926771", ",", "0.5428571428571428"}], "}"}]}], 
     "}"}]}], ";"}], "\n", "\n", "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"tfdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "tfdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"ffdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "ffdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"csdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "csdata", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.6546875110916862`*^9, 3.654687531480452*^9}, {
  3.654687616880952*^9, 3.65468767177041*^9}, {3.654688172526739*^9, 
  3.654688232693614*^9}, {3.654688471147828*^9, 3.654688480901989*^9}, {
  3.6546907062660503`*^9, 3.65469070855687*^9}, {3.6546914537626343`*^9, 
  3.654691456195821*^9}, {3.654692123982038*^9, 3.654692126355588*^9}, {
  3.654692165607965*^9, 3.654692168005849*^9}, {3.654699443036027*^9, 
  3.6546994572940407`*^9}, {3.654699562115329*^9, 3.654699599708886*^9}, {
  3.6546996495287533`*^9, 3.6546996530859823`*^9}, {3.6546996851775*^9, 
  3.654699689821459*^9}, {3.654699844963196*^9, 3.654699873971775*^9}, {
  3.654700025002277*^9, 3.654700026939303*^9}, {3.654700329388076*^9, 
  3.654700369315352*^9}, {3.6547004206443367`*^9, 3.654700437216226*^9}, {
  3.654700492749138*^9, 3.65470050616709*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQLc3uEWxz8L19fup856ymh/aXZm22+oDEP6MUdSvk
MoJv6rbQ9P0OBN8g2KH+5Jn39r9lgoR7tj+y3/ZG5PC7ee/t26+wPF775rF9
D1vCcbOq9/Yg1R/Dntmfc/rnlL4ewU9p1lswcd17e7BxH5/ZX91/+1ej63v7
T2HfLJeovbDfblLRlncYYX73a43oCb8R9ifFaoYnvkTYxwAGH+Dy264lrjt+
/L39zecTZiXseWrf5ySh2if23v6CounBCu3X9gt8+PJ2Pntn/wIkPfWtfWjt
fVV3F4T8xq3fxbW+vLOfVsu3kvPYa/tHFVFbH+i+h9tzmHlV6Jem9/ZAy4s9
fB/Yn0wNWJgx67291RI1ifzU+/a23I82dBYj+HYLOSWWL30PNe+2/bEmR3mf
Oe/tlUDW/b5jDwDEr8q3
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVj38803kcx8dKlh9N5cecsmnTpvwIdQ/Xj/fnypFCspTuhCMdWVLRUelO
mvIz4VwYD1l0atmcSHe01Rxd5UdY7djth80sybcflh4VbvfH+/H65/16vJ5P
SszR0DhjHA4XZLj/c0ec7qnoRdgWNzvbJpkZA4037o1RkLzBp3jq5gcCA9mY
SQqbSb7ga95Vv9yUgdx8ltdVkfbAd7hUTuACBvqq30tQTDoIF3UDF9o/05Go
Ury+gJQC6rbCSM4rOsrKzEvLJZ2HK/tNzCP66OhgCkbKIZWCcaX+0EgxHZmW
Blx9/eYa1Eb9mMZbQkd3+j39/VfywVOrWOj+y2oUniUbFufdhuuNdny7FavR
0aYTrlem74Kubbp5qMIZvS169tDISAgk9h3vrRRn9PB+l7Bv0wOwKVlhda6K
hhLSAyz2T3bCh1EmgeFAQ/cJnvO8I93waDo5I5NLRfXczjl72iOI374uiEem
ImTTUctsfAIfDkivXKxfhVy7ZW1dLn2Q2LIL98RtFRpCzKmGqX64EXxYrOI5
IdbixAGJxwAMNyQaN3g5ocfSfR2WhYMg8SNWbRVQENMvXKXRDEELjc3c5k5B
G2ry9tVQn0F2l5KtbyUj5VzJUPxPz8GpQbHslDcZjQX0cW3EUoiqzN0YneqI
BOWK9KeUYbCK55gcO7IS1V3E5zBOjUB+WzAh5PAKpI/9duLQnzLQ5ffii9Mc
kJlvdmiTrRxkmtKWtONfoDPEMXNVhAKaRPSoBxn2yPzeGtzzICUEkw+g4QIS
2nxy16z4oxKy8FJpWY4d4lzL567iqKD8MDmhtcwWRYZ59xB2joJ/GF9w6pIN
apHEldx7PwodYrm7RYE18nkiMNaWqcG6daTemLMckb85pC/x1YDJWkcJp2wZ
Wu+9/vJtrQae0Tw+ldcuRYq3BX9z2WOQzFWFxzVboUmv3WySpxZqrv1zRMIn
IueXrhNGg1qIZ27Rh5xegjqpqQPJmePQfnBvBkqyQJeGJLGhVB0MZRD0+iwz
hDFFl407dXCmenbEn0tAv4dZb/M59gLCu86uU5cvQtyerKrRpRPwWl9x0rd5
IZrpjji99MEEtK/1dcG4eKS8LNQTk16CXaJb0IZqI/T+6UbSOctJiI/5QZh9
ch6SS51E6e2TYDqbd98r6hPY9jgRNZGvgH5dW8sqmAGmhPfZYe4VxEeLY+S9
7+Bt1FWW7MYUEENS5v/KwEAYzWdbBmIQPdczo9WNwx99rJ3yYAxEOF6xefc4
tG5xsWrcjQERn+vqWTcOPIc6TtA+DPoX+cVmfD8OFdKK5vwYDJDVvd5lw1pI
DclWLU7HgExtrEOPx2ANOrB5UT0GyoBCZsUtNTjz7fHPfzP8B7KmhHlqcFop
fVh/E4Oi4B05YwlqsPscusevCYMQponQw1kNJm3bE9ntBp6Isy7d1aOgcvcu
xw9iUHM0yWj6kgpk1W8iByUY4I4HckhJKpBa8KlcqaGf4vIlBKqg9yVDsFVu
4E/XsnJNVdB+3bE78wUGyecipeRMJbTZ/Ju/e9Lgc37TCb8oJTSzK0IpGAYe
F+wtWZuVcCPWWi6aNuzb3rqQ9E4BdQMD3KIZDH52SOi4y1dAzddFCdEfDT4U
2rsFLAVwBEHuHrMGX5qKHkJXwK+OZvr5eQOvS1VkpUYO/wEeqlSP
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9651281009902796, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9656645917442752, 1.}, {0, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.65470050840979*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQLc3uEWxz8L29HBjcs780a7PVByT+GaWoWyGXEXxT
t4Wm73cg+AbBDvUnz7y3ZwCDB/bb3ogcfjfvvX1HOwg8t+9hSzhuVvXePhMM
Xtqfc/rnlL4ewU9p1lswcR2Cf3X/7V+NrjDz39lvN6loyzv83v7IYRB4bN/9
WiN6wu/39pcvgcAj+6RYzfDElwh5iDs+wOW3XUtcd/w4wj19ThKqfWII+xb4
8OXtfPbOPgoMXtmH1t5XdXdByG/c+l1c68s7OP9RRdTWB7rv4fYcZl4V+qXp
vf3HDyDwwP5kasDCjFkIvi33ow2dxQi+3UJOieVLEfxjTY7yPnMQfABHmM7q

      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVjn8803kcx+dHVygNs5mLtplVPBxp5XTx/pA4dSrbhBRKXN1cdNYP/VCS
fohO6aF03EOtyZUfE3N0NDWx6xJh8shivzI/xjcewyV0uz/ej9fj/Xi/X4/n
k7o/iRVvjMPhQgzzf26NH3rTNBzmW7+v5kRFLANpK3btHyAzwTeLGWUUw0BE
C9m1anIASB7X+HD2MtA33gRBEZkDr01ExrORDLSxY53wBvkAaASi7C1sBmr6
TbI+h8yD5SO1xfItDJSRfvVEFvkCxP9SLzV3ZaADPIx8hXwTrDKe2v045YyW
3Ay++3HiPihq60vI6c7ozw7PoCDHSmg/0++TTnBGERnyd5KrNfAgPMnF/CEd
JVWluN3W14N+84RpzgY6msztkRoZiSFOUjjU/NIJSZ+1iNs3PYegMO8n02FO
6FBq8LJIXTPYzPopg9U09MzM80vZz62A8+RZsU7QUAm/ecHe+SXEnb7lzjGl
IURsvMeueAXaSM0ui9tU5NYqr2txaQf21qIutRsVdSP2+B/jHcAMCfKtfUJB
iebcTplHJ3yrtn/ss4OC/ukNb7S81gWxJPGqNNFKxA6MUGo03UC5RsmXdDii
DcVXw4vpPUB3lRFTPjsgxUJe98Gzb8F6jJtCJDigD8HtfKKkFzbLwlROm1Yg
YcFA6hvqO4hRUd2cWV8jwWWTK2tO9sE22xbaoUR7NBW3eyThLzmkHm50IeWT
kUXARVYVqR/ydPmW9eV26DT+w1LlngHAciWmL7pJaOlTV9zbEAXYR0kFNjgS
8jm2Y14yq4C1Wytpu8hEVHg/m+9UqIQrMcNVxv62KDqM2Wa2TQVtBYsc+BEE
JJLF5z2dVoHy0yfPiyk2yPuV0HgwXw1e5/yiuoqsEWVLwlRegAZOunJjCLVW
aD1z/fWaQQ0szGmPit/j0cBkzt/8zA8Qqt/kVW6GR7p1oZlkz0GgUIdKHYIs
EWPUbcSoaxDoJXOLRQVLUTP9aGdyuhYWeDJHptQc/doti2PRh2BPLmf3JVsz
hLGbrhs3G/aZjNEY7mL0OMx2s/eRYfAoPc6kFC1C/LaMIpX1CFwWWN2rGTNB
M617Tlk/H4E6XVKrh70xUlwXT+EPj8Lls2n+njtwaPrNd+Tzljqoa0xoXh44
D8k3aU2pDTpYgo9MyRJ8AlIbDa+JHoODn29xy/2ngS0rm1uxMAZL0ryW932c
hMmYu4nyh+MgXC0OEJaPgzi2MtPyBwx2Xiqdn9Vr4Ul74rb+7Rh8DMx4NNOj
hVpfF6uKUAzOfRUdqa/TQtkKQWFIOAYdmTaisdNauNN7pzp7PwboQlqiwlQL
R3deVJqnYuCRzul7QRgEV7TXZ3EJBrhTuPpcpgYYlfYmb0sxKN7Yl5BD1ADN
sVda8sjQnxURsv5Vg90cixNYZeClcpMzGtTwVd333MwGg8/xnlXHAtSgdGcW
mHRhoOCV5UdxVCD/fSK6S2b4Z14KiNiggt5llXR+LwZ4/b5Jjp0KXo+uEfr3
YxCbQtq+Xa6EhgcrW9OHDf5Hzi/yO6CEOuL77FCd4b52b7VPoBKqM++wqJjB
d8Jr38bVSngYZ9vfpDf4JY81rNMpQNDZyc+dMfDdpT95vFZAsV/uodhZAx+7
Z+cmVEChMMTdY97ArzzTsuaGAm6ttJj68gUD4eEIHoOngP8ApC9P5A==
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9651281009902796, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9656645917442752, 1.}, {0, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.654700508506639*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQLc3uEWxz8L39R9F4IHpof2nWZqsPSPwzSlG3Qi4j
+KZuC03f70DwDYId6k+eeW8/ScUTiB7Zb3sjcvjdvPf2s2aCwEv7HraE42ZV
7+1vb8sFotf255z+OaWvR/BTmvUWTFyH4F/df/tXo+t7ewYw+GC/3aSiLe/w
e3sxkHUfn9p3v9aInvD7vf0VR1kgemKfFKsZnvgSIQ/TB5Pfdi1x3fHj7+1/
dq3/0bX+hX2fk4RqnxjCfQt8+PJ2PntnX3M0HIje2IfW3ld1d0HIb9z6XVzr
yzs4/1FF1NYHugj3HWZeFfqlCREeJ1MDFmbMQvBtuR9t6CxG8O0WckosX4rg
H2tylPeZg+ADALxSzIw=
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVznk81AkYx/HBbodr5R4rY4zaLpGotcXzlFIqm6PDdspVyrmd2N0i2loS
US0ZS4Naq4xOWsxglrZYxhjNahpzMsYw29RkXx30mz+e1/f1/ufzeqiRSaEx
hiQSKYg4/W6KUXLZo9v9AuNyBjLbaThyZ0fkENkLAh66PTZto6GtCT/vHnkd
+Bv10q+yabjUx7qKTt4GvnTrmJpmGn7Tu5x5mRwNy7j019xHNGRfb/e+SD4G
Dj5MC2otDc9m5Jz6hZwFY7P4m1hFNIw+piFfIBfBxVtOrPcxNJxVFFjx36tK
iJ2yqr9KouGjXs8NG5zqIPnaNs5ohQuGnxUOtufch65o3wD79S6YVH/U7dc3
jdC08Tz3kJiK2vyBJwYGLHiZ5PRTSiYVn7R2sHpWt0HhbVuvUkcqxqUGmn2n
5sB1yqPwGw3O2Drbc7o2oRN4/TfKInY7YzWDM+Uw7yngGsmLDDUF0bb5Rtid
Lli936r5rjsF3TqFDR2LemBQcSW95IQT9mPYxO8TvVBZLNzScXsuxhsf6eN7
9EE3rzEiUuuIzwQ7m83zeOC8avfPa79yxLCAcIlc3g/2JkE5/+7/EleU5+ws
dx2A5AN8rrjKAcVThf2HTj8HrZy6eImQjIrAHoZtuwD4E591jlDIyCweSuVS
B0E6r/b4SJg9Vp03urAw7QVc/othUJZjh7qoXarYP4VQObasl9NriybrzoXW
24lgyC+r3n2GLf5goTCV7BmCdPsEvnGgDZq2LCY9DxLD413VA6WXrNH3xNaP
7e/E4PqUofq+wwpLK3MZtFIJjLNWrmk0tsJ92726Z2+WgkR6qcYfLPEBP6aw
5a0UTiV+/Srr5Bz06WIaDl+VQcqZk2krmi3QeX2srnCdHBac1i0JH/4Cvb28
C+4Py2Gp8UIVLcEch7QX/2ZkKyAt9uOBve9NUb08JJvsOQyOc3dNOx02wflj
bioD3jCcEcuora9nI8f1eF9yxgismNLFje2dhZf6+VGhrkpgLvBe+VY6AzVh
7AJDjhISXW1YiUmf493tNv4+KaOQlfZjfJLQCBndZ+lSSxWEv7T8LTjaECc7
96Rbtqkgs8RjsrGLhOICls4icQzkISIrl9QpeMtdRc40V8PG8w/8C/LeQ3KR
Czu1SQ3hR+ktFjH/g123i4V83zgoTw75h6ToIIxf+8FxahxIlCi/hFwtaPdX
xAtrJiBfNzFzOXcCWBF12eZbNOAsfnbTTKeExz3xm0XfEg5u85ANKuGh36I5
d0IIsxsaG9hKqHWsKg3aSbi86llUrhJKBCX3ciMJR5zWNNKUcDz4nMQ4Vd/z
XBkbOgKLca/vzGoNeIiLO1rqFDC/zsHo+S3Cwflbi64owMVJ8KT6D8Lsc4K4
dAXYfwjdFlBPuPyoynqDAmY0bDyS3UQ4Isj8iEgOEnevYiOevkfaYWcmB2HZ
q308vr43KVJrZSAwq3NlCPS98YNtAhn8M7aQuVak7w2mJlTKoOkmpTNjVN+7
T29fJYMG25e5IWrCvTXzi6kyuJddEkrVEMaKusSZMqiJshGx3xB2zmsl86RQ
1dfHyJ8knJ+1SdMghfI1+XER7wiT0nmcMimUMoPcPT4STk7ZU5IlhWsUE930
tP7fg4qkw1L4BFM/X4E=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9651281009902796, 0.52},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9656645917442752, 1.}, {0.5428571428571428, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.65470050860003*^9}]
}, Open  ]]
},
WindowSize->{1920, 1017},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 9938, 243, 737, "Input"],
Cell[10521, 267, 3292, 66, 236, "Output"],
Cell[13816, 335, 3216, 65, 236, "Output"],
Cell[17035, 402, 3219, 65, 240, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
