(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19987,        480]
NotebookOptionsPosition[     19592,        462]
NotebookOutlinePosition[     19926,        477]
CellTagsIndexPosition[     19883,        474]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"tfdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9793505349747972", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9865178855847248", ",", "0.6666666666666666"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9762399985009462", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9824704405344127", ",", "0.696969696969697"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.977523563250698", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.973551070886503", ",", "0.9393939393939394"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9781700301684562", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9761650458148294", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9712650139599378", ",", "0.9393939393939394"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9715273483613469", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9832480746528754", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9851031536342685", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9696347930368955", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9733917964285046", ",", "0.696969696969697"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9819926171604175", ",", "0.696969696969697"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9738227743736766", ",", "0.9393939393939394"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9735229636292091", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9742912286619072", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9684168118874961", ",", "0.9393939393939394"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9766990837034123", ",", "0.8787878787878788"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9982948263908409", ",", "0.24242424242424243"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9912586429816179", ",", "0.3939393939393939"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.36363636363636365"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9895628384582233", ",", "0.42424242424242425"}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0.9967864035827385", ",", "0.3939393939393939"}], "}"}]}], 
    "}"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"ffdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9793505349747972", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9865178855847248", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9762399985009462", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9824704405344127", ",", "0.95"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.977523563250698", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.973551070886503", ",", "0.9"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9781700301684562", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9761650458148294", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9712650139599378", ",", "0.9"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9715273483613469", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9832480746528754", ",", "0.75"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9851031536342685", ",", "0.75"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9696347930368955", ",", "0.75"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9733917964285046", ",", "0.8"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9819926171604175", ",", "0.8"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9738227743736766", ",", "0.9"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9735229636292091", ",", "0.95"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9742912286619072", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9684168118874961", ",", "0.9"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9766990837034123", ",", "0.85"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9982948263908409", ",", "0.4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9912586429816179", ",", "0.45"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.6"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9895628384582233", ",", "0.4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9967864035827385", ",", "0.45"}], "}"}]}], "}"}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"csdata", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"0.9793505349747972", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9865178855847248", ",", "0.8095238095238095"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9762399985009462", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9824704405344127", ",", "0.9047619047619048"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.977523563250698", ",", "0.9523809523809523"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.973551070886503", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9781700301684562", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9761650458148294", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9712650139599378", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9715273483613469", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9832480746528754", ",", "0.7142857142857143"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9851031536342685", ",", "0.7142857142857143"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9696347930368955", ",", "0.7142857142857143"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9733917964285046", ",", "0.7619047619047619"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9819926171604175", ",", "0.7619047619047619"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9738227743736766", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9735229636292091", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9742912286619072", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9684168118874961", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9766990837034123", ",", "0.9523809523809523"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9982948263908409", ",", "0.38095238095238093"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9912586429816179", ",", "0.42857142857142855"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"1.0", ",", "0.5714285714285714"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9895628384582233", ",", "0.42857142857142855"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9967864035827385", ",", "0.42857142857142855"}], "}"}]}], 
     "}"}]}], ";"}], "\[IndentingNewLine]", "\n", 
  "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"tfdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "tfdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"ffdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "ffdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"csdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "csdata", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.6546875110916862`*^9, 3.654687531480452*^9}, {
  3.654687616880952*^9, 3.65468767177041*^9}, {3.654688172526739*^9, 
  3.654688232693614*^9}, {3.654688471147828*^9, 3.654688480901989*^9}, {
  3.6546907062660503`*^9, 3.65469070855687*^9}, {3.6546914537626343`*^9, 
  3.654691456195821*^9}, {3.654692123982038*^9, 3.654692126355588*^9}, {
  3.654692165607965*^9, 3.654692168005849*^9}, {3.654699443036027*^9, 
  3.6546994572940407`*^9}, {3.654699562115329*^9, 3.654699599708886*^9}, {
  3.6546996495287533`*^9, 3.6546996530859823`*^9}, {3.6546996851775*^9, 
  3.654699689821459*^9}, {3.654699844963196*^9, 3.654699873971775*^9}, {
  3.654700025002277*^9, 3.654700026939303*^9}, {3.654700329388076*^9, 
  3.654700369315352*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQ/VH34btrYe/tGcDgg/0BnQ9feie+tw8Fg6f2YlXB
q6JtEfIVljk3Uwve2/uZ3UyZ7PvMviz1ed19d4T8FH2/ySHq7+0F7H48aOZ/
Z/+HzcBG2/e9vXzNh4Ps8m/sN+3JqNpng+Dzz78WO0sEof6DaU/rATGEfP6y
/lVHyt7bx5ZcdBWPfWL/obSR53srgr+XfWm3PTuCLxdb3cumhnCf6KHPx6tz
EHxFOcbEPk2EfX0aYQsl1RH23bCtOZOii+CLNf1zcP37Dq5e0elivpwjQt5r
9eO/7J/e23OABc7bvylfz52y47092Dqzm/BwcRMHufC6/aOqCevrV723V5+d
s+mk2m37Pe39M9c8RagHAOZ/uKI=
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVk3s01AkUx722kozHRkYP0aSQDDPTW/fWToTYjIROSUJZdmWbNjbbksau
qB1bZ/Wgxk4oFSNTTQ+ZkEclGYY5sfOwg/HIGBntOmJ/+8c993zO93u+33Pu
OdcxOokVa2RgYBBIzP/bP1bTJh4M3fpL8/i96ZcMHCjfG60g0yEx35kdWM9A
WzPphSoyE3bH7tt0vZaBazcuLC4k7wE7w9oGqGHgpnc0we/kGLi9IU9xRsRA
8bU6xnkyG16XeliZ3mFgZkZOyjnyWbDISmTbcBkYw9aSs8mXIH97/6a1+xg4
75Jf0ZjuJmjue0QE9dHx0TsvX99lFZAhUNHdU+gYntnzvi5HCKZC3kKqOR2T
Ko+7X554DJLyBxzHKzQc53Y2GRrWQO86mrBtDQ2bXjTUtG6pBdrDOexjj7ww
PtXPPGKkHtibB9bTmF74wtRr9u63jTAdSH0slHliCb9+xn7lK2gIa31NifFE
tK3+M6T8DXSOdl4L+URF98YeUYNrK3TmDCeV5FKxA0NGb4++g0XL90oH7KiY
OD9BIqVKADJZY60RHvhaFlZNutAOEuHN8HtJazHEJ1ylVncAc9yyWZLtjut4
OWE8Sicw/YOEJ6rWoHLmYsfRn7ugibu+Vv/KDfv8Wvm2dTKQ6XT+H3WuKLii
SG1zfA8J28InW+a4YvGvxtkuP3aDvEB2e8zBBfWH9w3FPe0BubWrjLtzNZox
s1iVi+Rw6OQqF9LBVZhm2bdAtV8Bxyf1E/M4zrjguZtBV6ASojPkTQEVK9H7
h68/100pQTr1fU1CIwULbubyVxSooOOCjGqqXYGRofQW04BeCLMaqs80WYEP
pLEXn0/2guiGzSGfZU648Y3AqP+Pv+HWTuuULF9HXL4jTn+RqYYlyieV/Mjl
yKAz8oT9athAiX+TNt8BFePnm/mcPpi8wd2xPWopjtCCOWSvfnjyj4VJbd5i
dB52HzJs74eCLE/Rll4y1lNOSI5lDEC6rqwymWSHv3VID7MoGjib18ArirBF
bYg4z6heA0w/W96hewvxfqjNVxuTB2GevFrd3WON/JbMwl7rIdCUznUIdrPC
T437T1nXDgGvkOVteIeEyrwaveV3w6Cxn9mfbmmGk22byWdII3DUaPSIvmou
HrvkJE59NgJj/POf5SQTXNTiZKmO/ACCiuTTz0sNMER6d3rJzAdQSk9BGn8K
xg8WJfaUjUK6+Hh+TNxHqImq4JB2aUE5o8lJ9h+EJ62JAfIggu9nhDiQBuHh
Vler8mAtiOPsF7e0aeDukuKCwDAt8N4GlK2O0MBV2dWq3GgtRF0vb1IcGYAT
u7NU81MJvzf7iyDijm54wHtuCeFPmz3tKlaBc4W9cdctgqmXfWSZKnBaJmsq
uaOFdDWVlOWrArtp1h6fSiJvV3Sh6q0S5oh2JnCeaQE3nDvp+VQBKg/6FeN2
LRictNGZT3ZDz3VdZLuUYIVPevxP3SAzr6DwZUS/T4rFS5NueDvsItguJ/Jt
u93TvnwPz0odGjMGCf0B75shqgxEtn/lBo8Q+lLJvztEXVDFucpy1BLMMc4u
gi4oO2wjF08Q/aFxpeFBnVAskfC5n4i+6vx1QqkUeNu48VFTBK9sfmlxQAoF
gkAP6mciP3dqTwLxV/kOZvrZWYIn3NQNCR3wH6I+YQQ=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9679233245732383, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9684168118874961, 1.}, {0, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQ/VH34btrYe/tGcDgg/0BnQ9feie+tzcGg9f2YlXB
q6JtEfIVljk3Uwve26eBwTv7stTndffdEfJT9P0mh6i/tz97BgTe2P9hM7DR
9kWYt2lPRtU+GwSff/612FkiCPUfTHtaD4gh5POX9a86UgYz/4X9h9JGnu+t
CP5e9qXd9uwIvlxsdS+b2nv7WTNB4KW96KHPx6tzEHxFOcbEPk2EfX0aYQsl
1RH+uWFbcyZFF8n/Tf8cXP++g6tXdLqYL+eIkPda/fgv+yeY+Tft35Sv507Z
ATP/DjxcIOof2z+qmrC+fhVC/Z72/plrniLUAwBRNLoL
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVj3s01AkcxQe7bSWanAw/2yHjUREmRh02vt/ayHPKTCirEhFLo3dLu+2q
pRflMW2xatVEKjG22hSFZkKpPCdD0zwYYjC/smG3MDv7x/fc8z33nnvuxzo6
mR2rT6FQgnX3vwbEDrbVDYV6R/Me2J0ABr4rD4uWE0x4ImeuCvBmIM1QfPYO
sQ5sHAV+Rl4MdPZYWHyJ2ASq+pLEPE8Gera6CXKJnRBH5lX+4c7Aut+F7lnE
AUgM4HpVOTLweNqZH04Tv8IBik3okBkDdx4giVMED+Z1m8+5oHDB2Tz/K+8/
XAOv7i2pzUwXvN/qun69ZQV4h+ltj+Y54+bj0h7hmbtgvubQC8tJJ0yu3O90
8eMD6D6SMZoR5IRj2a+b9PRqIdFtOi3k9nJsqm+obVn9BPo95z9vpCzHhBR/
oy0jIlgl0BTEhDti/RxXbdnuRoi4ypTveOCAJXzRjIXdc9ipMu0yNHZApD26
yil/AcjfutacuwydGqVVDQ4t8HfLvpUBbUuxEzmaG5pWOBtkI1uyZCkmzU1s
FzPa4ZOP57+7Ti7BZkn4I+OzHcA6N9LIkdojx3ezUqXqhFTfJKtYN3tcWXQm
vMj2NRxOZasl5+1QMZPXGf9zF3w30xH/j9oW+/1b+DShBKja6ojUQFsU5MtT
2qx74PYGoJRetsHikwanlqW+AbpxtvPnj3Qcj4lQx1VLId5/fasgjI6G6zLY
lWYyON+Zb2dyyxp/pPbPU0bKgSdla8RzrXHeY0dKV7ACEtjdULt7MXod2jAt
/KQAFmPiVGmGFRZey+TbFCrh+3uzjlzws8RtocyXcwJ7IawnuOMtuQjviWPz
Hk/0wliPi1fypa/R44VAf+C3PnAVy7Ke7rDAxT5x43nrVIDJIp/9hgS6M91z
7g6oQKN/WNXfbIbysaxn/PR+YO0T8fi5NBxxC0knXAfgaJpfWZW3KdoPO6n1
OgaAdTr/Zq7eQhTZHmzfk/YONK7WVNENEzzXKY5h2w4CQ3ww8mH8AiQ5dTn6
okGgc2Iffnai4p+hpt967B2CJiVNu7TSCPkvj1/qNVFDJnU6TOVmiJONkUdM
nqjh/dGNP7HSZqMip3acyh2Gu9e6G1hvvsSJtm+IY8YjUPNo6q0k2gD38Oh1
KTUjEEXLEi64R0Gzl3SqatsoXBRxR0sTpoAjLptaNDMKjGbngohjkzC2/UqS
9KYGKCHP+hIYY1AbVZFuHESCINUlh9ivhoctSYEyFgnoFuQh8lXDX94OC8pD
SGgdjldyLdRQtqi4MDichPeRV1eI6oegQFJwJzOahMVg2s6lDsHBjRnKuSkk
FBl8NhHdfgeOuNXrqxISfsls4HEHVWBfYWHQVarL+/StJmpUQLeUNJXc0uWn
tSrhORWYT7E3+VaSUMf1cCdWqWBWlV9ieg2p21smFqb3gdKFmW/QQcIe01wa
YdML0ssftnWIdf6r8sfCCSVIjCps+RISsjOa47jPlfBqeJlgrUzHN/nFfeFe
JdRct2pMGyJB0X04nFungCra28yQEV1fLk9rzlPAnfQCtjWp6wusvC7cpYCb
Maayuo8kUGvUk+bzFVDc3s7PniQhasgYTzyVQ9Ga7ISoTzq+CL9saZIcCgXB
Loxp3d+cplixUA4XrAzHtVodz+pqxolqGfwHZzZbsQ==
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9679233245732383, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9684168118874961, 1.}, {0, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371955798*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQ/VH34btrYe/t5z2vBKJ39gd0Pnzpnfjevgos8NJe
rCp4VbQtQr7CMudmasF7e9vzn23Of35jX5b6vO6+O0J+ir7f5BD19/YMYPDB
/g+bgY22L0J+056Mqn02CD7//Guxs0QQ6j+Y9rQeEEPI5y/rX3Wk7L399tzb
23JvP7P/UNrI870Vwd/LvrTbnh3Bl4ut7mVTe28v0ZYIRC/sRQ99Pl6dg+Ar
yjEm9mki7OvTCFsoqY6w74ZtzZkUXQRfrOmfg+vfd3D1ik4X8+UcEfJeqx//
Zf8EM/+G/Zvy9dwpO97bg1wDRHB9k1Q8geiR/aOqCevrVyHk97T3z1zzFMEH
ACh6zZc=
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVkHk41AkYx0dam7Oyjh0pTJMcOZrBsyq9b6VDsrtoli4mUblKiVb1PBGj
dbM8WyM2NaF7xuqgJSPzOMo5ZmpoMKZxjWNKRCr2t3+8z/t8n/f7fD/f57UI
OukTsohEInkR8//eHTLcwR9hbA7ssLNa/4SOQw9/C+ojO8EAe0vug0d0NNIW
Z5aT3SE8iDFvXU5He1eD4kLyXjj78YKIwqPjhnY6709yMGQaNscb3KUj/1qd
cwb5DFTvC+uaKaRjYkLa76nkJDCRl6Q+Z9Ex+IyKnELOA/Gk2bgng45L8jxu
vP9wCyKbKlxwjIZP22k7d67iQvvpphybVBr6J0q769IewakNOufUbGh4siza
7upUJTg/nR4sq12Pk9mvG9XUakCg4Rfiz1yPjbX1NW2bXkCsduBxySdHDI3z
0N03JoAQe/ase5Yj1mrSFu5HNoAZ1TO/1N4RSziCeZM1L+Fz4c6EhjQHRKPq
m74Pm2EXe/i9baM92jVIK+pt2sCgY/xc6BJ7FKHvxJ2JdlgTjXr7f7LDCK1w
odhRCM3PLglcYtfhK4lftV5mJ1RG6CfVc2zRd4d/v0IhAsoBjQAHsQ26FKX5
FVFfg7lqZH+SgQ3K5nNFxy++geBxk8Pp26xxwKONY1QnAbecjanx8VbIY/fF
dVh0w6Hrd6rWPliLxX+op1ifewuzI+c1b/dY4vSR/cqj/0rBqtstzsDUErXd
k33KjHvhupPXJH33GrywbECn/2Af3HPakwnJVNR5bkt64yUD2imuO7V+NbrF
/vKtbk4GC8bMxVXTFCy4lc5ZXdAPh4NJQ0CnYADDqUXTUw5aKyaEsUwLfCwO
yX3+SQ52etiVnGOOrs28RYN/vYNmL/XvNG6Yofn2o9O57gpo9bUIC55fic5O
zjmPBhUwe2uV4bWzptg3mdHEYQ2A0UnFVOKsCY7RvVlk2iAIdLcPCf3JaDlq
p1TrHISCmSSzOZExCqgxwqiEISA9CQy77GiEWSLxER/qMGTXkL+MlRqgypef
s0gwDAZbC/iL1/6A/zAMt7meGgErduXVS6zlyGlJLJTrK6ExNiKybGEpzjQc
PK//Qgn+wddarct1UJZTM73sxChIth1ga8Rq4qeOjeRLemPAq+xS5UZpYFQe
hR9XNQZFkozwcWt1NG6hLFMEjIMsgxQ9Y0JCX/H9r6bz42AetqTxlcscTAbe
iJDenQCe1vkpfPwRaphclt4eFci44ij7HaPwrC3Cs/dnFfDvVa+cWToKTzbb
LH/orYKi0uKXNV1KuG9aXODlp4L46zFU70gl5Evyy9ODVEDKMpRE541AzK/J
/VpxKjA/wYBK+RDY4iG370tUwFwn1nW/qABLron6m9sqQKvqZzoeCqCskjSW
3CP81OJjYn0F/PjVZ++OMiLPNKb2aMk70KjYFc6qInjahjEpLXLod3Biq3cS
fZR7pa0r+kH694eATjFxH9yUcmVABhJdLpUjIXhyqguTK4PWUWve1l6C1z2V
9WGrDKpKzRoSRgh/rvVlK1EvVBj1pHuPEby0z7LKY71Qzsr3sVAROrFpg+eX
Hrh7xLCXP0X4T4dORFJ6oFgo5GTPEHlhrrsWHkuhaEt2KHOO0EGaN7M9pFDA
83Jw/Eb8b1/XF4uet3DFTHt6YYHQ3ncY5VFv4T+jNVed
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9679233245732383, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.9684168118874961, 1.}, {0, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.6547003720415087`*^9}]
}, Open  ]]
},
WindowSize->{1920, 1017},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 9504, 240, 627, "Input"],
Cell[10087, 264, 3182, 64, 236, "Output"],
Cell[13272, 330, 3137, 63, 236, "Output"],
Cell[16412, 395, 3164, 64, 236, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
