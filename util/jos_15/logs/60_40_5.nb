(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21229,        490]
NotebookOptionsPosition[     20833,        472]
NotebookOutlinePosition[     21167,        487]
CellTagsIndexPosition[     21124,        484]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"tfdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9928881774525498", ",", "0.7521367521367521"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9954569708057016", ",", "0.7350427350427351"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.983184693955332", ",", "0.8205128205128205"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9916609497675128", ",", "0.6068376068376068"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9814848692735727", ",", "0.7692307692307693"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9960362832532966", ",", "0.6410256410256411"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9860507660644867", ",", "0.9487179487179487"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9968518941992529", ",", "0.7435897435897436"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.6923076923076923"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9891912493330284", ",", "0.8888888888888888"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9963030718804788", ",", "0.7008547008547008"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9823767055415809", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9811189877277231", ",", "0.8290598290598291"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9938333714459944", ",", "0.8034188034188035"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9917143074929492", ",", "0.7521367521367521"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9952968976293926", ",", "0.7435897435897436"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9899077673603173", ",", "0.7777777777777778"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9991615214574283", ",", "0.6068376068376068"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9919582285235155", ",", "0.9658119658119658"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.989557130878878", ",", "0.6581196581196581"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.97658358106563", ",", "0.9572649572649573"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9856543943898163", ",", "0.9145299145299145"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9901211982620627", ",", "0.8461538461538461"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9861269913865387", ",", "0.8290598290598291"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.994008689686714", ",", "0.6495726495726496"}], "}"}]}], 
    "}"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"ffdata", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0.9928881774525498", ",", "0.7272727272727273"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9954569708057016", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.983184693955332", ",", "0.5909090909090909"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9916609497675128", ",", "0.5681818181818182"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9814848692735727", ",", "0.7272727272727273"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9960362832532966", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9860507660644867", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9968518941992529", ",", "0.7045454545454546"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1.0", ",", "0.5681818181818182"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9891912493330284", ",", "0.7727272727272727"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9963030718804788", ",", "0.5681818181818182"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9823767055415809", ",", "1.0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9811189877277231", ",", "0.5681818181818182"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9938333714459944", ",", "0.6136363636363636"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9917143074929492", ",", "0.7272727272727273"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9952968976293926", ",", "0.7045454545454546"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9899077673603173", ",", "0.7045454545454546"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9991615214574283", ",", "0.5227272727272727"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9919582285235155", ",", "0.6818181818181818"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.989557130878878", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.97658358106563", ",", "0.8636363636363636"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9856543943898163", ",", "0.6363636363636364"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9901211982620627", ",", "0.5681818181818182"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.9861269913865387", ",", "0.6818181818181818"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0.994008689686714", ",", "0.6590909090909091"}], "}"}]}], 
    "}"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"csdata", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"0.9928881774525498", ",", "0.7796610169491526"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9954569708057016", ",", "0.711864406779661"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.983184693955332", ",", "0.6949152542372882"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9916609497675128", ",", "0.6271186440677966"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9814848692735727", ",", "0.7796610169491526"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9960362832532966", ",", "0.6779661016949152"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9860507660644867", ",", "0.864406779661017"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9968518941992529", ",", "0.7796610169491526"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"1.0", ",", "0.6610169491525424"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9891912493330284", ",", "0.8135593220338984"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9963030718804788", ",", "0.6779661016949152"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9823767055415809", ",", "1.0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9811189877277231", ",", "0.8135593220338984"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9938333714459944", ",", "0.8305084745762712"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9917143074929492", ",", "0.847457627118644"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9952968976293926", ",", "0.7796610169491526"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9899077673603173", ",", "0.7796610169491526"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9991615214574283", ",", "0.6271186440677966"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9919582285235155", ",", "0.9322033898305084"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.989557130878878", ",", "0.6779661016949152"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.97658358106563", ",", "0.9830508474576272"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0.9856543943898163", ",", "0.8135593220338984"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9901211982620627", ",", "0.8305084745762712"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.9861269913865387", ",", "0.8983050847457628"}], "}"}], ",", 
      
      RowBox[{"{", 
       RowBox[{"0.994008689686714", ",", "0.6949152542372882"}], "}"}]}], 
     "}"}]}], ";"}], "\n", "\n"}], "\n", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"tfdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "tfdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"ffdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Show", "[", 
   RowBox[{
    RowBox[{"ListPlot", "[", "ffdata", "]"}], ",", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"csdata", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "csdata", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "1"}], "}"}]}], "]"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.6546875110916862`*^9, 3.654687531480452*^9}, {
   3.654687616880952*^9, 3.65468767177041*^9}, {3.654688172526739*^9, 
   3.654688232693614*^9}, {3.654688471147828*^9, 3.654688480901989*^9}, {
   3.6546907062660503`*^9, 3.65469070855687*^9}, {3.6546914537626343`*^9, 
   3.654691456195821*^9}, {3.654692123982038*^9, 3.654692126355588*^9}, {
   3.654692165607965*^9, 3.654692168005849*^9}, {3.654699443036027*^9, 
   3.6546994572940407`*^9}, {3.654699562115329*^9, 3.654699599708886*^9}, {
   3.6546996495287533`*^9, 3.6546996530859823`*^9}, {3.6546996851775*^9, 
   3.654699689821459*^9}, {3.654699844963196*^9, 3.654699873971775*^9}, {
   3.654700025002277*^9, 3.654700026939303*^9}, {3.654700329388076*^9, 
   3.654700369315352*^9}, {3.6547004206443367`*^9, 3.654700437216226*^9}, {
   3.654700492749138*^9, 3.65470050616709*^9}, 3.654700572523466*^9, 
   3.654700690899084*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxFz10og3EUBvCFK4UQIVlKEVLKFknPkLgQMvmYK4pWkkQaE62lRVmEC61d
TMMoX2mzsLnxGYZa8lVaY+8M77HiglL+XNjpdzpXp3Oe5OaO6pYggUAQz/p3
7p7Utdv2CHGqSMaLJv/exOENoV4xVKcY4nDhrdhBDyHBWMj4YIkbtK9sEdbF
bYwbIWOu92o54XpYfzWs96Kwd1+puSeIytQ5ZeoH2NLDazdHCfbHRoaHSjru
F3kI1v7jjf5jDoK/ekNPcyrjwd13hrx7npB40Mm8ICHb+WVxES5b2xkPeM5Z
Yuig/z25wzorbSEoTZo+k8YHSazKqDwlyHTmBp35CaWhZqNwO5BvOTHFpbgO
3G9w3IZJlwh5Wneu1u2FabpoKvkzkC9YfJ4ltBMGeW6A53godmo2uhYJH1UR
zCPSps9EmRKC0GdI8hl4LATPxChHCAXOYuYV+sn1uagVQv5aNPMMuUmcLdMG
/l3V71Y+OwhT5UeT5UcP+AGWHM8n
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVj3s0lAkchklpXUPM+JDLGLeRsqJd2fr9sqpVsXJZZZGTy+ZQqdRZpdJK
ZUkuJSxih6k0yxRZbcrkErW5NEZmazAjM2MYPs2SPbbY2T/e857nn+c9r82B
I4Gxy9TU1PxU+b93xo6/4spDtige+XaoOzFQVvvdgRHCHTp1z9boOzCQojOQ
U0/4QGXE/avmdgxc52lcXUYEQ+iSWZiHDQM39W3g5BMx0OqtmD5IMJD7S5vH
FSIZip7nmPZpMTD9fNaPPxMXwGewP6F8wgljkkkik7gGZcoIIy+2E352zbdy
5n0VaL4ovjhHd8Lf+9x27LCsA+fgL3ksliPuTRe+actqgP0iu1gewxGP3Dvu
UjT7EHbreVmdq3JAZe7rLnX1FphV3OfQHByw6+mzlt6vWiH4UGD8jpv2GJ/i
q7dP0Q7tJMZLCXt8quW2xD7UCSQ9H1PL7JDFbF80s3sBvAVtKKDYIVIe/xpU
+xKOpUuNK4vo6NIpbHrG6AVrfUM3pzV05GPQ9J3pPrjkGX3S7YYtJmon8AZc
eVBDNc13M7PFPwWhj/Vz+uFC4Sy/Pp+GQdv3isfG+HCYar2/To+GGyuyQivo
r8F4Vy23Ms8GRYsF/IPnBuGYVYyviY4NSnx7mZQ2AWSxUu+cu2KNnOKRlFc2
byC8p9k1YYU1Vl/WyHQ69RZOzdxYrFyyxLnosIm4R0Kov3N65/ealqjjczHw
HnUYIqpqxyj/WGCqgURXHD4CZ5bfcuH+a466T5zVBv1E0Hxw84rI5ea4+eS3
n9oWRJCob+u8OE9gaVU207ZUDPlEz1LWJ1OMDHHv1to1Co5xXTSLaSo+GIgt
ePJhFIYao/gV7yjo+ZKzTFr4DrQPPXFfPWmC1tvi5gp8xuB2TMK7S0PG6OHu
kdcgHYPmp0M4IV6NI8orz5kZEpihTa/dOmmEig17Mgg3KeR3+P1QMmKI9pMu
E+r9UqjtPhYglhhgO/0EL+m8DJLW9awKqV2FV/kD0YH0cUjO3ipsbdBDMoib
t6x9HJqUMx1Ehw7eDzH52vOoHIRvt+w+cVcLmd3pZaNGE6C4SnVlP1iJ853h
p41aJ0D4LIcju74CRXktcwaHJ0FX09tfN00DP7zyIn7SV8BagzRf3Rx1TLpG
46Y0KyBg08NWm7FFoHbTDMYip8Bx4tJv4r8XIGiA/dFicQrSanp4x6nzoNxf
mSismYaipMDPV8mU0BJVl6G/mwSD4sHrrR+n4I/exF3D/iS4Xl8g3vw1BY1b
GIa1e1Sct6b8feMUsC2qS/1CSYjKjGZZH52CEkFJffYBEnJTyMazEgWcCLgo
1k5R+cJWCjb1TIIzRmxeySJBzfwLs/qbcrCvM9MYvE2CNTWs/EWqHGiWgi7W
XRWvPkMb3ScH04+BwdvvkRCg08YwNJaDZtM3CRnNJKT96+915PI4iNe7F2v0
q3zCuHDnozIQlr+P7B9Q7QkyRd7+MhDo1dGZApWPz44Jc5ZBz6QTx3uYBHyp
TMyUSKH5llXnebnK13z2jGyfFJooQ9l7FCpuqlRf2iiF+oySQBtS9aehPYNi
LIWaaJNh7iwJHLZ2zrYeCVTzeMzceRK4t12MIu5KoGJrbnzUAgmiqoDC5MsS
KOX4rXf9pOKK42bZsRK4YaUzt7REwkxpYTnTWwL/AevHV9A=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9762176995197804, 0.5871794871794871},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.97658358106563, 1.}, {0.6068376068376068, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.65470050840979*^9, 3.65470057389657*^9, 3.6547006200451193`*^9, 
   3.654700692617488*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQfeRMeO7eo+/t3cRjSy66PrdP/Hh08olb7+1BPPHY
J/YXX/jtty97b/9yUbferheP7LdJ1O9bv/u9vf6uF0CRR/YsEx5+CcpA6Hes
PFbdfh+hf68WX9iuXgS/MXjiR9Nn7+179EAGPLNnAIMPcPPu/NXOKFn+3h7M
3fXCXsrwyq9tDxH2vXt+xXVh/nu4voxzO5YGpyLkHcQal1SffW+/GKR70WN7
d66tS+T3INy3TkblYcVNhP2R527zBq9F8FfMdJqq+BNm/wN7ZrMLevL73tuD
XF9y8al9xf6Q7cWrEP7RmHneVMcBZt9r+5XMi0SruxDyc6dsXia0HuG+jBVm
hlF9CPM2zD3i//rce3uw88Sf2gMA9xjPqw==
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVkHk41HkcxwfVljHTRDP6WceMpEhLGjlWfb5aOlEZSapJricilVom7W5I
m3VE6NDUo51hFY+rKZIyYdHpGMM8kWEOxvmLMgqx9o/38/rz9XrejIBIr2BN
AoHgsbD/uTtY1SocPLC1sMBIt3gJAw0U+wRIMSZ0BFpyIxczEI0oTnuEuQLB
xN584yIG+slxZd5dzBu8s/c5CTQYyKllU+l1LAhm4xICqmbpSHinzi4VOwce
hwfLGyboKCEuOeYv7DLgpMcsaQ8dBZ3DsSQsC2yj3LMpFXS0NGvX/U/jfKBe
1qsQ+tNRRYvtjh3GJfDcmmVgQaEj34TuD3XJAiALOkLc401QZFnUhltfnoJf
tn1HX4cxmkjvaNLQqAHroqSmgc3GqOllQ02zcy1U79X5ejXJCIVydpEOjdRD
2ifIZEkN0ctltvNFEY2gaggqS0OGKJ9XP2ew5jXMWFTpBGf9iBDt+d+s4rdw
q5YwXPjZAG1o7K5ssGwGju9XpqGPAWpHrLEHYy2gG0uvXluIoXDtk21imzZg
ihwbeEQMvZEcfE5OEwG5MoH0jL0Ksbb79ikU7WCV87twRqCPNucmH8w16wBx
WZi6iKaPeucy20/80QlMnz9Dv4XRkHJXM49WJwFjm+xjT15TUeltKaeV8QGE
N0TXyUZUlHdVK8niQhfkT9duTD+7Ek0G+g2FPOsGahaPFS/SQ0TXK15l+j3g
XCUoX75WD12kKHX6jkhhjFqRRbqii3RerCd0evSC7FSYjKlagbb8uvd73XQv
XPP2jJhyXoG4/BTeam4f/Bu5OtbyLgWxDzDfLdsjgxBrr5Rx6XL0WByc+UIt
g9ecmm+9yWTk+LZUs/+GHLZZL48o8yQhulvIZKarAiznavwq1URkx7TLEPQr
wNyB5Zn7WBtJJ1Jf8RKVUBC+E+u6sAyNbNqfiNn2A7fqHtt9zVJkPrxhSEPU
D/yozgf3+pegerPzbafjBsBg+5nItamL0bV2caCXmQr4CaNOGbsXIZwlzNCs
V8GTZ28+epC1UPkB6i+OZwbBgUScQRUaiPcu4a5Mdwjev7IypkYT0FTjkVjd
2iHgw7FocJiD3oyaScqpYajuXXd8Om0G1K0/Y/HkESD4VvhE632D01mmQk71
CNA1l7ocD1CD/jtTioI9CifijexJ0s/AEhfNGs6NgnBK3cU+MQ4Tx+6Hdz8c
AxWsGXaKGYUa/5JEsjsOBc79ds6xKqhqDt/T44mDL/2whTpooXur5Yri/TgQ
FrUYlnqqoMgwj+txEAf/N0+1zExVkCPJeZQSgMMqv9RW4qsBOL/vSp82B4f0
aGZEF20A1qOjW37IxyGmPJ5/oVwJ5iUGWp0FOKy7MXWTyVWCqbGkKb8QhxZO
ePLYwu+rZr28t5fh4ODiczbgkBKWVO48mViNg6rZAu0mKKHPmnlbS7TgH23p
wjwV0H1vnC0S40BpdWsW2StAQiox40lwqBRU1aYyFPB+2KJ0Ww8O9Iv8BxqT
cqj+x6QxbhAHiXZM9OAdOVTSPqbsH8Hh0thIGC9RDo8Sc7wYOA42bcfZRyPl
8DCQ2iP8gsPV23vcWrfJIa+tjZc+hQP6TeiQbCWHXJf0UP9pHD7521m50eTA
LfWwtvmOwy3Xhybz8zK4aUKcnJ/HYec6E72ngzL4D3JYU3U=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9762176995197804, 0.49886363636363634`},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.97658358106563, 1.}, {0.5227272727272727, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.65470050840979*^9, 3.65470057389657*^9, 3.6547006200451193`*^9, 
   3.6547006927135468`*^9}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGCQBGIQfeRMeO7eo+/tlTMsVv7+9MI+8ePRySduvbd3POx5
d/rxZ/YXX/jtty97b88p5fdqn80z+20S9fvW735vr14aLxct+MSeZcLDL0EZ
CP2Olceq2++/t79QEPT96can9nu1+MJ29b63t92mBFTx2r4xeOJH02cI9Qxg
8MF+xvEwFl71p/Z3/mpnlCx/bz9ll37/Wo5X9lKGV35te4gw793zK64L89/D
9WWc27E0OBWh3kGscUn12ff2Z5K1m9omv7J359q6RH7Pe3ug4UAXv7ZfJ6Py
sOImwv7Ic7d5g9ci+CtmOk1V/InwH7PZBT35fe/tgRyTOVfe2lfsD9levArh
Ho2Z5011HN7bnwA5v/S9/UrmRaLVXQj3zJ2yeZnQeoR7MlaYGUb1vbdfxwEK
0Df2G+Ye8X99DhG+ADvQy2s=
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVj3s01HkYh8flyCVzUIaRMGOmUonjFive78qKpOSS6iCR2kIRtmQ3tYyz
olJaxaqVcautkJJqMBqLZHI3Qwzj8hvD8MscNGdy2dk/3vOe94/n8z4fSvgF
v0hlAoHgo5j/9/7IqS62ONAVj996qmiVjkQvjoSPkO2ANf5nvcoKHZG0+m5V
k90hw1+VHPmdjnY5bSx5SA4Aqq2Qu1VGRz902lbeJZ+CQGn+7ufzdMT+i2N/
k5wA72KJWrUTdJR6PfPyDXIaMGIWq7htdHQqASdnkO+B0RnOmiyXjtTveT3+
Ol8M1cc6ou9voaM3nTb79plUwJx2eXB5HQ0dTR0a5GS+AuLeAdfWIzR0oSre
8sHCWyi4dEVqJTJH0uz+ViWlBmiPmdqmmWKOWhubGzr2fIBzEc3WO4nm6GyS
l/YxSRPMDSxvD/mbiho1bNaexbTABj1COcGRikqZTatG9DYo9YpfmmmhIESq
K/J/0Q6DbtUtXicoyLJlqLZ5ewcssLwza2RmqBf5zz2Z64TfHALMojLMULRm
VHefdTeUxVk43qWYoU/8oDrirR64enTYTTncFPl7HBVOTPRCcoehfCXNBDkU
ZgYV0vqh9uNsjaBmMxpdzen9OYUHvR+KSId4xmjSq4NJ4vDBGuaTi9WMUWXe
SFIXZRCi3pmmZdA2oZI/VDIsrnyBuN1rsVkeRmgx4vj06fdDULv7ckTCJTLS
ck/3qzIQgMChSzaTa4h+1ZlcLwweAfe3nBzPfw3Q+vodBJ7PKCS6nNnA+UZC
Lr8cWuHIRyGNPW3PNSShguIspnmBEHqb14WyPPVRaKAdV8N7DJTbVS2cz2xE
r/sic+qXxuB790s+L2MDcmqvVMZyx8EhRH7z2Hs9ZPbT6cUc9wk4hFGwc190
kb2d/Z1X2AQ8kY7tPampi0akNz8yGZPwmRCYnuiigyS2hxlkGwyKNh3k2dQT
0ZYZy2mlHgx27qE/dzfVRk20xO7Y6yKIPc5qTQzRQrd7+yL8aFPgmJKOx77V
QLg/+45y0xTklr0cKdusjl4G6u91ihNDJfeR/mqoGmJyUx+O6U3DAXndEZN6
VfStJThZ78M0FNMeDofIldHonYZFnfMzkO0nsBxwVUJLXc7k34kSUCeW6Lm4
rUHsPSo7iSWBsIvagWqN38GAS9WZCJ0F9viX5xHJMvDve7ZsvDoLnsHn8xw9
FkF64nH00NM58Oy8pnPg0zw0hFUwiAdwKCySOosMZuFdR7S34CAOXxvbZJhc
AjWu23VfHMbBWlj0GhuWwDPjkgKfIBzYpv5WGFMC+fz86qxwHDoLqqmTVhJI
9E0XaiYp+NwEjfF9M7ADhbisK1XwmUv9gkti2FJhpMIrx+Ha0885guNioJrw
W0v/UeR9LPUVuIjBcNkvwKMKB1/1oLZhFTGo1XpGMVg4hDHe1A3dngKhlV2e
So+CT7lcPFgmgqFH86E9fYr/j3xPDt4QAV+7gsbk40Co32YyGCOCzzMWlW4C
Rb9l/v0BWxGwykxbrosVd5JTJr8Bg1rScNZhCQ6jD3Q9+UUYVDPy/Si4wrdW
rMpnYPA0Ql/AXsChcinvKs8bg5Lubmb2N0U+6aIzbxcGhT9mnw2T44Ds98v6
dTEoqPSxsl5R+AVQX/cvTMJ9U63FtTWFX7w8rp83Cf8B5wVq+w==
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.9762176995197804, 0.6084745762711864},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0.97658358106563, 1.}, {0.6271186440677966, 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.654699866427462*^9, 3.654699874681007*^9}, 
   3.6547000278605022`*^9, 3.654700371869841*^9, 3.6547004416332693`*^9, 
   3.65470050840979*^9, 3.65470057389657*^9, 3.6547006200451193`*^9, 
   3.654700692803199*^9}]
}, Open  ]]
},
WindowSize->{1920, 1017},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 10019, 240, 649, "Input"],
Cell[10602, 264, 3460, 68, 240, "Output"],
Cell[14065, 334, 3350, 66, 240, "Output"],
Cell[17418, 402, 3399, 67, 240, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
