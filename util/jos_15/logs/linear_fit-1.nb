(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29978,        744]
NotebookOptionsPosition[     29313,        716]
NotebookOutlinePosition[     29648,        731]
CellTagsIndexPosition[     29605,        728]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", "\"\<Global`*\>\"", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CmaxTF", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"121.179000", ",", "237.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.169000", ",", "270.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.565000", ",", "240.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.682000", ",", "250.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.926000", ",", "254.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.081000", ",", "231.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.092000", ",", "224.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.157000", ",", "261.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.377000", ",", "217.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.994000", ",", "219.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.415000", ",", "222.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.037000", ",", "235.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.257000", ",", "234.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.421000", ",", "273.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.784000", ",", "220.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.601000", ",", "237.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.287000", ",", "238.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.849000", ",", "248.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.542000", ",", "268.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.482000", ",", "239.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.849000", ",", "246.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.225000", ",", "223.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.894000", ",", "219.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.667000", ",", "252.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.090000", ",", "224.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.146000", ",", "217.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.624000", ",", "219.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.605000", ",", "225.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.881000", ",", "219.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.222000", ",", "273.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.453000", ",", "218.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.839000", ",", "219.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.163000", ",", "237.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "246.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "231.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.997000", ",", "235.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"117.906000", ",", "250.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.083000", ",", "235.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.126000", ",", "220.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.271000", ",", "256.000000"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"CmaxTF", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "CmaxTF", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "122"}], "}"}]}], "]"}]}], "]"}]}], "Input",\

 CellChangeTimes->{{3.6546875110916862`*^9, 3.654687531480452*^9}, {
  3.654687616880952*^9, 3.65468767177041*^9}, {3.654688172526739*^9, 
  3.654688232693614*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGDQAGIQ/asua0+Jd5wDAwgsyHXodEx4esELyn9Q4BDTf+ir
xpNYCJ8hz+H89+DHS19D+Q75DqpsjVOdd0P5B/IdyvfNl9JnhenPcbh5DqgB
xmfIcTCN2+XJwwXlBxQ4PF46+4iCBJSvkO0Qxqe7aa49lJ+Q7XD4q0ZMvxSU
fyDb4V2NvWkcE0w+12HOEYUNRQJQvkOuQ+7z3ys/wtQLFDpIsoBMhLqvIdsh
wnLLibJnsXD/gs0Tgpmf61A6WYIlbBvMv/kOy4DO2/AI5r8CBybtdrGb86D8
B1D132DyeQ5pIHAOJp/t8GH5MW/znbFw/4DNWwVzT77DN6D3Drkiwueyb5JA
hCciPEDB8T4dER7yra8Dd6jB5HMcVn285JtkgZCf+BboIZj9QP8vct32+W8M
LHyyHcDB8RXhnvv+vdPzTiHCA2i75RZYfAD9g8IHxifI++fsEeHv0p3z/Hcl
Ij1EpVjf92dFyH8COk+AIw4e/mD/XYSFb4EDAB73uFM=
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVjnk01AkAxzU619HBSqIM1QwpSWZLm/nSoW2Qc0TNb5hCm98cyBXGZMrZ
YFv3RrXVJqvFYssUT23k2NYWPSwaV1Z0UDGVsPrj+77v+z7f931fKk/o6kdR
UVFxnNMXT5Esb2juf2BT8nn40NCMDBUF8heF9CvMyqShKt1pGX7QNF4XSy9n
ynUG9VhTMsTu87Q9Qf+TWbdF0V2ilCGMN97Cpbcyu4499YkckyFYZJPtTR9g
Lnp077h6vwwi5eAnT/o7Jjc/L2JrnQxvrG8Xs+mq0LRxyBGnyKBLlXvlTmqB
raQ3+GrOcUH3m65CAzBXNYWl5Z6Dxaa8YkamMXoLdZoW0c5h7V5XNWo1HYGl
3RrNRSkwiSPLJ85tQpZhwD8djBRUiBopk74WKFocKLWoSoZ8UVHvnnFLhBN6
XW27kuFYtnMg0Z2B5/qpZfebkjAV2CrnjG7Ht25REVOOSWDs0L9FY++E5Vuz
GnFnIjr4W4/XVexC/0xSoUNAIpq9aMpIYyBa6iP1G03A+ILUv1LYtqgUTXU3
hCfgqjxc3f+KHTqeLqWFf4rHV8YNG0xHdqOisOVrUWw8bmkHtQ7r7YX5oJu0
XD0e45r5I0K/fcjIdumzTT+L4PPUjPFce+SVa9Xo6Z+FZI1J4c7h/Ui2P5hi
U3AGdkR2lnDLAVh8x5guNjoDFo1YWcBjIaK875VfiRR9H65TMn9zQFHmwpbv
t0pR43ZAEj/oiOjeN5tvV8fBiefW4at9EAtzz1i6sOLwx13hI4q5M041nqdb
tZzGsHq/8nGDM2Q8vwtHD5+Gb1+m51mhC5JDylR6eiQoXxpzdZOWK+zeFi65
zJPgRly6deuvrjAvbzTiKmLxPPL5Ao6LG0qDtmmtGBKjrtJfb3DUDcJ59Hne
0zGIZa3W95G64xIZvP2bVXPZb7lbLc0D1x6WOBdRo2H8wLLV4J4Hht+F+v9u
HQWN69mJ3AA2KMvMOXMv8dPftIQCiie8p0UuUnYkKM6jwU/yPGFUvySNFRSB
Uo+xH0eYh1Arzrx5IyIc8y5RZ2Y7D6FgcVHjhfQw5Aoy8j9KvOAUV1a/PjMU
atnWpsPrvCFqX6qxp+AkDOdrqLRVe+PJ9KSz8lYIoicM1a4ePYyF2o9DUROM
qI2+3KCZw4iessxZ8zQI7nfaLllmHQGv5pXq+TERQlqSVv5rzcH2/m2XGeNC
MOiB7TFtHJiNXLQKMRJgpnJAsbGdg5NvexQ+egLU7T4y3NnJQe2n1clOKwRw
5zp+tFJw4K6W88yEIkBIlvnq1y84iDBLT+jt46N0/nuCmOWgQSDpdLzMh0lf
9JCNKYFj73xi6IZ8jAkmXr80I1A6VUDT0eXj9me+Ms+cwAfVnseqy/jYr8td
rNxGIFHba4NiloT/QVvTEuZc38qlJVNB4ufqBYK1HgRUImypqhdJnGCJwx55
EtgviW0eyyJh0TkpjvImkJFYHfoslUTt+//S2rkE6Lk7mqrEJBQbm8rSThBw
kFuEiAgSv1TZyXfxCeTcFxoQbBIC+zv3R4UEeptuPmQ5kZjhFbfahxIQdZno
02xI1I+t654IJ3B3IKBem0FCJs4fvHKKwPyX10SUzSTc1XVeucQQcH4/oDe2
noR+XurEbOzc/mdqXY8Bif8BWQo7bQ==
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{117.84790625000001`, 214.2},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{117.906, 121.624}, {217., 273.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.654687532202696*^9, 3.654687623170549*^9, 
  3.654687672449647*^9, 3.654688234588929*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"CmaxFF", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"121.179000", ",", "90.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.169000", ",", "115.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.565000", ",", "102.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.682000", ",", "99.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.926000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.081000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.092000", ",", "102.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.157000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.377000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.994000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.415000", ",", "96.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.037000", ",", "103.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.257000", ",", "105.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.421000", ",", "107.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.784000", ",", "95.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.601000", ",", "91.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.287000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.849000", ",", "99.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.542000", ",", "111.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.482000", ",", "103.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.849000", ",", "107.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.225000", ",", "95.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.894000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.667000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.090000", ",", "103.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.146000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.624000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.605000", ",", "102.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.881000", ",", "94.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.222000", ",", "116.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.453000", ",", "95.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.839000", ",", "95.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.163000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "106.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "102.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.997000", ",", "91.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"117.906000", ",", "99.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.083000", ",", "103.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.126000", ",", "96.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.271000", ",", "106.000000"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"CmaxFF", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "CmaxFF", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "122"}], "}"}]}], "]"}]}], "]"}]}], "Input",\

 CellChangeTimes->{{3.6546877106751137`*^9, 3.654687737780698*^9}, {
  3.654688262010283*^9, 3.654688269030954*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGDQAGIQ/asua0+Jd5wDAwg0hDl0OiY8veAF5R+IcYjpP/RV
40ksVD7S4fz34MdLX0P5ByIcVNkapzrvhslHOZTvmy+lzxoH5988B9QA50c6
mMbt8uThgvIZYhweL519REECJh/uEManu2muPYJ/+KtGTL8UTH2Ew7sae9M4
Jpj7Ih3mHFHYUCQA5TtEOeQ+/73yI0z9gSgHSRaQiTD3hjtEWG45UfYMxg+D
mCeEcG/pZAmWsG0I/y0DOm/DIxg/2oFJu13s5rxYuP1g9d9i4falgcA5hH0f
lh/zNt8JC59wiHmrEOH1Dei9Q64I/1z2TRKI8ET4HxQc79MRfPnW14E71BDh
uerjJd8kC4T8xLdAD8HsZ4h1WOS67fPfGJj54Q7g4PiKcN99/97peacQ7gHa
brlFIg4HP9IB5P1zsPgBhp9Ld87z35WI8IpKsb7vz4rwzyeg8wQ4EPEH9t9F
hH0AVIa6Ew==
      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVkHsw1AkAxyUdoRpqjhPVYuyuQq/dq3XZb0J51cY+fov9YTfv9mFVRuVx
5eSuVAihxWin6OQxlGwvdRh615pQ3ZRXic7jtKQazv3xme98/vvOhyKWB0To
6+np+c/z/55MM2t/2NfiloMnld88lWgo0XysoF1k11azXQ3nPXupnX0qrZ79
1LrukbmHEqlegu2xtL/YpjO5Y3R3JQ6JJ56G0rTsjDohg3BTQqlwKwii9bMP
2w42NzCVUEwPfBXQJtkSg+9dUqoSY6wbVXzaQjDa6T+8M1LCkqIRFk4tR+TL
BxllbfEYk70Ze11hg3Xcrx9aMuOxwamoiplnBxdv3fBqv3is9gwwodymobmF
YS4wjgf92P563SknfKgxsPr3kQINig79qfANKKb85BXzhwIawyvvPCY2Ict4
ZrmFtwL+da79mVwmVkii7tNMFfgWp9WIRraAk850dDonB3OrdSOV7wo15ebZ
Z5ZydEs3Rrc2bIPy6ktWb6kMD4XU6SQ7IMQr2emKtQwTi04/OsnfDvkd9LLK
pVBrEk0jL7qjdMeZv4/bSGFs1+7gOLwDPo0HbFjF+9G4Il47ZOWJVcaJq25Q
9mNiqWpYHuGFZbEG0aOqOChzKOcmCneCXlmt/GQbh7RV9ArXoV0I0/W3lqti
4U4W5MvX+8BsfW1Im1UsfKmkRYnYF5+8CxvqymLQ++Wyfl61H/oUS3g86xjc
CfRJyxjwh0FiyfXmsmjsFgd2h6/Yg9K4tuBCajSu35I/1nfhgC9N8nZUR2HI
tG/6eTsHW3Z7fzlGj0J4b57gN/leMH9Rj+WqI1G/LFnttDwA1Zb5jua2kag8
dpal/TMAnE61A1ERgcGkwUWivYFYpnKL49tHoPVapNXASCCmPHQK68p9SPVd
aR12nAvha3fNnvXzHmEW2EzloTtlpZ/rVQnsWjZpbe7x8OsCs9LRjRIsuVyQ
GRrFh1d2NiuoWoziJ9QTJfoCbNZzKcjaKIY+Z0T5okgAlUjys3NTOGp547nD
bAJ27bZrTjDCsaCMMjvXQ0BrNtdxWROGQtk51UyaECXRkbfSGGEwKWA5DtkH
oT24YyArIhRrDJbodd4OQoV67kVlPImjujUmakkwmg39ApgxIhxZGx4aPxuM
dfQXgQHKEHBvdpZtyg/BKWrGm11pwUh4+rvFK5YIDuBM1SQFgUmL60ruFMHc
gptzKV2I2Wv9b9d2ifDg7q+ysPkfrTtChnp6REiPrvGxOioEN9R/hvFWhKkm
44WnE4RIyHdZOfpRhO6QewkHxULUGnwmyTkRLpQ78zwgBL336Hs3RxIOzost
+74RGJfpRj+tI/HuJfNz8TSBG9+l00UuJM6n7nvGmySwyzLUaHozCaPndzM7
hglE7tnuWMMmMZxwaKZ2vlP57UWy1TwStU39r1IbCcT6phx6LCARLTZr3FpP
YEPPVMqRIBK2JuzcyWoCzZ8/nOkKJZETUuQbdYnA27UP6s7Ekjgwx7m1O4/A
pSZ3zTYpCeeKlPNG2QRkO2/eH5GTeM+pOnD/FIFZcZV250ES/HJDJ2Y6gbZx
+ze6RBJLfRmLx1MJZKWoBi4eJtE+KR6sPEKAa/rjP3uTSaRdOHtPkkjAuui0
bi6VxBbPOyqbBAL/AZIkJj4=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{117.84790625000001`, 88.7},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{117.906, 121.624}, {90., 116.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.654687738614937*^9, 3.654688272049354*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"CmaxCS", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"121.179000", ",", "100.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.169000", ",", "124.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.565000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.682000", ",", "113.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.926000", ",", "118.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.081000", ",", "116.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.092000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.157000", ",", "122.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.377000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.994000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.415000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.037000", ",", "113.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.257000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.421000", ",", "118.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.784000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.601000", ",", "100.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.287000", ",", "116.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.849000", ",", "108.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.542000", ",", "121.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.482000", ",", "113.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.849000", ",", "120.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.225000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.894000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.667000", ",", "121.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.090000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.146000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.624000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.605000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.881000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.222000", ",", "125.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.453000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.839000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.163000", ",", "116.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "122.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "112.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.997000", ",", "100.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"117.906000", ",", "109.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.083000", ",", "113.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.126000", ",", "104.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.271000", ",", "116.000000"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"CmaxCS", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "CmaxCS", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "122"}], "}"}]}], "]"}]}], "]"}]}], "Input",\

 CellChangeTimes->{{3.654687746763687*^9, 3.654687777254962*^9}, {
  3.6546882881548634`*^9, 3.6546882961176453`*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGDQAGIQ/asua0+Jd5wDAxhEOnQ6Jjy94AXjxzvE9B/6qvEk
FsqPcTj/Pfjx0tdQvkOMgypb41Tn3VB+Q6xD+b75UvqsMP2xDjfPATXA+TEO
pnG7PHm4oPyGOIfHS2cfUZCAyUc5hPHpbpprj+Af/qoR0y+F4L+rsTeNY4qD
2z/niMKGIgGE+bnPf6/8CFMPdI8kC8jEWLj+CMstJ8qewfiREPOEEO4tnSzB
ErYNJh/tsAzovA2PYP6Nc2DSbhe7OQ/hf7D6bzD1cQ5pIHAOYd+H5ce8zXci
+GDzViHM+wb03iFXhPsv+yYJRHgi/AsKjvfpCL586+vAHWoI9as+XvJNskDI
T3wL9BDMfod4h0Wu2z7/jUHIg4PjK8I99/17p+edgvFjHYC2W26RQMQPCh9o
H8j75+wR6cWlO+f570qYfdEOUSnW9/1ZEfHzCeg8AQ6E/WD/XUTYBwA7GaiK

      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVxX041AcAB/Dz1lpeWo/qbl7KUd0hYZW4yn2pFFLej9/v7ne5EIe7c1yy
4qyeqJlevTxdmGc8ZXVtFcIVpU5TkbZT0lrmra5sHc+8pDVt++PzfJgiaXiC
MY1GC/nP/xfmLeh4MKj1NbLZ4SUKVqC+UvO6ll3NdRSbjm8IUuCkldMyJbuO
u0mjuUgPVEAZwPMTs+9w8wkWsytAgb2i8W4hW8e1OEuz8PZXQC7zLSPYQ1yG
Xd2ApY8Csunh9zz2X1z3JYyi5hUKGDhN6mi2CQSOg0OfGSvAYGpiz0xZQzBM
Pii+mgmD5Lnh11p7WGpHmIKkTHi6qdReJU5oDbJzVS3NxNIt4ebMFjasC70l
6kcZcD6YWjf5jRvor+MdTAoyUC+7ZzwV54mbtCwL93UZ0Hxy4ffN46uRdHPt
xW69HCFX1g8difRC21nT5vpv5fg7RacRjHqj1bOZJg2Vw8vHrpEVvR7ikZzG
tDlyPE37Iqm9fiPoMDgMtKTjQSxrOtsJSIoM0DpL0zFudqyzMNoPyfmXnlxb
no4aTZZFYrU/LOXvHT9/IsM8p44VLm824arpBHOsQIbGhek6vc0WzG3457g5
V4Zxq4o30oQAGPk+k77rl0J+ilk8fmYrwkP6KkTbpchb4ly7Xr8No93V3Cy1
BP5UWanUIwj5VT8FKOZLEMyi6JWiYOSpzVd+OJCGgXfnjUt+2I42V9vDbi9S
0RoRlJc/HALRy6AbrwJTsUMU8TRu4U6YGXQxhU0puHZD2mXsHgqDmJ3cZ5MC
vcXg9M8dofBxrOBdLhAjbqCEd1gahvvbf2v21Cejbn5OjZt1OKj2vV+VByfj
+4MnOLqL4QjL9mCdaE7CSPaImSAsAoVVDVhlm4T2hkSb4dEILDchvaMK9kAZ
bGu361AkbtXFliRMJUKZsCDiFisKNb1OiQ6xiXDSrtbZt0XhpZfVovi7CbA8
X3ZEuCcask+168JZCTj7kFVQacxDLF3lfe9UPIxDR+W/qHjYknGv0tkoHpej
xk6/4cZgn79235rdu2FUxZz92BcD85B+j45uEc5Iiitm8mIxcr+dP8dDBPMy
jot+GYG2UrO3bqfj4GBqSetpIfChIkHwghaHA5MO5jW7SSgZJ+Ej2oX9rnHC
9FkSt8ubTTjlQkRe76laXcrH1zUmYT3ZFDK6j9KfcQQYPDV8+I6vAF7slN6c
HgHeT5tlaRl8zDYM9bv2CiApv91Qbc1H+ya+vq9PgGEoJw5a8REpDJlZ2y/A
w6Pv0v1M+cgodbd9+1qAKtvR1FYDicumExT1UYAA7iNR010SzgMHXvq6UCjO
V+1QZ5IYk0y+/WMlhXmuvKJCKYmmD2nTKncKud3WnWIxiW0M4dzpNRQSGUWB
zrtIJO70c/mRS4FzQbn5XDCJ71rMJEujKAx2xXOqHEmIg3P3dvEoRMuZ2Up7
Ep59U7n7CQqdi180UgwStyZeHe8VUrgm5K21tyLR73r/ynExhSPjgR6qGQLn
mv01G9MozJbMkWZPEJBsvX57VEpBzrlzKcZAYFak1m1VUCAObXCljxC4O7bs
+WQWhYesmeSpfgJFuRXD1V9S2NzZUPv4GYFIi8V/huVQ0Mjkr+ofE7BTHZv8
qKSwapH7iuJHBP4F27cjpA==
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{117.84790625000001`, 98.75},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{117.906, 121.624}, {100., 125.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.654687778443165*^9, 3.654688296728806*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"CmaxCSe", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"121.179000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.169000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.565000", ",", "37.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.682000", ",", "37.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.926000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.081000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.092000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.157000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.377000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.994000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.415000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.037000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.257000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.421000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.784000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.601000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.287000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.849000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.542000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.482000", ",", "37.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.849000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.225000", ",", "37.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.894000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"118.667000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.090000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.146000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.624000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.605000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.881000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.222000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"121.453000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.839000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.163000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.386000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.997000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"117.906000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.083000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"120.126000", ",", "36.000000"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"119.271000", ",", "36.000000"}], "}"}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", "=", 
   RowBox[{"LinearModelFit", "[", 
    RowBox[{"CmaxCSe", ",", "x", ",", "x"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"ListPlot", "[", "CmaxCSe", "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"m", "[", "\"\<BestFit\>\"", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "122"}], "}"}]}], "]"}]}], "]"}]}], "Input",\

 CellChangeTimes->{{3.654687822660387*^9, 3.6546878343721323`*^9}, {
  3.6546883539930353`*^9, 3.654688361585493*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGDQAGIQ/asua0+Jd5wDAxg4OXQ6Jjy94IXgx/Qf+qrxJBbC
b3ByOP89+PHS1wi+KlvjVOfdsXD15fvmS+mzIvTfPAfUgMQ3jdvlycOF4D9e
OvuIggSCH8anu2muPYJ/+KtGTL8Ugv+uxt40jgnBn3NEYUORAIKf+/z3yo9I
6iVZQCYi3BdhueVE2bNYVPOEEOpLJ0uwhG1DyC8DOm/DIwSfSbtd7OY8hP/B
6r8h5NNA4BxC/sPyY97mO9HMW4XgfwN675Arwv7LvkkCEZ4IPig43qcj+PKt
rwN3qCH4qz5e8k2yQPAnvgV66BzC/EWu2z7/jUHIg4PjK0L+vn/v9LxTCD7Q
dsstSPGBzgd5/xxS/Lh05zz/XYnQH5Vifd8fKb4/AZ0nwIHmv4sI9QCEeqKH

      "]]}, {}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVzn841AccB/DrottEm36MRcn0uPMrlWZF3Pv7vR9fcxTnx6Hue7nSSji0
6lFxqq0fy1zUuViaxZaWSknqivVLEVbt6um06iYULsrm3Eja7Y/P835ez+d5
Pu+Pq1whTmIyGIxwy/yf+3Ptm1qe3wxuqb//LvIlhfNHtb2VnHJu0WIX+vEL
CgVT3OYqOTVceU1Kg9xipVBCJHNucEcqWcqN3RQ2ywfvyjg6rvuhIIa6k0Jm
erAmgdPJ3ZF8gqk3UEg3d41KOP9wFzvsYNF6Cq8DLlbFciaiIn2+fXIzBUdX
bXzx8DRcCBzxHK+07NOevP6zchby3P/+8uJ3FBb4lFT5q90Q3r4rzSaVgotA
PNm1ngOqQlmsD6PgsTOlxpTnA2vVyebWeZb/05uZw4kLMFZAMIKmUNCyfv2L
P+iHZyvTlwoGhAg/G9i5N9of31B3szfeE+LtBp1WalyM1aGHG2dVC+G/xLmO
HRsIVmrB1IECIfSpC9c1ng9CkcQtKfZrIVri2eYsN0Ca4NIQEC3EoHV+6/5Y
Ar3bRc4CfyEqtFts15aTOFXXkdsyQwgbtyZ3zz4epNqPjb0mAeqmZ+h6Zgpw
/4kiYZ5egMEppX2KJCGyHZbcG6gTILPQ9dBgMYWBjIWih8UC5M72qAzsCcHh
LnXrF1kCkLSmSDE/FN4uhVEL4wUQsWmHo3IRdmc1GMSBAnT8e5ypPh0Gx1fR
ma8tPQ1Robm7u8KRluNkc26Mj2XyKH3i9OWo1m89bmvg48IVRRvTNwKeIr6o
s4GPHtvn5vtNEVj3NGjIUMZHYoda8q0iEoY9P5XH5fJR81F2hc80MYjR0rj1
q/g4sfNAgO6kGOvy7kwtI/nozuq2lkZGock/USf+jI/G2rUzu4xRMDI4xV5W
fChFTs6rdkVjtHDPmiMveFAm2UddZceADIr033eLB7ebfrpZ12KwjCWyO1LJ
g91xzV7ZV7Eo7D3d57mXhx9+Z+85ypTgiJFsM6zngRlhzPyjRIIDtYpa7zAe
qmPeHOzjxqFeM6nC5MXDhDLX8fftcThd3KbpsuOhOO1Q6UhuPPQ14wXL+0lM
1gR49sxNQL+6qUDcRmKOlR3jQX0CbmuuF207Q2K7ac7kitUr0H7W/dgcFYlt
XomyjPEVqDZqzxnTSURfflDmV7QSmf0vm5ZFkdh4d5/D4wApUljKbh8/Ev6c
DY+yH0jhRQhZfjNIjNd2GrweSWF8J/c4NZ1EI29lT3u7FFVapYhtcbQsfORz
gxQ+iy6pnKZZ7hX5Og30SuHL9na0sidRbTVE0++l8LOb6vHQloRHx/YXwZ40
lrY/Dd1iReJNmmnglTeNMfVoyuBEEhfHUs0lvjSuiB1UGywOcZR9YF5EI7g1
Qidjkli7nPA8w6WB326sCGGQOFZvneYSQ4P/84mUT8cIJItyNrdJaFjJb+Uf
fEtgQftwzrYEGo2zO6vtLL469FL1SEZDeNjZNGGUgMHrzllVMo2Q/arsPjOB
Xy6R2qBUGh+GVP24xuI06vJ1o4JGs1XztWfDBMblVTpqE43QHOYknYnArTdz
n5i20LAJdOGEW/x9TmlX+VYaLebA0NtDBKJtP+mPzKaRdz4uhbDYuSTf9F5J
IyxjU/7lfwj8B1vpKS8=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{117.84790625000001`, 35.95},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{117.906, 121.624}, {36., 37.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.654687835340103*^9, 3.654688362185135*^9}]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{536, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 4050, 105, 517, "Input"],
Cell[4633, 129, 3307, 66, 242, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7977, 200, 3889, 101, 495, "Input"],
Cell[11869, 303, 3218, 65, 242, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15124, 373, 3909, 101, 495, "Input"],
Cell[19036, 476, 3208, 65, 242, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22281, 546, 3872, 101, 363, "Input"],
Cell[26156, 649, 3141, 64, 240, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
