#!/usr/bin/python

import sys

def convert(file):
    jobs = 0
    horizon = 0
    nresources = 0
    f = open(file, 'r')
    for line in f:
        parts = line.split(':')
        if len(parts) == 2:
            if parts[0].startswith('projects'):
                assert(int(parts[1]) == 1)
            elif parts[0].startswith('jobs'):
                jobs = int(parts[1])
            elif parts[0].startswith('horizon'):
                horizon = int(parts[1])
            elif parts[0].startswith('  - renewable'):
                nresources = int(parts[1].split()[0])
        if jobs > 0 and horizon > 0 and nresources > 0: break

    for line in f:
        if line.startswith("PRECEDENCE RELATIONS"): break
    successors = parse_successors(f, jobs)
    for line in f:
        if line.startswith("REQUESTS/DURATIONS"): break
    (length, resources) = parse_req(f, jobs)
    for line in f:
        if line.startswith("RESOURCEAVAILABILITIES"): break
    f.next()
    line = f.next()
    capacities = [int(i) for i in line.split()]

    for res in range(0, len(capacities)):
        print "R %d %d \"R %d\"" % (res, capacities[res], res+1)
    
    print "T 0 0 %d \"%s\"" % (horizon, file)
    
    for job in range(0, jobs):
        print "A 0 %d %d \"A %d\"" % (job, max(1,length[job+1]), job+1)

    for res in range(0, nresources):
        for job in range(0, jobs):
            if resources[job+1][res] > 0:
                print "Q 0 %d %d %d" % (job, res, resources[job+1][res])

    for job in range(0, jobs):
        for nxt in successors[job+1]:
            print "P 0 %d 0 %d" % (job, nxt-1)


def parse_successors(f, jobs):
    s = dict()
    for line in f:
        if line.startswith("jobnr"): continue
        parts = [int(i) for i in line.split()]
        assert(len(parts) == parts[2]+3)
        s[parts[0]] = parts[3:]
        if parts[0] == jobs: return s

def parse_req(f, jobs):
    l = dict()
    r = dict()
    for line in f:
        if line.startswith("jobnr") or line.startswith("----"): continue
        parts = [int(i) for i in line.split()]
        l[parts[0]] = parts[2]
        r[parts[0]] = parts[3:]
        if parts[0] == jobs: return (l, r)

def main(argv=None):
    if argv is None:
        argv = sys.argv
    if len(argv) <= 1:
        print "Usage: %s input-file" % argv[0]
        return 1

    convert(argv[1])
    return 0

if __name__ == "__main__":
    sys.exit(main())
