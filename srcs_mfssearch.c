#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <math.h>
#include "fifo.h"
#include "srcs.h"

static int 
paircmp(const void *p1ptr, const void *p2ptr)
{
	pair_t *p1 = (pair_t*)p1ptr;
	pair_t *p2 = (pair_t*)p2ptr;
	
	return p1->y._int - p2->y._int;
}

static long int visited;
static long int dominated;

static real_t
mfsbranch(
	fifo_t *mfschain, int mfsid, plan_t *plan, plan_t *tc,
	matr_t *d, matr_t *D, int simnum, real_t *bestmk, fifo_t *tcup, 
	real_t *mk, matr_t *s, matr_t *S, matr_t *order, int i, int j, mkmeth_t mkmeth)
{
 	int ordlen;
 	
 	visited++;
	
 	srcs_plan_set_edge(plan, i, j, mfsid+1);
	srcs_plan_order(order, plan, &ordlen);
	if (ordlen < 0) {
		srcs_plan_set_edge(plan, i, j, 0);
  		return 1;
	}
	
	switch (mkmeth) {
		case MK_MCARLO:
			
 			srcs_mcarlo_est(s, d, plan, order, simnum);
 			//*mk = srcs_makespan_p(s, simnum, 0.95);
			srcs_matr_m1(mk, s, simnum, plan->n-1, 1);
 			break;
 		
 		case MK_PWL:
 			srcs_pwl_est(S, mk, plan, order, D);
 			break;
 		
 		default:
 			assert(!"unknown mkmeth");
 	}
 	 	
	if (*mk >= *bestmk) {
		dominated++;
		srcs_plan_set_edge(plan, i, j, 0);
  		return 1;
	}
	
	srcs_plan_tc_update(tc, i, j, mfsid+1, order, tcup);
		
	return 0;
}

static real_t
mfssearch(
	fifo_t *mfschain, matr_t *mfs, plan_t *plan,
	plan_t *tc, matr_t *sbs, matr_t *d, matr_t *D, int simnum, int simmax,
	real_t *bestmk, plan_t *best, 
	real_t mkinp, matr_t *sinp, matr_t *Sinp, matr_t *oinp, mkmeth_t mkmeth)
{
	int mfsid, rcvrid, rcvrnum, kidsnum, sbsid, i, j, i1, j1, kid;
	pair_t *kids;
	fifo_t tcup = {0};
	real_t ret;
	real_t mk, mkout;
	int dom;
	matr_t s = {0};
	matr_t S = {0};
	matr_t order = {0};
	//matr_t lsinp = {0};
	#if 0
	real_t lfi, lfj, esi, esj;
	#endif
	 	
	mfsid = fifo_pop(mfschain);
	
	switch (mkmeth) {
		
		case MK_MCARLO:
		
			srcs_matr_init(simnum, plan->n, &s);
			break;
		
		case MK_PWL:
			srcs_matr_init(1, D->cols, &S);
			break;
		
		default:
			assert(!"unknown mkmeth");
	}
			
	srcs_matr_init(1, plan->n, &order);
	
	//srcs_matr_init(simmax, plan->n, &lsinp);
	//srcs_mcarlo_lst(&lsinp, d, plan, oinp, *bestmk);
	
 	rcvrnum = mfs->cell[mfsid][0]._int;
	assert((kids = calloc(sizeof(pair_t), 2*rcvrnum)));
	kidsnum = 0;
	
	/* determine kids */
	for (rcvrid=0; rcvrid < rcvrnum; rcvrid++) {
		sbsid = mfs->cell[mfsid][rcvrid+1]._int;
		i = sbs->cell[sbsid][0]._int;
		j = sbs->cell[sbsid][1]._int;
		
  		if (tc->edge[i][j]) {
			kids[0].x._int = i;
			kids[0].y._int = j;
			kidsnum = 1;
			break;
		}
		if (tc->edge[j][i]) {
			kids[0].x._int = j;
			kids[0].y._int = i;
			kidsnum = 1;
			break;
		}
		
		#if 0
		
		lfi = lsinp.cell[0][i]._real + d->cell[0][i]._real;
		lfj = lsinp.cell[0][j]._real + d->cell[0][j]._real;
		esi = sinp->cell[0][i]._real;
		esj = sinp->cell[0][j]._real;
		
		if (lfi < esj) {
			kids[0].x._int = i;
			kids[0].y._int = j;
			kidsnum = 1;
			break;		
		}
		
		if (lfj < esi) {
			kids[0].x._int = j;
			kids[0].y._int = i;
			kidsnum = 1;
			break;		
		}
		#endif	
			
		kids[kidsnum].x._int = i;
		kids[kidsnum].y._int = j;
		kidsnum++;
		kids[kidsnum].x._int = j;
		kids[kidsnum].y._int = i;
		kidsnum++;	
 	}
 	
 	/* branch on each kid */
 	ret = DBL_MAX;
 	dom = 0;
  	for (kid=0; (kid < kidsnum) && !dom; kid++) {
		i = kids[kid].x._int;
		j = kids[kid].y._int;
	
		if (!tc->edge[i][j]) {
			if (mfsbranch(mfschain, mfsid, plan, tc, d, D, simnum,
				bestmk, &tcup, &mk, &s, &S, &order, i, j, mkmeth)) // here
				continue;
			
			if (fifo_empty(mfschain)) {			
				if (mk < *bestmk) {
					#if 0
					real_t actmk;
		 			srcs_mcarlo_est(&s, d, plan, &order, simmax);
 					actmk = srcs_makespan_p(&s, simmax, 0.85);
  					//actmk = makespan(s, simmax);	
					
 					if (actmk < *bestmk) {
						*bestmk = mk;
						srcs_plan_copy(best, plan);
					}
					//printf("%.1f\n", *bestmk);
					#else
					*bestmk = mk;
					srcs_plan_copy(best, plan);	
					#endif
 				}
				mkout = mk;
				
			} else {
			
				mkout = mfssearch(mfschain, mfs, plan, tc, sbs, 
						d, D, simnum, simmax, bestmk, best, 
						mk, &s, &S, &order, mkmeth); 			// here
				if ((mkinp != DBL_MAX) && (mkout == mkinp))
					dom = 1;
			}
			
			ret = MIN(ret, mkout);
			
			srcs_plan_set_edge(plan, i, j, 0);
			while(!fifo_empty(&tcup)) {
				i1 = fifo_pop(&tcup);
				j1 = fifo_pop(&tcup);
				srcs_plan_set_edge(tc, i1, j1, 0);
			}
			
 		} else if(!fifo_empty(mfschain))
			mfssearch(mfschain, mfs, plan, tc, sbs, 
			d, D, simnum, simmax, bestmk, best, 
			mkinp, sinp, Sinp, oinp, mkmeth);		
 	}
 
	fifo_push(mfschain, mfsid);
	free(kids);
	srcs_matr_free(&s);
	srcs_matr_free(&S);
	srcs_matr_free(&order);
	//srcs_matr_free(&lsinp);
	return ret;
}

int
srcs_mfssearch(
	plan_t *best, matr_t *sbs, matr_t *cvr, matr_t *map,
	matr_t *mfs, plan_t *plan, matr_t *d, matr_t *D,
	int simnum, int simmax, mkmeth_t mkmeth, real_t *mkptr, int *sec)
{
	fifo_t mfschain = {0};
	int mfsid;
	plan_t tc = {0};
	matr_t order = {0};
	matr_t s = {0};
	matr_t S = {0};
	int ordlen;
	real_t mk;
	real_t bestmk = DBL_MAX;
	pair_t mfscar[mfs->rows];
 	struct timeval tstart, tend;
 	
	/* validate arguments */
	if (simnum > d->rows)
		return 1;
	if (simmax < simnum)
		return 1;
	if (simmax > d->rows)
		return 1;
	
	/* init stats */
	visited = dominated = 0;
		
	/* init order */
	srcs_matr_init(1, plan->n, &order);
	srcs_plan_order(&order, plan, &ordlen);	
	if (ordlen < 0) {
		srcs_matr_free(&order);
		return 1;
	}
	
	/* sort mfss */
	for (mfsid=0; mfsid < mfs->rows; mfsid++) {
		mfscar[mfsid].x._int = mfsid;
		mfscar[mfsid].y._int = -mfs->cell[mfsid][0]._int;
	}
	qsort(mfscar, mfs->rows, sizeof(pair_t), paircmp);
	
	for (mfsid=0; mfsid < mfs->rows; mfsid++)
		fifo_push(&mfschain, mfscar[mfsid].x._int);
		
	/* init tc */
	srcs_plan_init(plan->n, &tc);
	srcs_plan_tc(&tc, plan, &order);
	
	/* init s */
	srcs_matr_init(simmax, d->cols, &s);
	
	/* init mk */
	switch (mkmeth) {
	
		case MK_MCARLO:

			srcs_mcarlo_est(&s, d, plan, &order, simnum);	
			// mk = srcs_makespan_p(&s, simnum, 0.95);
			srcs_matr_m1(&mk, &s, simnum, plan->n-1, 1);
			break;
		
		case MK_PWL:
			
			srcs_matr_init(1, D->cols, &S);
			srcs_pwl_est(&S, &mk, plan, &order, D);
			break;
		
		default:
			assert(!"mkmeth == MK_NONE");
	}
		
	//mk = makespan(&s, simnum);
	
	/* time the search call */
	gettimeofday(&tstart, NULL);
		
	mfssearch(&mfschain, mfs, plan, &tc, sbs, d, D, simnum, simmax,
		&bestmk, best, mk, &s, &S, &order, mkmeth);
	
	gettimeofday(&tend, NULL);
	
	/* accurate makespan of best */
 	srcs_plan_order(&order, best, &ordlen);
	assert(ordlen > 0);
	srcs_mcarlo_est(&s, d, best, &order, simmax);
	srcs_matr_m1(&mk, &s, simmax, plan->n-1, 1);
	// mk = srcs_makespan_p(&s, simmax, 0.95);
	
	*mkptr = mk;
	*sec = tend.tv_sec-tstart.tv_sec;
	
	/* tc of best */
	srcs_plan_tc(&tc, best, &order);
	
	/* cleanup*/
	srcs_plan_free(&tc);
	srcs_matr_free(&order);
	srcs_matr_free(&s);
	return 0;
}
