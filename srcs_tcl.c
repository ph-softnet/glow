#include <stdio.h>
#include <stdlib.h>
#include <tcl.h>
#include <assert.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include "fifo.h"
#include "srcs.h"

Tcl_HashTable 			ptrtab;
static int				ptrtab_get(Tcl_Obj *objkey, void **val);
static Tcl_HashEntry*	ptrtab_set(Tcl_Obj *objkey, void *val);
static int				ptrtab_del(Tcl_Obj *objkey);

#if 0
static int
solvecbfunc(solvecb_t *cb, int en, int maxen, 
			plan_t *sol, real_t rob, real_t m1, real_t m2)
{
	Tcl_Interp *ip;
	Tcl_Obj *body;
	Tcl_Obj *solkey;
	
	assert((cb->argc == 3));
	ip = *((Tcl_Interp**)cb->argv[0]);
	body = Tcl_DuplicateObj(*((Tcl_Obj**)cb->argv[1]));
	solkey = *((Tcl_Obj**)cb->argv[2]);
	
	Tcl_AppendPrintfToObj(body, " %s %d %d %.2f %.2f %.2f", 
		Tcl_GetString(solkey), en, maxen, rob, m1, m2);
		
	if (Tcl_EvalObjEx(ip, body, 0) != TCL_OK)
		return TCL_ERROR;

	return TCL_OK;
}

int solvestop;
static void
solvetimer(int sig)
{
	signal(sig, SIG_IGN);
	solvestop = 1;
}

static int
C_solve_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *sol;
	plan_t *plan;
	rvar_t *dur;
	rcons_t *res;
	matr_t *due;
	char *strrob;
	int rob;
	char *strssmeth;
	int ssmeth;
	int numsim;
	int maxenum;
	int cbindx;
	solvecb_t cb;
	Tcl_Obj *solkey;
	int timebudget;
	
	if (objc < 10) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: solution */
	if (ptrtab_get( (solkey=objv[1]), (void**)&sol)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: durations */
	if (!ptrtab_get( objv[3], (void**)&dur)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: resource constraints */
	if (!ptrtab_get( objv[4], (void**)&res)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[5]: due-date constraints */
	if (!ptrtab_get( objv[5], (void**)&due)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: maximum enumerations */	
	if (Tcl_GetIntFromObj(ip, objv[6], &maxenum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid maxenum: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: time budget (sec) */
	if (Tcl_GetIntFromObj(ip, objv[7], &timebudget) != TCL_OK) {
			result = Tcl_NewStringObj("invalid timebudget: ", -1);
			Tcl_AppendObjToObj(result, objv[7]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
	}
	
	/* objv[8]: robustness objective */	
	strrob = Tcl_GetString(objv[8]);
	if (!strncmp(strrob, "alpha", strlen("alpha")))
		rob = ROB_ALPHA;	
	else if (!strncmp(strrob, "beta", strlen("beta")))
		rob = ROB_BETA;			
	else {
		result = Tcl_NewStringObj("invald robustness type: ", -1);
		Tcl_AppendStringsToObj(result, strrob);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	

	/* objv[9]: ssmethod {sta, sim} */
	if (!strncmp((strssmeth=Tcl_GetString(objv[9])), "sta", strlen("sta")))
		ssmeth = SSMETH_STA;
	else if (!strncmp(strssmeth, "scu", strlen("scu")))
		ssmeth = SSMETH_SCU;
	else if (!strncmp(strssmeth, "sim", strlen("sim")))
		ssmeth = SSMETH_SIM;
	else {
		result = Tcl_NewStringObj("invalid ssmeth", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[(cbindx=10)]: number of simulations (if ssmethod=sim) */
	cbindx = 10;
	if (ssmeth == SSMETH_SIM) {
		if (objc < 11) {
			result = Tcl_NewStringObj("number of simulations missing", -1);
 			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}

		if (Tcl_GetIntFromObj(ip, objv[cbindx], &numsim) != TCL_OK) {
			result = Tcl_NewStringObj("invalid numsim: ", -1);
			Tcl_AppendObjToObj(result, objv[cbindx]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}
		
		cbindx++;
	}
	
	/* objv[cbindex]: callback */
	void *cb_argv[] = {
		&ip,
		&((Tcl_Obj**)objv)[cbindx],
		&solkey
	};
	cb.f = solvecbfunc;
	cb.stop = &solvestop;
	cb.argc = sizeof(cb_argv)/sizeof(void*);
	cb.argv = cb_argv;
	
	if (timebudget > 0) {
		signal(SIGALRM, solvetimer);
		alarm(timebudget);
	}
	
	/* make solver call and store solution */ 
	assert((sol = calloc(sizeof(plan_t), 1)));
	
	(void)ptrtab_set(objv[1], sol);
	

	if (srcs_solve(plan, dur, res, due, maxenum, rob, 
		ssmeth, numsim, sol, &cb, 1)) {
		result = Tcl_NewStringObj("srcs_solve failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	solvestop = 0;
	if (timebudget > 0)
		signal(SIGALRM, SIG_IGN);
	
	return TCL_OK;
}
#endif

static int
C_simul_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	matr_t *d;
	pair_t *dist;
 	matr_t *s;
 	matr_t *c;
 	int numsim;
 	bfsctx_t ctx;
 	
 	if (objc != 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1], objv[2]: stochastic schedule */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (!ptrtab_get( objv[2], (void**)&c)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: plan */
	if (!ptrtab_get( objv[3], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: scenarios */
	if (!ptrtab_get( objv[4], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[5]: number of simulations */
	if (Tcl_GetIntFromObj(ip, objv[5], &numsim) != TCL_OK)
		return TCL_ERROR;
		
	/* make the simulator call and store the samples */
	assert((dist = calloc(sizeof(pair_t), plan->n)));
	srcs_bfsctx_init(&ctx, plan->n);
	
	if (srcs_plan_bfs(&ctx, plan, 0, dist, 0)) {
		result = Tcl_NewStringObj("srcs_plan_bfs failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if(srcs_simulate(plan, dist, numsim, d, s, c)) {
		result = Tcl_NewStringObj("srcs_simulate failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	free(dist);
	srcs_bfsctx_free(&ctx);
	
	return TCL_OK;
}


static int
C_rob_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	char *strtype;
	Tcl_Obj *result;
	rvar_t *C;
	matr_t *due;
	int n, robtype;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
	/* objv[1]: completion times stoch. vector */
	if (!ptrtab_get( objv[1], (void**)&C)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: due-date constraints object */
	if (!ptrtab_get( objv[2], (void**)&due)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: n */
	if (Tcl_GetIntFromObj(ip, objv[3], &n) != TCL_OK) {
		result = Tcl_NewStringObj("invalid n: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: robustness type  */
	strtype = Tcl_GetString(objv[4]);
	
	/* compute robustness */
	if (!strncmp(strtype, "alpha", strlen("alpha")))
		robtype = ROB_ALPHA;	
	else if (!strncmp(strtype, "beta", strlen("beta")))
		robtype = ROB_BETA;
	else {
		result = Tcl_NewStringObj("invald robustness type: ", -1);
		Tcl_AppendStringsToObj(result, strtype);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	result = Tcl_NewDoubleObj(srcs_rob(C, due, n, robtype));
	Tcl_SetObjResult(ip, result);
	return TCL_OK;
}

static int
C_plan_new_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	int n;
	
	 	
 	if (objc != 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
	/* objv[1]: plan */
	if (ptrtab_get( objv[1], (void**)&plan)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: n */
	if (Tcl_GetIntFromObj(ip, objv[2], &n) != TCL_OK) {
		return TCL_ERROR;
	}
	
	assert((plan = calloc(sizeof(plan_t), 1)));
	if (srcs_plan_init(n, plan)) {
		result = Tcl_NewStringObj("srcs_plan_init failed ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		free(plan);
		return TCL_ERROR;
	}
 	
	(void)ptrtab_set(objv[1], plan);
	return TCL_OK;
}


static int
C_plan_set_edge_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	int a1, a2, e;
	
	if (objc != 5) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: plan */
	if (!ptrtab_get(objv[1], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2], objv[3]: a1, a2 */
	if (Tcl_GetIntFromObj(ip, objv[2], &a1) != TCL_OK)
		return TCL_ERROR;
	if (Tcl_GetIntFromObj(ip, objv[3], &a2) != TCL_OK)
		return TCL_ERROR;
		
	/* objv[4]: value */
	if (Tcl_GetIntFromObj(ip, objv[4], &e) != TCL_OK)
		return TCL_ERROR;
		
	if (srcs_plan_set_edge(plan, a1, a2, e)) {
		result = Tcl_NewStringObj("plan_set_edge failed: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_AppendStringsToObj(result, ", ");
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

static int
C_plan_get_edge_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	int a1, a2;
	
	if (objc != 4) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: plan */
	if (!ptrtab_get(objv[1], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2], objv[3]: a1, a2 */
	if (Tcl_GetIntFromObj(ip, objv[2], &a1) != TCL_OK)
		return TCL_ERROR;
	if (Tcl_GetIntFromObj(ip, objv[3], &a2) != TCL_OK)
		return TCL_ERROR;
	
	if (a1 < 0 || a2 < 0 || a1 >= plan->n || a2 >= plan->n)	{
		result = Tcl_NewStringObj("invalid params", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	result = Tcl_NewIntObj(plan->edge[a1][a2]);
	Tcl_SetObjResult(ip, result);
	
	return TCL_OK;
}

static int
C_plan_get_n_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	
	if (objc != 2) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: plan */
	if (!ptrtab_get(objv[1], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	result = Tcl_NewIntObj(plan->n);
	Tcl_SetObjResult(ip, result);
	
	return TCL_OK;
}

static int
C_plan_copy_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *dstplan, *srcplan;
	
	if (objc != 3) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: plan1 */
	if (!ptrtab_get(objv[1], (void**)&dstplan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: plan2 */
	if (!ptrtab_get(objv[2], (void**)&srcplan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_plan_copy(dstplan, srcplan)) {
		result = Tcl_NewStringObj("srcs_plan_copy failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}


static int
C_rvec_new_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	int k, n, i, j;
		
	if (objc != 4) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: random vector */
	if (ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2], objv[3]: k, n */
	if (Tcl_GetIntFromObj(ip, objv[2], &k) != TCL_OK)
		return TCL_ERROR;
	if (Tcl_GetIntFromObj(ip, objv[3], &n) != TCL_OK)
		return TCL_ERROR;
	
	assert((V=calloc(sizeof(rvar_t), n)));
	for (i=0; i < n; i++) {
		if (srcs_rvar_init(k, &V[i])) {
			for (j=0; j < i; j++)
				srcs_rvar_free(k, &V[i]);
			free(V);
			result = Tcl_NewStringObj("srcs_rvec_init failed: ", -1);
			return TCL_ERROR;
		}
	}
	
	(void)ptrtab_set(objv[1], V);

	return TCL_OK;
}


static int
C_rvec_set_m1_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	int i;
	double m1;
	
	if (objc != 4) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: random vector */
	if (!ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2], objv[3]: i, m1 */
	if (Tcl_GetIntFromObj(ip, objv[2], &i) != TCL_OK)
		return TCL_ERROR;
	if (Tcl_GetDoubleFromObj(ip, objv[3], &m1) != TCL_OK)
		return TCL_ERROR;
	
	V[i].m1 = (real_t)m1;
	
	return TCL_OK;
}

static int
C_rvec_set_cov_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	int i, j;
	double cov;
	
	if (objc != 5) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: random vector */
	if (!ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2], objv[3]: i, j */
	if (Tcl_GetIntFromObj(ip, objv[2], &i) != TCL_OK)
		return TCL_ERROR;
	if (Tcl_GetIntFromObj(ip, objv[3], &j) != TCL_OK)
		return TCL_ERROR;

	/* objv[4]: cov */
	if (Tcl_GetDoubleFromObj(ip, objv[4], &cov) != TCL_OK)
		return TCL_ERROR;
	
	srcs_rvar_set_cov(&V[i], j, (real_t)cov);
	
	return TCL_OK;
}

static int
C_rvec_get_mom_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	int i;
	
	if (objc != 3) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: random vector */
	if (!ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: i*/
	if (Tcl_GetIntFromObj(ip, objv[2], &i) != TCL_OK) {
		result = Tcl_NewStringObj("invalid i: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	assert((result=Tcl_NewListObj(0, NULL)));
	Tcl_ListObjAppendElement(ip, result, Tcl_NewDoubleObj((double)V[i].m1));
	Tcl_ListObjAppendElement(ip, result, Tcl_NewDoubleObj((double)V[i].m2));
	
	Tcl_SetObjResult(ip, result);
	return TCL_OK;
}

static int
C_rvec_get_cov_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	int i, j;
	
	if (objc != 3) {
		result = Tcl_NewStringObj("wrong num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[1]: random vector */
	if (!ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: i*/
	if (Tcl_GetIntFromObj(ip, objv[2], &i) != TCL_OK) {
		result = Tcl_NewStringObj("invalid i: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	assert((result=Tcl_NewListObj(0, NULL)));
	for (j=0; j < V->k; j++)
		Tcl_ListObjAppendElement(ip, result, Tcl_NewDoubleObj((double)V[i].cov[j]));
	
	Tcl_SetObjResult(ip, result);
	return TCL_OK;
}

int
C_rvec_sim_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *V;
	matr_t *sim;
	int n, numsim;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: V */
	if (!ptrtab_get( objv[1], (void**)&V)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: sim */
	if (!ptrtab_get( objv[2], (void**)&sim)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: numsim */
	if ((Tcl_GetIntFromObj(ip, objv[3], &numsim) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid numsim: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: n */
	if ((Tcl_GetIntFromObj(ip, objv[4], &n) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid n: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_rvec_sim(sim, numsim, n, V)) {
		result = Tcl_NewStringObj("srcs_rvec_sim failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

static int
C_vec_new_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	real_t *vec;
	int n;
	
	/* objv[1]: vector */
	if (ptrtab_get(objv[1], (void**)&vec)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: n */
	if ((Tcl_GetIntFromObj(ip, objv[2], &n) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid n: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	assert((vec = calloc(sizeof(real_t), 1)));
	/* todo: compose resource constraints */
	
	(void)ptrtab_set(objv[1], vec);

	return TCL_OK;
}



int
C_lgsta_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	rvar_t *S;
 	rvar_t *C;
 	plan_t *plan;
 	pair_t *dist;
 	rvar_t *D;
  	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1], objv[2]: stochastic schedule */
	if (!ptrtab_get( objv[1], (void**)&S)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (!ptrtab_get( objv[2], (void**)&C)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: plan */
	if (!ptrtab_get( objv[3], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: durations */
	if (!ptrtab_get( objv[4], (void**)&D)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	assert((dist=calloc(sizeof(pair_t), plan->n)));
	
	if (srcs_plan_bfs(NULL, plan, 0, dist, 0)) {
		result = Tcl_NewStringObj("srcs_plan_bfs failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if(srcs_lgsta(plan, dist, D, S, C)) {
		result = Tcl_NewStringObj("srcs_lgsta failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	
	free(dist);
	
	return TCL_OK;
}

int
C_rcons_new_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rcons_t *res;
	int k, n;
 	
 	if (objc != 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: resource constraints */
	if (ptrtab_get( objv[1], (void**)&res)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: k */
	if ((Tcl_GetIntFromObj(ip, objv[2], &k) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid k: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: n */
	if ((Tcl_GetIntFromObj(ip, objv[3], &n) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid n: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	assert((res=calloc(sizeof(rcons_t), 1)));
	if (srcs_rcons_init(k, n, res)) {
		result = Tcl_NewStringObj("srcs_rcons_new failed ", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	(void)ptrtab_set(objv[1], res);
	
	return TCL_OK;
}

int
C_rcons_set_cap_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rcons_t *res;
	int r, cap;
 	
 	if (objc != 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: resource constraints */
	if (!ptrtab_get( objv[1], (void**)&res)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: r */
	if ((Tcl_GetIntFromObj(ip, objv[2], &r) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid r: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: cap */
	if ((Tcl_GetIntFromObj(ip, objv[3], &cap) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid cap: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	if (srcs_rcons_set_cap(res, r, cap)) {
		result = Tcl_NewStringObj("srcs_rcons_set_cap failed ", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	return TCL_OK;
}

int
C_rcons_set_req_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rcons_t *res;
	int a, r, req;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: resource constraints */
	if (!ptrtab_get( objv[1], (void**)&res)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: a */
	if ((Tcl_GetIntFromObj(ip, objv[2], &a) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid a: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: r */
	if ((Tcl_GetIntFromObj(ip, objv[3], &r) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid r: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: req */
	if ((Tcl_GetIntFromObj(ip, objv[4], &req) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid req: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	if (srcs_rcons_set_req(res, a, r, req)) {
		result = Tcl_NewStringObj("srcs_rcons_set_req failed ", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_matr_new_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
	int rows, cols;
 	
 	if (objc != 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: rows */
	if ((Tcl_GetIntFromObj(ip, objv[2], &rows) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid rows: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: cols */
	if ((Tcl_GetIntFromObj(ip, objv[3], &cols) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid cols: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	assert((matr=calloc(sizeof(matr_t), 1)));
	if (srcs_matr_init(rows, cols, matr)) {
		result = Tcl_NewStringObj("srcs_matr_init failed ", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	(void)ptrtab_set(objv[1], matr);

	return TCL_OK;
}

int
C_matr_free_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
 	
 	if (objc != 2) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (matr == NULL)
		fprintf(stderr, "srcs::c_matr_free: matr = NULL\n");
	else
		srcs_matr_free(matr);
		
	Tcl_SetObjResult(ip, Tcl_NewIntObj(matr->rows));

	return TCL_OK;
}
int
C_matr_get_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
	int row, col;
	char *type;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: row */
	if ((Tcl_GetIntFromObj(ip, objv[2], &row) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid row: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: col */
	if ((Tcl_GetIntFromObj(ip, objv[3], &col) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	assert((type = Tcl_GetString(objv[4])));
	if (!strncmp(type, "real", strlen("real")))
		Tcl_SetObjResult(ip, Tcl_NewDoubleObj(matr->cell[row][col]._real));
	else if (!strncmp(type, "int", strlen("int")))
		Tcl_SetObjResult(ip, Tcl_NewIntObj(matr->cell[row][col]._int));
	else if (!strncmp(type, "ptr", strlen("ptr")))
		Tcl_SetObjResult(ip, (Tcl_Obj*)matr->cell[row][col]._ptr);
	else {
		result = Tcl_NewStringObj("invalid type: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	return TCL_OK;
}



int
C_matr_set_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
	int row, col;
	char *type;
 	
 	if (objc != 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: row */
	if ((Tcl_GetIntFromObj(ip, objv[2], &row) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid row: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: col */
	if ((Tcl_GetIntFromObj(ip, objv[3], &col) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: type */
	assert((type = Tcl_GetString(objv[4])));
	if (!strncmp(type, "real", strlen("real"))) {
		/* objv[5]: val */
		if ((Tcl_GetDoubleFromObj(ip, objv[5], &matr->cell[row][col]._real) != TCL_OK)) {
			result = Tcl_NewStringObj("invalid val: ", -1);
			Tcl_AppendObjToObj(result, objv[5]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}
		
	} else if (!strncmp(type, "int", strlen("int"))) {
		/* objv[5]: val */
		if ((Tcl_GetIntFromObj(ip, objv[5], &matr->cell[row][col]._int) != TCL_OK)) {
			result = Tcl_NewStringObj("invalid val: ", -1);
			Tcl_AppendObjToObj(result, objv[5]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}
 	} else if (!strncmp(type, "ptr", strlen("ptr"))) {
		/* objv[5]: val */
		matr->cell[row][col]._ptr = (void*)Tcl_DuplicateObj(objv[5]);
		
	} else {
		result = Tcl_NewStringObj("invalid type: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	return TCL_OK;
}

int
C_matr_add_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *m1, *m2;
 	char *typestr;
	int type;
 	
 	if (objc != 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: m1 */
	if (!ptrtab_get( objv[1], (void**)&m1)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: m2 */
	if (!ptrtab_get( objv[2], (void**)&m2)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
 
	assert((typestr = Tcl_GetString(objv[3])));
	if (!strncmp(typestr, "int", strlen("int")))
		type = 1;
	else if (!strncmp(typestr, "real", strlen("real")))
		type = 2;
	else {
		result = Tcl_NewStringObj("invalid type: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_matr_add(m1, m2, type)) {
 		result = Tcl_NewStringObj("srcs_matr_add failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	return TCL_OK;
}

int
C_matr_copy_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *m1, *m2;
  	
 	if (objc != 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: m1 */
	if (!ptrtab_get( objv[1], (void**)&m1)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: m2 */
	if (!ptrtab_get( objv[2], (void**)&m2)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_matr_copy(m1, m2)) {
 		result = Tcl_NewStringObj("srcs_matr_cpy failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	return TCL_OK;
}

int
C_matr_get_rows_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
 	
 	if (objc != 2) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	Tcl_SetObjResult(ip, Tcl_NewIntObj(matr->rows));

	return TCL_OK;
}

int
C_matr_get_cols_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
 	
 	if (objc != 2) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	Tcl_SetObjResult(ip, Tcl_NewIntObj(matr->cols));

	return TCL_OK;
}

int
C_matr_m1_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *matr;
	int simnum, col, type_int;
	char *type;
	real_t m1;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&matr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: simnum */
	if ((Tcl_GetIntFromObj(ip, objv[2], &simnum) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: col */
	if ((Tcl_GetIntFromObj(ip, objv[3], &col) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: type */
	type = Tcl_GetString(objv[4]);
	type_int = -1;
	if (!strcmp(type, "int"))
		type_int = 0;
	if (!strcmp(type, "real"))
		type_int = 1;
	
	if (srcs_matr_m1(&m1, matr, simnum, col, type_int)) {
 		result = Tcl_NewStringObj("srcs_matr_m1 failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	result = Tcl_NewDoubleObj(m1);
	Tcl_SetObjResult(ip, result);	
	
	return TCL_OK;
}

int
C_matr_p_j_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *mat;
	int simnum, col;
	real_t per;
	real_t res;
 	
 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: matrix */
	if (!ptrtab_get( objv[1], (void**)&mat)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: simnum */
	if ((Tcl_GetIntFromObj(ip, objv[2], &simnum) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: col */
	if ((Tcl_GetIntFromObj(ip, objv[3], &col) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: percentile */
	if ((Tcl_GetDoubleFromObj(ip, objv[4], &per) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid per: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
 
 	if (srcs_matr_p_j(mat, simnum, col, per, &res)) {
 		result = Tcl_NewStringObj("srcs_matr_p_j failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	result = Tcl_NewDoubleObj(res);
	Tcl_SetObjResult(ip, result);	
	
	return TCL_OK;
}


int
C_resprof_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	int r;
	rcons_t *res;
	matr_t *prof;
	matr_t *sim_s, *sim_c;
	int numsim, n;

 	if (objc != 8) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
 	/* objv[1]: r */
	if ((Tcl_GetIntFromObj(ip, objv[1], &r) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid r: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: res */
	if (!ptrtab_get( objv[2], (void**)&res)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: prof_s */
	if (!ptrtab_get( objv[3], (void**)&prof)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	/* objv[4]: sim_s */
	if (!ptrtab_get( objv[4], (void**)&sim_s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[5]: sim_c */
	if (!ptrtab_get( objv[5], (void**)&sim_c)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: numsim */
	if ((Tcl_GetIntFromObj(ip, objv[6], &numsim) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid numsim: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: numsim */
	if ((Tcl_GetIntFromObj(ip, objv[7], &n) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid n: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_resprof(r, res, prof, sim_s, sim_c, numsim, n)) {
		result = Tcl_NewStringObj("srcs_resprof failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_srand_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	int seed;

 	if (objc != 2) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
 	/* objv[1]: r */
	if ((Tcl_GetIntFromObj(ip, objv[1], &seed) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid seed: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	srcs_srand(seed);
	
	return TCL_OK;
}

int
C_lgsta_tard_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	rvar_t *C, *T;
	matr_t *due;
	int a;

 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
 	/* objv[1]: T */
	if (!ptrtab_get( objv[1], (void**)&T)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: C */
	if (!ptrtab_get( objv[2], (void**)&C)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: due */
	if (!ptrtab_get( objv[3], (void**)&due)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
 	/* objv[4]: a */
	if ((Tcl_GetIntFromObj(ip, objv[4], &a) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid a: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	if (srcs_lgsta_tard(C, due, T)) {
		result = Tcl_NewStringObj("srcs_lgsta_tard failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_simul_tard_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	matr_t *t, *c, *due;
	int numsim;

 	if (objc != 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
 	/* objv[1]: t */
	if (!ptrtab_get( objv[1], (void**)&t)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: c */
	if (!ptrtab_get( objv[2], (void**)&c)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
 	/* objv[3]: numsim */
	if ((Tcl_GetIntFromObj(ip, objv[3], &numsim) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid a: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
 	/* objv[4]: due */
	if (!ptrtab_get( objv[4], (void**)&due)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	if (srcs_simulate_tard(c, numsim, due, t)) {
		result = Tcl_NewStringObj("srcs_simulate_tard failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_mcarlo_crt_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *crt, *ecrt;
	matr_t *x, *d;
	matr_t *sort;
	plan_t *plan;
	int numsim, sink;
 	
 	if (objc != 9) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: crt */
	if (!ptrtab_get( objv[1], (void**)&crt)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: ecrt */
	if (!strcmp(Tcl_GetString(objv[2]), ".NULL"))
		ecrt = NULL;
	else if (!ptrtab_get( objv[2], (void**)&ecrt)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	/* objv[3]: x */
	if (!ptrtab_get( objv[3], (void**)&x)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: d */
	if (!ptrtab_get( objv[4], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
 	/* objv[5]: numsim */
	if ((Tcl_GetIntFromObj(ip, objv[5], &numsim) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid numsim: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	/* objv[6]: sort */
	if (!ptrtab_get( objv[6], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
			
	/* objv[7]: sort */
	if (!ptrtab_get( objv[7], (void**)&sort)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
 	/* objv[8]: sink */
	if ((Tcl_GetIntFromObj(ip, objv[8], &sink) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid sink: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
		
	if (srcs_mcarlo_crt(x, d, numsim, crt, ecrt, plan, sort, sink)) {
		result = Tcl_NewStringObj("srcs_crt_sim failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_plan_bfs_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	matr_t *bfs;
	int rand;
	pair_t *dist;
	int i;
 	
 	if (objc < 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: bfs */
	if (!ptrtab_get( objv[1], (void**)&bfs)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	rand = 0;
	if (objc >= 4) {
		/* objv[3]: rand */
		if ((Tcl_GetIntFromObj(ip, objv[3], &rand) != TCL_OK)) {
			result = Tcl_NewStringObj("invalid rand: ", -1);
			Tcl_AppendObjToObj(result, objv[3]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}
	}
	
	assert((dist = calloc(sizeof(pair_t), plan->n)));
	if (srcs_plan_bfs(NULL, plan, 0, dist, rand)) {
		result = Tcl_NewStringObj("srcs_crit_sim failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	for (i=0; i < plan->n; i++)
		bfs->cell[0][i]._int = dist[i].x._int;
	
	free(dist);
	
	return TCL_OK;
}

int
C_plan_bfs2_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *plan;
	matr_t *bfs;
	int rand;
  	
 	if (objc < 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: bfs */
	if (!ptrtab_get( objv[1], (void**)&bfs)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	rand = 0;
	if (objc >= 4) {
		/* objv[3]: rand */
		if ((Tcl_GetIntFromObj(ip, objv[3], &rand) != TCL_OK)) {
			result = Tcl_NewStringObj("invalid rand: ", -1);
			Tcl_AppendObjToObj(result, objv[3]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
		}
	}
	
	if (srcs_plan_bfs2(plan, 0, bfs)) {
		result = Tcl_NewStringObj("srcs_crit_sim failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_plan_tc_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	plan_t *tc, *plan;
	matr_t *order;
  	
 	if (objc < 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: tc */
	if (!ptrtab_get( objv[1], (void**)&tc)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: order */
	if (!ptrtab_get( objv[3], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_plan_tc(tc, plan, order)) {
		result = Tcl_NewStringObj("srcs_crit_sim failed", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	return TCL_OK;
}

int
C_ft_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result, *mfslistobj, *mfsobj;
	matr_t *ft, *q, *cp, *comp;
	int **mfslist;
	int mfslistlen;
	int l, m;
   	
 	if (objc < 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: ft */
	if (ptrtab_get( objv[1], (void**)&ft)) {
		result = Tcl_NewStringObj("already exists: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[2]: q */
	if (!ptrtab_get( objv[2], (void**)&q)) { 
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	/* objv[3]: cp */
	if (!ptrtab_get( objv[3], (void**)&cp)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
 	 	
	/* objv[4]: comp */
	if (!ptrtab_get( objv[4], (void**)&comp)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	//assert((ft = calloc(sizeof(matr_t), 1)));
	//(void)ptrtab_set(objv[1], ft);
	ft = NULL;
	
	if (srcs_ft(q, cp, comp, ft, &mfslist, &mfslistlen)) {
		result = Tcl_NewStringObj("srcs_ft_make failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
	
	assert((mfslistobj = Tcl_NewListObj(0, NULL)));
	for (l=0; l < mfslistlen; l++) {
		assert((mfsobj=Tcl_NewListObj(0,NULL)));
 		for (m=0; m < mfslist[l][0]; m++) {
 			Tcl_ListObjAppendElement(ip, mfsobj, Tcl_NewIntObj(mfslist[l][m+1]));
		}
 		Tcl_ListObjAppendElement(ip, mfslistobj, mfsobj);
	}
	
	result = mfslistobj;
	Tcl_SetObjResult(ip, result);
	return TCL_OK;
}


int
C_plan_order_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *order;
	plan_t *plan;
	int orderlen;
	
    	
 	if (objc < 3) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: order */
	if (!ptrtab_get( objv[1], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
			
	if (srcs_plan_order(order, plan, &orderlen)) {
		result = Tcl_NewStringObj("srcs_mfsbreak failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	} else {
		result = Tcl_NewIntObj(orderlen);
		Tcl_SetObjResult(ip, result);
	}
 	
	return TCL_OK;
}

int
C_plan_tc_update_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t  *order;
	plan_t *tc;
	int s, t, v;
	fifo_t f;
	int i, j;
	Tcl_Obj *flist;
    	
 	if (objc < 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: tc */
	if (!ptrtab_get( objv[1], (void**)&tc)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (Tcl_GetIntFromObj(ip, objv[2], &s) != TCL_OK) {
		result = Tcl_NewStringObj("invalid s: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (Tcl_GetIntFromObj(ip, objv[3], &t) != TCL_OK) {
		result = Tcl_NewStringObj("invalid t: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	if (Tcl_GetIntFromObj(ip, objv[4], &v) != TCL_OK) {
		result = Tcl_NewStringObj("invalid v: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[5]: order */
	if (!ptrtab_get( objv[5], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	

	
	fifo_init(&f);
			
	if (srcs_plan_tc_update(tc, s, t, v, order, &f)) {
		result = Tcl_NewStringObj("srcs_mfsbreak failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
	
	flist = Tcl_NewListObj(0, NULL);
	
 	while(!fifo_empty(&f)) {
		i = fifo_pop(&f);
		j = fifo_pop(&f);
		Tcl_ListObjAppendElement(ip, flist, Tcl_NewIntObj(i));
		Tcl_ListObjAppendElement(ip, flist, Tcl_NewIntObj(j));
	}
 	Tcl_SetObjResult(ip, flist); 	
	return TCL_OK;
}

int
C_mfssearch_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result, *resultvec[2];
	matr_t *sbs, *cvr, *map, *d, *D;
	plan_t *plan, *best;
	matr_t *mfs;
	int simnum, simmax;
	mkmeth_t mkmeth;
	char *mkmethstr;
	real_t mk;
	int sec;
	
 	if (objc < 11) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
 	/* objv[1]: best */
	if (!ptrtab_get( objv[1], (void**)&best)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[2]: sbs */
	if (!ptrtab_get( objv[2], (void**)&sbs)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[3]: cvr */
	if (!ptrtab_get( objv[3], (void**)&cvr)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[4]: map */
	if (!ptrtab_get( objv[4], (void**)&map)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	/* objv[5]: mfs */
	if (!ptrtab_get( objv[5], (void**)&mfs)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	 
		
	/* objv[6]: plan */
	if (!ptrtab_get( objv[6], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
			
	/* objv[7]: d */
	if (!ptrtab_get( objv[7], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
			
	if (Tcl_GetIntFromObj(ip, objv[8], &simmax) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simmax: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	mkmeth = MK_NONE;
	mkmethstr = Tcl_GetString(objv[9]);
	if (!strcmp(mkmethstr, "ssta-pwl"))
		mkmeth = MK_PWL;
	else if (!strcmp(mkmethstr, "mcarlo"))
		mkmeth = MK_MCARLO;

	
	switch (mkmeth) {
	
		case MK_MCARLO:
					
			if (Tcl_GetIntFromObj(ip, objv[10], &simnum) != TCL_OK) {
				result = Tcl_NewStringObj("invalid simnum: ", -1);
				Tcl_AppendObjToObj(result, objv[10]);
				Tcl_SetObjResult(ip, result);
				return TCL_ERROR;
			}

			
			D = NULL;
			
			if (srcs_mfssearch(best, sbs, cvr, map, 
				mfs, plan, d, D, simnum, simmax, MK_MCARLO, &mk, &sec)) {
				result = Tcl_NewStringObj("srcs_mfssearch failed", -1);
		 		Tcl_SetObjResult(ip, result);
				return TCL_ERROR;		
			}
			break;
		
		case MK_PWL:
		 	
			/* objv[8]: d */
			if (!ptrtab_get( objv[10], (void**)&D)) {
				result = Tcl_NewStringObj("does not exist: ", -1);
				Tcl_AppendObjToObj(result, objv[10]);
				Tcl_SetObjResult(ip, result);
				return TCL_ERROR;
			}
 
 			simnum = 0;
			
			if (srcs_mfssearch(best, sbs, cvr, map, 
				mfs, plan, d, D, simnum, simmax, MK_PWL, &mk, &sec)) {
				result = Tcl_NewStringObj("srcs_mfssearch failed", -1);
		 		Tcl_SetObjResult(ip, result);
				return TCL_ERROR;		
			}				
			break;
		
		default:
	
			result = Tcl_NewStringObj("unknown mkmethod: ", -1);
			Tcl_AppendObjToObj(result, objv[7]);
			Tcl_SetObjResult(ip, result);
			return TCL_ERROR;
			
	}
	
	resultvec[0] = Tcl_NewDoubleObj(mk);
	resultvec[1] = Tcl_NewIntObj(sec);
 	result = Tcl_NewListObj(2, resultvec);
	Tcl_SetObjResult(ip, result);	
	return TCL_OK;
}

int
C_pwl_est_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *S, *D, *order;
	plan_t *plan;
	real_t p95;
    	
 	if (objc < 5) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: S */
	if (!ptrtab_get( objv[1], (void**)&S)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
  
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[3]: order */
	if (!ptrtab_get( objv[3], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: D */
	if (!ptrtab_get( objv[4], (void**)&D)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
			
	if (srcs_pwl_est(S, &p95, plan, order, D)) {
		result = Tcl_NewStringObj("srcs_pwl_est failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
	
	Tcl_SetObjResult(ip, Tcl_NewDoubleObj(p95));
	return TCL_OK;
}

int
C_mcarlo_est_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *s, *d, *order;
	plan_t *plan;
	int simnum;
    	
 	if (objc < 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: s */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
  
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[3]: order */
	if (!ptrtab_get( objv[3], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: d */
	if (!ptrtab_get( objv[4], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (Tcl_GetIntFromObj(ip, objv[5], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
			
	if (srcs_mcarlo_est(s, d, plan, order, simnum)) {
		result = Tcl_NewStringObj("srcs_mcarlo_est failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
 	
	return TCL_OK;
}

int
C_mcarlo_lst_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *s, *d, *order;
	plan_t *plan;
 	real_t upper;
    	
 	if (objc < 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: s */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
  
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[3]: order */
	if (!ptrtab_get( objv[3], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: d */
	if (!ptrtab_get( objv[4], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	if (Tcl_GetDoubleFromObj(ip, objv[5], &upper) != TCL_OK) {
		result = Tcl_NewStringObj("invalid upper: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
					
	if (srcs_mcarlo_lst(s, d, plan, order, upper)) {
		result = Tcl_NewStringObj("srcs_mcarlo_lst failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
 	
	return TCL_OK;
}

int
C_mcarlo_lft_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *f, *d, *order;
	plan_t *plan;
 	real_t upper;
    	
 	if (objc < 6) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}

	/* objv[1]: f */
	if (!ptrtab_get( objv[1], (void**)&f)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
  
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&plan)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[3]: order */
	if (!ptrtab_get( objv[3], (void**)&order)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: d */
	if (!ptrtab_get( objv[4], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	if (Tcl_GetDoubleFromObj(ip, objv[5], &upper) != TCL_OK) {
		result = Tcl_NewStringObj("invalid upper: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
					
	if (srcs_mcarlo_lft(f, d, plan, order, upper)) {
		result = Tcl_NewStringObj("srcs_mcarlo_lft failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}
 	
	return TCL_OK;
}


int
C_makespan_p_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
	matr_t *s;
	int simnum;
	real_t P;
    	
 	if (objc < 4) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
 	
	/* objv[1]: s */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	 
	if (Tcl_GetIntFromObj(ip, objv[2], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	if (Tcl_GetDoubleFromObj(ip, objv[3], &P) != TCL_OK) {
		result = Tcl_NewStringObj("invalid P: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}			

	result = Tcl_NewDoubleObj(srcs_makespan_p(s, simnum, P));
 	Tcl_SetObjResult(ip, result); 	
	return TCL_OK;
}

static int
C_sgs_serial_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	plan_t *p;
	matr_t *s;
	matr_t *eseq;
	matr_t *d;
	matr_t *q;
	matr_t *b;
	
	if (objc < 7) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: s[] */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&p)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: d */
	if (!ptrtab_get( objv[3], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
	
	/* objv[4]: q */
	if (!ptrtab_get( objv[4], (void**)&q)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[5]: b */
 	if (!ptrtab_get( objv[5], (void**)&b)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: elig sequence */
	if (!ptrtab_get( objv[6], (void**)&eseq)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_sgs_serial(s, p, d, q, b, eseq)) {
		result = Tcl_NewStringObj("srcs_sgs_serial failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}		
	return TCL_OK;
}

static int
C_sgs_cutwalk_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	plan_t *p;
	plan_t *s;
	matr_t *eseq;
	matr_t *d;
	matr_t *q;
	matr_t *b;
	int simnum;
	real_t dt;
	
	if (objc < 7) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: s[] */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&p)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: d */
	if (!ptrtab_get( objv[3], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: simnum */
	if (Tcl_GetIntFromObj(ip, objv[4], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
		
	/* objv[5]: q */
	if (!ptrtab_get( objv[5], (void**)&q)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: b */
 	if (!ptrtab_get( objv[6], (void**)&b)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: elig sequence */
	if (!ptrtab_get( objv[7], (void**)&eseq)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	/* objv[8]: col */
	if ((Tcl_GetDoubleFromObj(ip, objv[8], &dt) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_sgs_cutwalk(s, p, d, simnum, q, b, eseq, dt)) {
		result = Tcl_NewStringObj("srcs_sgs_cutwalk failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}		
	return TCL_OK;
}

static int
C_sgs_cutwalk2_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	plan_t *p;
	plan_t *s;
	matr_t *eseq;
	matr_t *d;
	matr_t *q;
	matr_t *b;
	int simnum;
	real_t dt;
	
	if (objc < 7) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: s[] */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&p)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: d */
	if (!ptrtab_get( objv[3], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: simnum */
	if (Tcl_GetIntFromObj(ip, objv[4], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
		
	/* objv[5]: q */
	if (!ptrtab_get( objv[5], (void**)&q)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: b */
 	if (!ptrtab_get( objv[6], (void**)&b)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: elig sequence */
	if (!ptrtab_get( objv[7], (void**)&eseq)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	/* objv[8]: col */
	if ((Tcl_GetDoubleFromObj(ip, objv[8], &dt) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_sgs_cutwalk2(s, p, d, simnum, q, b, eseq, dt)) {
		result = Tcl_NewStringObj("srcs_sgs_cutwalk2 failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}		
	return TCL_OK;
}

static int
C_sgs_cutwalk3_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	plan_t *p;
	plan_t *s;
	matr_t *eseq;
	matr_t *d;
	matr_t *q;
	matr_t *b;
	int simnum;
	real_t dt;
	
	if (objc < 7) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: s[] */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&p)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: d */
	if (!ptrtab_get( objv[3], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: simnum */
	if (Tcl_GetIntFromObj(ip, objv[4], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
		
	/* objv[5]: q */
	if (!ptrtab_get( objv[5], (void**)&q)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: b */
 	if (!ptrtab_get( objv[6], (void**)&b)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: elig sequence */
	if (!ptrtab_get( objv[7], (void**)&eseq)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	/* objv[8]: col */
	if ((Tcl_GetDoubleFromObj(ip, objv[8], &dt) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_sgs_cutwalk3(s, p, d, simnum, q, b, eseq, dt)) {
		result = Tcl_NewStringObj("srcs_sgs_cutwalk3 failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}		
	return TCL_OK;
}

static int
C_sgs_cutwalk4_Cmd(ClientData data, Tcl_Interp *ip, 
				int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;
 	plan_t *p;
	plan_t *s;
	matr_t *eseq;
	matr_t *d;
	matr_t *q;
	matr_t *b;
	int simnum;
	real_t dt;
	
	if (objc < 7) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;	
	}

	/* objv[1]: s[] */
	if (!ptrtab_get( objv[1], (void**)&s)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	
	/* objv[2]: plan */
	if (!ptrtab_get( objv[2], (void**)&p)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[2]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[3]: d */
	if (!ptrtab_get( objv[3], (void**)&d)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[3]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
		
	/* objv[4]: simnum */
	if (Tcl_GetIntFromObj(ip, objv[4], &simnum) != TCL_OK) {
		result = Tcl_NewStringObj("invalid simnum: ", -1);
		Tcl_AppendObjToObj(result, objv[4]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}	
		
	/* objv[5]: q */
	if (!ptrtab_get( objv[5], (void**)&q)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[5]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[6]: b */
 	if (!ptrtab_get( objv[6], (void**)&b)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[6]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	/* objv[7]: elig sequence */
	if (!ptrtab_get( objv[7], (void**)&eseq)) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[7]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}

	/* objv[8]: col */
	if ((Tcl_GetDoubleFromObj(ip, objv[8], &dt) != TCL_OK)) {
		result = Tcl_NewStringObj("invalid col: ", -1);
		Tcl_AppendObjToObj(result, objv[8]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	
	if (srcs_sgs_cutwalk4(s, p, d, simnum, q, b, eseq, dt)) {
		result = Tcl_NewStringObj("srcs_sgs_cutwalk3 failed", -1);
 		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;		
	}		
	return TCL_OK;
}

int
C_ptr_del_Cmd(ClientData data, Tcl_Interp *ip, 
	int objc, Tcl_Obj *const objv[])
{
	Tcl_Obj *result;

 	if (objc != 2) {
 		result = Tcl_NewStringObj("invalid num. arguments", -1);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
 	}
	
	/* objv[1]: object name */
	if (!ptrtab_del(objv[1])) {
		result = Tcl_NewStringObj("does not exist: ", -1);
		Tcl_AppendObjToObj(result, objv[1]);
		Tcl_SetObjResult(ip, result);
		return TCL_ERROR;
	}
	 
	return TCL_OK;
}


/* register commands */
int DLLEXPORT
Srcs_Init(Tcl_Interp *ip)
{
	if (!Tcl_InitStubs(ip, TCL_VERSION, 0))
		return TCL_ERROR;

	/* initialize rng seed */
	srcs_srand(time(0));
		
	/* initialize hash tables */
	Tcl_InitHashTable(&ptrtab, TCL_STRING_KEYS);
 	
	Tcl_CreateObjCommand(ip, "srcs::c_simul",			C_simul_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_lgsta",			C_lgsta_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rob", 			C_rob_Cmd, NULL, NULL);
	
	
	Tcl_CreateObjCommand(ip, "srcs::c_plan_new",		C_plan_new_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_set_edge",	C_plan_set_edge_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_get_edge",	C_plan_get_edge_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_get_n",		C_plan_get_n_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_copy",		C_plan_copy_Cmd, NULL, NULL);
	

	Tcl_CreateObjCommand(ip, "srcs::c_rvec_new",		C_rvec_new_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rvec_set_m1",		C_rvec_set_m1_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rvec_set_cov",	C_rvec_set_cov_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rvec_get_mom",	C_rvec_get_mom_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rvec_get_cov",	C_rvec_get_cov_Cmd, NULL, NULL);	
	Tcl_CreateObjCommand(ip, "srcs::c_rvec_sim",		C_rvec_sim_Cmd, NULL, NULL);


	Tcl_CreateObjCommand(ip, "srcs::c_rcons_new",		C_rcons_new_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rcons_set_cap", 	C_rcons_set_cap_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_rcons_set_req", 	C_rcons_set_req_Cmd, NULL, NULL);
	
	Tcl_CreateObjCommand(ip, "srcs::c_vec_new",			C_vec_new_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_new", 		C_matr_new_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_free", 		C_matr_free_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_get", 		C_matr_get_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_set", 		C_matr_set_Cmd, NULL, NULL); 
	Tcl_CreateObjCommand(ip, "srcs::c_matr_copy", 		C_matr_copy_Cmd, NULL, NULL); 	
	Tcl_CreateObjCommand(ip, "srcs::c_matr_add", 		C_matr_add_Cmd, NULL, NULL); 
	
	Tcl_CreateObjCommand(ip, "srcs::c_matr_get_rows", 	C_matr_get_rows_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_get_cols", 	C_matr_get_cols_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_m1", 		C_matr_m1_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_matr_p_j", 		C_matr_p_j_Cmd, NULL, NULL);

	Tcl_CreateObjCommand(ip, "srcs::c_resprof", 		C_resprof_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_srand",	 		C_srand_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_lgsta_tard",		C_lgsta_tard_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_simul_tard",		C_simul_tard_Cmd, NULL, NULL);

	Tcl_CreateObjCommand(ip, "srcs::c_plan_bfs",		C_plan_bfs_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_bfs2",		C_plan_bfs2_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_tc",			C_plan_tc_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_order",		C_plan_order_Cmd, NULL, NULL);
	Tcl_CreateObjCommand(ip, "srcs::c_plan_tc_update",	C_plan_tc_update_Cmd, NULL, NULL);
	
	
	Tcl_CreateObjCommand(ip, "srcs::c_ft",				C_ft_Cmd, NULL, NULL);
 	Tcl_CreateObjCommand(ip, "srcs::c_mfssearch",		C_mfssearch_Cmd, NULL, NULL);
 	
   	Tcl_CreateObjCommand(ip, "srcs::c_mcarlo_est",		C_mcarlo_est_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_mcarlo_lst",		C_mcarlo_lst_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_mcarlo_lft",		C_mcarlo_lft_Cmd, NULL, NULL);
   
 	Tcl_CreateObjCommand(ip, "srcs::c_mcarlo_crt",		C_mcarlo_crt_Cmd, NULL, NULL);
 	
  	Tcl_CreateObjCommand(ip, "srcs::c_pwl_est",			C_pwl_est_Cmd, NULL, NULL);
	
  	Tcl_CreateObjCommand(ip, "srcs::c_makespan_p",		C_makespan_p_Cmd, NULL, NULL);
  	  	
    Tcl_CreateObjCommand(ip, "srcs::c_sgs_serial",		C_sgs_serial_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_sgs_cutwalk",		C_sgs_cutwalk_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_sgs_cutwalk2",		C_sgs_cutwalk2_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_sgs_cutwalk3",		C_sgs_cutwalk3_Cmd, NULL, NULL);
    Tcl_CreateObjCommand(ip, "srcs::c_sgs_cutwalk4",		C_sgs_cutwalk4_Cmd, NULL, NULL);
	
 	Tcl_CreateObjCommand(ip, "srcs::c_ptr_del", 		C_ptr_del_Cmd, NULL, NULL);

	return TCL_OK;
}



static int
ptrtab_get(Tcl_Obj *objkey, void **valptr)
{
	Tcl_HashEntry *entr;
 
	if (!(entr = Tcl_FindHashEntry(&ptrtab, Tcl_GetString(objkey))))
		return 0;
	
	*valptr = (void**)Tcl_GetHashValue(entr);
	return 1; 
}

static Tcl_HashEntry*
ptrtab_set(Tcl_Obj *objkey, void *val)
{
	Tcl_HashEntry *newentr;
	int isnew;
	assert((newentr=Tcl_CreateHashEntry(&ptrtab, Tcl_GetString(objkey), &isnew)));
	assert(isnew);
	Tcl_SetHashValue(newentr, val);
	
	return newentr;
}

static int
ptrtab_del(Tcl_Obj *objkey)
{
	Tcl_HashEntry *entr;
 
	if (!(entr = Tcl_FindHashEntry(&ptrtab, Tcl_GetString(objkey))))
		return 0;
	Tcl_DeleteHashEntry(entr);

 	return 1; 
}
