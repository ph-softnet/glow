#!/bin/sh
# \
exec ./tclsh $0 $*

source srcs.tcl

package require math::statistics
package require struct::set

set P [dict create]
 
proc PDFsample {P j} {
	global $P
	
	set q [dict get $P q:$j]
	set 1_q [expr 1.0-$q]
	
	if {[math::random] <= $q} {
		set mode a
	} else {
		set mode b
	}
	
	set lo [dict get $P lo:$j:$mode]
	set hi [dict get $P hi:$j:$mode]
	
	set t [expr $lo+($hi-$lo)*[math::random]]
	
	if {$t > $hi} {
		puts stderr "$t > $hi!"
	}
	return $t
}

#
# load rcpsp
#

if {[llength $argv] < 3} {
	puts stderr "use: [info script] <file> <simmax> <simnum>"
	exit 1
}
set tmspath [lindex $argv 0]
set simmax [lindex $argv 1]
set simnum [lindex $argv 2]

set conf [srcs::load_tms $tmspath]
set n [dict get $conf n]

for {set i 0} {$i < $n} {incr i} {
	#set p [expr max(0.2, 0.1*($i % 9))]
 	set p 0.5
	if { [expr $i%2] == 1 } {
		set q $p
	} else {
		set q [expr 1.0-$p]
		# if {$p >= 0.8} { set q 1.0 }
 	}
   	set m1 [expr [dict get $conf $i:D:m1]]
	
	dict set P q:$i $q
	dict set P lo:$i:a 0
	dict set P hi:$i:a 1
	dict set P lo:$i:b $m1
	dict set P hi:$i:b [expr 5*$m1]
}
 
#
# build mcarlo sample
#

srcs::c_matr_new .d $simmax $n
for {set m 0} {$m < $simmax} {incr m} {
 	for {set j 0} {$j < $n} {incr j} {
		set t [expr [format "%-2.1f\t" [PDFsample $P $j]]]
		srcs::c_matr_set .d $m $j real $t
	}
}

#
# build set of MFSs
#

set k [expr [dict get $conf k]-1]

srcs::plan_new .P $conf

array set subset {}

# read list of mfs

set mfsfd [open "|./util/mfs $tmspath" r]
set maxcvrnum 0
set maxrcvrnum 0
set subset(sbsnum) 0
set subset(mfsnum) 0

while {[gets $mfsfd mfs] >= 0} {
	lappend subset(mfs) [list $mfs [llength $mfs]]
}
set mfs [lsort -increasing -integer -index 1 $mfs]

foreach elem $subset(mfs) {
	lassign $elem mfs mfslen
	
	set subset(rcvr:$subset(mfsnum)) [list]
	for {set k1 0} {$k1 < [llength $mfs]} {incr k1} {
		set i [lindex $mfs $k1]
		
		for {set k2 [expr $k1+1]} {$k2 < [llength $mfs]} {incr k2} {
			set j [lindex $mfs $k2]
			
			if {![info exists subset(id:$i,$j)]} {
				set subset(id:$i,$j) [set sbsid $subset(sbsnum)]
				set subset(i:$sbsid) $i
				set subset(j:$sbsid) $j
				set subset(cvr:$sbsid) [list $subset(mfsnum)]
				incr subset(sbsnum)				
			} else {
				set sbsid $subset(id:$i,$j)
				struct::set include subset(cvr:$sbsid) $subset(mfsnum)
			}
			
			lappend subset(rcvr:$subset(mfsnum)) $sbsid
			set rcvrnum [llength $subset(rcvr:$subset(mfsnum))]
			set maxrcvrnum [expr max($maxrcvrnum,$rcvrnum)]

			set cvrnum [llength $subset(cvr:$sbsid)]
			set maxcvrnum [expr max($maxcvrnum,$cvrnum)]
		}
	}
	incr subset(mfsnum)
}
 
srcs::c_plan_new .sol $n
srcs::c_matr_new .sbs $subset(sbsnum) 4
srcs::c_matr_new .cvr $subset(sbsnum) $maxcvrnum
srcs::c_matr_new .map $n $n
srcs::c_matr_new .mfs $subset(mfsnum) [expr $maxrcvrnum+1] 
 
# fill-in sbs & cvr matrices & map 

for {set i 0} {$i < $n} {incr i} {
	for {set j 0} {$j < $n} {incr j} {
		srcs::c_matr_set .map $i $j int -1
	}
}

for {set sbsid 0} {$sbsid < $subset(sbsnum)} {incr sbsid} {
 	srcs::c_matr_set .sbs $sbsid 0 int [set i $subset(i:$sbsid)]
	srcs::c_matr_set .sbs $sbsid 1 int [set j $subset(j:$sbsid)]
	srcs::c_matr_set .sbs $sbsid 2 int [set cvrnum [llength $subset(cvr:$sbsid)]]
	srcs::c_matr_set .sbs $sbsid 3 int [llength $subset(cvr:$sbsid)]
 	
	srcs::c_matr_set .map $i $j int $sbsid
	srcs::c_matr_set .map $j $i int $sbsid
	
	for {set cvrid 0} {$cvrid < $cvrnum} {incr cvrid} {
		srcs::c_matr_set .cvr $sbsid $cvrid int \
		[lindex $subset(cvr:$sbsid) $cvrid]
	}
}

for {set mfsid 0} {$mfsid < $subset(mfsnum)} {incr mfsid} {
	srcs::c_matr_set .mfs $mfsid 0 int [llength [set rcvr $subset(rcvr:$mfsid)]]
	set rcvrid 1
	foreach sbsid $rcvr {
		srcs::c_matr_set .mfs $mfsid $rcvrid int $sbsid
		incr rcvrid
	}
}

#parray subset

srcs::c_mfssearch .sol .sbs .cvr .map .mfs .P .d $simnum $simmax 
