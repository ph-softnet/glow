#!/bin/sh
# \
exec tclsh $0 $*

source srcs.tcl

namespace eval ::srcs::mathematica {
}

# output in file `path' a resource use profile
# for each of the resources, according to schedule x

proc ::srcs::mathematica::profiles {x plotfilepath conf args} {
	set norm 0
	set binlen 0
	if { [llength $args] == 1 } {
		set norm [lindex $args 0]
	}
	if { [llength $args] == 2 } {
		set binlen [lindex $args 1]
	}
	if { $norm != 0 & $norm != 1 } {
		error "norm not 1 or 0"
	}
 	if { $binlen < 0 } {
		error "binlen < 0"
	}
 	
	set k [expr [dict get $conf k]-1]
	set tmsfilepath [dict get $conf path]
	
	if { [catch {exec touch $plotfilepath} err] } {
		error $err
	}
		
	# determine makespan
	set x [lsort -increasing -real -index 1 $x]
	set y [lindex [lindex $x end] 1]

	# represent x as a vector
	set xvec [list]
	foreach pair [lsort -index 0 -integer -increasing $x] {
		lappend xvec [lindex $pair 1]
	}
	
	set allplots [list]
	for {set r 0} {$r < $k} {incr r} {
		set points [srcs::usepoints $r $x $conf $norm]
		lappend allplots $points
	}
	set allplots "[string map {" " ", "} $allplots]"
	set xvec "[string map {" " ", "} $xvec]"
	exec cat - > [set tmpf [exec mktemp]] << "plotpoints = {$allplots};"
	exec cat - >> $tmpf << "xvec = {$xvec};"
	exec ./tms2math [dict get $conf path] >> $tmpf;
	puts [exec ./util/mathematica/profile $tmsfilepath $tmpf $plotfilepath $binlen]
	exec rm -f $tmpf	
}