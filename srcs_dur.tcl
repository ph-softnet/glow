
#
# make uniform distributions,
# make monte carlo sample
#

proc srcs::uni_d_new {dobj simmax conf {mode low}} {
	set n [dict get $conf n]
	
 	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
		
		switch $mode {
			low {
				set tlo [expr $m1-sqrt($m1)]
				set thi [expr $m1+sqrt($m1)]
			}
			
			high {
				set tlo [expr $m1-$m1]
				set thi [expr $m1+$m1]
			}
		}
			
		set t [math::statistics::random-uniform $tlo $thi $simmax]

		srcs::c_matr_set $dobj 0 $i real $m1
		for {set m 1} {$m < $simmax} {incr m} {
			set ti [lindex $t $m]
 			srcs::c_matr_set $dobj $m $i real $ti
		}
	}
}

#
# make beta distributions,
# make monte carlo sample
#

proc srcs::bet_d_new {dobj simmax conf {mode low}} {
	set n [dict get $conf n]
	
 	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
		
		# adjust parameters `a' and `b'
		switch $mode {
			low {
				set a [expr 0.5*$m1 - (1.0/3.0)];
			}
			
			high {
				set a [expr (1.0/6.0)];
			}
		}
		set b [expr $a*2.0]
					
		set xsample [math::statistics::random-beta $a $b $simmax]

		srcs::c_matr_set $dobj 0 $i real $m1
		for {set m 1} {$m < $simmax} {incr m} {
			set xi [lindex $xsample $m]
			set ti [expr (3.0/2.0)*$m1*$xi + 0.5*$m1]
 			srcs::c_matr_set $dobj $m $i real $ti
		}
	}
}

#
# make beta distributions,
#

proc srcs::vilches_d_new {dobj simmax conf mode {dis 1}} {
	set n [dict get $conf n]
	
	set a 2
	set b 5
	if {$mode == "low"} {
		set lo 0.75
		set hi 1.625
	}
	if {$mode == "med"} {
		set lo 0.5
		set hi 2.25
	}
	if {$mode == "high"} {
		set lo 0.25
		set hi 2.875
	}
	
 	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
 				
		set xsample [math::statistics::random-beta $a $b $simmax]

		srcs::c_matr_set $dobj 0 $i real $m1
		for {set m 1} {$m < $simmax} {incr m} {
			set xi [lindex $xsample $m]
			set di [expr $m1*($lo + ($hi-$lo)*$xi)]
			if {$dis == 1} {
				set di [expr int($di)]
			}
 			srcs::c_matr_set $dobj $m $i real $di
		}
	}
}


proc srcs::exp_d_new {dobj simmax conf} {
	set n [dict get $conf n]
	
	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
			
		set t [math::statistics::random-exponential $m1 $simmax]

		srcs::c_matr_set $dobj 0 $i real $m1
		for {set m 1} {$m < $simmax} {incr m} {
			set ti [lindex $t $m]
 			srcs::c_matr_set $dobj $m $i real $ti
		}
	}
}

#
# only 20% of tasks random
#

proc srcs::few_exp_d_new {dobj simmax conf {pct 0.5}} {
	set n [dict get $conf n]
	
	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
		
		if { [math::random] <= $pct } {
			set rnd 1
		} else {
			set rnd 0
		}
		
		# t[] = column of random durations for i
		set t [list]
		for {set m 0} {$m < $simmax} {incr m} {
			set ti $m1
			if {$rnd && [math::random] <= 0.5} {
				set ti 0
			}
			lappend t $ti
		}
		
		srcs::c_matr_set $dobj 0 $i real [expr $m1*0.5 + 0*0.5]
		for {set m 1} {$m < $simmax} {incr m} {
			set ti [lindex $t $m]
 			srcs::c_matr_set $dobj $m $i real $ti
		}
	}
}

#
# correlated durations
#

proc srcs::cor_d_new {dobj simmax conf} {
	set n [dict get $conf n]
	
 	for {set i 0} {$i < $n} {incr i} {
		set m1 [dict get $conf $i:D:m1]
		if {$m1 == 0} {continue}
			
		# set t [math::statistics::random-uniform $tlo $thi $simmax]

		srcs::c_matr_set $dobj 0 $i real $m1
		for {set m 1} {$m < $simmax} {incr m} {
			set ti [lindex $t $m]
 			srcs::c_matr_set $dobj $m $i real $ti
		}
	}
}
