

namespace eval var {
	namespace export var ptr varcp varify
	variable here [namespace current]
	
	proc var {name args} {
		variable here
		set ${here}::$name {*}$args
	}	

	proc ptr {name} {
		variable here
		return ${here}::$name
	}
	
	proc varcp {dst src} {
		set srcptr [ptr $src]
		set dstptr [ptr $dst]
		foreach v [info vars $srcptr.*] {
			var $dst[string map [list $srcptr ""] $v] [set $v]
		}
	}
	
	proc varify {dst src} {
		foreach {n v} [dict get $src] {
			var $dst.$n $v
		}
	}

}

namespace import var::*
