function [xnew] = solve_at(x0)
global n T b d

cvx_begin quiet;
variable x(n) nonnegative
expression gt(length(T),n)
expression G(length(T))

minimize norm(x(:)+d(:), Inf)
subject to

for p=1:length(T),
    t=T(p);
    % calculate Gt
    for i=1:n,
        gt(p, i) = g(t, i, x(i));
    end
    G(p) = sum(gt(p,:));
    
    % include constraint
    G(p) - (H(t,x0) + dot(Hgrad(t,x0),x(:)-x0(:))) <= b;
end

cvx_end;

xnew = x;