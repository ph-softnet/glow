function [val] = H(t, x)
global n

val = sum(arrayfun(@(i) (h(t,i,x(i))), 1:n));