function [val] = U(t, x)
global n

Gt = sum(arrayfun(@(i) (g(t,i,x(i))), 1:n));
Ht = sum(arrayfun(@(i) (h(t,i,x(i))), 1:n));

val = Gt - Ht;