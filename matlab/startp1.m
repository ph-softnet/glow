function [x0] = startp1()
global n b T d q

n=3;
b=4;
d=[3 3 2];
q=[3 2 2];
T=0:0.5:10;
x0=zeros(1,n);

t=0;
for i=1:n,
    x0(i)=t;
    t=i*(t+d(i));
end

x0=x0;