function [sol] = dc0()
global nrm_e nrm_d
% various parameters
nrm_d = 0.25;
nrm_e = 0.5;

% initialize problem
global n b T d q

xold = startp1();
xnew = xold;

tol = 10^-5;
max_k = 150;

for k=1:max_k
    oold = norm(xold(:)+d(:), Inf);
    xnew = solve_at(xold);
    onew = norm(xnew(:)+d(:), Inf);
    xdif = norm(xold(:)-xnew(:),2);
    
    fprintf('--\n');
    fprintf('xold: '); disp(xold');  
    fprintf('xnew: '); disp(xnew');
    fprintf('oold: %f, onew: %f, xdif: %f\n', oold, onew, xdif);  
    
    if xdif <= tol
        break
    end
    xold = xnew;
end;

trange = 0:0.05:onew;
Uall = arrayfun(@(t) (U(t, xnew)), trange);
Vall = arrayfun(@(t) (V(t, xnew)), trange);
Ub(1:length(trange)) = b;

figure
plot(trange, Uall, trange, Vall, trange, Ub);
sol = xnew;



