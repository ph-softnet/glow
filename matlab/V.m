function [use] = V(t, x)
global n d q
use = 0;
for i=1:n,
    if t >= x(i) && t < x(i)+d(i)
        use = use + q(i);
    end
end
