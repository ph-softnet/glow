function [val] = Hgrad(t, x)
global n d q
global nrm_e nrm_d

val = zeros(1, n);
for i=1:n,
    c3 = t-d(i)+nrm_d;
    c4 = t-nrm_d;
    term1 = (x(i)-c3)/norm([x(i)-c3, nrm_e], 2);
    term2 = (x(i)-c4)/norm([x(i)-c4, nrm_e], 2);
    val(i) = q(i)*(term1 + term2)/(4*nrm_d);
end