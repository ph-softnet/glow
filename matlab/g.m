function [val] = g(t, i, xi)
global d q
global nrm_e nrm_d

c1 = t-d(i)-nrm_d;
c2 = t+nrm_d;

val = q(i)*(norm([xi-c1, nrm_e],2) + norm([xi-c2, nrm_e],2))/(4*nrm_d);