function [val] = h(t, i, xi)
global nrm_e nrm_d
global d q

c3 = t-d(i)+nrm_d;
c4 = t-nrm_d;

val=q(i)*(norm([xi-c3, nrm_e],2) + norm([xi-c4, nrm_e],2))/(4*nrm_d);