TCL_LIB=tcl8.5

GSL_CFLAGS=-I./external/conf/gsl_include
GSL_LDFLAGS=-L./external/conf/gsl_lib -lgsl -lgslcblas

TCL_CFLAGS=-I./external/conf/tcl_include -DUSE_TCL_STUBS
TCL_LDFLAGS=-L./external/conf/tcl_lib -l$(TCL_LIB) -lm

CFLAGS=-Wall -fPIC -DPRINT=-1 -msse3 -mfpmath=sse -O2

OBJFILES=srcs_tcl.o \
	srcs.o \
	srcs_mfssearch.o \
 	srcs_sgs_serial.o \
	srcs_sgs_serial2.o \
	srcs_sgs_serial3.o \
	srcs_sgs_serial4.o \
	srcs_pwl.o \
	fifo.o pqueue.o twiddle.o

libsrcs.so: $(OBJFILES) Makefile
	$(CC) $(CFLAGS) $(OBJFILES)  $(TCL_LDFLAGS) $(GSL_LDFLAGS) -shared -o libsrcs.so
 
srcs_tcl.o: srcs_tcl.c srcs.h Makefile
	$(CC) $(CFLAGS) $(TCL_CFLAGS) -c srcs_tcl.c -o srcs_tcl.o

srcs.o: srcs.c srcs.h  Makefile
srcs_mfssearch.o: srcs_mfssearch.c srcs.h  Makefile
srcs_spgs.o: srcs_spgs.c srcs.h  Makefile	
srcs_sgs_serial.o: srcs_sgs_serial.c srcs.h  Makefile	
srcs_sgs_serial2.o: srcs_sgs_serial2.c srcs.h  Makefile	
srcs_sgs_serial3.o: srcs_sgs_serial3.c srcs.h  Makefile	
srcs_sgs_serial4.o: srcs_sgs_serial4.c srcs.h  Makefile	

twiddle.o: twiddle.c Makefile
 
clean:
	rm -f pqueue.o
	rm -f fifo.o
	rm -f srcs.o
	rm -f srcs_tcl.o
	rm -f srcs_mfssearch.o
	rm -f srcs_sgs_serial.o srcs_sgs_serial2.o srcs_sgs_serial3.o
	rm -f libsrcs.so
	

