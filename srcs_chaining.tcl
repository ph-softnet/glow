proc srcs::chaining {sol sch conf args} {
	set k [expr [dict get $conf k]-1]
	set n [dict get $conf n]
	
	# process schedule
	set sort [list]
	for {set i 0} {$i < $n} {incr i} {
		set si [lindex $sch $i]
		lappend sort [list $i [lindex $sch $i]]
	}
	set sort [lsort -index 1 -real -increasing $sort]
	set sort [concat {*}$sort]
	   
	# initialize chains
	set chains [dict create]
	for {set r 0} {$r < $k} {incr r} {
		set cap [dict get $conf $r:cap]
	
		for {set m 0} {$m < $cap} {incr m} {
			dict set chains $r:$m 0
		}
	}

	# chain in order of non-decreasing start-time
	foreach {j sj} $sort {
		if {$j == 0} {continue}
 		 		
		for {set r 0} {$r < $k} {incr r} {
			set qj	[dict get $conf $j:req:$r]
			set br	[dict get $conf $r:cap]
			set qj_done 0
						
			for {set u 0} {$u < $br && $qj_done != $qj} {incr u} {
				set i [dict get $chains $r:$u]
				set si [lindex $sch $i]
				set ci [expr $si + [dict get $conf $i:D:m1]]
						
				if {$i == $j} {error "srcs::chaining: i == j!"}
 				
				if {$ci <= $sj} {
					if { ![srcs::c_plan_get_edge $sol $i $j] } {
						srcs::c_plan_set_edge $sol $i $j 2
 					}
					dict set chains $r:$u $j
					incr qj_done
				}	
			}
			
			if {$qj_done < $qj} {error "srcs::chaining: qj_done < qj"} 
		}
	}	
}

proc srcs::chaining_rand {sol sch conf args} {
	set k [expr [dict get $conf k]-1]
	set n [dict get $conf n]
	
	# process schedule
	set sort [list]
	for {set i 0} {$i < $n} {incr i} {
		set si [lindex $sch $i]
		lappend sort [list $i [lindex $sch $i]]
	}
	set sort [lsort -index 1 -real -increasing $sort]
	set sort [concat {*}$sort]
	   
	# initialize chains
	set chains [dict create]
	for {set r 0} {$r < $k} {incr r} {
		set br [dict get $conf $r:cap]
	
		for {set u 0} {$u < $br} {incr u} {
			dict set chains $r:$u 0
		}
	}
	
	# chain in order of non-decreasing start-time
	foreach {j sj} $sort {
		if {$j == 0} {continue}
 		 		
		for {set r 0} {$r < $k} {incr r} {
			set qj	[dict get $conf $j:req:$r]
			set br	[dict get $conf $r:cap]
			set qj_done 0
						
			while {$qj_done < $qj} {
				set u [math::random $br]
				set i [dict get $chains $r:$u]
				if {$i == $j} {
					continue
				}
				set si [lindex $sch $i]
				set ci [expr $si + [dict get $conf $i:D:m1]]
				if {$ci > $sj} {
					continue
				}
			
				if { ![srcs::c_plan_get_edge $sol $i $j] } {
					srcs::c_plan_set_edge $sol $i $j 2
 				}
				dict set chains $r:$u $j
				incr qj_done
  			}
 		}
	}	
}

proc srcs::chaining_maxcc {sol sch conf args} {
	set k [expr [dict get $conf k]-1]
	set n [dict get $conf n]
	
	# process schedule
	set sort [list]
	for {set i 0} {$i < $n} {incr i} {
		set si [lindex $sch $i]
		lappend sort [list $i [lindex $sch $i]]
	}
	set sort [lsort -index 1 -real -increasing $sort]
	set sort [concat {*}$sort]
	   
	# initialize chains
	set chains [dict create]
	for {set r 0} {$r < $k} {incr r} {
		set cap [dict get $conf $r:cap]
	
		for {set m 0} {$m < $cap} {incr m} {
			dict set chains $r:$m 0
		}
	}

	# chain in order of non-decreasing start-time
	foreach {j sj} $sort {
		if {$j == 0} {continue}
 		 		
		for {set r 0} {$r < $k} {incr r} {
			set qj	[dict get $conf $j:req:$r]
			set br	[dict get $conf $r:cap]
			set qj_done 0
						
			for {set u 0} {$u < $br && $qj_done != $qj} {incr u} {
				set i [dict get $chains $r:$u]
				set si [lindex $sch $i]
				set ci [expr $si + [dict get $conf $i:D:m1]]
						
				if {$i == $j} {error "srcs::chaining: i == j!"}
 				
				if {$ci <= $sj} {
					if { ![srcs::c_plan_get_edge $sol $i $j] } {
						srcs::c_plan_set_edge $sol $i $j 2
 					}
					dict set chains $r:$u $j
					incr qj_done
				}	
			}
			
			if {$qj_done < $qj} {error "srcs::chaining: qj_done < qj"} 
		}
	}	
}
