#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tmsp.h"
#include "stjn.h"
#include "esta_plus.h"
#include "salloc.h"
#include "debug.h"

int leveling_constraints_before_chaining;

int get_conflict_type(conflict_t* c) {
    int a_before_b, b_before_a;
    
    a_before_b = BEFORE(c->i1, c->j1, c->i2, c->j2);
    b_before_a = BEFORE(c->i2, c->j2, c->i1, c->j1);

    if(!a_before_b && !b_before_a)
        return 1;
    if(!a_before_b && b_before_a)
        return 2;
    if(a_before_b && !b_before_a)
        return 3;
    return 4;
}

conflict_t* new_conflict(requirement* req1, requirement* req2) {
	conflict_t* c = (conflict_t*) safe_malloc(sizeof(conflict_t));

	c->i1 = req1->i;
	c->j1 = req1->j;
	c->i2 = req2->i;
	c->j2 = req2->j;

	c->type = get_conflict_type(c);

	return c;
}

/* puts all conflicts in the peak into the right conflict set t1 to t4 */
void classify_conflicts(peak_t* peak, List* t1, List* t2, List* t3, List* t4) {
	int i, j;
	List* list = 0;

	for (i = 0; i < peak->competing_activities->size - 1; i++) {
		for (j = i + 1; j < peak->competing_activities->size; j++) {
			requirement* req_i = (requirement*) list_get(peak->competing_activities, i);
			requirement* req_j = (requirement*) list_get(peak->competing_activities, j);

			conflict_t* c = new_conflict(req_i, req_j);

			switch (c->type) {
				case 1: list = t1; break;
				case 2: list = t2; break;
				case 3: list = t3; break;
				case 4: list = t4; break;
			}

			list_append(list, c);
		}
	}
}


double get_w_res(conflict_t* c, int div_S) {
	double S = 1;
	if (div_S) {
            S = (double) (min(SLACK(c->i1, c->j1, c->i2, c->j2), SLACK(c->i2, c->j2, c->i1, c->j1))) /
                         (max(SLACK(c->i1, c->j1, c->i2, c->j2), SLACK(c->i2, c->j2, c->i1, c->j1)));
	}
	double sqrtS = sqrt(S);

	return min(SLACK(c->i1, c->j1, c->i2, c->j2) / sqrtS,
                   SLACK(c->i2, c->j2, c->i1, c->j1) / sqrtS);
}

conflict_t* select_conflict(peak_t* peak) {
	int i;

	if (peak->competing_activities->size == 1) {
		// this activity alone requires more than is available in total
		return 0;
	}

	List* type1_conflicts = new_list();
	List* type2_conflicts = new_list();
	List* type3_conflicts = new_list();
	List* type4_conflicts = new_list();
	classify_conflicts(peak, type1_conflicts, type2_conflicts, type3_conflicts, type4_conflicts);

	if (type2_conflicts->size + type3_conflicts->size + type4_conflicts->size == 0) {
		// all conflicts are type 1, this peak is unresolvable
		return 0;
	}


	/* select conflict with minimal w_res
	 *
	 * w_{res}(a1,a2) = min(d(e_a1,s_a2)/S, d(e_a2,s_a1)/S)
	 * if all conflicts are type 4, then S = min/max, else S = 1
	 */
	conflict_t* minimal_conflict = 0;
	double minimal_w_res = 0;
	int div_S = (type2_conflicts->size + type3_conflicts->size == 0);

	for (i = 0; i < type2_conflicts->size; i++) {
		conflict_t* c = (conflict_t*) list_get(type2_conflicts, i);
		double w_res = get_w_res(c, div_S);
		if (!minimal_conflict || w_res < minimal_w_res) {
			minimal_conflict = c;
			minimal_w_res = w_res;
		}
	}
	for (i = 0; i < type3_conflicts->size; i++) {
		conflict_t* c = (conflict_t*) list_get(type3_conflicts, i);
		double w_res = get_w_res(c, div_S);
		if (!minimal_conflict || w_res < minimal_w_res) {
			minimal_conflict = c;
			minimal_w_res = w_res;
		}
	}
	if (div_S) {
		/* if there are type 2 or 3 conflicts, then the type 4 conflicts
		 * will not have minimal w_res so we need not check them
		 */
		for (i = 0; i < type4_conflicts->size; i++) {
			conflict_t* c = (conflict_t*) list_get(type4_conflicts, i);
			double w_res = get_w_res(c, div_S);
			if (!minimal_conflict || w_res < minimal_w_res) {
				minimal_conflict = c;
				minimal_w_res = w_res;
			}
		}
	}

	delete_list(type1_conflicts);
	delete_list(type2_conflicts);
	delete_list(type3_conflicts);
	delete_list(type4_conflicts);

	return minimal_conflict;

}

void select_precedence_constraint(conflict_t* c, precedence* p) {
        if(SLACK(c->i1, c->j1, c->i2, c->j2) > SLACK(c->i2, c->j2, c->i1, c->j1)) {
		p->i1 = c->i1; p->j1 = c->j1;
		p->i2 = c->i2; p->j2 = c->j2;
	} else {
		p->i1 = c->i2; p->j1 = c->j2;
		p->i2 = c->i1; p->j2 = c->j1;
	}
}

/* return list of activities that require the use of resource
 * r at the start of activity i, j
 */
List* get_competing_activities(int i, int j, int r) {
	int k;
	int demand = 0;
	List* competing_activities = new_list();
	
	for (k = 0; k < R(r)->requirements->size; k++) {
		requirement* req = (requirement*) list_get(R(r)->requirements, k);
		if (P_ik(i, j, req->i, req->j)) {
			list_append(competing_activities, req);
			demand += req->amount;
		}
	}
	debug("Checked all activities, peak is %d, capacity = %d.\n", demand, R(r)->capacity);
	if (demand <= R(r)->capacity) {
		debug("Demand <= capacity.\n");
		delete_list(competing_activities);
		return 0;
	}
	else {
		debug("Demand > capacity.\n");
		return competing_activities;
	}
}

int find_peak(peak_t* peak) {
	int i, j;
	for (i = 0; i < tmsp->n_resources; i++) {
		for (j = 0; j < R(i)->requirements->size; j++) {
			requirement* req = (requirement*) list_get(R(i)->requirements, j);
			debug("Looking for peak at start of %d/%d on resource %d.\n", req->i, req->j, i);

			List* competing_activities = get_competing_activities(req->i, req->j, i);
			if (competing_activities) {
				peak->i = req->i;
				peak->j = req->j;
				peak->r = i;
				peak->competing_activities = competing_activities;
				return 1;
			}
		}
	}
	debug("No peak found!\n");
	return 0;
}

int esta_plus() {
	/* Earliest Start Time Algorithm:
	 *
	 * 1. create STN
	 * 2. loop
	 * 3.   refresh temporal information in stn
	 * 4.   conflictSet = computeConflicts
	 * 5.   if no conflictSet { return EST schedule }
	 * 6.   if unsolvable conflictSet { return null }
	 * 7.   conflict = selectConflict
	 * 8.   pc = selectPrecedenceConstraint(conflict)
	 * 9.   postConstraint(pc)
	 * 10.end loop
	 */

	peak_t* peak = (peak_t*) malloc(sizeof(peak_t));
	precedence* p = (precedence*) malloc(sizeof(precedence));

	long iteration = 0;
	leveling_constraints_before_chaining = 0;

	while(1) {
		debug("Iteration %ld starting.\n", iteration++);
		if (!find_peak(peak)) {
			// no peak found, done
			free(peak); free(p);
			return 1;
		}
		debug("Peak found, selecting conflict.\n");

		conflict_t* conflict = select_conflict(peak);
		if (!conflict) {
			debug("Unresolvable peak found!\n");
			free(peak); free(p);
			return 0; // peak is unresolvable
		}
		debug("Conflict (%d,%d) <--> (%d,%d) selected.\n", conflict->i1, conflict->j1, conflict->i2, conflict->j2);
		delete_list(peak->competing_activities);

		select_precedence_constraint(conflict, p);
		debug("Precedence constraint selected (%d,%d) -> (%d,%d).\n", p->i1, p->j1, p->i2, p->j2);
		fprintf(stderr, "PC: %d %d %d %d\n", p->i1, p->j1, p->i2, p->j2);
                if(!stjn_add_precedence(p->i1, p->j1, p->i2, p->j2)) {
		/*if (!ifpc_add_edge(AS(p->i2,p->j2), AE(p->i1,p->j1), 0)) {*/
			debug("STN inconsistent!\n");
			free(peak); free(p);
			return 0;
		}
                //return 0;
		leveling_constraints_before_chaining++;
	}

}

