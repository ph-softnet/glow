#ifndef SALLOC_H
#define SALLOC_H

#include <stdlib.h>

extern void *safe_malloc(size_t size);
    /* Does malloc(size) with null-pointer check.
     * Deallocate with free().
     */

extern void *safe_realloc(void *ptr, size_t size);
    /* Does realloc(ptr, size) with null-pointer check.
     * Deallocate with free().
     */

extern char *safe_strdup(const char *string);
    /* Returns a copy of string with null-pointer check.
     * Deallocate with free().
     */

#endif
