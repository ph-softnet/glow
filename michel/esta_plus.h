#ifndef __ESTA_PLUS_H
#define __ESTA_PLUS_H

#include "tmsp.h"
#include "stjn.h"

extern int leveling_constraints_before_chaining;

typedef struct peak_t {
	int i; // train
	int j; // activity
	int r; // resource
	List* competing_activities;
} peak_t;

typedef struct conflict_t {
	int i1, j1;
	int i2, j2;
	int type;
} conflict_t;

int esta_plus();

/* Returns true iff a2's earliest possible run is running at a1.start.earliest. */
#define P_ik(i1,j1,i2,j2)	((i1 == i2 && j1 == j2) || \
                             (acts[i2][j2]->est <= acts[i1][j1]->est && acts[i1][j1]->est < (acts[i2][j2]->est + acts[i2][j2]->len)))
#define min(a,b)	(a > b ? b : a)
#define max(a,b)	(a > b ? a : b)

#endif
