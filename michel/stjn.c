#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "salloc.h"
#include "stjn.h"
#include "tmsp.h"
#include "list.h"
#include "queue.h"
#include "heap.h"
#include "debug.h"

void stjn_calculate_est();
int stjn_calculate_lst();

stjn_t *stjn;
node_t ***acts;

void set_precedence(int i1, int j1, int i2, int j2) {
    node_t *a, *b;
    a = acts[i1][j1];
    b = acts[i2][j2];
    list_append(a->next, b);
    list_append(b->prev, a);
}

int stjn_construct() {
    int i, j;

    debug("Allocating memory\n");
    stjn = (stjn_t *) malloc(sizeof(stjn_t));
    acts = (node_t ***) calloc(tmsp->n_trains, sizeof(node_t **));

    for(i = 0; i < tmsp->n_trains; i++) {
        debug("Processing train #%d\n", i);
        acts[i] = (node_t **) calloc(N(i), sizeof(node_t *));
        for(j = 0; j < N(i); j++) {
            debug("Processing task #%d (length %d)\n", j, D(i, j));
            acts[i][j] = (node_t *) malloc(sizeof(node_t));
            acts[i][j]->next = new_list();
            acts[i][j]->prev = new_list();
            acts[i][j]->len = D(i,j);
            acts[i][j]->est = RD(i);
            acts[i][j]->lst = DD(i) - D(i,j);
            acts[i][j]->i = i;
            acts[i][j]->j = j;
        }
    }

    debug("Done processing trains\n");
    for(i = 0; i < tmsp->precedences->size; i++) {
        debug("Adding precedence constraint between (%d, %d) and (%d, %d)\n",
                P(i)->i1, P(i)->j1, P(i)->i2, P(i)->j2);
        set_precedence(P(i)->i1, P(i)->j1, P(i)->i2, P(i)->j2);
    }

    /*stjn->startnodes = new_list();
    stjn->endnodes = new_list();
    for(i = 0; i < tmsp->n_trains; i++) {
        for(j = 0; j < N(i); j++) {
            if(acts[i][j]->prev->size == 0) {
                debug("Found start task (%d, %d)\n", i, j);
                list_append(stjn->startnodes, acts[i][j]);
            }
            if(!acts[i][j]->next->size == 0) {
                debug("Found end task (%d, %d)\n", i, j);
                list_append(stjn->endnodes, acts[i][j]);
            }
        }
    }*/
    stjn_calculate_est();
    return stjn_calculate_lst();
}

void stjn_calculate_est() {
    int i, j, order=0;
    Queue *q;
    node_t *n, *m;

    debug("\nCalculating EST\n");
    q = new_queue();
    for(i = 0; i < tmsp->n_trains; i++) {
        for(j = 0; j < N(i); j++) {
            if(acts[i][j]->prev->size == 0)
                queue_add(q, acts[i][j]);
            acts[i][j]->flag = acts[i][j]->prev->size;
        }
    }

    while((n = queue_remove(q)) != NULL) {
        debug("Processing (%d, %d), EST %d LEN %d\n", n->i, n->j, n->est, n->len);
        n->order = order;
        order++;

        for(i = 0; i < n->next->size; i++) {
            m = list_get(n->next, i);
            debug("Propagating to (%d, %d)\n", m->i, m->j);
            if(m->est < n->est + n->len) {
                debug("Updating EST from %d to %d\n", m->est, n->est + n->len);
                m->est = n->est + n->len;
            }

            m->flag--;
            if(!m->flag) {
                debug("Adding (%d, %d) to queue\n", m->i, m->j);
                queue_add(q, m);
            }
        }
    }
    delete_queue(q);
}

int stjn_calculate_lst() {
    int i, j, inconsistent=0;
    Queue *q;
    node_t *n, *m;

    debug("\nCalculating LST\n");
    q = new_queue();

    for(i = 0; i < tmsp->n_trains; i++) {
        for(j = 0; j < N(i); j++) {
            if(acts[i][j]->next->size == 0)
                queue_add(q, acts[i][j]);
            acts[i][j]->flag = acts[i][j]->next->size;
        }
    }

    while((n = queue_remove(q)) != NULL) {
        debug("Processing (%d, %d), LST %d LEN %d\n", n->i, n->j, n->lst, n->len);
        if(n->lst < n->est) {
            debug("INCONSISTENT at (%i, %i): EST %d, LST %d\n", n->i, n->j, n->est, n->lst);
            inconsistent = 1;
        }

        for(i = 0; i < n->prev->size; i++) {
            m = list_get(n->prev, i);
            debug("Propagating to (%d, %d)\n", m->i, m->j);
            if(m->lst > n->lst - m->len) {
                debug("Updating LST from %d to %d\n", m->lst, n->lst - m->len);
                m->lst = n->lst - m->len;
            }

            m->flag--;
            if(!m->flag) {
                debug("Adding (%d, %d) to queue\n", m->i, m->j);
                queue_add(q, m);
            }
        }
    }
    delete_queue(q);
    return !inconsistent;
}

int stjn_add_precedence(int i1, int j1, int i2, int j2) {
    node_t *n, *m;
    Heap *h;
    int i, inconsistent = 0;

    set_precedence(i1, j1, i2, j2);

    if(acts[i2][j2]->est < acts[i1][j1]->est + acts[i1][j1]->len) {
        n = acts[i1][j1];
        m = acts[i2][j2];
        debug("EST of (%d, %d) is increased to %d\n", m->i, m->j, n->est + n->len);
        m->est = n->est + n->len;
        h = new_heap();
        heap_add(h, m->order, m);

        while((n = heap_remove(h)) != NULL) {
            for(i = 0; i < n->next->size; i++) {
                m = list_get(n->next, i);
                debug("Checking (%d, %d)\n", m->i, m->j);
                if(m->est < n->est + n->len) {
                    debug("EST of (%d, %d) is increased to %d\n", m->i, m->j, n->est + n->len);
                    m->est = n->est + n->len;
                    if(m->est > m->lst) inconsistent = 1;
                    heap_add(h, m->order, m);
                }
            }
        }
        delete_heap(h);
    }

    if(acts[i1][j1]->lst > acts[i2][j2]->lst - acts[i1][j1]->len) {
        n = acts[i2][j2];
        m = acts[i1][j1];
        debug("LST of (%d, %d) is decreased to %d\n", m->i, m->j, n->lst - m->len);
        m->lst = n->lst - m->len;
        h = new_heap();
        heap_add(h, -m->order, m);

        while((n = heap_remove(h)) != NULL) {
            debug("Processing (%d, %d) with LST %d\n", n->i, n->j, n->lst);
            for(i = 0; i < n->prev->size; i++) {
                m = list_get(n->prev, i);
                debug("Checking (%d, %d) with LST %d\n", m->i, m->j, m->lst);
                if(m->lst > n->lst - m->len) {
                    debug("LST of (%d, %d) is decreased to %d\n", m->i, m->j, n->lst - m->len);
                    m->lst = n->lst - m->len;
                    if(m->est > m->lst) {
                        inconsistent = 1;
                        debug("Inconsistency found, m = (%d, %d) EST %d LST %d; n = (%d, %d) EST %d LST %d\n",
                                m->i, m->j, m->est, m->lst, n->i, n->j, n->est, n->lst);
                    }
                    heap_add(h, -m->order, m);
                }
            }
        }
        delete_heap(h);
    }

    return !inconsistent;
}

void print_est_schedule() {
    int i, j;
	
    for(i = 0; i < tmsp->n_trains; i++) {
        for(j = 0; j < N(i); j++) {
            fprintf(stderr, "EST: %d %d %d\n", i, j, acts[i][j]->est);
        }
    }
}
