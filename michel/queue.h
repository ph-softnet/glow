#ifndef QUEUE_H
#define QUEUE_H

typedef struct qelem {
    struct qelem *next;
    void *data;
} qelem;

typedef struct Queue {
    int size;
    qelem *head, *tail;
} Queue;

Queue *new_queue();
void delete_queue(Queue *q);
void queue_add(Queue *q, void *data);
void *queue_remove(Queue *q);

#endif

