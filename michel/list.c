#include "list.h"
#include "salloc.h"

#include <assert.h>
#include <string.h>

#define INITIAL_SIZE	16


List *new_list(void) {
	List *list = safe_malloc(sizeof(List));

	list->size = 0;
	list->allocated = INITIAL_SIZE;
	list->data = safe_malloc(INITIAL_SIZE * sizeof(void *));
	memset(list->data, 0, INITIAL_SIZE * sizeof(void *));
	return list;
}

void delete_list(List * list) {
	if (list != NULL) {
		free(list->data);
		free(list);
	}
}

void list_grow(List * list) {
	list->data = safe_realloc(list->data, 2 * list->allocated * sizeof(void *));
	memset(list->data + list->allocated, 0, list->allocated * sizeof(void *));
	list->allocated *= 2;
}

void list_append(List * list, void *data) {
	list_set(list, list->size++, data);
}

void list_set(List * list, int index, void *data) {
	while (list->allocated <= index)
		list_grow(list);
	list->data[index] = data;
	if (index + 1 > list->size)
		list->size = index + 1;
}

/*void *list_get(const List * list, int index) {
	assert(index < list->allocated);
	return list->data[index];
}*/

