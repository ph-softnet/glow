#ifndef LIST_H
#define LIST_H

typedef struct List
{
    int	size, allocated;
    void	**data;
}
List;

List	*new_list(void);
		/* Create a new list and allocate memory for it */

void	delete_list(List *);
		/* Deallocate a list.
		 * NOTE: This does not deallocate the items in the list
   		 * since the list is unaware of the type of data it contains.
		 */

void	list_grow(List *);
		/* Double the list capacity.
		 */

void	list_append(List * list, void *data);
		/* Set a data item in the list on position list->size.
		 */

void	list_set(List * list, int index, void *data);
		/* Set a data item in the list at the specified index.
		 */

//void	*list_get(const List *, int index);
#define list_get(l, i) (l->data[(i)])
		/* Return the 'index'th item in the list
		 * First item has index 0.
		 */

#endif
