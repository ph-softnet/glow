#ifndef __TMSP_H
#define __TMSP_H

#include "list.h"
//#include "stn.h"

typedef struct tmsp_t {
	List* resources;
	List* trains;
	List* precedences;
	int n_resources, n_trains, n_activities;
} tmsp_t;

typedef struct resource {
	int capacity;
	char* name;
	List* requirements;
} resource;

typedef struct train {
	int release_date;
	int due_date;
	List* activities;
	int n_activities;
	char* name;
	int start_vertex;
	int end_vertex;
} train;

typedef struct activity {
	int duration;
	char* name;
	List* requirements;
	int start_vertex;
	int end_vertex;
} activity;

typedef struct requirement {
	int i; // train index
	int j; // activity index
	int k; // resource index
	int amount;
} requirement;

typedef struct precedence {
	int i1, j1, i2, j2;
} precedence;

void add_resource(int, int, char*);
void add_train(int, int, int, char*);
void add_activity(int, int, int, char*);
void add_requirement(int, int, int, int);
void add_precedence(int, int, int, int);
void add_train_mutexes();

extern tmsp_t* tmsp;

// resources
#define R(i)		((resource*)list_get(tmsp->resources, i))
#define C(i)		(R(i)->capacity)

// precedences
#define P(i)		((precedence*)list_get(tmsp->precedences, i))

// treinen
#define T(i)		((train*)list_get(tmsp->trains, i))
#define RD(i)		(T(i)->release_date)
#define DD(i)		(T(i)->due_date)
#define N(i)		(T(i)->n_activities)
#define TS(i)		(T(i)->start_vertex)
#define TE(i)		(T(i)->end_vertex)

// activiteiten
#define A(i,j)		((activity*)list_get(T(i)->activities, j))
#define AS(i,j)		(A(i,j)->start_vertex)
#define AE(i,j)		(A(i,j)->end_vertex)
#define D(i,j)		(A(i,j)->duration)
#define EST(i,j)	(-W(AS(i,j), Z))
#define EET(i,j)	(-W(AE(i,j), Z))
#define REQ(i,j,k)	(A(i,j)->requirements->size <= k ? 0 : (requirement*)list_get(A(i,j)->requirements, k))
#define Q(i,j,k)	(REQ(i,j,k) == 0 ? 0 : REQ(i,j,k)->amount)

#endif
