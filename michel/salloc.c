#include "salloc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static void *null_pointer_check(void *ptr) {
	if (ptr == 0) {
		fprintf(stderr, "Out of memory\n");
		exit(1);
	}

	return ptr;
}


void *safe_malloc(size_t size) {
	return null_pointer_check(malloc(size));
}


void *safe_realloc(void *ptr, size_t size) {
	return null_pointer_check(realloc(ptr, size));
}


char *safe_strdup(const char *string) {
	return strcpy(safe_malloc(strlen(string) + 1), string);
}
