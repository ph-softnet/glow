#include <sys/time.h>
#include <string.h>
#include <stdio.h>

#include "timing.h"
#include "list.h"
#include "salloc.h"

List* timings = 0;
struct timeval* tv_end = 0;

timing_info* new_timing_info(char* name) {
	timing_info* ti = (timing_info*) safe_malloc(sizeof(timing_info));
	ti->name = safe_strdup(name);
	ti->start = (struct timeval*) malloc(sizeof(struct timeval));
	ti->total = 0;
	return ti;
}

void timing_init() {
	if (!timings) {
		timings = new_list();
	}
	if (!tv_end) {
		tv_end = (struct timeval*) malloc(sizeof(struct timeval));
	}
}


timing_info* find_timing_info(char* name) {
	int i;
	for (i = 0; i < timings->size; i++) {
		timing_info* ti = (timing_info*) list_get(timings, i);
		if (!strcmp(name, ti->name)) {
			return ti;
		}
	}
	return 0;
}

timing_info* find_or_create_timing_info(char* name) {
	timing_info* ti = find_timing_info(name);
	if (!ti) {
		ti = new_timing_info(name);
		list_append(timings, ti);
	}
	return ti;
}

void timing_start(char* name) {
	timing_init();
	timing_info* ti = find_or_create_timing_info(name);
	gettimeofday(ti->start, NULL);
}

void timing_stop(char* name) {
	double seconds = 0;
	gettimeofday(tv_end, NULL);
	timing_info* ti = find_timing_info(name);

	int diff_sec = tv_end->tv_sec - ti->start->tv_sec;
	int diff_usec = tv_end->tv_usec - ti->start->tv_usec;

	seconds += diff_sec;
	seconds += diff_usec / (double) 1000000;
	
	ti->total = ti->total + seconds;
}

void timing_print_summary() {
	int i;
	for (i = 0; timings && i < timings->size; i++) {
		timing_info* ti = ((timing_info*)list_get(timings, i));
		fprintf(stderr, "%s: %lf\n", ti->name, ti->total);
	}
}

