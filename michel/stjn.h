#ifndef __STJN_H
#define __STJN_H

#include "list.h"

typedef struct stjn_t {
    int n_jobs;
    int n_total_jobs;
    //List *startnodes, *endnodes;
} stjn_t;

typedef struct node_t {
    int len, est, lst, i, j, order;
    int flag;
    List *prev, *next;
} node_t;

extern node_t ***acts;

int stjn_add_precedence(int i1, int j1, int i2, int j2);
int stjn_construct();
void print_est_schedule();

#define BEFORE(ia, ja,  ib, jb)     (acts[ib][jb]->lst >= (acts[ia][ja]->est + acts[ia][ja]->len))
#define SLACK(ia, ja,  ib, jb)      (acts[ib][jb]->lst - acts[ia][ja]->est - acts[ia][ja]->len)
#endif
