#include <stdlib.h>
#include <stdio.h>

#include "salloc.h"
#include "tmsp.h"
#include "list.h"

tmsp_t* tmsp;

void tmsp_init() {
	if (!tmsp) {
		tmsp = (tmsp_t*) malloc(sizeof(tmsp_t));

		tmsp->resources = new_list();
		tmsp->trains = new_list();
		tmsp->precedences = new_list();

		tmsp->n_resources = 0;
		tmsp->n_trains = 0;
		tmsp->n_activities = 0;
	}
}

void add_resource(int i, int capacity, char* name) {
	tmsp_init();

	resource* r = (resource*) malloc(sizeof(resource));
	r->capacity = capacity;
	r->name = safe_strdup(name);
	r->requirements = new_list();

	list_set(tmsp->resources, i, r);
	tmsp->n_resources++;
}

void add_train(int i, int release_date, int due_date, char* name) {
	tmsp_init();

	train* t = (train*) malloc(sizeof(train));
	t->release_date = release_date;
	t->due_date = due_date;
	t->name = safe_strdup(name);
	t->activities = new_list();
	t->n_activities = 0;

	list_set(tmsp->trains, i, t);
	tmsp->n_trains++;
}

void add_activity(int i, int j, int duration, char* name) {
	tmsp_init();

	activity* a = (activity*) safe_malloc(sizeof(activity));
	a->duration = duration;
	a->name = safe_strdup(name);
	a->requirements = new_list();

	list_set(T(i)->activities, j, a);
	tmsp->n_activities++;
	N(i)++;
}

void add_requirement(int i, int j, int k, int q) {
	requirement* req = (requirement*) safe_malloc(sizeof(requirement));

	req->i = i;
	req->j = j;
	req->k = k;
	req->amount = q;

	list_set(A(i,j)->requirements, k, req); // save indexed by resource id in the activity
	list_append(R(k)->requirements, req);   // save in 'normal' list in the resource
}

void add_precedence(int i1, int j1, int i2, int j2) {
	precedence* p = (precedence*) malloc(sizeof(precedence));

	p->i1 = i1;
	p->j1 = j1;

	p->i2 = i2;
	p->j2 = j2;

	list_append(tmsp->precedences, p);
}


void add_train_mutexes() {
	int i,j,r;

	r = tmsp->n_resources;

	for (i = 0; i < tmsp->n_trains; i++, r++) {
		add_resource(r, 1, safe_strdup(T(i)->name));
		for (j = 0; j < N(i); j++) {
			add_requirement(i, j, r, 1);
		}
	}
}		
