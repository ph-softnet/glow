#include "queue.h"
#include "salloc.h"

Queue *new_queue() {
    Queue *q = safe_malloc(sizeof(Queue));
    
    q->size = 0;
    q->head = q->tail = NULL;

    return q;
}

void delete_queue(Queue *q) {
    qelem *e, *f;

    e = q->head;
    while(e != NULL) {
        f = e->next;
        free(e);
        e = f;
    }
    free(q);
}

void queue_add(Queue *q, void *data) {
    qelem *e;

    // Allocate new element
    e = safe_malloc(sizeof(qelem));
    e->data = data;
    e->next = NULL;

    if(q->tail) {
        // queue not empty
        q->tail->next = e;
    } else {
        // queue is empty
        q->head = e;
    }
    
    q->tail = e;
    q->size++;
}

void *queue_remove(Queue *q) {
    qelem *e;

    if(q->size == 0) return NULL;
    e = q->head;
    q->head = e->next;
    free(e);
    q->size--;
    if(q->size == 0) q->tail = NULL;
    return e->data;
}
